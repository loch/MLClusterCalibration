<!-- <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=default"></script> -->

Machine-learning-based topo-cluster calibration calibration
===========================================================

This respository contains the <tt>python</tt> code to train a local topo-cluster calibration to be applied instead of the classic <i>L</i>ocal <i>C</i>ell <i>W</i>eighting (LCW) calibration <a href="#Ref1">\[1\]</a>. Similar to LCW this calibration uses cluster moments as inputs for the training. The code uses [<tt>TensorFlow</tt>](https://www.tensorflow.org/)/[<tt>Keras</tt>](https://keras.io/) to learn the topo-cluster  response <i>R</i><sub>clus</sub> = <i>E</i><sub>clus,EM</sub>/<i>E</i><sub>clus,dep</sub> as a function of selected sets of cluster moments. The calibration function is then (<i>R</i><sub>clus</sub>)<sup>&minus;1</sup>. The principal tool for this _regression fit_ is a <i>D</i>eep <i>N</i>eural <i>N</i>etwork (DNN) \[<a href="#Ref2">2</a>,<a href="#Ref3">3</a>\]. 

The instructions given here are based on the documentation of the jet calibration training originally provided by Pierre-Antoine Delsart (LPSC Genoble, France, <delsart@in2p3.fr>) in the [README_original.md](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/README_original.md) file.  

## Table of content

<table>
<tr><td><b>[1 Setup](#1-setup)</b></td><td><i>General setup instructions for <tt>Windows10</tt> and <tt>CentOS7</tt></i></td></tr> <!--<br/>-->
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;[1.1 CentOS7](#11-centos7)</td><td><i>Specific setup instructions for setup in <tt>CentOS7</tt></i></td></tr><!--<br/>-->
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.1.1 Initial setup](#111-initial-setup)</td><td><i>One-time setup of workarea</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.1.2 At each login](#112-at-each-login)</td><td><i>Setup after each login/clean shell</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;[1.2 Windows10 (PowerShell)](#12-windows10-powershell)</td><td><i>Specific instructions for setup for <tt>Windows10</tt>/<tt>PowerShell</tt></i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.2.1 Initial setup](#121-initial-setup)</td><td><i>One-time setup of workarea</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.2.2 At each login](#122-at-each-login)</td><td><i>Setup after each login/clean shell</i></td></tr>
<tr><td><b>[2 Training](#2-training)</b></td><td><i>Instructions on how to configure and run the training</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;[2.1 Configuration](#21-configuration-of-the-training)</td><td><i>Configuring the inputs, network design, loss functions, etc.</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2.1.1 Defining variables and inputs](#211-defining-variables-and-inputs)</td><td><i>Setting up the training environment</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2.1.2 Configuring the DNN](#212-configuring-the-dnn)</td><td><i>Configuring the DNN</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;[2.2 Train the cluster calibration](#22-train-the-cluster-calibration)</td><td><i>Exexuting the training</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2.2.1 Initial training](#221-initial-training)</td><td><i>First full (re)training</i></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[2.2.2 Refitting](#222-refitting)</td><td><i>Refitting of previously trained network</i></td></tr>
<tr><td><b>[3 Analysis](#3-analysis)</b></td><td><i>Testing, validation and analysis</i></td></tr> 

<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.1.1 Initial setup](#111-initial-setup)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.1.2 At each login](#112-at-each-login)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;[1.2 Windows10 (PowerShell)](#12-windows10-powershell)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.2.1 Initial setup](#121-initial-setup)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[1.2.2 At each login](#122-at-each-login)<br/>
[2 Training](#2-training)<br/>
[3 Analysis](#3-analysis)<br/>
[References](#references)<br/>-->
</table>

## 1 Setup

The setup procedures discussed here are tailored for <tt>CentOS7</tt> (on a machine with a similar environment to <tt>lxplus</tt> at CERN) and  <tt>Windows10</tt>, respectively. In both cases [<tt>conda</tt>](https://docs/conda.io/latest/) is used for code management. The prompt symbol <tt>[prompt]>></tt> is a space holder for the actual prompt configured in the specific <tt>CentOS7</tt> or <tt>Windows10</tt> (using <tt>PowerShell</tt>) environment. 

### 1.1 CentOS7

It is assumed that the working environment sets up standard tools like [<tt>root</tt>](https://root.cern.ch) and <tt>git</tt>. For the ATLAS setup, this can be done by 

    [prompt]>> setupATLAS
    [prompt]>> lsetup git
    [prompt]>> lsetup "views LCG_97a_x86_64-centos7-gcc8-opt"

The [<tt>analysis/scripts/setup.sh</tt>](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/analysis/scripts/setup.sh) script in this repository sets this environment up. Note that setting up <tt>views</tt> is a more efficient and compact way to setup <tt>root</tt> (among others) and the many libraries it needs to load, as it provides one common path <tt>LD_LIBRARY_PATH</tt> to all needed static and dynamic load libraries. _This setup needs to be done for every login/clean shell_. 

It is recommended to use the latest version of the [<tt>miniconda</tt>](https://docs.conda.io/en/latest/miniconda.html) installer for <tt>conda</tt> which includes a <tt>python</tt> version &ge; 3.9. The installer can get downloaded by

    [prompt]>> wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

or by

    [prompt]>> wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.11.0-Linux-x86_64.sh


#### 1.1.1 Initial setup

The initial setup is only done once for a given (typically new) workarea. First, a directory for the python modules and the environment configurations needs to be created by

    [prompt]>> mkdir ./tensorflow0

To set up the <tt>conda</tt> environment, use [<tt>condaEnv.yaml</tt>](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/config/condaEnv.yaml) from this repository or create a <tt>yaml</tt> file with the following content:

```yaml
    name: tensorflow0
    channels:
      - conda-forge
      - anaconda
      - defaults
    dependencies:
      - python=3.9
      - cudatoolkit=11.2
      - cudnn=8.1
```
The <tt>conda</tt> environment is then created by typing

    [prompt]>> conda env create -f ~/condaEnv.yaml -p ./tensorflow0

Activate the environment with 

    [prompt]>> conda activate ./tensorflow0

The following <tt>python</tt> modules are needed as well: <tt>matplotlib</tt>, <tt>tensorflow-addons</tt>, <tt>uproot</tt>, <tt>h5py</tt>, and <tt>numexpr</tt>. These can be installed using <tt>conda</tt>:

    [prompt]>> conda install matplotlib tensorflow-addons uproot h5py numexpr

A module can be missing in the <tt>conda</tt> repository, or its version can be too old. In this case, the <tt>pip</tt> from the [<tt>PyPI</tt>](https://pypi.org/project/pip/) package can be used. ***Note that*** <b><tt>pip install</tt></b> ***and*** <b><tt>conda install</tt></b> ***do not mix well. First try*** <b><tt>conda install</tt></b>***, then install the missing modules with*** <b><tt>pip install</tt></b>***!*** 

Then create a working directory:

    [prompt]>> mkdir ./MLTopoCluster
    [prompt]>> cd ./MLTopoCluster
    [prompt:MLTopoCluster]>>

Clone the <tt>git</tt> repository into your working area:

    [prompt:MLTopoCluster]>> git clone https://gitlab.cern.ch/loch/MLClusterCalibration MLClusterCalibration

This creates a subdirectory <tt>./MLTopoCluster/MLClusterCalibration</tt> containing the cloned repository. It is suggested to create and configure a run directory for the training

    [prompt:MLTopoCluster]>> mkdir run
    [prompt:MLTopoCluster]>> cd run
    [prompt:MLTopoCluster/run]>> source ../MLClusterCalibration/ClusterCalibDNN/trainscripts/clustertraining/setupLinks.sh

After this, the run directory should look like this (example for user <tt>loch</tt>):

    [prompt:MLTopoCluster/run]>> ls -al
    lrwxrwxrwx 1 loch zp     110 Mar 10 15:23 buildBHfromTrainerCL.py -> ../ClusterCalibDNN/trainscripts/clustertraining/buildBHfromTrainerCL.py
	lrwxrwxrwx 1 loch zp      85 Mar 10 15:23 clusterCalibDNN.py -> ../ClusterCalibDNN/trainscripts/clusterCalibDNN.py
	lrwxrwxrwx 1 loch zp      89 Mar 10 15:23 plotAndDebugDNN.py -> ../ClusterCalibDNN/histoscripts/plotAndDebugDNN.py
	-rw-r--r-- 1 loch zp    5328 Mar 11 11:03 setupCluster.py
    [prompt:MLTopoCluster/run]>>

After this setup procedure, the working directory is **<tt>./MLTopoCluster</tt>**, the local copy of the repository is in **<tt>./MLTopoCluster/MLClusterCalibration</tt>** and the run directory is **<tt>./MLTopoCluster/run</tt>**.

#### 1.1.2 At each login

To prepare the training, testing and validation the `conda` setup is used.

    [prompt]>> conda
    (base)[prompt]>> conda activate ./tensorflow0
    (./tensorflow0)[prompt]>> cd ./MLTopoCluster/run
    [prompt::MLTopoCluster/run]>>

[Training](#2-training) and/or [analysis](#3-analysis) can now be run following the instructions given in sections [2](#2-training) and [3](#3-analysis) below. 

### 1.2 Windows10 (PowerShell)

The setup in <tt>PowerShell</tt> on <tt>Windows10</tt> may require to manually set the <tt>PYTHONPATH</tt> environment in every session. It is not recommended to add a specific <tt>PYTHONPATH</tt> globally and permanently, as this may interfere with other <tt>python</tt> codes run in <tt>PowerShell</tt>. 

#### 1.2.1 Initial setup

Similar to the initial setup for <tt>CentOS7</tt> discussed in section [1.1.1](#111-initial-setup), <tt>conda</tt> needs to be installed using <tt>[miniconda](https://docs.conda.io/en/latest/miniconda.html)</tt>. The <tt>Windows10</tt> installer can be found [here](https://docs.conda.io/en/latest/miniconda.html#windows-installers). It should install the <tt>Anaconda PowerShell</tt> app, which is fully set up for use. Start this app.

Again following the [<tt>CentOS7</tt> setup](#111-initial-setup), a directory for the <tt>conda</tt> modules and environment needs to be created:

    PS [prompt]>> mkdir C:\Conda 

This is only an example, the name and path can be freely chosen. Note that the path in this example is chosen to not be user specific, which in a given <tt>Windows10</tt> installation may require privileged user (administrator) login credentials. The <tt>conda</tt> configuration is loaded using the [<tt>condaEnv.yaml</tt>](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/config/condaEnv.yaml). Assuming this file is in the <tt>C:\Conda</tt> directory, the following command is used to configure the build and run environment

    PS [prompt]>> conda env create -f C:\Conda\condaEnv.yaml -p C:\Conda

Setting up a working area is similar to the setup for <tt>CentOS7</tt> described in section [1.1.1](#111-initial-setup). 

    PS [prompt]>> mkdir MLTopoCluster
    PS [prompt]>> cd MLTopoCluster
    PS [prompt]>> mkdir run

It is useful to install <tt>[git](https://github.com/git-guides/install-git)</tt> on the <tt>Windows10</tt> machine as well. It comes with the <tt>git bash</tt> app, which can be used to install a local clone of the repository in a similar way it is done in <tt>CentOS7</tt>, see section [1.1.1](#111-initial-setup).

The following <tt>python</tt> modules are needed as well: <tt>matplotlib</tt>, <tt>tensorflow-addons</tt>, <tt>uproot</tt>, <tt>h5py</tt>, and <tt>numexpr</tt>. These can be installed using <tt>conda</tt>:

    PS [prompt]>> conda install matplotlib tensorflow-addons uproot h5py numexpr

A module can be missing in the <tt>conda</tt> repository, or its version can be too old. In this case, the <tt>pip</tt> from the [<tt>PyPI</tt>](https://pypi.org/project/pip/) package can be used. ***Note that*** <b><tt>pip install</tt></b> ***and*** <b><tt>conda install</tt></b> ***do not mix well. First try*** <b><tt>conda install</tt></b>***, then install the missing modules with*** <b><tt>pip install</tt></b>***!***

Setting up the run directory requires to install a copy of the following scripts:

    PS [prompt]>> cd run
    PS [prompt]>> Copy-Item -Path "..\ClusterCalibDNN\trainscripts\clustertraining\buildBHfromTrainerCL.py" -Destination "buildBHfromTrainerCL.py"
	PS [prompt]>> Copy-Item -Path "..\ClusterCalibDNN\trainscripts\clusterCalibDNN.py" -Destination "clusterCalibDNN.py"
	PS [prompt]>> Copy-Item -Path "..\ClusterCalibDNN\histoscripts\plotAndDebugDNN.py" -Destination "plotAndDebugDNN.py"
	PS [prompt]>> Copy-Item -Path "..\ClusterCalibDNN\trainscripts\setupCluster.py" -Destination "setupCluster.py"

The file copies can also be done using the <tt>MS File Explorer</tt> application.

#### 1.2.2 At each login

After starting the <tt>Anaconda PowerShell</tt> app, the <tt>conda</tt> environment needs to be activated

    (base) PS [prompt]>> conda activate C:\Conda
    (C:\Conda) PS [prompt]>> 

As mentioned above, it may be necessary to set the <tt>PYTHONPATH</tt> environment:

    (C:\Conda) PS [prompt]>> $env:PYTHONPATH = "C:\Users\loch\MLTopoCluster\MLClusterCalibration"
    (C:\Conda) PS [prompt]>> cd MLTopoCluster/run
    (C:\Conda) PS [prompt:MLTopoCluster\run]>>

## 2. Training

Training typicall consist of an intial training step follwoed by refit and plotting steps. The following universal sets of commands for each step can be used in the <tt>run</tt> direcotry after the respective setup has been executed at each login. Results from previous steps can be recalled, with the latest being used automatically. 

### 2.1 Configuration of the training

The basic configuration of the training is done in [<tt>setupCluster.py</tt>](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/trainscripts/clustertraining/setupCluster.py). In the top part of this script relevant variables and their presentations are declared. These can be inputs to the training but also any other variable needed. The description is captured in the <tt>Variable</tt> object defined in [<tt>Variables.py</tt>](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/Variables.py). 

The default training configuration is defined in <tt>setupCluster.py</tt>, which is further dicussed in the Section 2.1.1. 

#### 2.1.1 Defining variables and inputs

The description of variables is useful for the training as well as for the graphical representation. It comprises a mandatory name and several optional parameters, including a title (e.g., for an axis label in a figure), a range, possible transformations and their reverse actions, and possible filters. Declaring variables as shown in <tt>Block 1</tt> of [setupCluster.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/trainscripts/setupCluster.py) has no immediate effect on the training. A variable declaration does not add it to the list of inputs or the list of target predictions. 

<a name="input_def">Defining</a> inputs for the ML training happens in <tt>Block 3</tt> of [setupCluster.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/trainscripts/setupCluster.py), together with other configuration parameters. The default configurations are:<sup><a href="#Foot1">&lowast;</a></sup>
```python
conf.update(
    nnName = "clusterCalib" ,               # network/project name
    inputFiles = 'JZ.topo-cluster.root',    # input data file
    treeName = "ClusterTree" ,              # name of ROOT tree in input data file
    inputDir = "./data",                    # relative or absolute path to input file directiory
    features = ["clusterEta",               # input: cluster rapidity          
                "clusterE",                 # input: cluster EM-scale energy
                "cluster_CENTER_LAMBDA",    # input: cluster depth in calorimeter
                "cluster_LONGITUDINAL",     # input: normalized longitudinal compactness measure
                "cluster_LATERAL",          # input: normalized lateral compactness measure
                "cluster_ENG_FRAC_EM",      # input: fraction of energy in EM calorimeter
                "cluster_FIRST_ENG_DENS",   # input: first moment of cell energy densities
                "cluster_SIGNIFICANCE",     # input: overall cluster signal significance
                "cluster_PTD",              # input: cluster fragmentation function
                "cluster_SECOND_TIME",      # input: second moment of cell time distribution
                "avgMu",                    # average number of pile-up collisions contributing to recorded event
                "nPrimVtx", ],              # number of primary vertices (hard scatter and pile-up) reconstructed for recorded event

    # ------------
    targets=['r_e', ],                      # prediction target
    # ------------
)
```
<a name="Foot1"><sup>&lowast;</sup></a>The input file(s) location specified in <tt>inputFiles</tt> and <tt>inputDir</tt> need to be adjusted to the point to the local file storage.[<a href="#input_def">back to top</a>]

The inputs are cluster kinematic variables and some cluster moments described in this [document](https://docs.google.com/document/d/1D4haSVnKQNqIuqz5BINUZn7HbIatXEjKqeiDYe3ATac/edit?usp=sharing) and kinematic (EM-scale) features of the topo-cluster described in Ref. <a href="#Ref1">\[1\]</a>. The average number of pile-up collisions <_&mu;_> affecting the event that contains a given topo-cluster is stored in <tt>avgMu</tt>, while <tt>nPriVtx</tt> stores the number of reconstructed primary collision vertices <i>N</i><sub>PV</sub> for the same event. These observables are strong indicators of the pile-up, either in-time (<i>N</i><sub>PV</sub>) or out-of-time (<_&mu;_>). Further details on the features (inputs), the target(s) and the predictions can be found in the project notes (link: [doc/ProjectNotes.docx](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/doc/ProjectNotes/docx)).

#### 2.1.2 Configuring the DNN

The configuration of the DNN includes (1) setting up the (hidden) layers, (2) defining the exit conditions, (3) defining the activation function, and (4) defining the loss function.   

### 2.2 Train the cluster calibration

The training is executed from the working directory. This is the directory that contains the <tt>setupCusters.py</tt> file, see instructions for your specific environment [in this section](#1-setup). The training steps are the same for the <tt>CentOS7</tt> and the <tt>MS PowerShell</tt> environment, with the respective prompt <tt><prompt></tt>:

    <prompt> python -i clusterCalibDNN.py setupClusters.py

This instruction performs the folowing setup tasks

From <tt>[clusterCalibDNN.py]()<tt>:

1. clones the default configuration defined in <tt>[ConfigUtils.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/ConfigUtils.py)</tt> into an object named <tt>conf</tt>;
1. imports the model definitions from <tt>[ModelDefinitions.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/ModelDefinitions.py)</tt>
1. imports the tools for reading and writing <tt>ROOT</tt> files, sorting and weighting data from <tt>[GeneratorFromRootFile.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/GeneratorFromRootFile.py)</tt>; 
1. imports numerical manipulations (data scaling and concatenation, etc.) from <tt>[NNUtils.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/NNUtils.py)</tt>;

From <tt>setupClusters.py</tt>:




#### 2.2.1 Initial training

1. load the model definitions from <tt>[ModelDefinitions.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/ModelDefinitions.py)</tt>;
1. import the variables descriptions, ranges and transformations from [Variablee.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/Variables.py);
1. defines the names, titles, ranges and transformations for all variables of interest and stores those definitions in the <tt>clustVar</tt> array (<tt>Block #1</tt> in <tt>[setupClusters.py](https://gitlab.cern.ch/loch/MLClusterCalibration/-/blob/main/ClusterCalibDNN/trainscripts/setupCluster.py)</tt>);
1. 

    <prompt> trainer.train(conf.update(nInputFiles=-1,modelBuilder=HeadIdCoreT0ST(),mode="reco:train"),reload=0,fitSequence=defaultSequenceCL)




#### 2.2.2 Refitting



## 3. Analysis

## References

<a name="Ref1">[1]</a> ATLAS Collaboration, <i>Topological cell clustering in the ATLAS calorimeters and its performance in LHC Run 1</i>, [Eur.Phys.J.C. 77 (2017) 490](https://inspirehep.net/literature/1426830).<br/>
<a name="Ref2">[2]</a> D. Guest, K. Cranmer, and D. Whiteson</a>, <i>Deep learning and its application to LHC physics"</i>, [Ann.Rev.Nucl.Part.Sci. 68 (2018) 161](https://inspirehep.net/literature/1680302).<br/>
<a name="Ref3">[3]</a> C.I.W. Group, <i>A living review of machine learning for particle physics</i>, URL: https://iml-wg.github.io/HEPML-LivingReview.<br/>
<!-- <table style="border:none;border-width:0px;border-color:white;" width="95%" align="center" cellspacing="0" cellpadding="0">
<tr><td align="left" valign="top">[<a id="topocluster">1</a>]</td><td align="left" valign="top">ATLAS Collaboration, <i>Topological cell clustering in the ATLAS calorimeters and its performance in LHC Run 1</i>, [Eur.Phys.J.C. 77 (2017) 490](https://inspirehep.net/literature/1426830).</td></tr>
<tr><td align="left" valign="top">[<a id="DNN">2</a>]</td><td align="left" valign="top">D. Guest, K. Cranmer, and D. Whiteson, <i>Deep learning and its application to LHC physics"</i>, [Ann.Rev.Nucl.Part.Sci. 68 (2018) 161](https://inspirehep.net/literature/1680302)</td></tr>
<tr><td>&nbsp;</td><td align="left" valign="top">C.I.W. Group, <i>A living review of machine learning for particle physics</i>, URL:https://iml-wg.github.io/HEPML-LivingReview</td></tr> 
</table>-->

