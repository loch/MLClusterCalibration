""" 
This sets up an analysis of histogram containers as produced by buildBHfromTrainer.py. 
 - Create a GraphicSession object
 - Load histo containers from the GraphicSession objects
 - loading analysis helper functions from responseAnalysis.py
"""
from ClusterCalibDNN.HistoAnalysis.BinnedArrays import *
from ClusterCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 

# Include a bunch of functions useful for analysis of response histograms :
exec(open('responseAnalysis.py').read())


plt.rc('xtick', labelsize=14)
plt.rc('ytick', labelsize=14)
plt.rc('font', size=16)
plt.rc('xaxis', labellocation='right')

plt.rc('legend', title_fontsize=22)
plt.rc('legend', fontsize=22)
        
    
# ********************************
# style functions
# They return a dictionnary of style (color, labels, line styles,etc...) according to the BinnedArrays they receive.
# They are called within bag.readBAFromFile() for each read BinnedArray
def atlasStyle(ba):
    if 'r_uncal' in ba.name:
        return dict(color='grey', label='no calib')
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    else:
        return dict(color='red', label='std calib')

def atlasStyleWithJES(ba):
    if 'r_uncal' in ba.name:
        return dict(color='grey', label='no calib')
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    elif 'jes' in ba.name:        
        return dict(color='blue', label='JES')
    else:
        return dict(color='red', label='JES+GSC')


def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))



sampleType = 'WZ'
sampleType = 'HHbb'
sampleType = ''

jetType = 'AntiKt4EMPFlow'

if sampleType == 'WZ':
    inputTag = 'WZplots'
elif sampleType == 'HHbb':
    inputTag = 'HHbbplots'
elif sampleType=='' and jetType == 'AntiKt4EMPFlow': # dijet
    inputTag = 'AntiKt4EMPFlow'


# Create a GraphicSession object to hold all the histo/graph containers
gs = bag.GraphicSession(
    bpsTag ='pTeta',
    plotDir = inputTag,

) # 



# Load histos with uncalibrated and atlas-calibrated responses : 
gs.readBAFromFile('smallRresp/AntiKt4EMPFlow_atlas'+gs.bpsTag+'_resp.h5', styleFunc=atlasStyleWithJES)
#gs.readBAFromFile('smallRresp/AntiKt4EMPFlow_atlas'+gs.bpsTag+'_respFlatW.h5', styleFunc=atlasStyleWithJES)

# Load histos with dnn-calibrated responses
#  the tag argument is given to distinguish the BinnedArray loaded from this file :
#   --> ex: if this file contains a BinnedHistos named 'histoContainer' and tag='XYZ' then it will be available as gs.histoContainerXYZ
gs.readBAFromFile('smallRresp/AntiKt4EMPFlowJetsMDN950e2f2bfb0r_e_r_mNoAnnotSplitCEtaGSAETAEN500_t0e_directpTetarespDNN.h5', tag='coreSplitAEtanh0' )
# can load as many BinnedHistos as we want  : 
#gs.readBAFromFile('smallRresp/AntiKt4EMPFlowJetsMDNA8ab26a9fae9r_eEtaGST0T1uoN740directSFOpSigF_directpTetarespDNN.h5', tag='simpLowptSFixSig' ,styleFunc=styleWithRecoBins)



# fix a few things which were not saved properly in earlier versions :
for b in gs.allBA: # allBA is the list of all BinnedArrays known to gs
    for a in b.bps.axis:
        if a.isGeV: a.title = a.title+'[GeV]'
    if isinstance(b, BinnedHistos):
        var = b.name.split('_')[0]
        if b.xaxis.name=='':
            b.xaxis.name = var
            b.xaxis.title = var

        

gs.bps = gs.allBA[0].bps
plt.ion()

        



