
import h5py
import numpy as np
# Import Keras (either standalone, either as embedded in tensorflow )
from .ImportKeras import *
from .ConfigUtils import ConfigDict
#****************************************
def debug_silent(*l):
    pass

debug = debug_silent
def switch_debugging(on=True):
    global debug
    debug = print if on else debug_silent


## ***********************************************************************
## Loss functions
## ***********************************************************************

def testMSE(y_true, y_pred):
    loss = K.square(y_true - y_pred)
    return loss


def wMSE(model,suppF=None):
    def _lossSupp(y_true, y_pred):
        loss = suppF*K.softsign(1./suppF*(y_pred-y_true)**2)
        return loss*model.classWeights        
    def _loss(y_true, y_pred):
        return (y_true - y_pred)**2*model.classWeights
    return _lossSupp if suppF else _loss

def wLogcosh(model,suppF=None):
    def _lossSupp(y_true, y_pred):
        x = y_true - y_pred
        lch = x + K.softplus(-2. * x) - K.log(2.)
        return suppF*K.softsign(1./suppF*lch)*model.classWeights        
    def _loss(y_true, y_pred):
        x = y_true - y_pred
        return (x + K.softplus(-2. * x) - K.log(2.))*model.classWeights
    return _lossSupp if suppF else _loss


def relativeMSE(model,f=None):
    def _relativeMSEsupp(y_true, y_pred):
        loss = f*K.softsign(1./f*(1 - y_pred/y_true)**2)
        return loss*model.classWeights
    def _relativeMSE(y_true, y_pred):
        loss = (1 - y_pred/y_true)**2
        return loss*model.classWeights
    return _relativeMSEsupp if f else _relativeMSE 

def relativeMAE(model,f=None):
    def _relativeMSEsupp(y_true, y_pred):
        loss = f*K.softsign(1./f*K.abs(1 - y_pred/y_true))
        return loss*model.classWeights
    def _relativeMSE(y_true, y_pred):
        loss = K.abs(1 - y_pred/y_true)
        return loss*model.classWeights
    return _relativeMSEsupp if f else _relativeMSE 



def dependsOnModel( f ):
    def _f(model):
        @tf.functions
        def _ff(x,y):
            return f(model, x,y)
        return _ff
    _f.dependsOnModel = True
    return _f


@tf.function
def smoothstep(x, a,b):
    x = tf.clip_by_value( (x-a)/(b-a), clip_value_min=0., clip_value_max=1.)
    x2 = tf.square(x)
    return (3*x2-2*x2*x)

@tf.function
def smoothgate(x, center, hwidth):
    e1 = center-hwidth
    e2 = -(center+hwidth) # minus because we'll pass the invert to smoothstep
    f = 0.3
    delta = f*hwidth
    
    return (smoothstep(x,e1-delta,e1+delta)+smoothstep(-x,e2-delta,e2+delta) )-1

    
# modelDepLosses = set()
# def isLossModelDependent(lossF, net):
#     global modelDepLosses
#     if lossF in modelDepLosses:
#         return lossF(net)
#     return lossF

@tf.function
def nullLoss(y_true, y_pred):
    return 0.

@tf.function
def mdnLoss(y_true, y_pred):
    sig = tf.maximum(y_pred[:,1],0.005)

    loss= tf.math.log(sig)+ 0.5*( (y_pred[:,0]-y_true[:,0])/sig )**2
    #tf.print("\n xxx ", y_true[:4], "   ", loss[:4])
    return loss

@tf.function
def mdnLossSF(y_true, y_pred):
    sig = tf.maximum(y_pred[:,1],0.005)

    loss= tf.math.log(sig)+ 0.5*( (y_pred[:,0]*y_true[:,0] -1)/sig )**2 - tf.math.log(y_pred[:0])
    #tf.print("\n xxx ", y_true[:4], "   ", loss[:4])
    return loss


@tf.function
def amdnLoss(y_true, y_pred):
    """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


    ## IMPORTANT !!!!
    ## Bug in keras/TF ??
    ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
    ## MUST USE: y_true[:,0] !!
    ## see test function below to reproduce
    mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
    totalSig = 0.5*( sig1+sig2 )

    sig = tf.where( y_true[:,0] > mu  , sig1 , sig2)
    #sig = tf.maximum(y_pred[:,1:],0.005)
    loss= tf.math.log(totalSig)+ 0.5*( (mu-y_true[:,0])/sig )**2
    return loss

@tf.function
def amdnLossSF(y_true, y_pred):
    """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


    ## IMPORTANT !!!!
    ## Bug in keras/TF ??
    ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
    ## MUST USE: y_true[:,0] !!
    ## see test function below to reproduce
    mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
    totalSig = 0.5*( sig1+sig2 )

    scaledY = y_true[:,0]*mu
    sig = tf.where( scaledY > 1  , sig1 , sig2)
    #sig = tf.maximum(y_pred[:,1:],0.005)
    loss= tf.math.log(totalSig)+ 0.5*( (scaledY-1)/sig )**2 - tf.math.log(y_pred[:,0])
    return loss



def truncamdnLoss(nsigma=3):
    if nsigma is None:
        return amdnLoss
    @tf.function    
    def _amdnLoss(y_true, y_pred):
        """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


        ## IMPORTANT !!!!
        ## Bug in keras/TF ??
        ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
        ## MUST USE: y_true[:,0] !!
        ## see test function below to reproduce
        #mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
        mu, sig1, sig2 = tf.unstack( tf.maximum( y_pred, 0.005) ,axis=1)
        totalSig = 0.5*( sig1+sig2 )

        sig = tf.where( y_true[:,0] > mu  , sig1 , sig2)
        #sig = tf.maximum(y_pred[:,1:],0.005)
        normDist = (mu-y_true[:,0])/sig
        loss= tf.math.log(totalSig)+ 0.5*(  normDist )**2

        #nsig = sig*nsigma
        m = tf.cast( tf.logical_and(normDist < nsigma , normDist > -nsigma), tf.float32)

        loss*=tf.stop_gradient(m)
        return loss
    return _amdnLoss


def truncamdnLossSF(nsigma=None):
    if nsigma is None:
        return amdnLossSF
    @tf.function    
    def _amdnLoss(y_true, y_pred):
        """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


        ## IMPORTANT !!!!
        ## Bug in keras/TF ??
        ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
        ## MUST USE: y_true[:,0] !!
        ## see test function below to reproduce
        #y_pred = tf.maximum(y_pred, 0.005) # avoid nan if any of the 3 values is 0
        #mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
        mu, sig1, sig2 = tf.unstack( tf.maximum( y_pred, 0.005) ,axis=1)
        totalSig = 0.5*( sig1+sig2 )

        scaledY = y_true[:,0]*mu
        sig = tf.where( scaledY > 1  , sig1 , sig2)

        normDist = (scaledY-1)/sig + 1.0e-10
        loss= tf.math.log(totalSig)+ 0.5*(  normDist )**2 - tf.math.log(mu)

        #nsig = sig*nsigma
        m = tf.cast( tf.logical_and(normDist < nsigma , normDist > -nsigma), tf.float32)

        #loss = tf.math.multiply_no_nan( loss, tf.stop_gradient(m))
        loss *=tf.stop_gradient(m)
        return loss
    return _amdnLoss

def truncFrozenSigLossSF(nsigma):
    @tf.function    
    def _amdnLoss(y_true, y_pred):
        """ MDN loss assuming an assymetric double-sided gaussian as the pdf. Thus expects y_pred.shape == (:,3) """


        ## IMPORTANT !!!!
        ## Bug in keras/TF ??
        ## if using directly (y_true - mu ) --> memory allocation issues (systems try to allocate (batch_size,batch_size)
        ## MUST USE: y_true[:,0] !!
        ## see test function below to reproduce
        #y_pred = tf.maximum(y_pred, 0.005) # avoid nan if any of the 3 values is 0
        #mu, sig1, sig2 = y_pred[:,0], tf.maximum(y_pred[:,1],0.005), tf.maximum(y_pred[:,2],0.005) #tf.unstack(y_pred,axis=1)
        mu, sig1, sig2 = tf.unstack( tf.maximum( y_pred, 0.005) ,axis=1)

        scaledY = y_true[:,0]*mu
        sig = tf.where( scaledY > 1  , sig1 , sig2)

        normDist = (scaledY-1)/sig + 1.0e-10
        loss=  0.5*(  normDist )**2 - tf.math.log(mu)

        #nsig = sig*nsigma
        m = tf.cast( tf.logical_and(normDist < nsigma , normDist > -nsigma), tf.float32)

        #loss = tf.math.multiply_no_nan( loss, tf.stop_gradient(m))
        loss *=tf.stop_gradient(m)
        return loss
    return _amdnLoss


@tf.function
def amdnLossTest0(y_true, y_pred):
    # ok
    return tf.math.reduce_sum(y_pred)


@tf.function
def amdnLossTest2(y_true, y_pred):
    # ok
    return (y_true)**2+tf.math.reduce_sum(y_pred)

@tf.function
def amdnLossTest4(y_true, y_pred):
    # crash out2 and out3
    return (y_true-y_pred[:,1])+tf.math.reduce_sum(y_pred)

@tf.function
def amdnLossTest5(y_true, y_pred):
    # ok out2 & out 3
    return (y_true[:,0]-y_pred[:,1])+tf.math.reduce_sum(y_pred)








def truncMSE(nsigma=None):
    @tf.function
    def _f(y_true, y_pred):
        return (y_pred[:,0]-y_true[:,0])**2
    return truncGEN(nsigma, f=_f)

def truncMAE(nsigma=None):
    @tf.function
    def _f(y_true, y_pred):
        return tf.abs(y_pred[:,0]-y_true[:,0])
    return truncGEN(nsigma, f=_f)
    
def truncGEN(nsigma=None, f=None):    
    @tf.function
    def _truncatedLoss(y_true, y_pred):
        sig = K.maximum(y_pred[:,1],0.005)
        diff = (y_pred[:,0]-y_true[:,0])        
        nsig = sig*nsigma

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        #sig = tf.boolean_mask(sig, m )
        #diff = tf.boolean_mask(diff, m)
        normDist = (diff/sig)**2
        #tf.print( " n=",n , tf.math.reduce_sum( K.log(sig) ) ,"   __ ")
        loss = f(y_true,y_pred) # diff**2

        m = tf.logical_and(diff < nsig , diff > -nsig)

        # loss = tf.boolean_mask(loss, m)
        # n = n0/tf.cast(tf.shape(loss)[0], tf.float32)

        n =tf.cast(m,tf.float32)* n0 / tf.math.count_nonzero(m,dtype=tf.float32)
        #tf.print(" n=", n[:4], "  rr ",tf.math.count_nonzero(m), )

        tf.stop_gradient(n)
        loss *=n
        return loss
        #tf.print("\n xxx ", y_true[:4], "   ", loss[:4])
    return _truncatedLoss

def truncExpMDN(nsigma=None, ):
    nsigma2 = nsigma*nsigma
    erfF = np.vectorize(np.math.erf)
    erf = erfF(nsigma/np.sqrt(2))
    sigmaFactor = 1./ ( (1-np.sqrt(2./np.pi)*nsigma*np.exp(-0.5*nsigma2)/erf))
    @tf.function
    def _truncMSE(y_true, y_pred):
        sig = K.maximum(tf.abs(y_pred[:,1]),0.005)
        diff = (y_pred[:,0]-y_true[:,0])        

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        #sig = tf.boolean_mask(sig, m )
        #diff = tf.boolean_mask(diff, m)
        normDist = (diff/sig)**2
        #tf.print( " n=",n , tf.math.reduce_sum( K.log(sig) ) ,"   __ ")
        #loss = f(y_true,y_pred) # diff**2
        loss = (tf.math.log(sig)+ 0.5*normDist*sigmaFactor)

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        n = tf.exp(-normDist/nsigma2)

        # normalize
        n *= n0/ tf.math.reduce_sum(n)
        #tf.print(" n=", n[:4], "  rr ",tf.math.count_nonzero(m), )

        n = tf.stop_gradient(n)
        loss *=n
        return loss
    #tf.print("\n xxx ", y_true[:4], "   ", loss[:4])
    return _truncMSE



def truncmdnLoss(nsigma=None, normByG=False,   mPenalty=None):
    if nsigma is None:
        return mdnLoss

    # compute truncated gaussian normalization as function of nsigma and sigma
    erfF = np.vectorize(np.math.erf)
    erf = erfF(nsigma/np.sqrt(2))
    sigmaFactor = 1./ ( (1-np.sqrt(2./np.pi)*nsigma*np.exp(-0.5*nsigma**2)/erf)) # > 1 up to 3.4 at nsigma=1

    # an optional penalty term for very high prediction.
    # if given, the penalty added to the loss will be
    #  pStrenght*( (y_pred/pStart)**2-1)  for all cases where y_pred>pStart
    pStart, pStrenght = mPenalty if mPenalty else (0,0)
    @tf.function
    def _truncmdnLoss(y_true, y_pred):

        # by convention, assume predictions in the form (mu, sig)            
        sig = K.maximum(y_pred[:,1],0.005) # force sigma>0 to avoid generating nan values
        diff = (y_pred[:,0]-y_true[:,0])        
        nsig = sig*nsigma

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        normDist = (diff/sig)**2

        # the normal MDN loss (except for the sigmaFactor)
        loss = (tf.math.log(sig)+ 0.5*normDist*sigmaFactor)

        # Find where to truncate at n sigma (--> m will be 0 where truncated and 1 elsewhere)
        m = tf.logical_and(diff < nsig , diff > -nsig)

        # Normalize w.r.t number of truncated
        if normByG:
            n = tf.cast(m,tf.float32)*tf.exp(-normDist)
        else:
            n = tf.cast(m,tf.float32)* n0 / K.maximum( 1., tf.math.count_nonzero(m,dtype=tf.float32))
            

        
        # Apply truncation & normalization to the loss values 
        loss *= tf.stop_gradient(n)

        return loss

    @tf.function
    def _truncmdnLossPen(y_true, y_pred):
        sig = K.maximum(y_pred[:,1],0.005)
        #tf.print("ooooooo ", sig[:5] )
        diff = (y_pred[:,0]-y_true[:,0])        
        nsig = sig*nsigma

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        #sig = tf.boolean_mask(sig, m )
        #diff = tf.boolean_mask(diff, m)
        normDist = (diff/sig)**2
        #tf.print( " n=",n , tf.math.reduce_sum( K.log(sig) ) ,"   __ ")
        loss = (tf.math.log(sig)+ 0.5*normDist*sigmaFactor)

        # calculate a penalty term in the form pStrenght* ( (v/pStart)**2 - 1 )
        pDiff = y_pred[:,0] - pStart
        #tf.print(" ****** ", y_pred[:,0] , pStart)


        penalty = tf.where( pDiff<0., 0., pStrenght*pDiff*pDiff )

        #tf.print( " loss=",loss, " penalty=",penalty)
        loss+=penalty

        m = tf.logical_and(diff < nsig , diff > -nsig)


        # loss = tf.boolean_mask(loss, m)
        # n = n0/tf.cast(tf.shape(loss)[0], tf.float32)

        n =tf.cast(m,tf.float32)* n0 / tf.math.count_nonzero(m,dtype=tf.float32)
        #tf.print(" n=", n[:4], "  rr ",tf.math.count_nonzero(m), )

        #tf.stop_gradient(n)
        loss *= tf.stop_gradient(n)

        return loss

    
    if mPenalty:
        return _truncmdnLossPen
    else:
        return _truncmdnLoss        

def truncmdnLossSp(nsigma=None,  sPenalty=1e-2):
    if nsigma is None:
        return mdnLoss
    
    # compute truncated gaussian normalization as function of nsigma and sigma
    erfF = np.vectorize(np.math.erf)
    erf = erfF(nsigma/np.sqrt(2))
    sigmaFactor = 1./ ( (1-np.sqrt(2./np.pi)*nsigma*np.exp(-0.5*nsigma**2)/erf)) # > 1 up to 3.4 at nsigma=1

    @tf.function
    def _truncmdnLoss(y_true, y_pred):
        
        # by convention, assume predictions in the form (mu, sig)            
        sig = K.maximum(y_pred[:,1],0.005) # force sigma>0 to avoid generating nan values
        diff = (y_pred[:,0]-y_true[:,0])        
        nsig = sig*nsigma

        n0 = tf.cast(tf.shape(sig)[0], tf.float32)
        normDist = (diff/sig)**2

        # the normal MDN loss (except for the sigmaFactor)
        loss = (tf.math.log(sig)+ 0.5*normDist*sigmaFactor)

        # Find where to truncate at n sigma (--> m will be 0 where truncated and 1 elsewhere)
        m = tf.logical_and(diff < nsig , diff > -nsig)

        # Normalize w.r.t number of truncated
        n =tf.cast(m,tf.float32)* n0 / K.maximum( 1., tf.math.count_nonzero(m,dtype=tf.float32))

        # Apply truncation & normalization to the loss values 
        loss *= tf.stop_gradient(n)
        loss += sPenalty*sig*sig
        return loss
    return _truncmdnLoss



def lgkLoss( alpha, beta):
    if beta==0.:
        return gkLoss(alpha)
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred-y_true)
        e = tf.exp(-0.5*diff**2/alpha)
        return  beta*tf.abs(diff) - e
    return _lgkLoss

def gkLoss( alpha):
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred-y_true)
        e = -tf.exp(-0.5*diff**2/alpha)
        return e
    return _lgkLoss

def agkLoss( alpha):
    norm = 1/np.sqrt(alpha)
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred-y_true)
        e = -tf.exp(-0.5*diff**2/alpha)
        return e*norm
    return _lgkLoss


def lgk1Loss( alpha, beta):
    if beta==0.:
        return gk1Loss(alpha)
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred*y_true-1)
        e = tf.exp(-0.5*diff**2/alpha)
        return  beta*tf.abs(diff) - e
    return _lgkLoss

def agk1Loss( alpha):
    norm = 1/np.sqrt(alpha)
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred*y_true-1)
        e = -tf.exp(-0.5*diff**2/alpha)
        return e*norm
    return _lgkLoss

def gk1Loss( alpha):
    @tf.function
    def _lgkLoss(y_true, y_pred):
        diff = (y_pred*y_true-1)
        e = -tf.exp(-0.5*diff**2/alpha)
        return e
    return _lgkLoss

        
@tf.function
def mse1D(y_true, y_pred):
    diff= (y_pred-y_true)
    return (diff[:,0::2])**2
    
@dependsOnModel
def mdnOrMxeLoss(model, y_true, y_pred):
    doAxE = model.recenteringW #K.cast(model.go, K.floatx() )        
    sig = K.maximum(y_pred[:,1],0.005)
    #sig2 = sig*sig    
    normDist= ( (y_pred[:,0]-y_true[:,0])/sig )**2
    absDist = (y_pred[:,0]-y_true[:,0])**2
    #loss = K.switch( K.all(doAxE>0), absDist, K.log(sig)+ 0.5*normDist)
    loss = absDist*doAxE +(1-doAxE)* (K.log(sig)+ 0.5*normDist)
    return loss

@tf.function
def diffSqr(y_true, y_pred):
    return tf.abs(y_true**2-y_pred**2)


def standardPred( trainer, i=1, j=1):
    stda = trainer.inscaler.mean.reshape(1,15)*0
    @tf.function
    def _stdPred(y_true, y_pred):
        return trainer.net(stda)[i][:,j]
    return _stdPred




class LossCollector:
    def __init__(self, lossFunc, *axis):
        from ClusterCalibDNN.HistoAnalysis.BinnedArrays import BinnedPhaseSpace
        self.bps = BinnedPhaseSpace( 'lossColl', *axis)
        self.lossFunc = lossFunc
        self.cont = None
        self.globLosses = ConfigDict()
        
    def accumulate(self, trainer, nmax=None, globLosses={}):
        from ClusterCalibDNN.HistoAnalysis.BinnedArrays import BinnedPhaseSpace, BinnedScalar, AxisSpec

        lossCont = BinnedScalar( 'loss', self.bps )

        vindex = [ trainer.varIndexAndScaler(a.name)[0] for a in self.bps.axis ]

        trainer.gen_valid.set_batch_size(200000)
        trainer.gen_valid.load()
        x,y = trainer.gen_valid.current_unfiltered_sample(formatted=True)

        # Ntmp=200000
        nmax = nmax or trainer.gen_valid.file_helper.Nentries
        x = x[:nmax]


        preds = trainer.predict_formatted( x, rescalePreds=False, batch_size=200000, verbose=1)
        npred = preds.shape[0]
        nOut = trainer.config.nPredictedParam

        print('--> ',preds[:5,:nOut] )
        print('--> from ',x[:5] )

        if len(trainer.config.originalTargets)>1:
            yE = y[0]
        else:
            yE=y
        
        l = self.lossFunc(yE[:npred].reshape(npred,1), preds[:,:nOut]).numpy() # E

        
        for k, lFunc in globLosses.items():
            lossL = self.globLosses.setdefault(k,[])
            lossL.append( tf.math.reduce_sum(lFunc( yE[:npred].reshape(npred,1), preds[:,:nOut])  ).numpy() /npred )
            #self.lossContList.append(lossCont)        
        
        x,y = trainer.gen_valid.current_unfiltered_sample(formatted=False)

        vList = [ x[:npred,i] for i in vindex ]
        for i,a in enumerate(self.bps.axis):
            if a.name=='eta':
                vList[i] = np.abs(vList[i])

        coords = np.stack( vList, 1).T
        #lossCont.accumulate(coords, l[:,0] )
        lossCont.accumulate(coords, l.reshape(npred,) )


            
        if self.cont is None:
            self.cont = BinnedScalar.concat( [lossCont], name = self.lossFunc.__name__ )
        else:
            # append lossCont to existing cont, along the last dimension
            bps = self.cont.bps
            Nloss = bps.shape()[-1]+1
            extBPS = BinnedPhaseSpace(bps.name, *(self.cont.bps.axis[:-1]+( AxisSpec('n',Nloss,range=(0,Nloss) ), )) )
            extlossContCont = BinnedScalar( self.cont.name, extBPS )
            extshape = lossCont.array.shape+(1,)
            extlossContCont.array = np.concatenate( [ self.cont.array , lossCont.array.reshape(extshape) ], -1) 
            extlossContCont.counts = np.concatenate( [ self.cont.counts , lossCont.counts.reshape(extshape) ], -1)
            extlossContCont.nentries = self.cont.nentries + lossCont.nentries
            self.cont = extlossContCont
        self.cont.mean = self.cont.array/ self.cont.counts


    def removeLast(self):
        from ClusterCalibDNN.HistoAnalysis.BinnedArrays import BinnedPhaseSpace, AxisSpec
        bps = self.cont.bps
        n = bps.shape()[-1]
        nbps = BinnedPhaseSpace(bps.name, *(self.cont.bps.axis[:-1]+( AxisSpec('n',n-1,range=(0,n-1) ), )) )
        self.cont.bps = nbps
        self.cont.array = self.cont.array[...,:-1]
        self.cont.counts = self.cont.counts[...,:-1]
        self.cont.mean = self.cont.mean[...,:-1]
        
    def save(self, fname):
        if self.cont is None:
            return
        self.cont.saveInH5(fname)

    def load(self, fname):
        from ClusterCalibDNN.HistoAnalysis.BinnedArrays import BinnedScalar
        self.cont=BinnedScalar(self.lossFunc.__name__, bps=None , initArrays=False)
        try:            
            self.cont.loadFromH5(fname, loadContent=True)
            self.cont.mean = self.cont.array/ self.cont.counts            
        except KeyError:
            print('WARNING No Binned scalar ',self.cont.name)
            self.cont = None

    def draw(self,coords):
        from matplotlib import pyplot as plt
        ax= ax or plt.gca()
        ax.cla()
        x = np.arange(self.cont.shape[-1])
        plt.plot(x, self.cont.mean[ coords, :])
        

## ***********************************************************************
## activation functions
## ***********************************************************************

def leakRelu(x,):
    from tensorflow.keras.activations import relu    
    return relu(x,alpha=0.01,max_value=5.)

#@tf.function
def softsign2(x):
    #from keras.activations import softsign    
    return x/(0.5*K.abs(x)+1)
#softsign2 = np.vectorize(softsign2_ , otypes= [np.float])

def mish(x):
    return x*K.tanh(K.softplus(x))

def tanhp(x):
    return 1+K.tanh(x)


def tanhSF(x):
    return 2-K.tanh(x)

def tanh0(x):
    y0,y1 = tf.unstack(x,axis=1)
    return tf.stack([tf.tanh(y0),tf.sigmoid(y1) ] ,axis=1)


def gaus(sigma):
    sigma2 = -(0.5/sigma)**2
    def _g(x):
        return K.exp(x*x*sigma2)
    return _g

activationDict = ConfigDict(
    mish = mish,
    lisht = tfa.activations.lisht,
    lrelu = leakRelu,
    softsign2 = softsign2,
    gaus1= gaus(1),
    gaus05= gaus(0.5),    
    tanhp = tanhp,
    tanh0 = tanh0,
    tanhSF = tanhSF
)

#mish = np.vectorize(mish_ , otypes= [np.float])



## ***********************************************************************
## CallBacks
## ***********************************************************************


#Directory to store the log and call tensorboard for profiling

def tensorBoardProfile():
    from datetime import datetime
    log_dir="logs/logs_" + datetime.now().strftime("%Y%m%d-%H%M%S")
    TensorBoard = keras.callbacks.TensorBoard
    tensorboard_callback = TensorBoard(log_dir=log_dir,  histogram_freq=1, profile_batch=3)
    return tensorboard_callback


class NBatchLogger(keras.callbacks.Callback):
    def __init__(self,display=100):
        '''
        display: Number of batches to wait before outputting loss
        '''
        self.seen = 0
        self.display = display

    def on_batch_end(self,batch,logs={}):
        self.seen += logs.get('size', 0)
        if self.seen % self.display == 0:
            print ('\n{0}/{1} - Batch Loss: {2}'.format(self.seen,0,
                                                        logs['loss']) )

class FlagLossDecrease(keras.callbacks.Callback):
    lossT = -2.
    def on_epoch_end(self,ep, logs,):
        loss = logs['loss']
        #if K.get_value(self.model.recenteringW)
        lowLoss = ( loss<self.lossT)
        if lowLoss:
            print( " REACHED low loss !!! ",loss , " < ", self.lossT )
            K.set_value(self.model.recenteringW , 1.0  )
            print ('epoch end', ep ,logs, K.eval(self.model.recenteringW))


class ShowLR(keras.callbacks.Callback):
    def on_batch_end(self, batch, logs):
        print(" lr= ", self.model.optimizer._decayed_lr('float32').numpy())

class SaveGoodLoss(keras.callbacks.Callback):
    def __init__(self, lossT=-3.5, ):
        super(SaveGoodLoss, self).__init__()
        self.lossT = lossT

        
    def on_batch_end(self, batch, logs):
        if self.model.gen.num_seensofar > 2e7:
            loss= logs['loss']
            if loss < self.lossT:
                self.model.trainer.save_model('goodL'+'{:.3f}'.format(abs(loss)))
                self.lossT -= 0.2

class ShuffleInputFiles(keras.callbacks.Callback):
    def __init__(self,  ):
        super(ShuffleInputFiles, self).__init__()

        
    def on_epoch_end(self, batch, logs):
        iL= self.model.trainer.gen.shuffle_files()
        print('ShuffleInputFiles --> file0 = ', self.model.trainer.gen.chain.file_helpers[0].fname, iL)

class CopyModel(keras.callbacks.Callback):
    def __init__(self, trainer, t=-1.5 ):
        super(CopyModel, self).__init__()
        self.threshold = t
        self.trainer = trainer
        self.otrainer = None
        
    def on_batch_end(self, batch, logs):
        l = logs['loss']
        if np.isfinite(l) and  l < self.threshold:
            if self.otrainer is None:
                self.otrainer = self.trainer.copy()
            else:
                self.otrainer.copyWeights(self.trainer, nLayer=-1, verbose=False)
            #self.copy = keras.models.clone_model(self.model)
            
        

class ValidCallBack(keras.callbacks.Callback):
    def __init__(self,  lossFunc, ):
        super(ValidCallBack, self).__init__()
        self.lossFunc=lossFunc
        self.valLossE=[]
        self.valLossM=[]
        
    def on_epoch_end(self, batch, logs):
        iL= self.model.trainer.validation(loss = self.lossFunc, batch_size=200000)
        print( "validation : " , iL)
        from numbers import Number
        if isinstance(iL, Number):
            self.valLossE.append(iL)
        elif len(iL)==0:
            self.valLossE.append(iL[0])
        else:
            self.valLossE.append(iL[1])
            self.valLossM.append(iL[2])

    def toDict(self):
        d = dict(
            valLossE = self.valLossE,
            valLossM = self.valLossM,
            )
        return d

    def fromDict(self, d):
        self.valLossE = d.get('valLossE', [] )
        self.valLossM = d.get('valLossM', [] )

            
class StopAtNan(keras.callbacks.Callback):
    def __init__(self, trainer ):
        super(StopAtNan, self).__init__()
        self.trainer = trainer
    def on_batch_end(self, batch, logs):
        if np.isnan(logs['loss']):
            self.model.stop_training = True
            print("NAN at batch ", batch)
            x,y = self.trainer.gen[batch]
            if np.any(np.isnan(x)):
                print(' nan in x')
            if np.any(np.isnan(y)):
                print(' nan in y')
            raise Exception
        
from collections import defaultdict
        
    
class LogLossNumSeen(keras.callbacks.Callback):
    scaled = False
    def __init__(self, ninterval=10e6, ):
        self.njetprocessed= 0
        self.ninterval = ninterval
        # if lossnames==():
        #     lossnames = [ 'loss' , 'outputE_loss' ,'outputM_loss' ]
        self.lossnames = dict(loss='loss') #lossnames
        #self.metrics = [ 'outputE__lgkLoss','outputM__lgkLoss' ]
        #self.lossD = dict( (ln,[]) for ln in lossnames+ ['njetprocessed']+self.metrics)
        self.lossD = None
        
    def on_epoch_end(self, ep, logs):
        self.lossD.setdefault('njetatepoch',[]).append(self.model.gen.num_seensofar)
        
    def on_batch_end(self, batch, logs):
        njetprocessed = self.model.gen.num_seensofar
        if self.njetprocessed < njetprocessed:
            self.njetprocessed = njetprocessed
            for k, lname in self.lossnames.items():
                self.lossD[k].append( logs[lname]) 
            for l in self.metrics:
                self.lossD.setdefault(l,[]).append( logs.get(l,9) ) 
            self.lossD['njetprocessed'].append(njetprocessed)
            self.njetprocessed += self.ninterval


    def setupTrainer(self,trainer):
        outLayersName = [l.name for l in trainer.net.layers if l.outbound_nodes==[] ]
        if 'outputE' in outLayersName:
            self.lossnames['E_loss'] = 'outputE_loss'
        if 'outputM' in outLayersName:
            self.lossnames['M_loss'] = 'outputM_loss'

        if len(outLayersName)==1:
            self.lossnames['E_loss'] = "loss"
            
        #     self.lossnames['E_loss'] = [ l.name+"_loss" for l in outLayers if 'outputE' in l.name][0]
        # except:
        #     self.lossnames['E_loss'] = [ "scale_output" for l in outLayers if 'scale_output' in l.name][0]
        # if 'r_m' in trainer.config.targets:
        #     self.lossnames['M_loss'] = [ l.name+"_loss" for l in outLayers if 'outputM' in l.name][0]
        # else:
        #     self.lossnames['E_loss'] = "loss"
        # self.metrics = [l.name+'lgkloss' for l in outLayers]
        self.metrics = []
        if self.lossD is None:
            self.lossD = dict( (ln,[]) for ln in [*self.lossnames]+ ['njetprocessed']+self.metrics)        
        else:
            # check if pre-existing output loss exists (ex: when copying over different models)
            # if so alias the existing loss entry
            print('AAAAAAAAAAAAAAAAAAAAAAA ')
            tmpD = dict()
            for k,v in self.lossD.items():
                if k in self.metrics: continue
                if 'outputE' in k:
                    tmpD[ 'E_loss' ] = v
                elif 'outputM' in k:
                    tmpD[ 'M_loss' ] = v
            self.lossD.update(**tmpD)
        
    def toDict(self):
        d = dict(self.lossD)
        d['ninterval'] = self.ninterval
        return d

    def fromDict(self, d):
        self.ninterval = d.pop('ninterval')
        self.lossD = d
        # temporary fix compatibility with old saved copies:
        for k,v in list(d.items()):
            if 'activation_' in k:
                d[k.replace('activation_', 'output')] = v
        l = d['njetprocessed']
        self.njetprocessed = l[-1] if len(l)>0 else 0
        
    def equalizeLengths(self):
        N = len(self.lossD['njetprocessed'])
        for k,l in self.lossD.items():
            if len(l)<N:
                self.lossD[k] = [l[0]]*(N-len(l))+l
        
                
    def draw(self, plotLosses=True, plotMetrics=False, toPlots=[], norm=False, ax=None, plotEpochsAt=-5, **args):
        from matplotlib import pyplot as plt
        ax= ax or plt.gca()
        ax.cla()
        x = self.lossD['njetprocessed']
        pList = list(toPlots)
        axOpt = dict(
            ylabel=args.pop('ylabel', 'loss'),
            xlabel=args.pop('xlabel', 'Num jet processed'),
            )
        if plotLosses: pList += self.lossnames.keys()
        if plotMetrics: pList+= self.metrics
        nx = len(x)
        for n in pList:
            a = np.array(self.lossD[n])
            startx = 0 if len(a)==nx else len(a)
            if norm : a -= a[-20:].mean()
            #if not self.scaled: a+=5 # to allow log plots
            ax.plot(x[-startx:], a, label=n, **args)
        ax.set_ylim([-5,2])
        self.vlines=[]
        for x in self.lossD['njetatepoch'][plotEpochsAt:]:
            self.vlines.append(ax.axvline(x, ls='--',linewidth=1, color='black') )
        ax.legend()
        ax.set(**axOpt)
        self.scaled=True


    def truncateLosses(self):
        for n in self.lossnames+['njetprocessed']:
            l = self.lossD[n]
            del( l[:len(l)//2] )


    
# from cyclicLR import CyclicLR, CyclicLR1C, CyclicAdam, CyclicNoOp



# #earlyStopLoss = EarlyStopGoodLoss(-1.9,)

# cyclNoop = CyclicNoOp()
# cycLR1C = CyclicLR1C(0.0001, 0.1, 100)
# cyclAdam =CyclicAdam()





## ***********************************************************************
## Custom input variable rescaler
## ***********************************************************************

def concatScaler(s1, s2):
    s = MeanStdScaler()
    if isinstance(s1, np.ndarray):
        s.mean = np.concatenate( [s1.mean, s2.mean])
        s.std = np.concatenate( [s1.std, s2.std])
    s.offset = 0
    return s

'''
Default dictionary to describe data and converted data ranges. 
'''
defaultRangeDict =  {
    "dataRangeLower" :  1.,
    "dataRangeUpper" : -1.,
    "convRangeLower" : -1.,
    "convRangeUpper" :  1.,
}

class VariableScaler:
    """Apply linear scaling to variables (features or targets) so they fit in ~ [-1, 1]. 
    The is to act on a variable X to scale it to (X-mean)/std  where mean & std are typically the mean and std of X (could be anything, ex median & IQR)
    
    This class performs actually the operation 
      (X-mean)/(std*scaleFactor) + offset
    so that mean,std are automatically calculated and scaleFactor, offset can be manually adjusted to better fit in ~[-1,1]. 
    
    X is an array representing several variable of shape (N_events, N_variables). 
    mean, std, scaleFactor, offset are array of shape (N_variables,)
    
    To adjust scaleFactor & offset, see plotAndDebugDNN.py the method histogramInputs(), showVariablesScaleFactors() & setScaleFactors()
    """

    paramValid=False
    def __init__(self, offset=np.array([0.]), scaleFactor=np.array([1.]), fromVars=None, fromRange=None):
        '''
        Default parameter settings for transformations. Added [x_min,x_max] projection on [v_min,v_max] such that
        f_trans(x) = v_min + (v_max-v_min)/(x_max-x_min) x (x-x_min) = offset + slope x (x-base) 
        '''
        self.std= np.array([1.])
        self.mean = np.array([0.])
        self.offset         = offset
        self.scaleFactor    = scaleFactor    
        self.rangeValid     = False         ### flags if numerical ranges are provided fro the data values and the transformed values
        self.dataRangeLower = 0.            ### minimum value (lower range boundary) for data range
        self.dataRangeUpper = 0.            ### maximum value (upper range boundary) for data range
        self.convRangeLower = 0.            ### minimum value (lower range boundary) for transformed (converted) data range
        self.convRangeUpper = 0.            ### maximum value (upper range boundary) for transfomred (converted) data range
        self.base           = 0.            ### base data value for conversion
        if fromVars is not None:
            self.setParamFromVars(fromVars)
        if fromRange is not None:
            self.setRangeFromDict(fromRange)

    def setSize(self, n):
        self.offset = np.zeros(n)
        self.scaleFactor = np.ones(n)

    def setParamFromVars(self,varList):
        self.scaleFactor = np.array([ v.scaleFactor for v in varList ] ,dtype=np.float32) 
        self.offset = np.array([ v.offset for v in varList ] ,dtype=np.float32) 
        self.paramValid = True
    
    def setRangeFromDict(self,rangeDict):
        if rangeDict["dataRangeLower"] < rangeDict["dataRangeUpper"] and rangeDict["convRangeLower"] < rangeDict["convRangeUpper"]:
            self.rangeValid     = True
            self.dataRangeLower = rangeDict["dataRangeLower"]
            self.dataRangeUpper = rangeDict["dataRangeUpper"]
            self.convRangeLower = rangeDict["convRangeLower"]
            self.convRangeUpper = rangeDict["convRangeUpper"]
            self.offset         = self.convRangeLower
            self.slope          = (self.convRangeUpper-self.convRangeLower)/(self.dataRangeUpper-self.dataRangeLower)
            self.baseValue      = self.dataRangeLower
            
    def setFromMeanStdErr(self,X):
        self.scaleFactor = 1/ X.std(0)
        self.offset      = - X.mean(0)*self.scaleFactor
        self.paramValid = True

    def scale(self, X):
        if self.paramValid:
            r = X*self.scaleFactor
            r += self.offset
            return r
        elif self.rangeValid:
            r =  self.offset
            r += self.slope*(X-self.base)
            return r
        else:
            return X

    def scale_inplace(self, X):
        if self.paramValid:
            # use in-place operations : attempt to minimize memory usage in case of big arrays (?) 
            X *= self.scaleFactor
            X +=self.offset
        elif self.rangeValid:
            X -= self.baseValue
            X *= self.slope
            X += self.offset          

    def full_scale_factor(self):
        return self.std*self.scaleFactor

    def full_offset(self): # actually the offset when unscaling !
        return self.mean - self.offset*self.full_scale_factor()

    def scale_parameters(self,i=None):
        # returns (a,b) such that  x_scaled = a*x+b
        if i is None:
            return (self.scaleFactor, self.offset )
        return (self.scaleFactor[i], self.offset[i] )
    
    def transform(self, X):
        return self.scale(X)


    def unscale(self,X):
        if self.paramValid:
            # use in-place operations : attempt to minimize memory usage in case of big arrays (?) 
            r= X - self.offset
            r /= self.scaleFactor
            return r
        elif self.rangeValid:
            r =  self.base
            r += (X-self.offset)/self.slope
            return r
        else:
            return X

    def unscale_inplace(self,X):
        if self.paramValid:
            X -=self.offset
            X /= self.scaleFactor
        elif self.rangeValid:
            X -= self.offset
            X /= self.slope
            X += self.base


    def save(self, f, name):
        if isinstance(f,str):            
            scaleParF = h5py.File(f, 'a')
        else:
            scaleParF = f
        if name in scaleParF:
            scaleParF[name][:]=np.stack([self.offset, self.scaleFactor])
        else:
            scaleParF.create_dataset(name, data=np.stack([self.offset, self.scaleFactor]) )
        if isinstance(f,str): scaleParF.close()

    def loadFromFile(self, f, name):
        if isinstance(f,str):            
            scaleParF = h5py.File(f, 'r')
        else:
            scaleParF = f
        self.offset ,self.scaleFactor = scaleParF[name][:]
            
        if isinstance(f,str): scaleParF.close()
        self.paramValid = True
        

MeanStdScaler = VariableScaler

class NoScaler(VariableScaler):
    def scale_inplace(self, X):
        pass
    def scale(self, X):
        return  X
    def unscale(self,X):
        return X
    def unscale_inplace(self,X):
        pass
    

class MedianIQRScaler(VariableScaler):
    pass
    
class MinMaxScaler(VariableScaler):
    pass




## ***********************************************************************
## Custm Layers
## ***********************************************************************

class AddConstants(layers.Layer):
    
    def __init__(self, constants, **kwargs):
        super(AddConstants, self).__init__(**kwargs)
        self.constant0 = constants

    def build(self, input_shape):
        def ff(shape,dtype=None):
            return K.constant(self.constant0)
        self.w = self.add_weight(name='const',shape=( len(self.constant0), ),
                                 initializer= ff, 
                                 trainable=False)

        
    def call(self, inputs):
        return tf.add(inputs, self.w)

    def get_config(self):
        config = super(AddConstants, self).get_config()
        config.update({'constant0': list(self.constant0)})
        return config            


class AnnotatedLayer(layers.Layer):
    
    def __init__(self, dim, centers, sigmas2, **kwargs):
        super(AnnotatedLayer, self).__init__(**kwargs)
        self.centers = centers
        self.sigmas2  = sigmas2
        self.dim = dim
        #print("UUUUUUUUUUU", sigmas2, self.sigmas2)

    def build(self, input_shape):
        self.c_w = tf.constant( self.centers, shape = ( len(self.centers),) , dtype='float32')
        self.w_w = tf.constant( self.sigmas2, shape = ( len(self.centers),) , dtype='float32')
        
        #self.z = self.add_weight(name='zzz',shape=( len(self.sigmas2), ), initializer= lambda s,dtype: tf.constant(np.array([-0.5]),shape=s,dtype='float32'), trainable=False)

    @tf.function
    def call(self, inputs):
        anotatedF = inputs[:,self.dim:self.dim+1]
        n = tf.subtract(tf.tile(anotatedF, [1,len(self.sigmas2)]) , self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )-0.5
        return tf.concat( [annot, anotatedF ],axis=1)
    
    def get_config(self):
        config = super(AnnotatedLayer, self).get_config()
        config.update( dict(centers= self.centers,
                            sigmas2 = self.sigmas2,
                            dim = self.dim,)        
        )
        return config            

    
    def test(self, entry):
        tf = np
        n = tf.subtract(entry, self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )-0.5
        return annot


class OneMinus(layers.Layer):
    def __init__(self,   **kwargs):
        super(OneMinus, self).__init__(**kwargs)
    @tf.function
    def call(self, inputs):
        return 1.-inputs
    
class GausAnnotation(layers.Layer):
    
    def __init__(self, dim, centers, sigmas2, includeInput=False, offset=-0.5,  **kwargs):
        super(GausAnnotation, self).__init__(**kwargs)
        self.centers = centers
        self.sigmas2  = sigmas2
        self.dim = dim
        self.includeInput = includeInput
        self.offset = offset
        #print("UUUUUUUUUUU", sigmas2, self.sigmas2)

    def build(self, input_shape):
        self.c_w = tf.constant( self.centers, shape = ( len(self.centers),) , dtype='float32')
        self.w_w = tf.constant( self.sigmas2, shape = ( len(self.centers),) , dtype='float32')
        

    @tf.function
    def call(self, inputs):
        entry = tf.tile( inputs[:,self.dim:self.dim+1], [1,len(self.sigmas2)] )
        n = tf.subtract(entry, self.c_w)
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        if self.includeInput:
            return tf.concat( [annot, inputs[:,self.dim:self.dim+1] ], 1)
        return annot
    
    def get_config(self):
        config = super(GausAnnotation, self).get_config()
        config.update( dict(centers= self.centers,
                            sigmas2 = self.sigmas2,
                            dim = self.dim,
                            includeInput = self.includeInput,
                            offset = self.offset,

                            )        
        )
        return config            

    
    def test(self, entry):
        tf = np
        n = tf.subtract(entry, self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        return annot
    
class GausAnnotationSym(GausAnnotation):
    @tf.function
    def call(self, inputs):
        entry = tf.tile( tf.abs(inputs[:,self.dim:self.dim+1]), [1,len(self.sigmas2)] )
        n = tf.subtract(entry, self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        if self.includeInput:
            return tf.concat([annot, inputs[:,self.dim:self.dim+1]] , 1)
        return annot

    def test(self, entry):
        tf = np
        n = tf.subtract(tf.abs(entry), self.c_w) 
        annot = tf.exp( -0.5*n*n/self.w_w  )+self.offset
        return annot


class GateAnnotationSym(GausAnnotation):
    @tf.function
    def call(self, inputs):
        entry = tf.tile( inputs[:,self.dim:self.dim+1], [1,len(self.sigmas2)] )
        annot = smoothgate( entry, self.c_w, self.w_w )
        if self.includeInput:
            return tf.concat([annot, inputs[:,self.dim:self.dim+1]] , 1)
        return annot

    
class ExtractN(layers.Layer):
    def __init__(self, pos, **args):
        super(ExtractN,self).__init__(**args)
        self.pos = pos


    @tf.function
    def call(self, inputs):
        N,n0 = self.pos
        return inputs[:,n0:n0+N]

    def get_config(self):
        config = super(ExtractN, self).get_config()
        config.update( pos=self.pos )                
        return config            

class ExcludeIndices(layers.Layer):
    def __init__(self, exclude, **args):
        super(ExcludeIndices,self).__init__(**args)
        self.exclude = exclude

    def build(self, input_shape):
        self.include = [ i for i in range(input_shape[1]) if i not in self.exclude]


    @tf.function
    def call(self, inputs):
        l = [ inputs[:,i:i+1] for i in self.include ]
        return tf.concat( l , 1 ) 

    def get_config(self):
        config = super(ExcludeIndices, self).get_config()
        config.update( exclude=self.exclude )        
        
        return config            
        
class TrapezoidAnnotation(layers.Layer):
    
    def __init__(self, dim, trapezes, **kwargs):
        super(TrapezoidAnnotation, self).__init__(**kwargs)
        self.trapezes = trapezes
        self.dim = dim
        #print("UUUUUUUUUUU", sigmas2, self.sigmas2)

    def build(self, input_shape):
        self.t_mins    = tf.constant( self.trapezes[0], shape = ( len(self.trapezes[0]),) , dtype='float32')
        self.t_maxs    = tf.constant( self.trapezes[1], shape = ( len(self.trapezes[1]),) , dtype='float32')
        self.t_deltas  = tf.constant( self.trapezes[2], shape = ( len(self.trapezes[2]),) , dtype='float32')
        #print( self.trapezes[2] )
        if not isinstance(self.trapezes[2], np.ndarray ):
            t2 = np.array(self.trapezes[2])
        else: t2 = self.trapezes[2]
        self.t_deltas_inv  = tf.constant( 1./t2, shape = ( len(self.trapezes[2]),) , dtype='float32')
        
        
        #self.z = self.add_weight(name='zzz',shape=( len(self.sigmas2), ), initializer= lambda s,dtype: tf.constant(np.array([-0.5]),shape=s,dtype='float32'), trainable=False)

    @tf.function
    def call(self, inputs):
        entry = tf.tile( inputs[:,self.dim:self.dim+1], [1,len(self.t_mins)] )
        out  = tf.where( entry < self.t_mins-self.t_deltas, 0., 1 )
        out  *= tf.where( entry < self.t_mins, (entry-self.t_mins+self.t_deltas)*self.t_deltas_inv, 1)
        #tf.print(  " out=", out ) 
        #out *= tf.where( entry >= self.t_mins+self.t_deltas, (entry-self.t_mins)*self.t_delta_inv,1)
        out  = tf.where( entry >= self.t_maxs, (self.t_maxs+self.t_deltas-entry)*self.t_deltas_inv,out)
        out  *= tf.where( entry > self.t_maxs+self.t_deltas, 0., 1)
        out -= 0.5
                                  
        return out
    
    def get_config(self):
        config = super(TrapezoidAnnotation, self).get_config()
        config.update( dict(trapezes = self.trapezes,
                            dim = self.dim,)        

        )
        return config            
    
    def test(self, entry):
        tf = np
        out  = tf.where( entry < self.t_mins-self.t_deltas, 0., 1 )
        out  *= tf.where( entry < self.t_mins, (entry-self.t_mins+self.t_deltas)*self.t_deltas_inv, 1)
        #tf.print(  " out=", out ) 
        #out *= tf.where( entry >= self.t_mins+self.t_deltas, (entry-self.t_mins)*self.t_delta_inv,1)
        out  = tf.where( entry >= self.t_maxs, (self.t_maxs+self.t_deltas-entry)*self.t_deltas_inv,out)
        out  *= tf.where( entry > self.t_maxs+self.t_deltas, 0., 1)


        out -= 0.5

        return out


class MinMaxLinearAnnot(layers.Layer):
    
    def __init__(self, dim, thresholds, **kwargs):
        super(MinMaxLinearAnnot, self).__init__(**kwargs)
        self.thresholds= thresholds
        self.dim = dim
        #print("UUUUUUUUUUU", )

    def build(self, input_shape):

        thresholds = self.thresholds #self.trainer.scaleInputValues(self.vname, self.thresholds)
        self.t_minT    = tf.constant( (thresholds[0], -1.e10), shape = ( 2,) , dtype='float32')
        self.t_maxT    = tf.constant( (1e10, thresholds[1] ), shape = ( 2,) , dtype='float32')

    @tf.function
    def call(self, inputs):
        entry = tf.tile( inputs[:,self.dim:self.dim+1], [1,2] )
          
        out  = tf.where( entry < self.t_minT, entry-self.t_minT[0], 0.  )
        out  = tf.where( entry > self.t_maxT, entry-self.t_maxT[1], out )
                                  
        return out
    
    def get_config(self):
        config = super(MinMaxLinearAnnot, self).get_config()
        config.update( dict(thresholds = self.thresholds,
                            dim = self.dim,)                               
                      )
        return config            
    
    def test(self, entry):
        tf = np

        out  = tf.where( entry < self.t_minT, entry-self.t_minT[0], 0.  )
        out  = tf.where( entry > self.t_maxT, entry-self.t_maxT[1], out )

        return out


class EtaLinC(layers.Layer):
    def __init__(self, ntargets, etaind, etaT, rlow, scaleF, **args):
        super(EtaLinC,self).__init__(**args)
        self.ntargets = ntargets
        self.etaind = etaind
        self.otherPar = dict(etaT=etaT,rlow=rlow,scaleF=scaleF)
        self.coeffs = self.linCoeff( etaT, rlow, scaleF)
        
    # def build(self, input_shape):
    #     n = self.ntargets
    #     c = (1,) if n==1 else (1,0.)
    #     self.corr0    = tf.constant( c, shape = ( n,) , dtype='float32')


    @tf.function
    def call(self, inputs):
        a,b,t = self.coeffs
        eta = tf.abs(inputs[:,self.etaind:self.etaind+1])
        return tf.where( eta > t, a*eta+b,1  ) 

    def get_config(self):
        config = super(EtaLinC, self).get_config()
        config.update( dict( ntargets = self.ntargets,
                             etaind = self.etaind,
                             ) )
        config.update( **self.otherPar)
        return config            


    def linCoeff(self, etaT, rlow, etaScale):
        ymax = rlow # (1./rlow)
        y0, x0 = ymax , 4.3/etaScale
        y1, x1 = 1, etaT/etaScale
        a = ( y0 -y1) / (x0-x1)
        b = y0 - a *x0
        return a,b , x1
    
    def test(self, inputs, scale=1.):
        tf = np
        a,b,t = self.coeffs
        eta = tf.abs(inputs[:,self.etaind:self.etaind+1])
        return tf.where( eta > t, a*eta+b,1  ) 



class ScaleOutput(layers.Layer):
    def __init__(self, a, b, **args):
        super(ScaleOutput,self).__init__(**args)
        self.a = a
        self.b = b
        
    def build(self, input_shape):
        pass

    @tf.function
    def call(self, inputs):
        out, scaleF = inputs
        scaled_out = scaleF*out+self.b/self.a*(scaleF-1)
        #scaled_out[:,1].assign( out[:,1] )
        #tf.scatter_update( [1], out[:,1] )
        return tf.stack( [scaled_out[:,0] , out[:,1] ] ,1)

    def get_config(self):
        config = super(ScaleOutput, self).get_config()
        config.update( dict( a= self.a,
                             b = self.b,
                             ))
        return config            


    def test(self, out, scaleF ):
        return scaleF*out+self.b/self.a*(scaleF-1)


class AddJES(layers.Layer):
    def __init__(self,  **args):
        super(AddJES,self).__init__(**args)
        
    def build(self, input_shape):
        pass

    @tf.function
    def call(self, inputs):
        out, JES = inputs
        return out + tf.stack( [ JES , tf.zeros_like(JES)], 1 )

    
    
class DuplicateEntry(layers.Layer):
    def __init__(self, i, N, **args):
        super(DuplicateEntry,self).__init__(**args)
        self.i = i
        self.N = N

    @tf.function
    def call(self, inputs):
        if self.N==1:
            return inputs[:,self.i:self.i+1]
        return tf.stack( [inputs[:,self.i] ]*self.N ,1)

    def get_config(self):
        config = super(DuplicateEntry, self).get_config()
        config.update( dict( i= self.i,
                             N = self.N,
                             ))
        return config            
        
    
def reset_regularizer(model, v, regType='all', layers=None):

    if regType=='all':regTypes = ['kernel_regularizer','bias_regularizer']
    elif isinstance(regType,str): regTypes=[regType]
    else: regTypes=regType  # assuming regType is a list
    if layers is None : layers = model.layers
    else: layers = [ model.get_layer(lname) for lname in layers ]

    for l in layers:
        if isinstance(l, tf.keras.Model):
            reset_regularizer(l, v, regTypes)
            continue
        for regType in regTypes:
            reg=getattr( l, regType, None)
            if reg is None:continue            
            reg.l2[()] = v
            



def show_regularizer(model, prefix='' ):
    regTypes = ['kernel_regularizer','bias_regularizer']
    for l in model.layers:
        if isinstance(l, tf.keras.Model):
            show_regularizer(l, prefix+'--')
            continue
        for regType in regTypes:            
            reg=getattr( l, regType, None)
            if reg is None:continue
            print(prefix, l.name, regType, reg.l2)


def findDenseAncestorLayers(model, initname='outputE'):
    """Returns the Dense layers inside model which are ancestors of the layer named initname
      returns a list of layers.
      Reccursively inspect all sub-models inside model
    """
    layers=[]
    targetL=None
    # inspect sub-models and look for the layers named initname
    for l in model.layers:
        if isinstance(l, tf.keras.Model):
            layers += findDenseAncestorLayers(l,initname)
        elif l.name == initname:
            targetL = l
            break

    if targetL is None:
        # return what we have found in sub-models (can be [])
        return layers

    Dense = keras.layers.Dense    
    ancestors = dict()
    def _recL(l):
        if 'dense' in l.name:
            ancestors[l.name] = l
        for n in l.inbound_nodes:
            for ltuple in n.iterate_inbound():
                # ltuple is in the form (layer, ..,..., tensor)
                _recL(ltuple[0])
    _recL(targetL)
    
    layers += [ancestors[n] for n in sorted(ancestors.keys()) ]
    return layers


def printLayerNorms(model, ancestorsOf=None):
    layerL = model.layers if ancestorsOf is None else findDenseAncestorLayers(model,ancestorsOf)
    for li,layer in enumerate(layerL):

        if 'dense' in layer.name:
            a=layer.get_weights()[0]
            b=layer.get_weights()[1]
            nL = [np.linalg.norm(a[:,i]) for i in range(a.shape[1])]
            print( '\n{} layer {} w (max,min )norms =({:.2f},{:.4f})  bias (max,min)=({:.2f},{:.4f}) '.format(li, layer.name,
                max(nL), min(nL),  max(b), min(b)) )
    


def copyWeights(src, dest, nLayer=-1, layerFilter=lambda l:False, verbose=True):
    """Copy weights between 2 keras models : src to dest  """
    
    for lcount, l in enumerate(dest.layers):
        if lcount==nLayer: break
        if layerFilter(l): continue
        if l.get_weights() == []:
            continue
        try:
            lo = src.get_layer( l.name )
        except:
            print ("WARNING  : weight layer ",l.name, l.output.shape, " not found in source !")
            continue
        if verbose:
            print( 'this layer ',l.name, l.output.shape,  '   from  ', lo.name, lo.output.shape)
        l.set_weights(lo.get_weights())
