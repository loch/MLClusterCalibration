/// This file is a template of a c++ source code.
///  It is supposed to be filled automatically from the treeFlattener.py script
///  according to the variables existing in the input TTrees.



$vecInput 
/// Helper dedicated to read jet variable from a single JZ slice
struct JetReader {
  JetReader() :  $varInit  {}

  void init(TTree * tree) {
    reader.SetTree(tree);
  }
  TTreeReader reader;
  // in TTreeReader vars
  // TTreaderValue<Float_t> v(reader, "v");
  $declInVar

  float sampleWFactor=1;
};


/// Flattener run the event loop over input events and write out flattened ntuples
///  Holds a vector<JetReader> each of them corresponding to 1 JZ slice
///  During the event loop the event to be written out is read from one JetReader chosen randomly (but proportionnaly to the number of event in each slice)
struct Flattener {
  Flattener() {}
  void cpp_run(  ){
    //init(); done in python
    initOutTree( );
    int njet = 0;
    int nevent = 0;
    
    neventPerOutFile = neventPerOutFile < 0 ? maxEvents*3 : neventPerOutFile;
    
    while (readNext()) { // read randomly from  one of the input reader (each corresponding to 1 JZ slice)
      //std::cout << "  nevent  "<< nevent << "  --> "<< refVec.GetSize() << "   njet="<< njet << std::endl;
      //std::cout << "  nevent  "<< std::endl;

      TTreeReaderArray<float> * refVec_ptr=0;
#ifdef VECTOR_INPUT
      // prepare a pointer to a reference vector in case one of the scalar var needs it
      refVec_ptr = & currentReader->$ref;
      TTreeReaderArray<float> & refVec = *refVec_ptr;
#endif

      $eventLevelCut
      // scalar copy code
      // out_v = *v;
      if( true ){ // we set to false when debugging
      $scalarCopy
	}		    
      // vector copy code
#ifdef VECTOR_INPUT
      size_t Nvalues__ = refVec.GetSize();
      $resetOutVect 
      for(size_t i=0; i< Nvalues__ ;i++){
	// out_v = v[i]
	if(true){// we set to false when debugging
	$vectorCopy
	  if( m_outputFlat ) outTree->Fill();
	}
      }
      njet+=Nvalues__;
      //std::cout<< " nvalues__ "<< Nvalues__ << "   "<< njet  <<std::endl;
      if( ! m_outputFlat ) outTree->Fill();


#else
      njet++;
      outTree->Fill();
#endif
      nevent++;
      if( njet>=neventPerOutFile) {
	initOutTree();
	njet=0;
	for(size_t i=0;i< nreader; i++) std::cout<< " " <<readerTestN[i];
	std::cout<< std::endl;
	for(size_t i=0;i< nreader; i++) readerTestN[i]=0;
      }
      if( (nevent %1000000)==0) {std::cout<< "nevent processed "<< nevent<< std::endl;}
      if(nevent == maxEvents) break;
      //
    }
    std::cout<< " writing file "<< std::endl;
    outTree->Write();
    outFile->Close();
  }

  void init() {
    resetCumulDist();
    nreader = jetreaders.size();
    readerRemainingEv.resize( nreader);
    for(size_t i=0;i< nreader; i++){ readerRemainingEv[i] = jetreaders[i]->reader.GetTree()->GetEntries(); }
    readerTestN.resize(nreader);
    for(size_t i=0;i< nreader; i++) readerTestN[i]=0;
    buildReaderIndices();
  }

  
  bool resetCumulDist(){
    nreader = jetreaders.size();
    size_t evtPerReader = maxEvents/nreader;
    cumulDist.resize(nreader);
    int tot = 0;
    for(size_t i=0;i< nreader; i++){
      int nreaderTot = jetreaders[i]->reader.GetTree()->GetEntries();
      nreaderTot = nreaderTot > evtPerReader ? evtPerReader : nreaderTot;
      if(jetreaders[i]->reader.GetEntryStatus()  != TTreeReader::kEntryBeyondEnd)
	tot+= nreaderTot - jetreaders[i]->reader.GetCurrentEntry();
      cumulDist[i] = tot;
    }
    for(float &c :cumulDist) c /= tot;
    return (tot>0);
  }
  
  bool readNext(){    
    int ireader = rndReaderIndex();
    //std::cout << " reading from "<< ireader << std::endl;
    currentReader =  jetreaders[ireader];
    if(jetreaders[ireader]->reader.Next()) return true;
    bool remain = resetCumulDist();
    if(remain) return readNext();
    return false;    
  }

  int rndReaderIndex(){

    float r = rnd.Rndm();
    int i = std::lower_bound(cumulDist.begin(), cumulDist.end(), r) - cumulDist.begin() ;
    return i ;
  }
  


  
  void initOutTree() {
    if ( outTree ){
      std::cout << " terminating tree in "<< outFile->GetName() << std::endl;
      outTree->SetFileNumber(currentOutIndex);
      outFile = outTree->ChangeFile(outFile);
      currentOutIndex++;
    } else {
      std::cout << " creating "<<outFileName+".root" << std::endl;
      outFile = new TFile(outFileName+".root", "recreate");
      outTree = new TTree(jetreaders[0]->reader.GetTree()->GetName() , "jet tree");

      //create branch
      // outTree->Branch("v",&out_v , "v/F");
      $createBranch
    }
    
    
  }


  bool buildReaderIndices(){
    nreader = jetreaders.size();
    std::vector<float> freq(nreader);
    size_t min = (size_t) 1000*1000*1000*10;
    int nonExhaustedN=0;
    for(size_t i=0;i< nreader; i++){
      int nreaderTot = readerRemainingEv[i];
      if(nreaderTot>0) {
	if ( nreaderTot<min) min = nreaderTot;
	nonExhaustedN++;
      }
      freq[i] = nreaderTot;
    }
    if (nonExhaustedN==0) return false;
    for(size_t i=0;i< nreader; i++) freq[i] /= (float) min;
    std::cout << " min = "<< min << std::endl;
    readerIndices.clear();
    for(size_t i=0;i< nreader; i++) {
      if( readerRemainingEv[i]==0 ) continue;
      std::cout << i << " --> freq= "<< std::lrint( freq[i] ) << "  -- "<< freq[i] << std::endl;
      for(int j =0; j< std::lrint( freq[i] );j++) readerIndices.push_back(i);
    }

    
    return true;
  }
  
  bool readNext2(){    
    int ireader = nextReader();
    //std::cout << " reading from "<< ireader << std::endl;
    currentReader =  jetreaders[ireader];
    jetreaders[ireader]->reader.Next();
    readerRemainingEv[ireader]--;
    readerTestN[ireader]++;
    if( readerRemainingEv[ireader]==0){
      return buildReaderIndices();
    }
    return true;    
  }

  int nextReader(){
    int r = readerIndices[ currentReaderI % readerIndices.size() ];
    currentReaderI++;
    return r;
  }

  
  size_t maxEvents = -1 ;
  int neventPerOutFile = -1;
  
  
  std::vector<JetReader*> jetreaders;
  std::vector<float> cumulDist;
  size_t nreader=0;
  TRandom2 rnd;
  JetReader *currentReader;
  
  // out branch vars
  // float out_v;
  $declOutVar
  
  TString  outFileName;
  TTree * outTree = nullptr;
  TFile * outFile = nullptr;
  int currentOutIndex = 0;


  //
  std::vector<int> readerIndices;
  std::vector<int> readerRemainingEv;
  int currentReaderI=0;

  std::vector<int> readerTestN;

  bool m_outputFlat = $outputFlat ;
};
