param([boolean]$force=$false,[string]$thispath="MLTopoCluster\MLClusterCalibration")

if ( $force -eq $false ) { 
    if ( -not (Test-Path env:PYTHONPATH) ) { 
        $env:PYTHONPATH = $env:HOMEDRIVE+$env:HOMEPATH+'\'+$thispath
        Write-Output "[setup] PYTHONPATH = $env:PYTHONPATH [set]"
    } else {
        $env:PYTHONPATH += ';'+$env:HOMEDRIVE+$env:HOMEPATH+'\'+$thispath
        Write-Output "[setup] PYTHONPATH = $env:PYTHONPATH [append]"
    }
} else {
    $env:PYTHONPATH = $env:HOMEDRIVE+$env:HOMEPATH+'\'+$thispath
    Write-Output "[setup] PYTHONPATH = $env:PYTHONPATH [overwrite]"
}