from ClusterCalibDNN.HistoAnalysis.BinnedArrays import BinnedArrays
from ClusterCalibDNN.HistoAnalysis import BinnedArraysGraphics as bag 

# Include a bunch of functions useful for analysis of response histograms :
exec(open('responseAnalysis.py').read())

            
# ********************************
# style functions
# They return a dictionnary of style (color, labels, line styles,etc...) according to the BinnedArray they receive.
# They are called within bag.readBAFromFile() for each read BinnedArray
def atlasStyle(ba):
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    elif 'cal' in ba.name:
        return dict(color='red', label='HAD calib')
    elif 'dnn' in ba.name:
        return dict(color='purple', label='DNN')
        
    
def atlasStyleWithJES(ba):
    if 'uncal' in ba.name:
        return dict(color='black', label='no calib')
    elif 'jes' in ba.name:        
        return dict(color='blue', label='JES')
    else:
        return dict(color='red', label='JES+GSC')

def describeChargeBin(axis,i):
    return [ 'charged $\pi$', 'neutral $\pi$'][i]
# ********************************



inputTag = 'Clusters'

gs = bag.GraphicSession(

    #bpsTag ='Eetacharge',
    bpsTag ='Eeta',
    #bpsTag ='ELMoEabseta',
    plotDir = inputTag,

) # 


# Read histos containers :
gs.readBAFromFile('CalibClusterTree_atlas'+gs.bpsTag+'_respBH0.h5', styleFunc=atlasStyle)

#gs.readBAFromFile('clusterCalibMDNdd10e1a62cfr_eNoAnnotT0T1uoN500_directrespBH.h5', tag='0',
#                  color='purple')        


# dirty hack to assign a 'hdesc' to each histo container...
for k, bh in gs.__dict__.items():
    if not isinstance(bh, BinnedArrays): continue        
    bh.hdesc = k.split('_')[0]


gs.bps = gs.allBA[0].bps
import types
for bh in gs.allBA:
    a = bh.bps.axis[-1]
    if a.name=='charge':
        a.describeBin = types.MethodType(describeChargeBin, a)
plt.ion()


