# this file is meant to be included from the main ClusterCalibDNN.py script 
import ClusterCalibDNN.ModelDefinitions as mod
from ClusterCalibDNN.Variables import Variable,VarGenExpr2

###########
# Block 1 #
###########
##
## Defining direct or derived variables, their ranges and possible transformations 

clustVar = [
    Variable("clusterE"              , title="$E_{EM}$"                 , h_range=(0,2000), array_transfo=[ "log(a)"  ], array_revtransfo=["exp(a)"]),
    Variable("clusterEta"            , title='$y_{clus}^{EM}$'          , h_range=(-4.85,4.85) , pre_filter= lambda eta : abs(eta)<0.8),
    Variable("clusterECalib"         , title="$E_{LCW}$"                , h_range=(0,2000), ),
    Variable("cluster_CENTER_LAMBDA" , title="$\langle\lambda\rangle$"  , h_range=(0,2000) ),
    Variable("cluster_SECOND_LAMBDA" , title="$#LT\lambda^2#GT$"        , h_range=(0,250000) ),
    Variable("cluster_LATERAL"       , title="$\langlem^2_{lat}\rangle$", h_range=(0,1.01) ),
    Variable("cluster_ENG_FRAC_EM"   , title="$f_{emc}$"                , h_range=(0,1.01) ),
    Variable("cluster_FIRST_ENG_DENS", title="$rho_{clus}$"             , h_range=(0,0.0014), array_transfo=[ "where(a<=0.,c_01*c_0e5,a)","log(a)"  ], array_revtransfo=["exp(a)"]),
    Variable("cluster_SIGNIFICANCE"  , title="$\zeta^{EM}_{clus}$"      , h_range=(0,30) ),
    Variable("cluster_PTD"           , title="$p_{T}D$"                 , h_range=(0,1.01) ),
    Variable("cluster_LONGITUDINAL"  , title="$<m^2_{long}>$"           , h_range=(0,1.01) ),
    Variable("cluster_SECOND_TIME"   , title="$\sigma^{2}_{t}$"         , h_range=(-150.,250.) ),
    Variable("cluster_ENG_CALIB_TOT" , title="$E^{dep}_{clus}$"         , h_range=(0,2000) ),
    Variable("cluster_HAD_WEIGHT"    , title="$HAD_w$"                  , h_range=(0,2000) ),
    Variable("avgMu"                 , title="$\langle\mu\rangle$"      , h_range=(0,100) ),
    Variable("nPrimVtx"              , title="$N_{PV}$"                 , h_range=(0,100) ),
    ]

###########
# Block 2 #
###########

##
## Define network 

class HeadIdCoreT0ST(mod.ModelDefiner, mod.Identity,  mod.DenseCoreT0, mod.Layer1Tail):
    N = 200
    last_activation = "tanhp"

###########
# Block 3 #
###########
##
## Configure training input data, input variables and target(s)

conf.update(
    nnName = "clusterCalib" ,               # network/project name
    inputFiles = 'JZ.topo-cluster.root',    # input data file
    treeName = "ClusterTree" ,              # name of ROOT tree in input data file
    inputDir = "./data",                    # relative or absolute path to input file directiory
    features = ["clusterEta",               # input: cluster rapidity          
                "clusterE",                 # input: cluster EM-scale energy
                "cluster_CENTER_LAMBDA",    # input: cluster depth in calorimeter
                "cluster_LONGITUDINAL",     # input: normalized longitudinal compactness measure
                "cluster_LATERAL",          # input: normalized lateral compactness measure
                "cluster_ENG_FRAC_EM",      # input: fraction of energy in EM calorimeter
                "cluster_FIRST_ENG_DENS",   # input: first moment of cell energy densities
                "cluster_SIGNIFICANCE",     # input: overall cluster signal significance
                "cluster_PTD",              # input: cluster fragmentation function
                "cluster_SECOND_TIME",      # input: second moment of cell time distribution
                "avgMu",                    # average number of pile-up collisions contributing to recorded event
                "nPrimVtx", ],              # number of primary vertices (hard scatter and pile-up) reconstructed for recorded event

    # ------------
    targets=['r_e', ],                      # prediction target
    # ------------
)

## set the reference variables for our sample
refV=Variable.referenceVars    

###########
# Block 4 #
###########
##
## N/A for cluster calibration - we do not add the jet energy resolution to the reference variables in this case 

def addJes():
    conf.features.append('jesR')
    conf.inputFriends = 'AntiKt4EMPFlowJetsJES_*.root'
    
###########
# Block 5 #
###########
##
## Set the truth (prediction aim). Note that the target r_e is an alias for r_raw_e or r_cal_e (if so configured)

Variable.aliasConfig.e_var.true = "cluster_ENG_CALIB_TOT"
def checkEandMVariants():                                      ### This function checks if the clusterE variable (the EM-scale energy)
    for vfull in ['clusterE', ]:                               ### is in the list of features. If it is found, it adds two responses to
        if vfull in conf.features:                             ### the list of reference variables in refV, first the EM response r_raw_e=E_EM/E_true
            refV['r_raw_e'].numerator = vfull                  ### (clusterE/cluster_ENG_CALIB_TOT) and then the LCW response r_cal_e=E_LCW/E_true
            refV['r_raw_e'].denom = "cluster_ENG_CALIB_TOT"    ### (clusterEHad/cluster_ENG_CALIB_TOT). r_cal_e is not used for training.
            refV['r_raw_e'].reset_depenencies()
            
            refV['r_cal_e'].numerator = vfull+'Had'
            refV['r_cal_e'].denom = "cluster_ENG_CALIB_TOT"
            refV['r_cal_e'].reverse_update=True
            refV['r_cal_e'].reset_depenencies()
            Variable.aliasConfig.e_var['e_reco'] = vfull
checkEandMVariants()

###########
# Block 6 #
###########
##
## Below we adapt the reference variables to our sample - adjust the scale factors and offsets of variable normalizations
## Check the normalized histo with :
##     exec(open('plotAndDebugDNN.py').read())
##     trainer.histogramInputs(formatted=True)
refV.clusterEta.setScaleParameters(sf=1.000, o=0.000 ) 
refV.clusterE.setScaleParameters(sf=0.25, o=-0.25 )
refV.cluster_CENTER_LAMBDA.setScaleParameters(sf=0.0008, o=-0.950 ) 
refV.cluster_LONGITUDINAL.setScaleParameters(sf=1.90, o=-0.900 ) 
refV.cluster_LATERAL.setScaleParameters(sf=1.800, o=-0.900 ) 
refV.cluster_FIRST_ENG_DENS.setScaleParameters(sf=0.16, o=2.5 )  
refV.cluster_SIGNIFICANCE.setScaleParameters(sf=0.02, o=-0.9 ) 
refV.cluster_ENG_FRAC_EM.setScaleParameters(sf=2.000, o=-0.900 ) 
refV.cluster_PTD.setScaleParameters(sf=2, o=-1.1 )
refV.cluster_SECOND_TIME.setScaleParameters(sf=0.005,o=-1.)
refV.avgMu.setScaleParameters(sf=0.02,o=-1.)
refV.nPrimVtx.setScaleParameters(sf=0.02,o=-1.)


def defaultSequenceCl(trainer):
    args = trainer.config.refitParams
    #trainer.config.callbacks+=[ utils.earlyStopLoss , keras.callbacks.TerminateOnNaN()]
    #if not trainer.gen.singleTarget: utils.earlyStopLoss.lossT = -5.5
    print("aaaaa0")
    if trainer.config.lossType=="MDNA":
        lossFunc = lambda t: utils.amdnLoss
    elif trainer.config.lossType=="MDN":
        lossFunc = lambda t: utils.truncmdnLoss(t)

        #loss = utils.mdnLoss if trainer.config.nPredictedParam else utils.lgkLoss(1e-3, 1e-3)
    trainer.refit( batch_size = 1000, epochs=1 , steps_per_epoch=30000, optimizer='rada', loss=lossFunc(None), **args)
    if trainer.net.stop_training:
        print("ERROR")
        return
    print("aaaaa1")
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-3), epochs=5, loss=lossFunc(None) , **args)

    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =25000, optimizer=radaOpt(1e-4), epochs=5, loss=lossFunc(None), **args )
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =35000,  optimizer=radaOpt(1e-4), nepoch=3 , loss=lossFunc(None)  , **args  )
    trainer.save_model()
    trainer.reweightAnomalousRow()
    trainer.refit( batch_size =15000,  optimizer=radaOpt(1e-4), nepoch=3 , loss=lossFunc(3)  , metrics=[], **args  )
    trainer.save_model()

