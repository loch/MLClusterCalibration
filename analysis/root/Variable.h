// -*- c++ -*-
#ifndef VARIABLE_H
#define VARIABLE_H
// -- c++ containers
#include <string>
#include <vector>
#include <map>
#include <tuple>
// -- c++ algorithms
#include <functional>
#include <algorithm>
// -- c++ stdlib
#include <cmath>
#include <cstdio>
#include <cassert>
// -- package includes
#include "Type.h"
// -- implementation
#include "Variable.icc"
#endif 