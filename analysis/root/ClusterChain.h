// -*- c++ -*-
#ifndef CLUSTERCHAIN_H
#define CLUSTERCHAIN_H

#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TIter.h"
#include "TObjArray.h"

#include <vector>
#include <string>

class ClusterChain {
private:
    TChain*     _chain    = { nullptr };     
    std::string _treeName = { "ClusterTree" } 
public:
    ClusterChain() { }
    ClusterChain(const std::vector<std::string>& fileNames,const std::string& treeName="ClusterTree") 
    : _treeName(treeName)
    {
        _chain = new TChain(_treeName.c_str()); 
        for ( const auto& fn : fileNames ) {
            _chain->Add(fn.c_str());
        }
    } 
    TChain*     ptr()                             { return _chain; }
    TFile*      filePtr()                         { return ptr()->GetFile(); }
    TTree*      treePtr()                         { return ptr()->GetTree(); }
    std::string treeName() const                  { return std::string(ptr()->GetName()); }
    std::string fileName() const                  { return std::string(ptr()->GetFile()->GetName()); }
    std::vector<std::string> allFileNames() const { 
        std::vector<std::string> lOfFiles;
        for ( auto iter(ptr()->GetListOfFiles()->begin()); iter!=ptr()->GetListOfFiles()->end(); iter++ ) {
            if ( *iter != nullptr ) { lOfFiles.push_back((*iter)->GetName()); }
        }
        return lOfFiles;
    }
    int         numberOfFiles()   const { return ptr()->GetListOfFiles()->GetEntries(); }
    long long   numberOfEntries() const { return _chain->GetEntries(); }
};
#endif