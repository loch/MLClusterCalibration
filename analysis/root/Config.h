// -*- c++ -*-
//
#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <vector>
#include <map>

#include <algorithm>

namespace Config {
  namespace Directives {
    namespace OS { 
      static const std::string unknown = { "__UNKNOWN__" }; 
      static const std::vector<std::string> defaults = {
#if defined(__unix)
        "__unix",
#endif
#if defined(__unix__)
        "__unix__",
#endif
#if defined(_WIN32)
        "_WIN32",
#endif
#if defined(_win32)
        "_win32",
#endif
#if defined(WIN32)
        "WIN32",
#endif
#if defined(_WIN64)
        "_WIN64",
#endif
#if defined(_win64)
        "_win64",
#endif
#if defined(WIN64)
        "WIN64",
#endif
        unknown
      };
      static std::vector<std::string> actual() {
        std::vector<std::string> al(defaults);
	std::vector<std::string>::iterator fiter(std::find(al.begin(),al.end(),unknown));
        while ( fiter != al.end() ) { fiter = al.erase(fiter); fiter = std::find(fiter,al.end(),unknown); }
        return al;
      } 
    } // Config::Directives::OS
  } // Config::Directives
  namespace Compiler {  
    namespace Version {
      static const std::map<long,std::string> tags = { 
	{ 201102L, "c++11" }, { 201402L, "c++14" }, { 201702L, "c++17" }, { 202002L, "c++20" }, { -1L, "c++" }
      };
#if defined(__cplusplus)
      static const long id = { __cplusplus };
#elif defined (__cpluscplus__)
      static const long id = { __cplusplus__ };
#else
      static const long id = { -1 };
#endif
      static bool isCplusplus11() { return id == 201102L; }
      static bool isCplusplus14() { return id == 201402L; }
      static bool isCplusplus17() { return id == 201702L; }
      static bool isCplusplus20() { return id == 202002L; }
      static bool isVersion(int vid) { 
	long rvid(-1); 
	switch ( vid ) { 
	case 11: rvid = 201102L; break;
	case 14: rvid = 201402L; break; 
	case 17: rvid = 201702L; break;
	case 20: rvid = 202002L; break; 
	default: break; 
	} 
	return rvid;
      }
      static const std::string& name() { return tags.at(id); }
    } // Config::Compiler::Version 
  } // Config::Compiler
} // Config

#if (defined(__unix) || defined(__unix__)) && !(defined(_WIN32) || defined(_win32) || defined(_WIN64) || defined(_win64) )
#define OS_LINUX 1
#endif
#if !(defined(__unix) || defined(__unix__)) && (defined(_WIN32) || defined(_win32) || defined(_WIN64) || defined(_win64) )
#define OS_WINDOWS 1
#endif
#endif
