// -*- c++ -*-
#ifndef AXIS_H
#define AXIS_H
// -- c++ containers
#include <string>
#include <vector>
#include <tuple>
#include <map>
// -- c++ functionals
#include <functional>
// -- c++ nuemricals
#include <limits>
// -- c++ stdlib
#include <cmath>
#include <cstdio>
// -- package includes
#include "Type.h"
#include "Variable.h"
// -- implementation
#include "Axis.icc"
#endif