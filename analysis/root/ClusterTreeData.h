// -*- c++ -*-

//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr  4 10:13:57 2022 by ROOT version 6.24/06
// from TTree ClusterTree/ClusterTree
// found on file: ../../../../MLClusterCalibration/data/data.04.04.2022/JZ1.topo-cluster.root
//////////////////////////////////////////////////////////

#ifndef ClusterTreeData_h
#define ClusterTreeData_h

#include "TChain.h"
#include "TLeaf.h"
#include "TBranch.h"
#include "TH1D.h"

#include "Utils.h"

#include <string>
#include <vector>
#include <map>
#include <tuple>
 
#include <cmath>
#include <cstdio>

// Header file for the classes stored in the TTree if any.

class ClusterTreeData {

  /////////////////
  // Public data //
  /////////////////

public:

  TChain*         fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  // -- event level data ----------------------------------------------------
  Int_t   runNumber                  = { 0  }; // run number
  Int_t   eventNumber                = { 0  }; // event number
  Float_t avgMu                      = { 0. }; // average number of interactions per bunch crossing
  Int_t   nPrimVtx                   = { 0  }; // number of primary vertices
  Int_t   nCluster 	                 = { 0  }; // number of clusters/event
  // -- reco jet level data -------------------------------------------------
  Float_t jetCalE                    = { 0. }; // [FINAL]       jet energy    Final jet calibration applied 
  Float_t jetCalPt                   = { 0. }; // [FINAL]       jet pT      
  Float_t jetCalEta 	               = { 0. }; // [FINAL]       jet rapidity
  Float_t jetCalPhi 	               = { 0. }; // [FINAL]       jet azimuth 
  Float_t jetRawE 	                 = { 0. }; // [CONSTITUENT] jet energy    This is the LC scale for these samples!   
  Float_t jetRawPt 	                 = { 0. }; // [CONSTITUENT] jet pT      
  Float_t jetRawEta                  = { 0. }; // [CONSTITUENT] jet rapidity
  Float_t jetRawPhi 	               = { 0. }; // [CONSTITUENT] jet azimuth 
  Int_t   jetNConst 	               = { 0  }; // number of jet constituents
  // -- truth jet level data ------------------------------------------------
  Float_t truthJetMatchRadius        = { 0. }; // [TRUTH]       matching distance to reco jet
  Float_t truthJetE 	               = { 0. }; // [TRUTH]       jet energy  
  Float_t truthJetPt 	               = { 0. }; // [TRUTH]       jet pT      
  Float_t truthJetRap 	             = { 0. }; // [TRUTH]       jet rapidity
  Float_t truthJetPhi 	             = { 0. }; // [TRUTH]       jet azimuth 
  // -- cluster level data --------------------------------------------------
  Int_t   clusterIndex 	             = { 0  }; // index of cluster in event
  Int_t   cluster_nCells             = { 0  }; 
  Int_t   cluster_nCells_tot         = { 0  };
  // -- cluster kinematics data ---------------------------------------------
  Float_t clusterECalib              = { 0. }; // [LCW] cluster energy 			
  Float_t clusterPtCalib             = { 0. }; // [LCW] cluster pT			
  Float_t clusterEtaCalib            = { 0. }; // [LCW] cluster rapidity		
  Float_t clusterPhiCalib            = { 0. }; // [LCW] cluster azimuth			
  Float_t cluster_sumCellECalib      = { 0. }; // [LCW] cluster energy summed from cells
  Float_t cluster_fracECalib 	       = { 0. }; // [LCW] cluster fractional contribution to energy sum of all clusters in jet/particle 
  Float_t cluster_fracECalib_ref     = { 0. }; // [LCW] cluster fractional contribution to jet/particle energy 
  Float_t clusterE 		               = { 0. }; // [EM]  cluster energy 			
  Float_t clusterPt                  = { 0. }; // [EM]  cluster pT			
  Float_t clusterEta 		             = { 0. }; // [EM]  cluster rapidity		
  Float_t clusterPhi 		             = { 0. }; // [EM]  cluster azimuth			
  Float_t cluster_sumCellE 	         = { 0. }; // [EM]  cluster energy summed from cells
  Float_t cluster_fracE 	           = { 0. }; // [EM]  cluster fractional contribution to energy sum of all clusters in jet/particle 
  Float_t cluster_fracE_ref 	       = { 0. }; // [EM]  cluster fractional contribution to jet/particle energy 
  Float_t cluster_time 		           = { 0. }; // [EM]  cluster time 
  // -- cluster moment data -------------------------------------------------
  Float_t cluster_EM_PROBABILITY     = { 0. };
  Float_t cluster_HAD_WEIGHT         = { 0. };
  Float_t cluster_OOC_WEIGHT 	     = { 0. };
  Float_t cluster_DM_WEIGHT  	     = { 0. };
  Float_t cluster_ENG_CALIB_TOT      = { 0. };
  Float_t cluster_ENG_CALIB_OUT_T    = { 0. };
  Float_t cluster_ENG_CALIB_DEAD_TOT = { 0. };
  Float_t cluster_CENTER_MAG 	     = { 0. };
  Float_t cluster_FIRST_ENG_DENS     = { 0. };
  Float_t cluster_FIRST_PHI	     = { 0. };
  Float_t cluster_FIRST_ETA	     = { 0. };
  Float_t cluster_SECOND_R	     = { 0. };
  Float_t cluster_SECOND_LAMBDA      = { 0. };
  Float_t cluster_DELTA_PHI	     = { 0. };
  Float_t cluster_DELTA_THETA        = { 0. };
  Float_t cluster_DELTA_ALPHA 	     = { 0. };
  Float_t cluster_CENTER_X 	     = { 0. };
  Float_t cluster_CENTER_Y 	     = { 0. };
  Float_t cluster_CENTER_Z 	     = { 0. };
  Float_t cluster_CENTER_LAMBDA      = { 0. };
  Float_t cluster_LATERAL 	     = { 0. };
  Float_t cluster_LONGITUDINAL 	     = { 0. };
  Float_t cluster_ENG_FRAC_EM        = { 0. };
  Float_t cluster_ENG_FRAC_MAX 	     = { 0. };
  Float_t cluster_ENG_FRAC_CORE      = { 0. };
  Float_t cluster_SECOND_ENG_DENS    = { 0. };
  Float_t cluster_ISOLATION 	     = { 0. };
  Float_t cluster_ENG_BAD_CELLS      = { 0. };
  Float_t cluster_N_BAD_CELLS 	     = { 0. };
  Float_t cluster_N_BAD_CELLS_CORR   = { 0. };
  Float_t cluster_BAD_CELLS_CORR_E   = { 0. };
  Float_t cluster_BADLARQ_FRAC 	     = { 0. };
  Float_t cluster_ENG_POS 	     = { 0. };
  Float_t cluster_SIGNIFICANCE 	     = { 0. };
  Float_t cluster_CELL_SIGNIFICANCE  = { 0. };
  Float_t cluster_CELL_SIG_SAMPLING  = { 0. };
  Float_t cluster_AVG_LAR_Q 	     = { 0. };
  Float_t cluster_AVG_TILE_Q 	     = { 0. };
  Float_t cluster_ENG_BAD_HV_CELLS   = { 0. }; 
  Float_t cluster_N_BAD_HV_CELLS     = { 0. };
  Float_t cluster_PTD 		     = { 0. };
  Float_t cluster_MASS 		     = { 0. };
  Float_t cluster_SECOND_TIME 	     = { 0. };
  Float_t CalibratedE                = { 0. };
  Float_t ResponsePrediction         = { 0. };
  Float_t ResponseFluctuation        = { 0. };
					     
  // List of branches			     
  TBranch* b_runNumber                  = { nullptr };  //!	     
  TBranch* b_eventNumber                = { nullptr };  //!
  TBranch* b_avgMu                      = { nullptr };  //!
  TBranch* b_nPrimVtx                   = { nullptr };  //!
  TBranch* b_jetCalE                    = { nullptr };  //!
  TBranch* b_jetCalPt                   = { nullptr };  //!
  TBranch* b_jetCalEta                  = { nullptr };  //!
  TBranch* b_jetCalPhi                  = { nullptr };  //!
  TBranch* b_jetRawE                    = { nullptr };  //!
  TBranch* b_jetRawPt                   = { nullptr };  //!
  TBranch* b_jetRawEta                  = { nullptr };  //!
  TBranch* b_jetRawPhi                  = { nullptr };  //!
  TBranch* b_jetNConst                  = { nullptr };  //!
  TBranch* b_truthJetMatchRadius        = { nullptr };  //!
  TBranch* b_truthJetE                  = { nullptr };  //!
  TBranch* b_truthJetPt                 = { nullptr };  //!
  TBranch* b_truthJetRap                = { nullptr };  //!
  TBranch* b_truthJetPhi                = { nullptr };  //!
  TBranch* b_nCluster                   = { nullptr };  //!
  TBranch* b_clusterIndex               = { nullptr };  //!
  TBranch* b_cluster_nCells             = { nullptr };  //!
  TBranch* b_cluster_nCells_tot         = { nullptr };  //!
  TBranch* b_clusterECalib              = { nullptr };  //!
  TBranch* b_clusterPtCalib             = { nullptr };  //!
  TBranch* b_clusterEtaCalib            = { nullptr };  //!
  TBranch* b_clusterPhiCalib            = { nullptr };  //!
  TBranch* b_cluster_sumCellECAlib      = { nullptr };  //!
  TBranch* b_cluster_fracECalib         = { nullptr };  //!
  TBranch* b_cluster_fracECalib_ref     = { nullptr };  //!
  TBranch* b_clusterE                   = { nullptr };  //!
  TBranch* b_clusterPt                  = { nullptr };  //!
  TBranch* b_clusterEta                 = { nullptr };  //!
  TBranch* b_clusterPhi                 = { nullptr };  //!
  TBranch* b_cluster_sumCellE           = { nullptr };  //!
  TBranch* b_cluster_time               = { nullptr };  //!
  TBranch* b_cluster_fracE              = { nullptr };  //!
  TBranch* b_cluster_fracE_ref          = { nullptr };  //!
  TBranch* b_cluster_EM_PROBABILITY     = { nullptr };  //!
  TBranch* b_cluster_HAD_WEIGHT         = { nullptr };  //!
  TBranch* b_cluster_OOC_WEIGHT         = { nullptr };  //!
  TBranch* b_cluster_DM_WEIGHT          = { nullptr };  //!
  TBranch* b_cluster_ENG_CALIB_TOT      = { nullptr };  //!
  TBranch* b_cluster_ENG_CALIB_OUT_T    = { nullptr };  //!
  TBranch* b_cluster_ENG_CALIB_DEAD_TOT = { nullptr };  //!
  TBranch* b_cluster_CENTER_MAG         = { nullptr };  //!
  TBranch* b_cluster_FIRST_ENG_DENS     = { nullptr };  //!
  TBranch* b_cluster_FIRST_PHI          = { nullptr };  //!
  TBranch* b_cluster_FIRST_ETA          = { nullptr };  //!
  TBranch* b_cluster_SECOND_R           = { nullptr };  //!
  TBranch* b_cluster_SECOND_LAMBDA      = { nullptr };  //!
  TBranch* b_cluster_DELTA_PHI          = { nullptr };  //!
  TBranch* b_cluster_DELTA_THETA        = { nullptr };  //!
  TBranch* b_cluster_DELTA_ALPHA        = { nullptr };  //!
  TBranch* b_cluster_CENTER_X           = { nullptr };  //!
  TBranch* b_cluster_CENTER_Y           = { nullptr };  //!
  TBranch* b_cluster_CENTER_Z           = { nullptr };  //!
  TBranch* b_cluster_CENTER_LAMBDA      = { nullptr };  //!
  TBranch* b_cluster_LATERAL            = { nullptr };  //!
  TBranch* b_cluster_LONGITUDINAL       = { nullptr };  //!
  TBranch* b_cluster_ENG_FRAC_EM        = { nullptr };  //!
  TBranch* b_cluster_ENG_FRAC_MAX       = { nullptr };  //!
  TBranch* b_cluster_ENG_FRAC_CORE      = { nullptr };  //!
  TBranch* b_cluster_SECOND_ENG_DENS    = { nullptr };  //!
  TBranch* b_cluster_ISOLATION          = { nullptr };  //!
  TBranch* b_cluster_ENG_BAD_CELLS      = { nullptr };  //!
  TBranch* b_cluster_N_BAD_CELLS        = { nullptr };  //!
  TBranch* b_cluster_N_BAD_CELLS_CORR   = { nullptr };  //!
  TBranch* b_cluster_BAD_CELLS_CORR_E   = { nullptr };  //!
  TBranch* b_cluster_BADLARQ_FRAC       = { nullptr };  //!
  TBranch* b_cluster_ENG_POS            = { nullptr };  //!
  TBranch* b_cluster_SIGNIFICANCE       = { nullptr };  //!
  TBranch* b_cluster_CELL_SIGNIFICANCE  = { nullptr };  //!
  TBranch* b_cluster_CELL_SIG_SAMPLING  = { nullptr };  //!
  TBranch* b_cluster_AVG_LAR_Q          = { nullptr };  //!
  TBranch* b_cluster_AVG_TILE_Q         = { nullptr };  //!
  TBranch* b_cluster_ENG_BAD_HV_CELLS   = { nullptr };  //!
  TBranch* b_cluster_N_BAD_HV_CELLS     = { nullptr };  //!
  TBranch* b_cluster_PTD                = { nullptr };  //!
  TBranch* b_cluster_MASS               = { nullptr };  //!
  TBranch* b_cluster_SECOND_TIME        = { nullptr };  //!
  TBranch* b_CalibratedE                = { nullptr };  //!
  TBranch* b_ResponsePrediction         = { nullptr };  //!
  TBranch* b_ResponseFluctuation        = { nullptr };  //!                                      

  ////////////////////
  // Public methods //
  ////////////////////

public:

  // Constructor
  ClusterTreeData(TChain* chain=static_cast<TChain*>(nullptr));
  // destructor
  virtual ~ClusterTreeData();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

public:

  typedef Utils::File::Category::Name category_t;
  //                 Edep  Eem   Elcw  pTem  pTlcw yem   ylcw
  typedef std::tuple<TH1D*,TH1D*,TH1D*,TH1D*,TH1D*,TH1D*,TH1D*> payload_t;
  typedef std::map<category_t,payload_t>                        map_t;
  typedef map_t::iterator                                       iterator;
  typedef map_t::const_iterator                                 const_iterator;

  enum class Scale { EM = 0x01, LCW = 0x02, DEPOSITED=0x04, UNKNOWN = 0x00 };

  static payload_t emptyPayload; 

  map_t _histogramCache; 

  category_t category(const std::string& name);
  template<class FILE> category_t category(FILE* fptr) { return category( fptr != nullptr ? fptr->GetName() : Utils::File::Category::name(Utils::File::Category::Type::UNKNOWN) ); } 
  bool       bookHists(const category_t& fkey);
  payload_t& getHists (const category_t& fkey);
  // tuple position         value
  //         0              energy deposited in cluster
  //         1              EM cluster energy
  //         2              LCW cluster energy
  //         3              EM cluster pT
  //         4              LCW cluster pT
  //         5              EM cluster rapidity
  //         6              LCW cluster rapidity
  TH1D* h_e(Scale s,payload_t& hists) { 
    switch ( s ) { 
    case Scale::DEPOSITED: return std::get<0>(hists);
    case Scale::EM:        return std::get<1>(hists);
    case Scale::LCW:       return std::get<2>(hists);
    default:               return nullptr; 
    }
  } 
  TH1D* h_pt (Scale s,payload_t& hists) { return s == Scale::EM ? std::get<3>(hists) : s == Scale::LCW ? std::get<4>(hists) : nullptr; } 
  TH1D* h_rap(Scale s,payload_t& hists) { return s == Scale::EM ? std::get<5>(hists) : s == Scale::LCW ? std::get<6>(hists) : nullptr; }

  template<class T> bool fillE  (payload_t& hists,Scale s,T value,double weight=1.0) { TH1D* hptr(h_e(s,hists)  ); if ( hptr != nullptr ) { hptr->Fill(value,weight); return true; } else { return false; } }
  template<class T> bool fillPt (payload_t& hists,Scale s,T value,double weight=1.0) { TH1D* hptr(h_pt(s,hists) ); if ( hptr != nullptr ) { hptr->Fill(value,weight); return true; } else { return false; } }
  template<class T> bool fillRap(payload_t& hists,Scale s,T value,double weight=1.0) { TH1D* hptr(h_rap(s,hists)); if ( hptr != nullptr ) { hptr->Fill(value,weight); return true; } else { return false; } }

  map_t& histograms();

  iterator begin(); 
  iterator end(); 
  iterator find(const category_t& fkey); 
  const_iterator begin() const; 
  const_iterator end() const; 
  const_iterator find(const category_t& fkey) const; 

};

#endif

/////////////////////
// Implementations //
/////////////////////

#ifdef ClusterTreeData_cxx
ClusterTreeData::ClusterTreeData(TChain* tree) : fChain(tree) { Init(fChain); }
ClusterTreeData::~ClusterTreeData() { if ( fChain != nullptr ) { delete fChain->GetCurrentFile(); } }

Int_t    ClusterTreeData::GetEntry(Long64_t entry) { return fChain != nullptr ? fChain->GetEntry(entry) : 0; }
Long64_t ClusterTreeData::LoadTree(Long64_t entry) {
  // Set the environment to read one entry
  if ( fChain == nullptr ) { return -5; }
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  // valid operation
  if ( fChain->GetTreeNumber() != fCurrent ) { fCurrent = fChain->GetTreeNumber(); Notify(); }
  return centry;
}

void ClusterTreeData::Init(TTree *ptree) {
  // check input
  if ( ptree == nullptr ) { printf("[ClusterTreeData::Init(TTree* = %p)] ABRT invalid pointer to tree\n",(void*)ptree); return; }
  // tree contains only simple data types
  ptree->SetMakeClass(1);
  fCurrent = -1;
  // allocate tree data to stores 
  std::vector<std::string> missedLeaves; std::vector<std::string> foundLeaves; std::vector<std::string> vNames;   
  vNames.push_back("runNumber"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),runNumber                 ,b_runNumber                 ,foundLeaves,missedLeaves);
  vNames.push_back("eventNumber"               ); Utils::Tree::allocateBranch(ptree,vNames.back(),eventNumber               ,b_eventNumber               ,foundLeaves,missedLeaves);
  vNames.push_back("avgMu"                     ); Utils::Tree::allocateBranch(ptree,vNames.back(),avgMu                     ,b_avgMu                     ,foundLeaves,missedLeaves);
  vNames.push_back("nPrimVtx"                  ); Utils::Tree::allocateBranch(ptree,vNames.back(),nPrimVtx                  ,b_nPrimVtx                  ,foundLeaves,missedLeaves);
  vNames.push_back("jetCalE"                   ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalE                   ,b_jetCalE                   ,foundLeaves,missedLeaves);
  vNames.push_back("jetCalPt"                  ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalPt                  ,b_jetCalPt                  ,foundLeaves,missedLeaves);
  vNames.push_back("jetCalEta"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalEta                 ,b_jetCalEta                 ,foundLeaves,missedLeaves);
  vNames.push_back("jetCalPhi"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalPhi                 ,b_jetCalPhi                 ,foundLeaves,missedLeaves);
  vNames.push_back("jetRawE"                   ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawE                   ,b_jetRawE                   ,foundLeaves,missedLeaves);
  vNames.push_back("jetRawPt"                  ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawPt                  ,b_jetRawPt                  ,foundLeaves,missedLeaves);
  vNames.push_back("jetRawEta"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawEta                 ,b_jetRawEta                 ,foundLeaves,missedLeaves);
  vNames.push_back("jetRawPhi"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawPhi                 ,b_jetRawPhi                 ,foundLeaves,missedLeaves);
  vNames.push_back("jetNConst"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),jetNConst                 ,b_jetNConst                 ,foundLeaves,missedLeaves);
  vNames.push_back("truthJetMatchRadius"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetMatchRadius       ,b_truthJetMatchRadius       ,foundLeaves,missedLeaves);
  vNames.push_back("truthJetE"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetE                 ,b_truthJetE                 ,foundLeaves,missedLeaves);
  vNames.push_back("truthJetPt"                ); Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetPt                ,b_truthJetPt                ,foundLeaves,missedLeaves);
  vNames.push_back("truthJetRap"               ); Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetRap               ,b_truthJetRap               ,foundLeaves,missedLeaves);
  vNames.push_back("truthJetPhi"               ); Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetPhi               ,b_truthJetPhi               ,foundLeaves,missedLeaves);
  vNames.push_back("nCluster"                  ); Utils::Tree::allocateBranch(ptree,vNames.back(),nCluster                  ,b_nCluster                  ,foundLeaves,missedLeaves);
  vNames.push_back("clusterIndex"              ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterIndex              ,b_clusterIndex              ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_nCells"            ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_nCells            ,b_cluster_nCells            ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_nCells_tot"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_nCells_tot        ,b_cluster_nCells_tot        ,foundLeaves,missedLeaves);
  vNames.push_back("clusterECalib"             ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterECalib             ,b_clusterECalib             ,foundLeaves,missedLeaves);
  vNames.push_back("clusterPtCalib"            ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPtCalib            ,b_clusterPtCalib            ,foundLeaves,missedLeaves);
  vNames.push_back("clusterEtaCalib"           ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterEtaCalib           ,b_clusterEtaCalib           ,foundLeaves,missedLeaves);
  vNames.push_back("clusterPhiCalib"           ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPhiCalib           ,b_clusterPhiCalib           ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_sumCellECalib"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_sumCellECalib     ,b_cluster_sumCellECAlib     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_fracECalib"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracECalib        ,b_cluster_fracECalib        ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_fracECalib_ref"    ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracECalib_ref    ,b_cluster_fracECalib_ref    ,foundLeaves,missedLeaves);
  vNames.push_back("clusterE"                  ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterE                  ,b_clusterE                  ,foundLeaves,missedLeaves);
  vNames.push_back("clusterPt"                 ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPt                 ,b_clusterPt                 ,foundLeaves,missedLeaves);
  vNames.push_back("clusterEta"                ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterEta                ,b_clusterEta                ,foundLeaves,missedLeaves);
  vNames.push_back("clusterPhi"                ); Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPhi                ,b_clusterPhi                ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_sumCellE"          ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_sumCellE          ,b_cluster_sumCellE          ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_time"              ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_time              ,b_cluster_time              ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_fracE"             ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracE             ,b_cluster_fracE             ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_fracE_ref"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracE_ref         ,b_cluster_fracE_ref         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_EM_PROBABILITY"    ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_EM_PROBABILITY    ,b_cluster_EM_PROBABILITY    ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_HAD_WEIGHT"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_HAD_WEIGHT        ,b_cluster_HAD_WEIGHT        ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_OOC_WEIGHT"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_OOC_WEIGHT        ,b_cluster_OOC_WEIGHT        ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_DM_WEIGHT"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DM_WEIGHT         ,b_cluster_DM_WEIGHT         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_CALIB_TOT"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_TOT     ,b_cluster_ENG_CALIB_TOT     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_CALIB_OUT_T"   ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_OUT_T   ,b_cluster_ENG_CALIB_OUT_T   ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_CALIB_DEAD_TOT"); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_DEAD_TOT,b_cluster_ENG_CALIB_DEAD_TOT,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CENTER_MAG"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_MAG        ,b_cluster_CENTER_MAG        ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_FIRST_ENG_DENS"    ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_ENG_DENS    ,b_cluster_FIRST_ENG_DENS    ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_FIRST_PHI"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_PHI         ,b_cluster_FIRST_PHI         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_FIRST_ETA"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_ETA         ,b_cluster_FIRST_ETA         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_SECOND_R"          ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_R          ,b_cluster_SECOND_R          ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_SECOND_LAMBDA"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_LAMBDA     ,b_cluster_SECOND_LAMBDA     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_DELTA_PHI"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_PHI         ,b_cluster_DELTA_PHI         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_DELTA_THETA"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_THETA       ,b_cluster_DELTA_THETA       ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_DELTA_ALPHA"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_ALPHA       ,b_cluster_DELTA_ALPHA       ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CENTER_X"          ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_X          ,b_cluster_CENTER_X          ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CENTER_Y"          ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_Y          ,b_cluster_CENTER_Y          ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CENTER_Z"          ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_Z          ,b_cluster_CENTER_Z          ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CENTER_LAMBDA"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_LAMBDA     ,b_cluster_CENTER_LAMBDA     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_LATERAL"           ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_LATERAL           ,b_cluster_LATERAL           ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_LONGITUDINAL"      ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_LONGITUDINAL      ,b_cluster_LONGITUDINAL      ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_FRAC_EM"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_EM       ,b_cluster_ENG_FRAC_EM       ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_FRAC_MAX"      ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_MAX      ,b_cluster_ENG_FRAC_MAX      ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_FRAC_CORE"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_CORE     ,b_cluster_ENG_FRAC_CORE     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_SECOND_ENG_DENS"   ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_ENG_DENS   ,b_cluster_SECOND_ENG_DENS   ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ISOLATION"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ISOLATION         ,b_cluster_ISOLATION         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_BAD_CELLS"     ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_BAD_CELLS     ,b_cluster_ENG_BAD_CELLS     ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_N_BAD_CELLS"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_CELLS       ,b_cluster_N_BAD_CELLS       ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_N_BAD_CELLS_CORR"  ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_CELLS_CORR  ,b_cluster_N_BAD_CELLS_CORR  ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_BAD_CELLS_CORR_E"  ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_BAD_CELLS_CORR_E  ,b_cluster_BAD_CELLS_CORR_E  ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_BADLARQ_FRAC"      ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_BADLARQ_FRAC      ,b_cluster_BADLARQ_FRAC      ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_POS"           ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_POS           ,b_cluster_ENG_POS           ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_SIGNIFICANCE"      ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SIGNIFICANCE      ,b_cluster_SIGNIFICANCE      ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CELL_SIGNIFICANCE" ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CELL_SIGNIFICANCE ,b_cluster_CELL_SIGNIFICANCE ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_CELL_SIG_SAMPLING" ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CELL_SIG_SAMPLING ,b_cluster_CELL_SIG_SAMPLING ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_AVG_LAR_Q"         ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_AVG_LAR_Q         ,b_cluster_AVG_LAR_Q         ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_AVG_TILE_Q"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_AVG_TILE_Q        ,b_cluster_AVG_TILE_Q        ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_ENG_BAD_HV_CELLS"  ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_BAD_HV_CELLS  ,b_cluster_ENG_BAD_HV_CELLS  ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_N_BAD_HV_CELLS"    ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_HV_CELLS    ,b_cluster_N_BAD_HV_CELLS    ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_PTD"               ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_PTD               ,b_cluster_PTD               ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_MASS"              ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_MASS              ,b_cluster_MASS              ,foundLeaves,missedLeaves);
  vNames.push_back("cluster_SECOND_TIME"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_TIME       ,b_cluster_SECOND_TIME       ,foundLeaves,missedLeaves);
  vNames.push_back("CalibratedE"               ); Utils::Tree::allocateBranch(ptree,vNames.back(),CalibratedE               ,b_CalibratedE               ,foundLeaves,missedLeaves);
  vNames.push_back("ResponsePrediction"        ); Utils::Tree::allocateBranch(ptree,vNames.back(),ResponsePrediction        ,b_ResponsePrediction        ,foundLeaves,missedLeaves);
  vNames.push_back("ResponseFluctuation"       ); Utils::Tree::allocateBranch(ptree,vNames.back(),ResponseFluctuation       ,b_ResponseFluctuation       ,foundLeaves,missedLeaves);
  //
  printf("[ClusterTreeData::Init(...)] INFO found %zu known branches, %zu unknown branches of %zu expected branches.\n",foundLeaves.size(),missedLeaves.size(),vNames.size());
  int nw(0);
  for ( auto varName : foundLeaves  ) { nw = std::max(nw,static_cast<int>(varName.length())); }
  for ( auto varName : missedLeaves ) { nw = std::max(nw,static_cast<int>(varName.length())); }
  // 
  int nwords(4); int iwords(0); 
  for ( const auto& varName : missedLeaves ) { 
    if ( iwords == 0 ) { 
      printf("[ClusterTreeData::Init(...)] DEBG leaves not allocated: "); 
    } else if ( iwords % nwords == 0 ) { 
      printf("\n[ClusterTreeData::Init(...)] DEBG leaves not allocated: ");  
    } 
    printf(" %-*.*s",nw,nw,varName.c_str()); ++iwords; 
  }
  if ( !missedLeaves.empty() ) { printf("\n"); }
  iwords = 0; 
  for ( const auto& varName : foundLeaves ) { 
    if ( iwords == 0 ) { printf("[ClusterTreeData::Init(...)] DEBG leaves allocated ...: "); } 
    else if ( iwords % nwords == 0 ) { 
      printf("\n[ClusterTreeData::Init(...)] DEBG leaves allocated ...: ");  
    } 
    printf(" %-*.*s",nw,nw,varName.c_str()); ++iwords;
  }
  if ( !foundLeaves.empty() ) { printf("\n"); } 
  // vNames.push_back("runNumber"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),runNumber                 ,b_runNumber                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("eventNumber"               ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),eventNumber               ,b_eventNumber               )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("avgMu"                     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),avgMu                     ,b_avgMu                     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("nPrimVtx"                  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),nPrimVtx                  ,b_nPrimVtx                  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetCalE"                   ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalE                   ,b_jetCalE                   )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetCalPt"                  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalPt                  ,b_jetCalPt                  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetCalEta"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalEta                 ,b_jetCalEta                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetCalPhi"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetCalPhi                 ,b_jetCalPhi                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetRawE"                   ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawE                   ,b_jetRawE                   )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetRawPt"                  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawPt                  ,b_jetRawPt                  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetRawEta"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawEta                 ,b_jetRawEta                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetRawPhi"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetRawPhi                 ,b_jetRawPhi                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("jetNConst"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),jetNConst                 ,b_jetNConst                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("truthJetMatchRadius"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetMatchRadius       ,b_truthJetMatchRadius       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("truthJetE"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetE                 ,b_truthJetE                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("truthJetPt"                ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetPt                ,b_truthJetPt                )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("truthJetRap"               ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetRap               ,b_truthJetRap               )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("truthJetPhi"               ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),truthJetPhi               ,b_truthJetPhi               )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("nCluster"                  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),nCluster                  ,b_nCluster                  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterIndex"              ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterIndex              ,b_clusterIndex              )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_nCells"            ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_nCells            ,b_cluster_nCells            )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_nCells_tot"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_nCells_tot        ,b_cluster_nCells_tot        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterECalib"             ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterECalib             ,b_clusterECalib             )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterPtCalib"            ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPtCalib            ,b_clusterPtCalib            )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterEtaCalib"           ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterEtaCalib           ,b_clusterEtaCalib           )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterPhiCalib"           ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPhiCalib           ,b_clusterPhiCalib           )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_sumCellECalib"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_sumCellECalib     ,b_cluster_sumCellECAlib     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_fracECalib"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracECalib        ,b_cluster_fracECalib        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_fracECalib_ref"    ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracECalib_ref    ,b_cluster_fracECalib_ref    )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterE"                  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterE                  ,b_clusterE                  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterPt"                 ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPt                 ,b_clusterPt                 )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterEta"                ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterEta                ,b_clusterEta                )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("clusterPhi"                ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),clusterPhi                ,b_clusterPhi                )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_sumCellE"          ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_sumCellE          ,b_cluster_sumCellE          )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_time"              ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_time              ,b_cluster_time              )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_fracE"             ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracE             ,b_cluster_fracE             )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_fracE_ref"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_fracE_ref         ,b_cluster_fracE_ref         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_EM_PROBABILITY"    ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_EM_PROBABILITY    ,b_cluster_EM_PROBABILITY    )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_HAD_WEIGHT"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_HAD_WEIGHT        ,b_cluster_HAD_WEIGHT        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_OOC_WEIGHT"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_OOC_WEIGHT        ,b_cluster_OOC_WEIGHT        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_DM_WEIGHT"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DM_WEIGHT         ,b_cluster_DM_WEIGHT         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_CALIB_TOT"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_TOT     ,b_cluster_ENG_CALIB_TOT     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_CALIB_OUT_T"   ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_OUT_T   ,b_cluster_ENG_CALIB_OUT_T   )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_CALIB_DEAD_TOT"); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_CALIB_DEAD_TOT,b_cluster_ENG_CALIB_DEAD_TOT)) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CENTER_MAG"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_MAG        ,b_cluster_CENTER_MAG        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_FIRST_ENG_DENS"    ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_ENG_DENS    ,b_cluster_FIRST_ENG_DENS    )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_FIRST_PHI"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_PHI         ,b_cluster_FIRST_PHI         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_FIRST_ETA"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_FIRST_ETA         ,b_cluster_FIRST_ETA         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_SECOND_R"          ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_R          ,b_cluster_SECOND_R          )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_SECOND_LAMBDA"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_LAMBDA     ,b_cluster_SECOND_LAMBDA     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_DELTA_PHI"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_PHI         ,b_cluster_DELTA_PHI         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_DELTA_THETA"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_THETA       ,b_cluster_DELTA_THETA       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_DELTA_ALPHA"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_DELTA_ALPHA       ,b_cluster_DELTA_ALPHA       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CENTER_X"          ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_X          ,b_cluster_CENTER_X          )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CENTER_Y"          ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_Y          ,b_cluster_CENTER_Y          )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CENTER_Z"          ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_Z          ,b_cluster_CENTER_Z          )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CENTER_LAMBDA"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CENTER_LAMBDA     ,b_cluster_CENTER_LAMBDA     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_LATERAL"           ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_LATERAL           ,b_cluster_LATERAL           )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_LONGITUDINAL"      ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_LONGITUDINAL      ,b_cluster_LONGITUDINAL      )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_FRAC_EM"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_EM       ,b_cluster_ENG_FRAC_EM       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_FRAC_MAX"      ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_MAX      ,b_cluster_ENG_FRAC_MAX      )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_FRAC_CORE"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_FRAC_CORE     ,b_cluster_ENG_FRAC_CORE     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_SECOND_ENG_DENS"   ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_ENG_DENS   ,b_cluster_SECOND_ENG_DENS   )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ISOLATION"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ISOLATION         ,b_cluster_ISOLATION         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_BAD_CELLS"     ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_BAD_CELLS     ,b_cluster_ENG_BAD_CELLS     )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_N_BAD_CELLS"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_CELLS       ,b_cluster_N_BAD_CELLS       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_N_BAD_CELLS_CORR"  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_CELLS_CORR  ,b_cluster_N_BAD_CELLS_CORR  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_BAD_CELLS_CORR_E"  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_BAD_CELLS_CORR_E  ,b_cluster_BAD_CELLS_CORR_E  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_BADLARQ_FRAC"      ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_BADLARQ_FRAC      ,b_cluster_BADLARQ_FRAC      )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_POS"           ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_POS           ,b_cluster_ENG_POS           )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_SIGNIFICANCE"      ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SIGNIFICANCE      ,b_cluster_SIGNIFICANCE      )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CELL_SIGNIFICANCE" ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CELL_SIGNIFICANCE ,b_cluster_CELL_SIGNIFICANCE )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_CELL_SIG_SAMPLING" ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_CELL_SIG_SAMPLING ,b_cluster_CELL_SIG_SAMPLING )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_AVG_LAR_Q"         ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_AVG_LAR_Q         ,b_cluster_AVG_LAR_Q         )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_AVG_TILE_Q"        ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),uster_AVG_TILE_Q          ,b_cluster_AVG_TILE_Q        )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_ENG_BAD_HV_CELLS"  ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_ENG_BAD_HV_CELLS  ,b_cluster_ENG_BAD_HV_CELLS  )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_N_BAD_HV_CELLS"    ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_N_BAD_HV_CELLS    ,b_cluster_N_BAD_HV_CELLS    )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_PTD"               ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_PTD               ,b_cluster_PTD               )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_MASS"              ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_MASS              ,b_cluster_MASS              )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
  // vNames.push_back("cluster_SECOND_TIME"       ); if (Utils::Tree::allocateBranch(ptree,vNames.back(),cluster_SECOND_TIME       ,b_cluster_SECOND_TIME       )) { fLeaves.push_back(vNames.back());  } else { mLeaves.push_back(vNames.back()); }
}

Bool_t ClusterTreeData::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ClusterTreeData::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ClusterTreeData::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ClusterTreeData_cxx
