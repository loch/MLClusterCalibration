///@name Declaration of variables in ClusterTree
///
/// Code for use in analysis modules. For a description of these variables, see the code for the ROOT branches in ClusterTreeBranches.h
///@{
#define CLUSTERTREE_VARIABLE_DECL                \
Int_t    entry                      = { 0  };    \
Long64_t runNumber                  = { 0  };    \
Int_t    eventNumber                = { 0  };    \
Float_t  avgMu                      = { 0. };    \
Int_t    nPrimVtx                   = { 0  };    \
Int_t    nCluster 	                = { 0  };    \
Float_t  truthE                     = { 0. };    \
Float_t  truthPt                    = { 0. };    \
Float_t  truthEta                   = { 0. };    \
Float_t  truthPhi                   = { 0. };    \
Int_t    truthPDG                   = { 0  };    \
Float_t  jetCalE                    = { 0. };    \
Float_t  jetCalPt                   = { 0. };    \
Float_t  jetCalEta 	                = { 0. };    \
Float_t  jetCalPhi 	                = { 0. };    \
Float_t  jetRawE 	                = { 0. };    \
Float_t  jetRawPt 	                = { 0. };    \
Float_t  jetRawEta                  = { 0. };    \
Float_t  jetRawPhi 	                = { 0. };    \
Int_t    jetNConst 	                = { 0  };    \
Float_t  truthJetMatchRadius        = { 0. };    \
Float_t  truthJetE 	                = { 0. };    \
Float_t  truthJetPt 	            = { 0. };    \
Float_t  truthJetRap 	            = { 0. };    \
Float_t  truthJetPhi 	            = { 0. };    \
Int_t    clusterIndex 	            = { 0  };    \
Int_t    cluster_nCells             = { 0  };    \
Int_t    cluster_nCells_tot         = { 0  };    \
Float_t  clusterECalib              = { 0. };    \
Float_t  clusterPtCalib             = { 0. };    \
Float_t  clusterEtaCalib            = { 0. };    \
Float_t  clusterPhiCalib            = { 0. };    \
Float_t  cluster_sumCellECalib      = { 0. };    \
Float_t  cluster_fracECalib 	    = { 0. };    \
Float_t  cluster_fracECalib_ref     = { 0. };    \
Float_t  clusterE 		            = { 0. };    \
Float_t  clusterPt                  = { 0. };    \
Float_t  clusterEta 		        = { 0. };    \
Float_t  clusterPhi 		        = { 0. };    \
Float_t  cluster_sumCellE 	        = { 0. };    \
Float_t  cluster_fracE 	            = { 0. };    \
Float_t  cluster_fracE_ref 	        = { 0. };    \
Float_t  cluster_time 		        = { 0. };    \
Float_t  cluster_EM_PROBABILITY     = { 0. };    \
Float_t  cluster_HAD_WEIGHT         = { 0. };    \
Float_t  cluster_OOC_WEIGHT 	    = { 0. };    \
Float_t  cluster_DM_WEIGHT  	    = { 0. };    \
Float_t  cluster_ENG_CALIB_TOT      = { 0. };    \
Float_t  cluster_ENG_CALIB_OUT_T    = { 0. };    \
Float_t  cluster_ENG_CALIB_DEAD_TOT = { 0. };    \
Float_t  cluster_CENTER_MAG 	    = { 0. };    \
Float_t  cluster_FIRST_ENG_DENS     = { 0. };    \
Float_t  cluster_FIRST_PHI	        = { 0. };    \
Float_t  cluster_FIRST_ETA	        = { 0. };    \
Float_t  cluster_SECOND_R	        = { 0. };    \
Float_t  cluster_SECOND_LAMBDA      = { 0. };    \
Float_t  cluster_DELTA_PHI	        = { 0. };    \
Float_t  cluster_DELTA_THETA        = { 0. };    \
Float_t  cluster_DELTA_ALPHA 	    = { 0. };    \
Float_t  cluster_CENTER_X 	        = { 0. };    \
Float_t  cluster_CENTER_Y 	        = { 0. };    \
Float_t  cluster_CENTER_Z 	        = { 0. };    \
Float_t  cluster_CENTER_LAMBDA      = { 0. };    \
Float_t  cluster_LATERAL 	        = { 0. };    \
Float_t  cluster_LONGITUDINAL 	    = { 0. };    \
Float_t  cluster_ENG_FRAC_EM        = { 0. };    \
Float_t  cluster_ENG_FRAC_MAX 	    = { 0. };    \
Float_t  cluster_ENG_FRAC_CORE      = { 0. };    \
Float_t  cluster_SECOND_ENG_DENS    = { 0. };    \
Float_t  cluster_ISOLATION 	        = { 0. };    \
Float_t  cluster_ENG_BAD_CELLS      = { 0. };    \
Float_t  cluster_N_BAD_CELLS 	    = { 0. };    \
Float_t  cluster_N_BAD_CELLS_CORR   = { 0. };    \
Float_t  cluster_BAD_CELLS_CORR_E   = { 0. };    \
Float_t  cluster_BADLARQ_FRAC 	    = { 0. };    \
Float_t  cluster_ENG_POS 	        = { 0. };    \
Float_t  cluster_SIGNIFICANCE 	    = { 0. };    \
Float_t  cluster_CELL_SIGNIFICANCE  = { 0. };    \
Float_t  cluster_CELL_SIG_SAMPLING  = { 0. };    \
Float_t  cluster_AVG_LAR_Q 	        = { 0. };    \
Float_t  cluster_AVG_TILE_Q 	    = { 0. };    \
Float_t  cluster_ENG_BAD_HV_CELLS   = { 0. };    \
Float_t  cluster_N_BAD_HV_CELLS     = { 0. };    \
Float_t  cluster_PTD 		        = { 0. };    \
Float_t  cluster_MASS 		        = { 0. };    \
Float_t  cluster_SECOND_TIME 	    = { 0. };    \
Float_t  CalibratedE                = { 0. };    \
Float_t  ResponseMeasure            = { 0. };    \
Float_t  ResponsePrediction         = { 0. };    \
Float_t  ResponseFluctuation        = { 0. }
///@}
///@name Branch allocation implementation
///@{
#define CLUSTERTREE_ALLOCATE_BRANCHES( TREE, NAMES, FLEAVES, MLEAVES )                                                                                                 \
NAMES.push_back("runNumber"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),runNumber                 ,b_runNumber                 ,FLEAVES,MLEAVES); \
NAMES.push_back("eventNumber"               ); Utils::Tree::allocateBranch(TREE,NAMES.back(),eventNumber               ,b_eventNumber               ,FLEAVES,MLEAVES); \
NAMES.push_back("avgMu"                     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),avgMu                     ,b_avgMu                     ,FLEAVES,MLEAVES); \
NAMES.push_back("nPrimVtx"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),nPrimVtx                  ,b_nPrimVtx                  ,FLEAVES,MLEAVES); \
NAMES.push_back("truthE"                    ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthE                    ,b_truthE                    ,FLEAVES,MLEAVES); \
NAMES.push_back("truthPt"                   ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthPt                   ,b_truthPt                   ,FLEAVES,MLEAVES); \
NAMES.push_back("truthEta"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthEta                  ,b_truthEta                  ,FLEAVES,MLEAVES); \
NAMES.push_back("truthPhi"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthPhi                  ,b_truthPhi                  ,FLEAVES,MLEAVES); \
NAMES.push_back("truthPDG"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthPDG                  ,b_truthPDG                  ,FLEAVES,MLEAVES); \
NAMES.push_back("jetCalE"                   ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetCalE                   ,b_jetCalE                   ,FLEAVES,MLEAVES); \
NAMES.push_back("jetCalPt"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetCalPt                  ,b_jetCalPt                  ,FLEAVES,MLEAVES); \
NAMES.push_back("jetCalEta"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetCalEta                 ,b_jetCalEta                 ,FLEAVES,MLEAVES); \
NAMES.push_back("jetCalPhi"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetCalPhi                 ,b_jetCalPhi                 ,FLEAVES,MLEAVES); \
NAMES.push_back("jetRawE"                   ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetRawE                   ,b_jetRawE                   ,FLEAVES,MLEAVES); \
NAMES.push_back("jetRawPt"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetRawPt                  ,b_jetRawPt                  ,FLEAVES,MLEAVES); \
NAMES.push_back("jetRawEta"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetRawEta                 ,b_jetRawEta                 ,FLEAVES,MLEAVES); \
NAMES.push_back("jetRawPhi"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetRawPhi                 ,b_jetRawPhi                 ,FLEAVES,MLEAVES); \
NAMES.push_back("jetNConst"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),jetNConst                 ,b_jetNConst                 ,FLEAVES,MLEAVES); \
NAMES.push_back("truthJetMatchRadius"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthJetMatchRadius       ,b_truthJetMatchRadius       ,FLEAVES,MLEAVES); \
NAMES.push_back("truthJetE"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthJetE                 ,b_truthJetE                 ,FLEAVES,MLEAVES); \
NAMES.push_back("truthJetPt"                ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthJetPt                ,b_truthJetPt                ,FLEAVES,MLEAVES); \
NAMES.push_back("truthJetRap"               ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthJetRap               ,b_truthJetRap               ,FLEAVES,MLEAVES); \
NAMES.push_back("truthJetPhi"               ); Utils::Tree::allocateBranch(TREE,NAMES.back(),truthJetPhi               ,b_truthJetPhi               ,FLEAVES,MLEAVES); \
NAMES.push_back("nCluster"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),nCluster                  ,b_nCluster                  ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterIndex"              ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterIndex              ,b_clusterIndex              ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_nCells"            ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_nCells            ,b_cluster_nCells            ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_nCells_tot"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_nCells_tot        ,b_cluster_nCells_tot        ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterECalib"             ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterECalib             ,b_clusterECalib             ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterPtCalib"            ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterPtCalib            ,b_clusterPtCalib            ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterEtaCalib"           ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterEtaCalib           ,b_clusterEtaCalib           ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterPhiCalib"           ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterPhiCalib           ,b_clusterPhiCalib           ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_sumCellECalib"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_sumCellECalib     ,b_cluster_sumCellECAlib     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_fracECalib"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_fracECalib        ,b_cluster_fracECalib        ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_fracECalib_ref"    ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_fracECalib_ref    ,b_cluster_fracECalib_ref    ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterE"                  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterE                  ,b_clusterE                  ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterPt"                 ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterPt                 ,b_clusterPt                 ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterEta"                ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterEta                ,b_clusterEta                ,FLEAVES,MLEAVES); \
NAMES.push_back("clusterPhi"                ); Utils::Tree::allocateBranch(TREE,NAMES.back(),clusterPhi                ,b_clusterPhi                ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_sumCellE"          ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_sumCellE          ,b_cluster_sumCellE          ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_time"              ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_time              ,b_cluster_time              ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_fracE"             ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_fracE             ,b_cluster_fracE             ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_fracE_ref"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_fracE_ref         ,b_cluster_fracE_ref         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_EM_PROBABILITY"    ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_EM_PROBABILITY    ,b_cluster_EM_PROBABILITY    ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_HAD_WEIGHT"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_HAD_WEIGHT        ,b_cluster_HAD_WEIGHT        ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_OOC_WEIGHT"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_OOC_WEIGHT        ,b_cluster_OOC_WEIGHT        ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_DM_WEIGHT"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_DM_WEIGHT         ,b_cluster_DM_WEIGHT         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_CALIB_TOT"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_CALIB_TOT     ,b_cluster_ENG_CALIB_TOT     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_CALIB_OUT_T"   ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_CALIB_OUT_T   ,b_cluster_ENG_CALIB_OUT_T   ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_CALIB_DEAD_TOT"); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_CALIB_DEAD_TOT,b_cluster_ENG_CALIB_DEAD_TOT,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CENTER_MAG"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CENTER_MAG        ,b_cluster_CENTER_MAG        ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_FIRST_ENG_DENS"    ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_FIRST_ENG_DENS    ,b_cluster_FIRST_ENG_DENS    ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_FIRST_PHI"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_FIRST_PHI         ,b_cluster_FIRST_PHI         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_FIRST_ETA"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_FIRST_ETA         ,b_cluster_FIRST_ETA         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_SECOND_R"          ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_SECOND_R          ,b_cluster_SECOND_R          ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_SECOND_LAMBDA"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_SECOND_LAMBDA     ,b_cluster_SECOND_LAMBDA     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_DELTA_PHI"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_DELTA_PHI         ,b_cluster_DELTA_PHI         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_DELTA_THETA"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_DELTA_THETA       ,b_cluster_DELTA_THETA       ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_DELTA_ALPHA"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_DELTA_ALPHA       ,b_cluster_DELTA_ALPHA       ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CENTER_X"          ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CENTER_X          ,b_cluster_CENTER_X          ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CENTER_Y"          ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CENTER_Y          ,b_cluster_CENTER_Y          ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CENTER_Z"          ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CENTER_Z          ,b_cluster_CENTER_Z          ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CENTER_LAMBDA"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CENTER_LAMBDA     ,b_cluster_CENTER_LAMBDA     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_LATERAL"           ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_LATERAL           ,b_cluster_LATERAL           ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_LONGITUDINAL"      ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_LONGITUDINAL      ,b_cluster_LONGITUDINAL      ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_FRAC_EM"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_FRAC_EM       ,b_cluster_ENG_FRAC_EM       ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_FRAC_MAX"      ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_FRAC_MAX      ,b_cluster_ENG_FRAC_MAX      ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_FRAC_CORE"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_FRAC_CORE     ,b_cluster_ENG_FRAC_CORE     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_SECOND_ENG_DENS"   ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_SECOND_ENG_DENS   ,b_cluster_SECOND_ENG_DENS   ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ISOLATION"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ISOLATION         ,b_cluster_ISOLATION         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_BAD_CELLS"     ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_BAD_CELLS     ,b_cluster_ENG_BAD_CELLS     ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_N_BAD_CELLS"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_N_BAD_CELLS       ,b_cluster_N_BAD_CELLS       ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_N_BAD_CELLS_CORR"  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_N_BAD_CELLS_CORR  ,b_cluster_N_BAD_CELLS_CORR  ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_BAD_CELLS_CORR_E"  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_BAD_CELLS_CORR_E  ,b_cluster_BAD_CELLS_CORR_E  ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_BADLARQ_FRAC"      ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_BADLARQ_FRAC      ,b_cluster_BADLARQ_FRAC      ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_POS"           ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_POS           ,b_cluster_ENG_POS           ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_SIGNIFICANCE"      ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_SIGNIFICANCE      ,b_cluster_SIGNIFICANCE      ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CELL_SIGNIFICANCE" ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CELL_SIGNIFICANCE ,b_cluster_CELL_SIGNIFICANCE ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_CELL_SIG_SAMPLING" ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_CELL_SIG_SAMPLING ,b_cluster_CELL_SIG_SAMPLING ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_AVG_LAR_Q"         ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_AVG_LAR_Q         ,b_cluster_AVG_LAR_Q         ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_AVG_TILE_Q"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_AVG_TILE_Q        ,b_cluster_AVG_TILE_Q        ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_ENG_BAD_HV_CELLS"  ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_ENG_BAD_HV_CELLS  ,b_cluster_ENG_BAD_HV_CELLS  ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_N_BAD_HV_CELLS"    ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_N_BAD_HV_CELLS    ,b_cluster_N_BAD_HV_CELLS    ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_PTD"               ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_PTD               ,b_cluster_PTD               ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_MASS"              ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_MASS              ,b_cluster_MASS              ,FLEAVES,MLEAVES); \
NAMES.push_back("cluster_SECOND_TIME"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),cluster_SECOND_TIME       ,b_cluster_SECOND_TIME       ,FLEAVES,MLEAVES); \
NAMES.push_back("CalibratedE"               ); Utils::Tree::allocateBranch(TREE,NAMES.back(),CalibratedE               ,b_CalibratedE               ,FLEAVES,MLEAVES); \
NAMES.push_back("ResponseMeasure"           ); Utils::Tree::allocateBranch(TREE,NAMES.back(),ResponseMeasure           ,b_ResponseMeasure           ,FLEAVES,MLEAVES); \
NAMES.push_back("ResponsePrediction"        ); Utils::Tree::allocateBranch(TREE,NAMES.back(),ResponsePrediction        ,b_ResponsePrediction        ,FLEAVES,MLEAVES)
//NAMES.push_back("ResponseFluctuation"       ); Utils::Tree::allocateBranch(TREE,NAMES.back(),ResponseFluctuation       ,b_ResponseFluctuation       ,FLEAVES,MLEAVES) 
///@}
///@name Book branches
///
/// The branch booking is automatically configured using the branches that are available in the input tree. 
///@{
#define CLUSTERTREE_BOOK_BRANCHES( OTREE, ATREE, MTREE, BNAMES )                                                                                                                              \
auto originalLeaves   = OTREE->GetListOfLeaves();                                                                                                                                             \
auto additionalLeaves = ATREE->GetListOfLeaves();                                                                                                                                             \
int  nLeaves(originalLeaves->GetEntries()+additionalLeaves->GetEntries());                                                                                                                    \
if ( nLeaves > 0 ) { BNAMES.reserve(nLeaves);   }                                                                                                                                             \
for ( TIter fleave(originalLeaves->begin()); fleave != originalLeaves->end(); ++fleave ) {                                                                                                     \
    if ( *fleave != nullptr && std::find(BNAMES.begin(),BNAMES.end(),(*fleave)->GetName()) == BNAMES.end() ) { BNAMES.push_back((*fleave)->GetName()); }                                                           \
}                                                                                                                                                                                             \
for ( TIter fleave(additionalLeaves->begin()); fleave != additionalLeaves->end(); ++fleave ) {                                                                                                \
    if ( *fleave != nullptr && std::find(BNAMES.begin(),BNAMES.end(),(*fleave)->GetName()) == BNAMES.end() ) { BNAMES.push_back((*fleave)->GetName()); }                                                           \
}                                                                                                                                                                                             \
if ( std::find(BNAMES.begin(),BNAMES.end(),"runNumber"                  ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "runNumber"                 ,runNumber                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"eventNumber"                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "eventNumber"               ,eventNumber               ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"avgMu"                      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "avgMu"                     ,avgMu                     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"nPrimVtx"                   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "nPrimVtx"                  ,nPrimVtx                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"nCluster" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "nCluster" 	                ,nCluster                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthE"                     ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthE"                    ,truthE                    ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthPt"                    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthPt"                   ,truthPt                   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthEta"                   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthEta"                  ,truthEta                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthPhi"                   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthPhi"                  ,truthPhi                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthPDG"                   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthPDG"                  ,truthPDG                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetCalE"                    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetCalE"                   ,jetCalE                   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetCalPt"                   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetCalPt"                  ,jetCalPt                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetCalEta" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetCalEta" 	            ,jetCalEta                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetCalPhi" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetCalPhi" 	            ,jetCalPhi                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetRawE" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetRawE" 	                ,jetRawE                   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetRawPt" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetRawPt" 	                ,jetRawPt                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetRawEta"                  ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetRawEta"                 ,jetRawEta                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetRawPhi" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetRawPhi" 	            ,jetRawPhi                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"jetNConst" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "jetNConst" 	            ,jetNConst                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthJetMatchRadius"        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthJetMatchRadius"       ,truthJetMatchRadius       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthJetE" 	                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthJetE" 	            ,truthJetE                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthJetPt" 	            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthJetPt" 	            ,truthJetPt                ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthJetRap" 	            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthJetRap" 	            ,truthJetRap               ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"truthJetPhi" 	            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "truthJetPhi" 	            ,truthJetPhi               ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterIndex" 	            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterIndex" 	            ,clusterIndex              ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_nCells"             ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_nCells"            ,cluster_nCells            ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_nCells_tot"         ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_nCells_tot"        ,cluster_nCells_tot        ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterECalib"              ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterECalib"             ,clusterECalib             ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterPtCalib"             ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterPtCalib"            ,clusterPtCalib            ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterEtaCalib"            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterEtaCalib"           ,clusterEtaCalib           ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterPhiCalib"            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterPhiCalib"           ,clusterPhiCalib           ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_sumCellECalib"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_sumCellECalib"     ,cluster_sumCellECalib     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_fracECalib" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_fracECalib" 	    ,cluster_fracECalib        ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_fracECalib_ref"     ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_fracECalib_ref"    ,cluster_fracECalib_ref    ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterE" 		            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterE" 		            ,clusterE                  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterPt"                  ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterPt"                 ,clusterPt                 ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterEta" 		        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterEta" 		        ,clusterEta                ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"clusterPhi" 		        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "clusterPhi" 		        ,clusterPhi                ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_sumCellE" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_sumCellE" 	        ,cluster_sumCellE          ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_fracE" 	            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_fracE" 	        ,cluster_fracE             ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_fracE_ref" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_fracE_ref" 	    ,cluster_fracE_ref         ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_time" 		        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_time" 		        ,cluster_time              ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_EM_PROBABILITY"     ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_EM_PROBABILITY"    ,cluster_EM_PROBABILITY    ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_HAD_WEIGHT"         ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_HAD_WEIGHT"        ,cluster_HAD_WEIGHT        ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_OOC_WEIGHT" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_OOC_WEIGHT" 	    ,cluster_OOC_WEIGHT 	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_DM_WEIGHT"  	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_DM_WEIGHT"  	    ,cluster_DM_WEIGHT  	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_CALIB_TOT"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_CALIB_TOT"     ,cluster_ENG_CALIB_TOT     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_CALIB_OUT_T"    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_CALIB_OUT_T"   ,cluster_ENG_CALIB_OUT_T   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_CALIB_DEAD_TOT" ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_CALIB_DEAD_TOT",cluster_ENG_CALIB_DEAD_TOT); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CENTER_MAG" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CENTER_MAG" 	    ,cluster_CENTER_MAG 	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_FIRST_ENG_DENS"     ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_FIRST_ENG_DENS"    ,cluster_FIRST_ENG_DENS    ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_FIRST_PHI"	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_FIRST_PHI"	        ,cluster_FIRST_PHI	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_FIRST_ETA"	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_FIRST_ETA"	        ,cluster_FIRST_ETA	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_SECOND_R"	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_SECOND_R"	        ,cluster_SECOND_R	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_SECOND_LAMBDA"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_SECOND_LAMBDA"     ,cluster_SECOND_LAMBDA     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_DELTA_PHI"	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_DELTA_PHI"	        ,cluster_DELTA_PHI	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_DELTA_THETA"        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_DELTA_THETA"       ,cluster_DELTA_THETA       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_DELTA_ALPHA" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_DELTA_ALPHA" 	    ,cluster_DELTA_ALPHA 	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CENTER_X" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CENTER_X" 	        ,cluster_CENTER_X 	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CENTER_Y" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CENTER_Y" 	        ,cluster_CENTER_Y 	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CENTER_Z" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CENTER_Z" 	        ,cluster_CENTER_Z 	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CENTER_LAMBDA"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CENTER_LAMBDA"     ,cluster_CENTER_LAMBDA     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_LATERAL" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_LATERAL" 	        ,cluster_LATERAL           ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_LONGITUDINAL" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_LONGITUDINAL" 	    ,cluster_LONGITUDINAL      ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_FRAC_EM"        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_FRAC_EM"       ,cluster_ENG_FRAC_EM       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_FRAC_MAX" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_FRAC_MAX" 	    ,cluster_ENG_FRAC_MAX 	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_FRAC_CORE"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_FRAC_CORE"     ,cluster_ENG_FRAC_CORE     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_SECOND_ENG_DENS"    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_SECOND_ENG_DENS"   ,cluster_SECOND_ENG_DENS   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ISOLATION" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ISOLATION" 	    ,cluster_ISOLATION 	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_BAD_CELLS"      ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_BAD_CELLS"     ,cluster_ENG_BAD_CELLS     ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_N_BAD_CELLS" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_N_BAD_CELLS" 	    ,cluster_N_BAD_CELLS	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_N_BAD_CELLS_CORR"   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_N_BAD_CELLS_CORR"  ,cluster_N_BAD_CELLS_CORR  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_BAD_CELLS_CORR_E"   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_BAD_CELLS_CORR_E"  ,cluster_BAD_CELLS_CORR_E  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_BADLARQ_FRAC" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_BADLARQ_FRAC" 	    ,cluster_BADLARQ_FRAC      ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_POS" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_POS" 	        ,cluster_ENG_POS           ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_SIGNIFICANCE" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_SIGNIFICANCE" 	    ,cluster_SIGNIFICANCE      ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CELL_SIGNIFICANCE"  ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CELL_SIGNIFICANCE" ,cluster_CELL_SIGNIFICANCE ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_CELL_SIG_SAMPLING"  ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_CELL_SIG_SAMPLING" ,cluster_CELL_SIG_SAMPLING ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_AVG_LAR_Q" 	        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_AVG_LAR_Q" 	    ,cluster_AVG_LAR_Q 	       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_AVG_TILE_Q" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_AVG_TILE_Q" 	    ,cluster_AVG_TILE_Q        ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_ENG_BAD_HV_CELLS"   ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_ENG_BAD_HV_CELLS"  ,cluster_ENG_BAD_HV_CELLS  ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_N_BAD_HV_CELLS"     ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_N_BAD_HV_CELLS"    ,cluster_N_BAD_HV_CELLS    ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_PTD" 		        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_PTD" 		        ,cluster_PTD               ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_MASS" 		        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_MASS" 		        ,cluster_MASS 		       ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"cluster_SECOND_TIME" 	    ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "cluster_SECOND_TIME" 	    ,cluster_SECOND_TIME 	   ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"CalibratedE"                ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "CalibratedE"               ,CalibratedE               ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"ResponseMeasure"            ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "ResponseMeasure"           ,ResponseMeasure           ); }   \
if ( std::find(BNAMES.begin(),BNAMES.end(),"ResponsePrediction"         ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "ResponsePrediction"        ,ResponsePrediction        ); } 
//if ( std::find(BNAMES.begin(),BNAMES.end(),"ResponseFluctuation"        ) != BNAMES.end() ) { Utils::Tree:: createBranch(MTREE, "ResponseFluctuation"       ,ResponseFluctuation       ); }
///@}




























































































































































































