#ifndef CLUSTERTREESTATE_H
#define CLUSTERTREESTATE_H
///@brief State of algorithm indicators
namespace ClusterTreeState {
    static const unsigned short bitModuleOff = { 0x00 }; ///< Indicator for "module is off" 
    static const unsigned short bitModuleOn  = { 0x10 }; ///< Indicator for "module is on"
    static const unsigned short bitInitAll   = { 0x01 }; ///< Indicator for "all intializations successful"
    static const unsigned short bitInitOrig  = { 0x02 }; ///< Indicator for "initialization of original tree successful"
    static const unsigned short bitInitAdd   = { 0x04 }; ///< Indicator for "Initialization of additional tree successful"
    static const unsigned short bitInitMerge = { 0x08 }; ///< Indicator for "Initialization of merged tree successfil"
    ///@brief Test if required bit is set
    ///@return @c true if bit is set, else @c false
    ///@param tvar bit pattern to be tested
    ///@param mask mask of bits required to be set 
    static bool hasBit   (unsigned short tvar,unsigned short mask) { return (tvar & mask ) == mask;    }
    ///@brief Test if module is on
    ///@return @c true if bit is set, else @c false
    ///@param tvar bit pattern to be tested 
    static bool moduleOn (unsigned short tvar) { return hasBit(tvar,bitModuleOn ); }
    ///@brief Test all initializations
    ///@return @c true if successful, else @c false
    ///@param tvar bit pattern to be tested
    static bool initAll  (unsigned short tvar) { return hasBit(tvar,bitInitAll  ); }
    ///@brief Test initialization of additional tree
    ///@return @c true if successful, else @c false
    ///@param tvar bit pattern to be tested
    static bool initAdd  (unsigned short tvar) { return hasBit(tvar,bitInitAdd  ); }
    ///@brief Test initialization of merged tree
    ///@return @c true if successful, else @c false
    ///@param tvar bit pattern to be tested    
    static bool initMerge(unsigned short tvar) { return hasBit(tvar,bitInitMerge); }
    ///@brief Test initialization of original tree
    ///@return @c true if successful, else @c false
    ///@param tvar bit pattern to be tested
    static bool initOrig (unsigned short tvar) { return hasBit(tvar,bitInitOrig ); }
} // <anonymous>
#endif