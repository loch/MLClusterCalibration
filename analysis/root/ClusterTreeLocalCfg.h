#ifndef CLUSTERTREELOCALCFG_H
#define CLUSTERTREELOCALCFG_H

#include "Config.h"

#include <string>

namespace Config {
    namespace Directory {
        static const std::string home = ".";
        static const std::string work = home;
        static const std::string test = home;
#if defined(OS_WINDOWS) && OS_WINDOWS == 1
        static const std::string data = "C:/Users/loch/MLTopoCluster/MLClusterCalibration/analysis/data";
#else
        static const std::string data = "/eos/home-l/loch/MLClusterCalibration/data.04.04.2022";
#endif
    } // Config::Directory
    namespace File {
        namespace Input {
            static const std::string originalData   = "JZ.topo-cluster.root"; 
            static const std::string additionalData = "ml_results.06.21.2022.root"; 
        } // Config::File::Input
        namespace Output {
            static const std::string mergedData     = "ml_jets.root";
        } // Config::File::Output
    } // Config::File
    namespace Path {
        static const std::string originalData   = Config::Directory::data+std::string("/")+Config::File::Input::originalData;
        static const std::string additionalData = Config::Directory::data+std::string("/")+Config::File::Input::additionalData;
        static const std::string mergedData     = Config::Directory::work+std::string("/")+Config::File::Output::mergedData;
    } // Config::Path
    namespace Tree {
        static const std::string originalName   = "ClusterTree";
        static const std::string additionalName = originalName;
        static const std::string mergedName     = originalName;   
    } // Config::Tree
} // Config
#endif
