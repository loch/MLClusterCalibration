// -*- c++ -*-
#ifndef UTILS_H
#define UTILS_H

///////////////////
// ROOT includes //
///////////////////

#include "TTree.h"
#include "TFile.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TList.h"
#include "TCollection.h"
#include "TChain.h"
#include "TChainElement.h"

////////////////////////////
// C++ standard container //
////////////////////////////

#include <string>
#include <vector>
#include <map>
#include <tuple>

/////////////////////////////
// C++ standard algorithms //
/////////////////////////////

#include <algorithm>

#if defined(__cplusplus ) && __cplusplus > 201402L 
#include <optional>
#define  _cplusplus_have_optional
#endif

/////////
// I/O //
/////////

#include <iostream>

///////////////////
// C++ C-headers //
///////////////////

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>

////////////
// Macros //
////////////

#define _KEY_NAME( NAME )      { NAME, #NAME }
#define _KEY_CHAR( NAME, MAP ) { NAME, MAP.at( NAME ).c_str() } 
#define _NAME_KEY( NAME )      { #NAME, NAME }

///////////////
// Utilities //
///////////////
namespace Utils {
  namespace Format {
    namespace Dictionary { 
      namespace Bool {
        static const std::map<bool,std::string> toName  = { _KEY_NAME( true )        , _KEY_NAME( false )         };
	      static const std::map<bool,const char*> toChar  = { _KEY_CHAR( true, toName ), _KEY_CHAR( false, toName ) };
	      static const std::map<std::string,bool> toState = { _NAME_KEY( true )        , _NAME_KEY( false )         };
	      static const std::map<bool,std::string> stateToName = { { true, "on" }, { false, "off" } };
	      static const std::map<bool,const char*> stateToChar = { { true, stateToName.at(true).c_str() }, { false, stateToName.at(false).c_str() } };
	      static const std::map<std::string,bool> nameToState = { { stateToName.at(true), true }, { stateToName.at(false), false } };
        static const std::map<bool,std::string> typeToName  = { { true, "Particle" }, { false, "Jet"  } };
        static const std::map<bool,const char*> typeToChar  = { { true, "Particle" }, { false, "Jet"  } };
        static const std::map<std::string,bool> typeToState = { { "Particle", true }, { "Jet", false  } };
      } // Utils::Format::Dictionary::Bool
    } // Utils::Format::Dictionary
    namespace Bool {
      namespace Flag {
        static const std::string& toName (bool bf) { return Dictionary::Bool::toName.at(bf); }
        static const char*        toChar (bool bf) { return Dictionary::Bool::toChar.at(bf); }
        static bool               toState(const std::string& sstate) { 
	        const auto fiter(Dictionary::Bool::toState.find(sstate)); return fiter != Dictionary::Bool::toState.end() ? fiter->second : false; 
        }
      } // Utils::Format::Bool::Flag
      namespace State {
        static const std::string& toName (bool bf) { return Dictionary::Bool::stateToName.at(bf); }
        static const char*        toChar (bool bf) { return Dictionary::Bool::stateToChar.at(bf); }
        static bool               toState(const std::string& sstate) { 
	        const auto fiter(Dictionary::Bool::nameToState.find(sstate)); return fiter != Dictionary::Bool::nameToState.end() ? fiter->second : false; 
        }
      } // Utils::Format::Bool::State
      namespace Type {
        static const std::string& toName (bool bf) { return Dictionary::Bool::typeToName.at(bf); }
        static const char*        toChar (bool bf) { return Dictionary::Bool::typeToChar.at(bf); }
        static bool               toState(const std::string& sstate) { 
	        const auto fiter(Dictionary::Bool::typeToState.find(sstate)); return fiter != Dictionary::Bool::typeToState.end() ? fiter->second : false; 
        } // Utils::Format::Bool::Type
      } // Utils::Format::Bool::Type
      static const std::string& toName (bool bf)                   { return Flag::toName(bf);      }
      static const char*        toChar (bool bf)                   { return Flag::toChar(bf);      }
      static bool               toState(const std::string& sstate) { return Flag::toState(sstate); } 
#ifdef _cplusplus_have_optional
      std::optional<bool>  toValidState(const std::string& sstate) { 
	const auto fiter(Dictionary::Bool::toState.find(sstate)); 
	return fiter == Dictionary::Bool::toState.end() ? std::nullopt : std::optional(fiter->second); 
      }
#endif
    } // Utils::Format::Bool
    namespace File { 
      static std::string removePath(const std::string& fileName) {
	      auto ipos = fileName.find_last_of('/'); 
        return ipos == std::string::npos ? fileName : fileName.substr(ipos+1); 
      }
    } // Utils::Format::File
  } // Utils::Format
 /*  static TTree* mergeTrees(TTree* oTree,const std::string& fTreeName,const std::string& fFileName) {
    // make a friend first
    // auto fptr = oTree->AddFriend(fTreeName.c_str(),fFileName.c_str());
    // TTree* fTree = nullptr; 
    // if ( fptr != nullptr ) { 
    //     fTree = fptr->GetTree(); 
    //     printf("[Utils::mergeTrees] INFO added tree \042%s\042 (%lli entries) as friend to tree \042%s\042 (%lli entries)\n",
    //             fTree->GetName(),fTree->GetEntries(),oTree->GetName(),oTree->GetEntries());
    // } else {
    //     printf("[Utils::mergeTrees] ABRT failed to merge tree \042%s\042/\042%s\042  with tree \042%s\042\n",
    //             fFileName.c_str(),fTreeName.c_str(),oTree->GetName());
    //     return (TTree*)0;
    // }
    // printf("[mergeTrees] INFO merge %lli entries from tree \042%s\042 with %lli entries from tree \042%s\042\n",
    //         oTree->GetEntries(),oTree->GetName(),fTree->GetEntries(),fTree->GetName()); 
    // return oTree->CloneTree(std::min(fTree->GetEntries(),oTree->GetEntries())); 
    
    // clone tree and add branch
    TTree* cTree = oTree->CloneTree();
    float calibratedE = { 0. };
    cTree->Branch("CalibratedE",&calibratedE,"CalibratedE/F");
    // get predictions
    TFile* fFile = new TFile(fFileName.c_str(),"READ");
    TTree* fTree = fFile->Get<TTree>(fTreeName.c_str());
    float calibratedEPredict = { 0. };
    TBranch* cEPBranch = { (TBranch*)0 };
    fTree->SetBranchAddress("CalibratedE",&calibratedEPredict,&cEPBranch);  
    Long64_t kentries(cTree->GetEntries());
    Long64_t fentries(fTree->GetEntries());
    Long64_t nentries(std::min(kentries,fentries));
    for ( Long64_t ientry(0); ientry<nentries; ++ientry ) {
      if ( fTree->LoadTree(ientry) < 0 ) { break; }
      fTree->GetEntry(ientry);
      calibratedEPredict = calibratedE;
      cTree->Fill();
      
    }
    return cTree;
  } */
  namespace Tree {
    // known variable types (only c++ types here!)
    template <class T> std::string leafList(const std::string& varName,T& /*data*/) { return ""; }
    template<> std::string leafList(const std::string& varName,char&               /*data*/) { static std::string varType = "/C"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,unsigned char&      /*data*/) { static std::string varType = "/b"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,short&              /*data*/) { static std::string varType = "/S"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,unsigned short&     /*data*/) { static std::string varType = "/s"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,int&                /*data*/) { static std::string varType = "/I"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,unsigned int&       /*data*/) { static std::string varType = "/i"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,float&              /*data*/) { static std::string varType = "/F"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,double&             /*data*/) { static std::string varType = "/D"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,long long&          /*data*/) { static std::string varType = "/L"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,unsigned long long& /*data*/) { static std::string varType = "/l"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,long&               /*data*/) { static std::string varType = "/G"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,unsigned long&      /*data*/) { static std::string varType = "/g"; return varName+varType; }
    template<> std::string leafList(const std::string& varName,bool&               /*data*/) { static std::string varType = "/o"; return varName+varType; }
    //
    template<class T> static bool allocateBranch(TTree* ptree,const std::string& varName,T& var,TBranch*& pBranch) { 
      //printf("[Utils::Tree::allocateBranch(...)] DEBG Tree <%s> branch name \042%s\42 address 0x%p\n",ptree->GetName(),varName.c_str(),(void*)ptree->FindLeaf(varName.c_str())); 
      if ( ptree->FindLeaf(varName.c_str()) != nullptr ) { ptree->SetBranchAddress(varName.c_str(),&var,&pBranch); return true; } else { return false; }  
    } 
    template<class T> static bool allocateBranch(TTree* ptree,const std::string& varName,T& var,TBranch*& pbranch,std::vector<std::string>& foundLeaves,std::vector<std::string>& missedLeaves) {
      if ( allocateBranch(ptree,varName,var,pbranch ) ) { foundLeaves.push_back(varName); return true; } else { missedLeaves.push_back(varName); return false; } 
    } 
    template<class T> static TBranch* createBranch(TTree* ptree,const std::string& varName,T& var) { return ptree->Branch(varName.c_str(),&var,leafList< T >(varName,var).c_str()); } 
   } // namespace Utils::Tree
  namespace String {
    static bool ends_with(const std::string& value,const std::string& ending) { 
      return value.length() >= ending.length() && std::equal(ending.rbegin(),ending.rend(),value.rbegin());
    }
    static std::string merge(const std::string& aname,const std::string& bname,const std::string sep="/") { 
      return !ends_with(aname,sep) ? aname+sep+bname : aname+bname;
    } 
    static bool is_equal(const std::string& astr,const std::string& bstr) { 
      return std::equal(astr.begin(),astr.end(),bstr.begin(),bstr.end(),[](char a, char b) { return tolower(a) == tolower(b); });
    }
    template<class L> static bool in_list(const std::string& a,const L& alist,bool caseSensitive=false) {
      if ( caseSensitive ) { return std::find(alist.begin(),alist.end(),a) != alist.end(); }
      bool inlist(false);
      auto flist = alist.begin();
      while ( flist != alist.end() && !inlist ) { inlist = is_equal(a,*flist); ++flist; }
      return inlist; 
    }
    static const char* print_it(const std::string& xstr) { return xstr.c_str(); }
  } // namespace String
  
  ///////////////////
  // File handling //
  ///////////////////

  namespace File {
    namespace Debug {
      namespace Default {
	static const bool on  = { true };
      } // Utils::File::Debug::Default
      static bool on = { Default::on };
    } // Utils::File::Debug
    namespace Directory {
      namespace Default { 
	static const std::string name = { "./" }; // directory containing files
      } // namespace Utils;:File::Directory::Default
    } // namespace Utils::File::Directory
    namespace Extension { 
      namespace Default { 
	static const std::string name = { ".root" }; // file name extension
      } // namespace Utils::File::Extension::Default
      // checks if file has requested etenstion in name
      static bool check(const std::string& fname,const std::string& extension=Default::name) { return String::ends_with(fname,extension); }
    } // namespace Utils::File::Extension
    namespace Mode { 
      static const std::string read     = { "READ"     }; // open file for reading 
      static const std::string recreate = { "RECREATE" }; // open file for writing (overwrite if file exists)
      static const std::string write    = { "WRITE"    }; // open file for writing (fails if file exists)
      static const std::string append   = { "APPEND"   }; // open file for writing (appends new data if file exists)
      namespace Default { 
	static const std::string mode = read;           // standard access
      } // namespace Utils::File::Mode::Default
    } // namespace Utils::File::Mode
    namespace Name {
      namespace Input { 
	// known file names
	static const std::vector<std::string> known = { "JZ0.topo-cluster.root", "JZ1.topo-cluster.root", "JZ2.topo-cluster.root", "JZ3.topo-cluster.root" };
      } // namespace Utils::File::Name::Input
      namespace Pattern {
	      static const std::vector<std::string> excluded = { ".sys.v#", ".sys", "v#" }; // eos tags shoud not be included in file names
	      static bool reject(const std::string& fname,const std::vector<std::string>& excl=excluded) { for ( const auto& sp : excl ) { if ( fname.find(sp) != std::string::npos ) { return true; } } return false; }  
      } // namespcae Utils::File::Name::Pattern
    } // namespace Utils::File::Name
    namespace Type { 
      template<class OBJ> static TSystemFile* ptr(OBJ* optr)         { return optr != nullptr ? (TSystemFile*)(optr) : nullptr; }
      template<class OBJ> static bool         isDirectory(OBJ* optr) { TSystemFile* fptr(ptr(optr)); return fptr != nullptr && fptr->IsDirectory(); }
    } // namespace Utils::File::Type
    namespace Directory {
      static std::vector<TSystemFile*>  getFiles(const std::string& dname=File::Directory::Default::name,const std::string& ename=File::Extension::Default::name) { 
	TSystemDirectory tdir(dname.c_str(),dname.c_str());
	TList*           flst(tdir.GetListOfFiles());
	printf("[Utils::File::Directory::getFiles(...)][%s] DEBG system directory \042%s\042 contains %i files\n",dname.c_str(),tdir.GetName(),flst->GetEntries());
	std::vector<TSystemFile*> ffiles;
	if ( flst != nullptr ) { 
	  TIter iter(flst); TSystemFile* fptr = nullptr; 
	  while ( iter() != nullptr ) { 
	    fptr = File::Type::ptr(*iter); 
	    if ( fptr != nullptr                               && 
		 !File::Name::Pattern::reject(fptr->GetName()) && 
		 File::Extension::check(fptr->GetName(),ename) ) { ffiles.push_back(fptr); } } 
	}
	for ( auto fptr : ffiles ) { printf("[Utils::File::Directory::getFiles(...)][%s] INFO file \042%s\042 accepted\n",tdir.GetName(),fptr->GetName()); }
	return ffiles;
      }
      static std::vector<TFile*> allocateFiles(const std::string& dname=File::Directory::Default::name,const std::string& ename=File::Extension::Default::name,const std::string& access=File::Mode::Default::mode) { 
	std::vector<TSystemFile*> ffiles(getFiles(dname,ename));
	std::vector<TFile*>       tfiles; tfiles.reserve(ffiles.size());
	auto ifiles(ffiles.begin()); 
	while ( ifiles != ffiles.end() && *ifiles != nullptr ) { TFile* fptr = new TFile((*ifiles)->GetName(),access.c_str()); if ( fptr != nullptr ) { tfiles.push_back(fptr); } } 
	return tfiles;
      }
      static std::vector<TFile*> allocateFiles(const std::vector<std::string>& fileList,const std::string& access=File::Mode::Default::mode) {
	std::vector<TFile*> tfiles;
	TFile* fptr = nullptr;
	for ( const auto& fn : fileList ) { 
	  if ( (fptr = new TFile(fn.c_str(),access.c_str())) != nullptr ) { 
	    tfiles.push_back(fptr); 
	  } else { 
	    printf("[File::allocate(...)] WARN cannot open or create file with name \042%s\042 and access %s, skip!\n",fn.c_str(),access.c_str()); 
	  }
	}
	return tfiles; 
      }
      static std::vector<std::string> find(const std::string& dname=File::Directory::Default::name,const std::string& ename=File::Extension::Default::name,bool fullPath=false) { 
	std::vector<TSystemFile*> ffiles(getFiles(dname,ename));
	printf("[Utils::File::Directory::find(...)] DEBG %zu number of files with extension \042%s\042 found in directory \042%s\042\n",ffiles.size(),ename.c_str(),dname.c_str());  
	std::vector<std::string>  nfiles;
	for ( auto fptr : ffiles ) { if ( fptr != nullptr ) { if ( fullPath ) { nfiles.push_back(String::merge(dname,fptr->GetName())); } else { nfiles.push_back(fptr->GetName()); } } }
	return nfiles; 
      }
    } // namespace Utils::File::Directory
    namespace List {
      template<class CONTAINER> static CONTAINER inChain(TChain* pchain) {
	      CONTAINER files;
	      // get list of files
	      auto lf(pchain->GetListOfFiles());
	      if ( lf == nullptr ) { printf("[Utils::File::List::inChain()] WARN chain %s: cannot find any files\n",pchain->GetName()); return files; }
	      // store file names
	      TIter fiter(lf); while ( fiter() != nullptr ) { TChainElement* pe = static_cast<TChainElement*>(*fiter); if ( pe != nullptr ) { files.push_back({ pe->GetTitle() }); } }
	      return files;
      }
    } // namespace Utils::File::List
    namespace Category {
      typedef std::string Name; 
      enum Type { JZ0=0x01, JZ1=0x02, JZ2=0x04, JZ3=0x08, UNKNOWN=0x00 };
      namespace Dictionary { 
	static const std::map<Type,Name>        typeToName = { _KEY_NAME( JZ0 )            , _KEY_NAME( JZ1 )            , _KEY_NAME( JZ2 )            , _KEY_NAME( JZ3 )            , _KEY_NAME( UNKNOWN )             };
	static const std::map<Name,Type>        nameToType = { _NAME_KEY( JZ0 )            , _NAME_KEY( JZ1 )            , _NAME_KEY( JZ2 )            , _NAME_KEY( JZ3 )            , _NAME_KEY( UNKNOWN )             };
	static const std::map<Type,const char*> typeToChar = { _KEY_CHAR( JZ0, typeToName ), _KEY_CHAR( JZ1, typeToName ), _KEY_CHAR( JZ2, typeToName ), _KEY_CHAR( JZ3, typeToName ), _KEY_CHAR( UNKNOWN, typeToName ) };
      } // Utils::File::Category:Dictionary
      // translations
      static const Name& name     (Type t)        { return Dictionary::typeToName.at(t); }
      static const char* character(Type t)        { return Dictionary::typeToChar.at(t); }
      static       Type  type     (const Name& n) { const auto f(Dictionary::nameToType.find(n)); return f != Dictionary::nameToType.end() ? f->second : UNKNOWN; }
      // extractions
      static std::string extract(const std::string& fileName,bool knownOnly=true) { std::string fcat(Utils::Format::File::removePath(fileName)); return knownOnly ? fcat.substr(0,fcat.find_first_of('.')) : fcat.substr(0,fcat.find_first_of('.')); }
    } // namespace Utils::File::Category
  } // namespace Utils::File
  namespace Processing {
    enum class Ticker { FIXED= 0x01, DYNAMIC = 0x02, LOGDYNAMIC = 0x10 | DYNAMIC, UNKNOWN = 0x00 };
    template<class T> static bool ticker(T ievt,bool firstEvent,Ticker t=Ticker::LOGDYNAMIC,T offSet=0,T maxStep=1000) {
      if ( firstEvent ) {
        if ( t != Ticker::LOGDYNAMIC ) { 
            if ( t == Ticker::FIXED) { printf("[Utils::Processing::ticker(...)] WARN ticker specification Ticker::FIXED not yet implemented\n"); } 
            else if ( t == Ticker::DYNAMIC ) { printf("[Utils::Processing::ticker(...)] WARN ticker specification Ticker::DYNAMIC not yet implemented\n"); }
            else { printf("[Utils::Processing::ticker(...)] WARN unknown ticker specification\n"); }
        }
        return true; 
      } 
      T kevt(ievt+offSet);
      T istep = std::min( { maxStep, static_cast< T >(std::pow(10,std::log10(static_cast<double>(kevt)))) } );
      return (kevt % istep) == 0; 
    }
    template<class T> static int ndigits(T value) { return value != T(0) ? static_cast<int>(std::log10(static_cast<double>(value)))+1 : 1; }
  } // namespace Utils::Processing

  ///////////////////////////
  // Printing and messages //
  ///////////////////////////

    /* namespace Message {
        enum Level { INFO = 0x01, WARNING = 0x02, ERROR = 0x04, DEBUG = 0x08, UNKNOWN = 0x00 };
        static const std::map<Level,std::string> levelToName = { _KEY_NAME( INFO ), _KEY_NAME( WARNING ) _KEY_NAME( ERROR ), _KEY_NAME( DEBUG ), _KEY_NAME( UNKNOWN ) };
        static const std::map<Level,const char*> levelToChar = { _KEY_CHAR( INFO, levelToName ), _KEY_CHAR( WARNING, levelToName ), _KEY_CHAR( ERROR, levelToName ), _KEY_CHAR( DEBUG, levelToName ), _KEY_CHAR( UNKNOWN, levelToName ) };
        static const std::map<std::string,Level> nameToLevel = { _NAME_KEY( INFO ), _NAME_KEY( WARNING ) _NAME_KEY( ERROR ), _NAME_KEY( DEBUG ), _NAME_KEY( UNKNOWN ) };
        class Format {
        public:
          Format(const std::string& module,const std::string& method="");
          virtual ~Format() { }
          // configuration
          void setMaxMessages(Level l=INFO,int nmsg=500);
          void setFieldWidthEntry  (int fwidth);
          void setFieldWidthModule (int fwidth);
          void setFieldWidthMethod (int fwidth);
          void setFieldWidthMessage(int fwidth);
          // add/print a line
          virtual std::string format(const std::string& fmtStr,...);
          virtual std::string format(int nentry,const std::string& fmtStr,...);
          virtual std::string format(int nentry,const std::string& fmtStr,...);
          virtual std::string getLine(const std::string& fmtStr,...);
          virtual std::string getLine(int nentry,const std::string& fmtStr,...);
          virtual void printLine(const std::string& fmtStr,...);
          virtual void printLine(int nentry,const std::string& fmtStr,...);
          // stream a line
          virtual std::ostream& toStream() const;
          virtual std::ostream& toStream(const std::string& fmtStr,...);
          virtual std::ostream& toStream(std::ostream& ostr,int nentry,const std::string& fmtStr,...);
          // print all lines
          virtual std::vector<std::string> lines()  const; 
          virtual std::ostream&            stream() const;
          // description of this module and other internal data
          virtual std::vector<std::string> decription() const;
          virtual int numberOfLines() const;
        private:
          // -- message store
          std::map<std::tuple<std::string,Level>,int> _msgStore;
          // -- max message counts
          std::map<Level,int> _maxMsg = { { INFO, 500 }, { WARNING, 500 }, { ERROR, 500 }, { DEBUG, 500 }, { UNKNOWN, 500 } };
          // -- module and method name
          std::string _module = { "Unknown" };
          std::string _method = { ""        };
          // -- helper and local store
          char _buffer[256];            // buffer for formatting
          int _fWidthEntry   = { 7 };   // width of entry counter
          int _fWidthModule  = { 0 };   // width of module name
          int _fWidthMethod  = { 0 };   // width of method name
          int _fWidthMessage = { 0 };
          static std::string _separator;     // separator 
          static std::string _leftBracket;   // left bracket
          int f_width(const std::vector<std::string>& vstr);
        };
    } // namespace Utils::Print
 */
  namespace Random {
    static std::string string(int nchars=16) {
      static std::string _charSet = { "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" }; 
      static int         _charLen = _static_cast<int>(charSet.length()); 
      srand(time(NULL));
      std::string rstr;
      for ( int i(0); i<nchars; ++i ) { rtsr += _charSet.at(rand()%_charLen); }
      return rstr;
    }
  } // Utils::Random
} // namespace Utils
// std::ostream& operator<<(std::ostream& ostr,const Utils::Messages::Print& pmodule) { ostr << pmodule.stream(); return ostr; }
#endif
