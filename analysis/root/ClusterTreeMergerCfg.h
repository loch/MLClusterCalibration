// -*- c++ -*-
#ifndef CLUSTERTREEMERGERCFG_H
#define CLUSTERTREEMERGERCFG_H

#include <TTree.h>
#include <TFile.h>

#include "Utils.h"

#include <string>

#include <algorithm>

#include <cctype>
#include <cstdio>

namespace Config {
  namespace Tree {
    static const std::vector<std::string> readModes   = { "read", "update" };
    static const std::vector<std::string> noReadModes = { "recreate", "new", "create" };
    static bool readAccess (const std::string& faccess) { return Utils::String::in_list(faccess,readModes  ,false); }
    static bool writeAccess(const std::string& faccess) { return Utils::String::in_list(faccess,noReadModes,false); }
  } // Config::Tree
  struct TreeDescription {
    std::string treeName   = { "" };
    std::string fileName   = { "" };
    std::string fileAccess = { "READ"  };
    TTree*      treePtr    = { nullptr };
    TFile*      filePtr    = { nullptr };
    int         nLeaves    = { -1 };
    int         nBranches  = { -1 };
    //
    TreeDescription() { }
    TreeDescription(const std::string& tname,const std::string& fname,const std::string& faccess="READ") : treeName(tname), fileName(fname), fileAccess(faccess) {
      filePtr = new TFile(fileName.c_str(),fileAccess.c_str());
      if ( filePtr == nullptr ) { printf("[TreeDescription] WARN cannot access file \042%s\042 in mode %s\n",fileName.c_str(),fileAccess.c_str()); return; }
      printf("[Config::TreeDescription][%s] INFO opened file \042%s\042\n",fileAccess.c_str(),filePtr->GetName()); 
      if ( Tree::readAccess(fileAccess) ) { 
	treePtr = (TTree*)filePtr->FindObjectAny(treeName.c_str());
	if ( treePtr != nullptr ) { 
	  nLeaves = static_cast<int>(treePtr->GetListOfLeaves()->GetEntries()); nBranches = static_cast<int>(treePtr->GetListOfBranches()->GetEntries());
	  printf("[Config::TreeDescription][%s] INFO tree <%s> allocated at 0x%p in file \042%s\042\n",fileAccess.c_str(),treePtr->GetName(),(void*)treePtr,filePtr->GetName()); 
	} else {
	  printf("[Config::TreeDescription][%s] WARN tree <%s> not found in in file \042%s\042\n",fileAccess.c_str(),treeName.c_str(),filePtr->GetName()); 
	}
      } else { 
	treePtr = new TTree(treeName.c_str(),treeName.c_str()); 
	printf("[Config::TreeDescription][%s] INFO tree <%s> created at 0x%p in file \042%s\042\n",fileAccess.c_str(),treePtr->GetName(),(void*)treePtr,filePtr->GetName()); 
      }
      if ( treePtr == nullptr ) { printf("[Config::TreeDescription] WARN cannot access tree \042%s\042 in file \042%s\042 mode <%s>\n",treeName.c_str(),filePtr->GetName(),fileAccess.c_str()); return; }
    }
    //
    TreeDescription(const TreeDescription& t) : treeName(t.treeName), fileName(t.fileName), fileAccess(t.fileAccess), treePtr(t.treePtr), filePtr(t.filePtr), nLeaves(t.nLeaves), nBranches(t.nBranches) { }
    TreeDescription& operator=(const TreeDescription& t) {
      if ( this == &t ) { return *this; }
      treeName   = t.treeName;
      fileName   = t.fileName;
      fileAccess = t.fileAccess;
      treePtr    = t.treePtr; 
      filePtr    = t.filePtr;
      nLeaves    = t.nLeaves;
      nBranches  = t.nBranches;
      return *this; 
    } 
    //
    static TTree* getTree(TreeDescription& t) {
      if ( t.treePtr != nullptr ) { return t.treePtr; } 
      if ( t.filePtr == nullptr ) { 
        t.filePtr = new TFile(t.fileName.c_str(),t.fileAccess.c_str());
        if ( t.filePtr == nullptr ) { printf("[TreeDescription] WARN cannot access file \042%s\042 in mode %s\n",t.fileName.c_str(),t.fileAccess.c_str()); return (TTree*)0; }
      }
      if ( Tree::readAccess(t.fileAccess) ) { 
          t.treePtr = t.filePtr->Get<TTree>(t.treeName.c_str()); 
      } else { 
          t.treePtr = new TTree(t.treeName.c_str(),t.treeName.c_str()); 
      }
      return t.treePtr; 
    }
  };
}
struct ClusterTreeMergerCfg {
  Config::TreeDescription originalTree   = { Config::TreeDescription() };
  Config::TreeDescription additionalTree = { Config::TreeDescription() };
  Config::TreeDescription mergedTree     = { Config::TreeDescription() };
  ClusterTreeMergerCfg() { }
  ClusterTreeMergerCfg(const ClusterTreeMergerCfg& ct) : originalTree(ct.originalTree), additionalTree(ct.additionalTree), mergedTree(ct.mergedTree) {}
  ClusterTreeMergerCfg& operator=(const ClusterTreeMergerCfg& ct) {
    if ( this == &ct ) { return *this; }
    originalTree   = ct.originalTree;
    additionalTree = ct.additionalTree;
    mergedTree     = ct.mergedTree;
    return *this;
  }
};
namespace Config {
  static void print(const ClusterTreeMergerCfg& cfg) {
    printf("[ClusterTreeMerger][config] Original tree (inputs):\n");
    printf("                        Address ....... 0x%p\n",(void*)cfg.originalTree.treePtr  );
    printf("                        Name .......... %s\n",cfg.originalTree.treeName.c_str()  );
    printf("                        File address .. 0x%p\n",(void*)cfg.originalTree.filePtr  );
    printf("                        File name ..... %s\n",cfg.originalTree.fileName.c_str()  );
    printf("                        File access ... %s\n",cfg.originalTree.fileAccess.c_str()); 
    printf("                        # leaves ...... %i\n",cfg.originalTree.nLeaves           );
    printf("                        # branches .... %i\n",cfg.originalTree.nBranches         );
    printf("[ClusterTreeMerger][config] Additional tree (results):\n");
    printf("                        Address ....... 0x%p\n",(void*)cfg.additionalTree.treePtr  );
    printf("                        Name .......... %s\n",cfg.additionalTree.treeName.c_str()  ); 
    printf("                        File address .. 0x%p\n",(void*)cfg.additionalTree.filePtr  );
    printf("                        File name ..... %s\n",cfg.additionalTree.fileName.c_str()  ); 
    printf("                        File access ... %s\n",cfg.additionalTree.fileAccess.c_str()); 
    printf("                        # leaves ...... %i\n",cfg.additionalTree.nLeaves           );
    printf("                        # branches .... %i\n",cfg.additionalTree.nBranches         );
    printf("[ClusterTreeMerger][config] Merged tree (output):\n");
    printf("                        Address ....... 0x%p\n",(void*)cfg.mergedTree.treePtr  );
    printf("                        Name .......... %s\n",cfg.mergedTree.treeName.c_str()  ); 
    printf("                        File address .. 0x%p\n",(void*)cfg.mergedTree.filePtr  );
    printf("                        File name ..... %s\n",cfg.mergedTree.fileName.c_str()  );
    printf("                        File access ... %s\n",cfg.mergedTree.fileAccess.c_str()); 
    printf("                        # leaves ...... %i\n",cfg.mergedTree.nLeaves           );
    printf("                        # branches .... %i\n",cfg.mergedTree.nBranches         );
  }    
}
#endif
