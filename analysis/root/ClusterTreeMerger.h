// -*- c++ -*-

#ifndef ClusterTreeMerger_h
#define ClusterTreeMerger_h

#include "ClusterTreeDefs.h"
#include "ClusterTreeHdrs.h"
#include "ClusterTreeMergerCfg.h"
#include "ClusterTreeState.h"

///@brief Merging @c ROOT::TTree data structures
///
/// This algorithm merges @c ROOT::TTree data structures. These are defined in a configuration structure
/// @c ClusterTreeMergerCfg, which contains file and tree names and pointers to respective @c ROOT::TFile 
/// and @c ROOT::TTree objects. The algorithm reads the @c OriginalTree tagged data from the input file 
/// containing all input topo-cluster data and the @c AdditionalTree from another input file containing the
/// results of the machine-learned response and calibration. The output is the @c MergedTree, which is filled
/// with the content of both the @c OriginalTree and the @c AdditionalTree by pointing to the respective memory
/// location of the read data. This avoids explicit and potentially costly copying of large amount of data. 
/// The algorithm is turned off/inactive if the number of entries in the @c OriginalTree and the @c AdditionalTree 
/// does not match.      
class ClusterTreeMerger {
protected:
   ///@brief Flags indicates if input data is from particle simulations (@c true) or jets (@c false).
   bool           flag_isParticle = { false                          };
   ///@brief Bitmask reflecting the status of the algorithm
   unsigned short status_mask     = { ClusterTreeState::bitModuleOff };
public :
   ///@brief Pointer to @c OriginalTree input data
   TTree*          fChain   = { (TTree*)0 };   //!
   ///@brief @c OriginalTree input counter (number of trees counter)
   Int_t           fCurrent = { 0 };           //!
   ///@brief Pointer to @c AdditionalTree input data
   TTree*          fChainAdditional = { (TTree*)0 };
   ///@brief Pointer to @c MergedTree output data
   TTree*          fChainMerged     = { (TTree*)0 };
   ///@brief Configuration module for this algorithm
   ClusterTreeMergerCfg cfgTrees = { ClusterTreeMergerCfg() };
   ///@name Tree type indicator
   ///@{
   enum TreeType { OriginalTree = 0x01, AdditionalTree = 0x02, MergedTree = 0x04, UnknownTree = 0x00 }; ///< Tree types
   static const std::map<TreeType,std::string> treeTypeToName;                                          ///< Dictionary for @c TreeType to @c std::string translation
   static const std::map<TreeType,const char*> treeTypeToChar;                                          ///< Dictionary for @c TreeType to @c char* translation
   static const std::map<std::string,TreeType> treeNameToType;                                          ///< Dictionary for @c std::string tp @c TreeType translation
   ///@}

   /////////////////////////////////////
   // Memory allocation for leaf data //
   /////////////////////////////////////

   CLUSTERTREE_VARIABLE_DECL;
   
   //////////////
   // Branches //
   //////////////

   #include "ClusterTreeBranches.icc"

   /////////////
   // Methods //
   /////////////

   ClusterTreeMerger(const ClusterTreeMergerCfg& cfg);
   virtual ~ClusterTreeMerger();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Int_t    GetEntry(TTree* tree,Long64_t entry);   
   virtual Long64_t LoadTree(Long64_t entry);
   virtual Long64_t LoadTree(TTree* tree,Long64_t entry,bool update=false);
   virtual bool     BookBranches(TTree* tree);
   virtual bool     Init(TreeType tt);
   virtual bool     InitOriginalTree(); 
   virtual bool     InitAdditionalTree(); 
   virtual bool     InitMergedTree(); 
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
  

   ///////////////
   // Accessors //
   ///////////////

   TTree* originalTree()   { return cfgTrees.originalTree.treePtr;   } 
   TTree* additionalTree() { return cfgTrees.additionalTree.treePtr; } 
   TTree* mergedTree()     { return cfgTrees.mergedTree.treePtr;     }

   /////////////
   // Helpers //
   /////////////

   bool setup(); 
   bool isParticle() const { return flag_isParticle;  }
   bool isJet()      const { return !flag_isParticle; }
   bool isActive()   const { return ClusterTreeState::moduleOn(status_mask); }
   bool checkBranch(TTree* ptree,const std::string& bname);  
   int  treeTypeWidth() const; 

   ////////////
   // Caches //
   ////////////

protected:

   std::vector<std::string> inputBranchNames;
   std::vector<std::string> missedBranchNames;
   std::vector<std::string> knownBranchNames; 
   std::vector<std::string> bookedBranchNames; 

   bool copyModeFlag = { false };
  
};

#endif

#ifdef ClusterTreeMerger_cxx

///////////
// Setup //
///////////

bool ClusterTreeMerger::setup() {
   // historic
   fChain = cfgTrees.originalTree.treePtr; 
   if ( fChain == nullptr ) { printf("[ClusterTreeMerger] ABRT cannot instantiate object - invalid pointer to original tree\n"); return false; }
   status_mask |= ClusterTreeState::bitInitOrig;
   // additional data
   if ( cfgTrees.additionalTree.treePtr == nullptr || cfgTrees.additionalTree.filePtr == nullptr ) { 
      copyModeFlag = true; 
      printf("[ClusterTreeMerger] WARN invalid configuration of additional input tree (TTree* = 0x%p, TFile* = 0x%p), turn copy mode %s\n",
            (void*)cfgTrees.additionalTree.treePtr,(void*)cfgTrees.additionalTree.filePtr,Utils::Format::Bool::State::toChar(copyModeFlag));
   } else {
      status_mask |= ClusterTreeState::bitInitAdd;
   }
   //  if ( cfgTrees.additionalTree.filePtr == nullptr ) {
   //     cfgTrees.additionalTree.filePtr = new TFile(cfgTrees.additionalTree.fileName.c_str(),cfgTrees.additionalTree.fileAccess.c_str());
   //     if ( cfgTrees.additionalTree.filePtr == nullptr ) {
   //        printf("[ClusterTreeMerger] ABRT cannot open file \042%s\042 with additional tree\n",cfgTrees.additionalTree.fileName.c_str());
   //        return false; 
   //     } else {
   //        cfgTrees.additionalTree.treePtr = cfgTrees.additionalTree.filePtr->Get<TTree>(cfgTrees.additionalTree.treeName.c_str());
   //        if ( cfgTrees.additionalTree.treePtr == nullptr ) {
   //           printf("[ClusterTreeMerger] ABRT cannot access tree \042%s\042 in file \042%s\042\n",
   //                 cfgTrees.additionalTree.treeName.c_str(),cfgTrees.additionalTree.filePtr->GetName());
   //           return false;
   //        }
   //        status_mask |= ClusterTreeState::bitInitAdd; 
   //        fChainAdditional = cfgTrees.additionalTree.treePtr;
   //     }
   //  }
   //
   // output data
   if ( cfgTrees.mergedTree.treePtr == nullptr || cfgTrees.mergedTree.filePtr == nullptr ) {
      printf("[ClusterTreeMerger] ABRT invalid configuration of merged output tree (TTree* = %p or TFile* = %p)\n",(void*)cfgTrees.mergedTree.treePtr,(void*)cfgTrees.mergedTree.filePtr);
   } else {
      status_mask |= ClusterTreeState::bitInitMerge;
   }
   //  if ( cfgTrees.mergedTree.filePtr == nullptr ) { 
   //     cfgTrees.mergedTree.filePtr = new TFile(cfgTrees.mergedTree.fileName.c_str(),cfgTrees.mergedTree.fileAccess.c_str());
   //     if ( cfgTrees.mergedTree.filePtr == nullptr ) {
   //        printf("[ClusterTreeMerger] ABRT cannot open file \042%s\042 for merged tree\n",cfgTrees.mergedTree.fileName.c_str());
   //        return false; 
   //     } else {
   //        cfgTrees.mergedTree.filePtr->cd(); 
   //        cfgTrees.mergedTree.treePtr = new TTree(cfgTrees.mergedTree.treeName.c_str(),cfgTrees.mergedTree.treeName.c_str());
   //        status_mask |= ClusterTreeState::bitInitMerge;
   //        fChainMerged = cfgTrees.mergedTree.treePtr;
   //     }
   //  }
   //
   // print the configuration
   Config::print(cfgTrees); 
   //
   if ( ClusterTreeState::initOrig(status_mask)  && 
        ClusterTreeState::initMerge(status_mask) && 
      ( copyModeFlag || ClusterTreeState::initAdd(status_mask)) ) { status_mask |= ClusterTreeState::bitModuleOn; }
   return ClusterTreeState::moduleOn(status_mask);
}

/////////////////
// Constructor //
/////////////////

ClusterTreeMerger::ClusterTreeMerger(const ClusterTreeMergerCfg& cfg) : cfgTrees(cfg) {
   // Setup 
   if ( !this->setup() ) { 
      printf("[ClusterTreeMerger] WARN Failed to set up module, cannot execute - module will be inactive!\n"); 
      printf("                    Status bits after tree allocation and creation:\n");
      printf("                    Module ...................... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::moduleOn(status_mask)));
      printf("                    Original tree allocated ..... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initOrig(status_mask)));
      printf("                    Additional tree allocated ... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initAdd(status_mask)));
      printf("                    Merged tree booked .......... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initMerge(status_mask)));
      printf("                    Copy mode ................... %s\n",Utils::Format::Bool::State::toChar(copyModeFlag));
      printf("[ClusterTreeMerger] DEBG tree configurations:\n");
      Config::print(cfgTrees);
      printf("");
   } else {
     fChain           = cfgTrees.originalTree.treePtr;
     fChainAdditional = cfgTrees.additionalTree.treePtr;
     fChainMerged     = cfgTrees.mergedTree.treePtr; 
     // declare branches
      status_mask = ClusterTreeState::bitModuleOff;
      if ( InitOriginalTree()   ) { status_mask |= ClusterTreeState::bitInitOrig;  } 
      if ( InitAdditionalTree() ) { status_mask |= ClusterTreeState::bitInitAdd;   } 
      if ( InitMergedTree()     ) { status_mask |= ClusterTreeState::bitInitMerge; }
      if (ClusterTreeState::initOrig(status_mask) && ClusterTreeState::initMerge(status_mask) && (copyModeFlag || ClusterTreeState::initAdd(status_mask)) ) {
         status_mask |= ClusterTreeState::bitInitAll;
         status_mask |= ClusterTreeState::bitModuleOn;
      }
      printf("[ClusterTreeMerger] INFO Status bits after allocating or declaring tree branches:\n");
      printf("                    Module ...................... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::moduleOn(status_mask)));
      printf("                    Original tree allocated ..... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initOrig(status_mask)));
      printf("                    Additional tree allocated ... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initAdd(status_mask)));
      printf("                    Merged tree booked .......... %s\n",Utils::Format::Bool::State::toChar(ClusterTreeState::initMerge(status_mask)));
      printf("                    Copy mode ................... %s\n",Utils::Format::Bool::State::toChar(copyModeFlag));
   }
   if ( !ClusterTreeState::moduleOn(status_mask) || !ClusterTreeState::initAll(status_mask) ) { 
      printf("[ClusterTreeMerger] WARN module is inactive due to failures in tree allocation or generation\n"); 
   }
}

ClusterTreeMerger::~ClusterTreeMerger()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

//////////////
// Get data //
//////////////

Int_t ClusterTreeMerger::GetEntry(Long64_t entry) {
   // Read contents of entry.
   return GetEntry(originalTree(),entry);
}

Int_t ClusterTreeMerger::GetEntry(TTree* tPtr,Long64_t entry) {
   if ( tPtr == nullptr ) return 0;
   return tPtr->GetEntry(entry);
}

Long64_t ClusterTreeMerger::LoadTree(Long64_t entry) { return LoadTree(originalTree(),entry,true); }

Long64_t ClusterTreeMerger::LoadTree(TTree* tPtr,Long64_t entry,bool update) {
   // Set the environment to read one entry
   if ( tPtr == nullptr ) return -5;
   Long64_t centry = tPtr->LoadTree(entry);
   if ( centry < 0 ) return centry;
   if ( update && tPtr->GetTreeNumber() != fCurrent ) {
      fCurrent = tPtr->GetTreeNumber();
      Notify();
   }
   return centry;
}

//////////////////////
// Initialize trees //
//////////////////////

bool ClusterTreeMerger::InitOriginalTree()   { return Init(OriginalTree  ); }
bool ClusterTreeMerger::InitAdditionalTree() { return !copyModeFlag && Init(AdditionalTree); }
bool ClusterTreeMerger::InitMergedTree()     { return Init(MergedTree    ); }

bool ClusterTreeMerger::Init(TreeType tt=OriginalTree) {
//  if ( !ClusterTreeState::moduleOn(status_mask) ) {
//     printf("[ClusterTreeMerger::Init(..)] ABRT module is not properly initialized\n");  
//     return false; 
//  } 
  // get the requested input tree from configuration
  TTree* t = nullptr;
  fCurrent = 0; 
  if ( tt == OriginalTree ) { 
    fCurrent = -1; 
    t        = cfgTrees.originalTree.treePtr;  
  } else if ( tt == AdditionalTree ) { 
    t = cfgTrees.additionalTree.treePtr;
  } else if ( tt == MergedTree ) {
    t = cfgTrees.mergedTree.treePtr;
    if ( BookBranches(t) ) { 
      printf("[ClusterTreeMerger] INFO booked %i branches for tree %s (type %s)\n",(int)t->GetListOfLeaves()->GetEntries(),t->GetName(),treeTypeToChar.at(tt));
      return true; 
    } else {
      printf("[ClusterTreeMerger] WARN failed to book branches for tree %s (type %s)\n",t->GetName(),treeTypeToChar.at(tt)); 
      return false;
    }
   } else {
    printf("[ClusterTreeMerger] ABRT invalid pointer to requested input tree (type %s)\n",treeTypeToChar.at(tt)); 
    return false;
  }
  //  printf("[ClusterTreeMerger] INFO set up branch addresses for tree \042%s\042 (type %s)\n",t->GetName(),treeTypeToChar.at(tt));  
  t->SetMakeClass(1);
  // book keeping all/found/missed branches
  size_t nKnownBranches (knownBranchNames.size());
  size_t nInputBranches (inputBranchNames.size());
  size_t nMissedBranches(missedBranchNames.size());
  // allocate branches
  if ( checkBranch(t,"runNumber"                 ) && t->FindLeaf("runNumber"                 ) != nullptr ) { t->SetBranchAddress("runNumber"                 ,&runNumber                 ,&b_runNumber                 ); }
  if ( checkBranch(t,"eventNumber"               ) && t->FindLeaf("eventNumber"               ) != nullptr ) { t->SetBranchAddress("eventNumber"               ,&eventNumber               ,&b_eventNumber               ); }
  if ( checkBranch(t,"avgMu"                     ) && t->FindLeaf("avgMu"                     ) != nullptr ) { t->SetBranchAddress("avgMu"                     ,&avgMu                     ,&b_avgMu                     ); }
  if ( checkBranch(t,"nPrimVtx"                  ) && t->FindLeaf("nPrimVtx"                  ) != nullptr ) { t->SetBranchAddress("nPrimVtx"                  ,&nPrimVtx                  ,&b_nPrimVtx                  ); }
  if ( checkBranch(t,"truthE"                    ) && t->FindLeaf("truthE"                    ) != nullptr ) { t->SetBranchAddress("truthE"                    ,&truthE                    ,&b_truthE                    ); }
  if ( checkBranch(t,"truthPt"                   ) && t->FindLeaf("truthPt"                   ) != nullptr ) { t->SetBranchAddress("truthPt"                   ,&truthPt                   ,&b_truthPt                   ); }
  if ( checkBranch(t,"truthEta"                  ) && t->FindLeaf("truthEta"                  ) != nullptr ) { t->SetBranchAddress("truthEta"                  ,&truthEta                  ,&b_truthEta                  ); }
  if ( checkBranch(t,"truthPhi"                  ) && t->FindLeaf("truthPhi"                  ) != nullptr ) { t->SetBranchAddress("truthPhi"                  ,&truthPhi                  ,&b_truthPhi                  ); }
  if ( checkBranch(t,"truthPDG"                  ) && t->FindLeaf("truthPDG"                  ) != nullptr ) { t->SetBranchAddress("truthPDG"                  ,&truthPDG                  ,&b_truthPDG                  ); }
  if ( checkBranch(t,"jetCalE"                   ) && t->FindLeaf("jetCalE"                   ) != nullptr ) { t->SetBranchAddress("jetCalE"                   ,&jetCalE                   ,&b_jetCalE                   ); }
  if ( checkBranch(t,"jetCalPt"                  ) && t->FindLeaf("jetCalPt"                  ) != nullptr ) { t->SetBranchAddress("jetCalPt"                  ,&jetCalPt                  ,&b_jetCalPt                  ); }
  if ( checkBranch(t,"jetCalEta"                 ) && t->FindLeaf("jetCalEta"                 ) != nullptr ) { t->SetBranchAddress("jetCalEta"                 ,&jetCalEta                 ,&b_jetCalEta                 ); }
  if ( checkBranch(t,"jetCalPhi"                 ) && t->FindLeaf("jetCalPhi"                 ) != nullptr ) { t->SetBranchAddress("jetCalPhi"                 ,&jetCalPhi                 ,&b_jetCalPhi                 ); }
  if ( checkBranch(t,"jetRawE"                   ) && t->FindLeaf("jetRawE"                   ) != nullptr ) { t->SetBranchAddress("jetRawE"                   ,&jetRawE                   ,&b_jetRawE                   ); }
  if ( checkBranch(t,"jetRawPt"                  ) && t->FindLeaf("jetRawPt"                  ) != nullptr ) { t->SetBranchAddress("jetRawPt"                  ,&jetRawPt                  ,&b_jetRawPt                  ); }
  if ( checkBranch(t,"jetRawEta"                 ) && t->FindLeaf("jetRawEta"                 ) != nullptr ) { t->SetBranchAddress("jetRawEta"                 ,&jetRawEta                 ,&b_jetRawEta                 ); }
  if ( checkBranch(t,"jetRawPhi"                 ) && t->FindLeaf("jetRawPhi"                 ) != nullptr ) { t->SetBranchAddress("jetRawPhi"                 ,&jetRawPhi                 ,&b_jetRawPhi                 ); }
  if ( checkBranch(t,"jetNConst"                 ) && t->FindLeaf("jetNConst"                 ) != nullptr ) { t->SetBranchAddress("jetNConst"                 ,&jetNConst                 ,&b_jetNConst                 ); }
  if ( checkBranch(t,"truthJetMatchRadius"       ) && t->FindLeaf("truthJetMatchRadius"       ) != nullptr ) { t->SetBranchAddress("truthJetMatchRadius"       ,&truthJetMatchRadius       ,&b_truthJetMatchRadius       ); }
  if ( checkBranch(t,"truthJetE"                 ) && t->FindLeaf("truthJetE"                 ) != nullptr ) { t->SetBranchAddress("truthJetE"                 ,&truthJetE                 ,&b_truthJetE                 ); }
  if ( checkBranch(t,"truthJetPt"                ) && t->FindLeaf("truthJetPt"                ) != nullptr ) { t->SetBranchAddress("truthJetPt"                ,&truthJetPt                ,&b_truthJetPt                ); }
  if ( checkBranch(t,"truthJetRap"               ) && t->FindLeaf("truthJetRap"               ) != nullptr ) { t->SetBranchAddress("truthJetRap"               ,&truthJetRap               ,&b_truthJetRap               ); }
  if ( checkBranch(t,"truthJetPhi"               ) && t->FindLeaf("truthJetPhi"               ) != nullptr ) { t->SetBranchAddress("truthJetPhi"               ,&truthJetPhi               ,&b_truthJetPhi               ); }
  if ( checkBranch(t,"nCluster"                  ) && t->FindLeaf("nCluster"                  ) != nullptr ) { t->SetBranchAddress("nCluster"                  ,&nCluster                  ,&b_nCluster                  ); }
  if ( checkBranch(t,"clusterIndex"              ) && t->FindLeaf("clusterIndex"              ) != nullptr ) { t->SetBranchAddress("clusterIndex"              ,&clusterIndex              ,&b_clusterIndex              ); }
  if ( checkBranch(t,"cluster_nCells"            ) && t->FindLeaf("cluster_nCells"            ) != nullptr ) { t->SetBranchAddress("cluster_nCells"            ,&cluster_nCells            ,&b_cluster_nCells            ); }
  if ( checkBranch(t,"cluster_nCells_tot"        ) && t->FindLeaf("cluster_nCells_tot"        ) != nullptr ) { t->SetBranchAddress("cluster_nCells_tot"        ,&cluster_nCells_tot        ,&b_cluster_nCells_tot        ); }
  if ( checkBranch(t,"clusterECalib"             ) && t->FindLeaf("clusterECalib"             ) != nullptr ) { t->SetBranchAddress("clusterECalib"             ,&clusterECalib             ,&b_clusterECalib             ); }
  if ( checkBranch(t,"clusterPtCalib"            ) && t->FindLeaf("clusterPtCalib"            ) != nullptr ) { t->SetBranchAddress("clusterPtCalib"            ,&clusterPtCalib            ,&b_clusterPtCalib            ); }
  if ( checkBranch(t,"clusterEtaCalib"           ) && t->FindLeaf("clusterEtaCalib"           ) != nullptr ) { t->SetBranchAddress("clusterEtaCalib"           ,&clusterEtaCalib           ,&b_clusterEtaCalib           ); }
  if ( checkBranch(t,"clusterPhiCalib"           ) && t->FindLeaf("clusterPhiCalib"           ) != nullptr ) { t->SetBranchAddress("clusterPhiCalib"           ,&clusterPhiCalib           ,&b_clusterPhiCalib           ); }
  if ( checkBranch(t,"cluster_sumCellECalib"     ) && t->FindLeaf("cluster_sumCellECalib"     ) != nullptr ) { t->SetBranchAddress("cluster_sumCellECalib"     ,&cluster_sumCellECalib     ,&b_cluster_sumCellECAlib     ); }
  if ( checkBranch(t,"cluster_fracECalib"        ) && t->FindLeaf("cluster_fracECalib"        ) != nullptr ) { t->SetBranchAddress("cluster_fracECalib"        ,&cluster_fracECalib        ,&b_cluster_fracECalib        ); }
  if ( checkBranch(t,"cluster_fracECalib_ref"    ) && t->FindLeaf("cluster_fracECalib_ref"    ) != nullptr ) { t->SetBranchAddress("cluster_fracECalib_ref"    ,&cluster_fracECalib_ref    ,&b_cluster_fracECalib_ref    ); }
  if ( checkBranch(t,"clusterE"                  ) && t->FindLeaf("clusterE"                  ) != nullptr ) { t->SetBranchAddress("clusterE"                  ,&clusterE                  ,&b_clusterE                  ); }
  if ( checkBranch(t,"clusterPt"                 ) && t->FindLeaf("clusterPt"                 ) != nullptr ) { t->SetBranchAddress("clusterPt"                 ,&clusterPt                 ,&b_clusterPt                 ); }
  if ( checkBranch(t,"clusterEta"                ) && t->FindLeaf("clusterEta"                ) != nullptr ) { t->SetBranchAddress("clusterEta"                ,&clusterEta                ,&b_clusterEta                ); }
  if ( checkBranch(t,"clusterPhi"                ) && t->FindLeaf("clusterPhi"                ) != nullptr ) { t->SetBranchAddress("clusterPhi"                ,&clusterPhi                ,&b_clusterPhi                ); }
  if ( checkBranch(t,"cluster_sumCellE"          ) && t->FindLeaf("cluster_sumCellE"          ) != nullptr ) { t->SetBranchAddress("cluster_sumCellE"          ,&cluster_sumCellE          ,&b_cluster_sumCellE          ); }
  if ( checkBranch(t,"cluster_time"              ) && t->FindLeaf("cluster_time"              ) != nullptr ) { t->SetBranchAddress("cluster_time"              ,&cluster_time              ,&b_cluster_time              ); }
  if ( checkBranch(t,"cluster_fracE"             ) && t->FindLeaf("cluster_fracE"             ) != nullptr ) { t->SetBranchAddress("cluster_fracE"             ,&cluster_fracE             ,&b_cluster_fracE             ); }
  if ( checkBranch(t,"cluster_fracE_ref"         ) && t->FindLeaf("cluster_fracE_ref"         ) != nullptr ) { t->SetBranchAddress("cluster_fracE_ref"         ,&cluster_fracE_ref         ,&b_cluster_fracE_ref         ); }
  if ( checkBranch(t,"cluster_EM_PROBABILITY"    ) && t->FindLeaf("cluster_EM_PROBABILITY"    ) != nullptr ) { t->SetBranchAddress("cluster_EM_PROBABILITY"    ,&cluster_EM_PROBABILITY    ,&b_cluster_EM_PROBABILITY    ); }
  if ( checkBranch(t,"cluster_HAD_WEIGHT"        ) && t->FindLeaf("cluster_HAD_WEIGHT"        ) != nullptr ) { t->SetBranchAddress("cluster_HAD_WEIGHT"        ,&cluster_HAD_WEIGHT        ,&b_cluster_HAD_WEIGHT        ); }
  if ( checkBranch(t,"cluster_OOC_WEIGHT"        ) && t->FindLeaf("cluster_OOC_WEIGHT"        ) != nullptr ) { t->SetBranchAddress("cluster_OOC_WEIGHT"        ,&cluster_OOC_WEIGHT        ,&b_cluster_OOC_WEIGHT        ); }
  if ( checkBranch(t,"cluster_DM_WEIGHT"         ) && t->FindLeaf("cluster_DM_WEIGHT"         ) != nullptr ) { t->SetBranchAddress("cluster_DM_WEIGHT"         ,&cluster_DM_WEIGHT         ,&b_cluster_DM_WEIGHT         ); }
  if ( checkBranch(t,"cluster_ENG_CALIB_TOT"     ) && t->FindLeaf("cluster_ENG_CALIB_TOT"     ) != nullptr ) { t->SetBranchAddress("cluster_ENG_CALIB_TOT"     ,&cluster_ENG_CALIB_TOT     ,&b_cluster_ENG_CALIB_TOT     ); }
  if ( checkBranch(t,"cluster_ENG_CALIB_OUT_T"   ) && t->FindLeaf("cluster_ENG_CALIB_OUT_T"   ) != nullptr ) { t->SetBranchAddress("cluster_ENG_CALIB_OUT_T"   ,&cluster_ENG_CALIB_OUT_T   ,&b_cluster_ENG_CALIB_OUT_T   ); }
  if ( checkBranch(t,"cluster_ENG_CALIB_DEAD_TOT") && t->FindLeaf("cluster_ENG_CALIB_DEAD_TOT") != nullptr ) { t->SetBranchAddress("cluster_ENG_CALIB_DEAD_TOT",&cluster_ENG_CALIB_DEAD_TOT,&b_cluster_ENG_CALIB_DEAD_TOT); }
  if ( checkBranch(t,"cluster_CENTER_MAG"        ) && t->FindLeaf("cluster_CENTER_MAG"        ) != nullptr ) { t->SetBranchAddress("cluster_CENTER_MAG"        ,&cluster_CENTER_MAG        ,&b_cluster_CENTER_MAG        ); }
  if ( checkBranch(t,"cluster_FIRST_ENG_DENS"    ) && t->FindLeaf("cluster_FIRST_ENG_DENS"    ) != nullptr ) { t->SetBranchAddress("cluster_FIRST_ENG_DENS"    ,&cluster_FIRST_ENG_DENS    ,&b_cluster_FIRST_ENG_DENS    ); }
  if ( checkBranch(t,"cluster_FIRST_PHI"         ) && t->FindLeaf("cluster_FIRST_PHI"         ) != nullptr ) { t->SetBranchAddress("cluster_FIRST_PHI"         ,&cluster_FIRST_PHI         ,&b_cluster_FIRST_PHI         ); }
  if ( checkBranch(t,"cluster_FIRST_ETA"         ) && t->FindLeaf("cluster_FIRST_ETA"         ) != nullptr ) { t->SetBranchAddress("cluster_FIRST_ETA"         ,&cluster_FIRST_ETA         ,&b_cluster_FIRST_ETA         ); }
  if ( checkBranch(t,"cluster_SECOND_R"          ) && t->FindLeaf("cluster_SECOND_R"          ) != nullptr ) { t->SetBranchAddress("cluster_SECOND_R"          ,&cluster_SECOND_R          ,&b_cluster_SECOND_R          ); }
  if ( checkBranch(t,"cluster_SECOND_LAMBDA"     ) && t->FindLeaf("cluster_SECOND_LAMBDA"     ) != nullptr ) { t->SetBranchAddress("cluster_SECOND_LAMBDA"     ,&cluster_SECOND_LAMBDA     ,&b_cluster_SECOND_LAMBDA     ); }
  if ( checkBranch(t,"cluster_DELTA_PHI"         ) && t->FindLeaf("cluster_DELTA_PHI"         ) != nullptr ) { t->SetBranchAddress("cluster_DELTA_PHI"         ,&cluster_DELTA_PHI         ,&b_cluster_DELTA_PHI         ); }
  if ( checkBranch(t,"cluster_DELTA_THETA"       ) && t->FindLeaf("cluster_DELTA_THETA"       ) != nullptr ) { t->SetBranchAddress("cluster_DELTA_THETA"       ,&cluster_DELTA_THETA       ,&b_cluster_DELTA_THETA       ); }
  if ( checkBranch(t,"cluster_DELTA_ALPHA"       ) && t->FindLeaf("cluster_DELTA_ALPHA"       ) != nullptr ) { t->SetBranchAddress("cluster_DELTA_ALPHA"       ,&cluster_DELTA_ALPHA       ,&b_cluster_DELTA_ALPHA       ); }
  if ( checkBranch(t,"cluster_CENTER_X"          ) && t->FindLeaf("cluster_CENTER_X"          ) != nullptr ) { t->SetBranchAddress("cluster_CENTER_X"          ,&cluster_CENTER_X          ,&b_cluster_CENTER_X          ); }
  if ( checkBranch(t,"cluster_CENTER_Y"          ) && t->FindLeaf("cluster_CENTER_Y"          ) != nullptr ) { t->SetBranchAddress("cluster_CENTER_Y"          ,&cluster_CENTER_Y          ,&b_cluster_CENTER_Y          ); }
  if ( checkBranch(t,"cluster_CENTER_Z"          ) && t->FindLeaf("cluster_CENTER_Z"          ) != nullptr ) { t->SetBranchAddress("cluster_CENTER_Z"          ,&cluster_CENTER_Z          ,&b_cluster_CENTER_Z          ); }
  if ( checkBranch(t,"cluster_CENTER_LAMBDA"     ) && t->FindLeaf("cluster_CENTER_LAMBDA"     ) != nullptr ) { t->SetBranchAddress("cluster_CENTER_LAMBDA"     ,&cluster_CENTER_LAMBDA     ,&b_cluster_CENTER_LAMBDA     ); }
  if ( checkBranch(t,"cluster_LATERAL"           ) && t->FindLeaf("cluster_LATERAL"           ) != nullptr ) { t->SetBranchAddress("cluster_LATERAL"           ,&cluster_LATERAL           ,&b_cluster_LATERAL           ); }
  if ( checkBranch(t,"cluster_LONGITUDINAL"      ) && t->FindLeaf("cluster_LONGITUDINAL"      ) != nullptr ) { t->SetBranchAddress("cluster_LONGITUDINAL"      ,&cluster_LONGITUDINAL      ,&b_cluster_LONGITUDINAL      ); }
  if ( checkBranch(t,"cluster_ENG_FRAC_EM"       ) && t->FindLeaf("cluster_ENG_FRAC_EM"       ) != nullptr ) { t->SetBranchAddress("cluster_ENG_FRAC_EM"       ,&cluster_ENG_FRAC_EM       ,&b_cluster_ENG_FRAC_EM       ); }
  if ( checkBranch(t,"cluster_ENG_FRAC_MAX"      ) && t->FindLeaf("cluster_ENG_FRAC_MAX"      ) != nullptr ) { t->SetBranchAddress("cluster_ENG_FRAC_MAX"      ,&cluster_ENG_FRAC_MAX      ,&b_cluster_ENG_FRAC_MAX      ); }
  if ( checkBranch(t,"cluster_ENG_FRAC_CORE"     ) && t->FindLeaf("cluster_ENG_FRAC_CORE"     ) != nullptr ) { t->SetBranchAddress("cluster_ENG_FRAC_CORE"     ,&cluster_ENG_FRAC_CORE     ,&b_cluster_ENG_FRAC_CORE     ); }
  if ( checkBranch(t,"cluster_SECOND_ENG_DENS"   ) && t->FindLeaf("cluster_SECOND_ENG_DENS"   ) != nullptr ) { t->SetBranchAddress("cluster_SECOND_ENG_DENS"   ,&cluster_SECOND_ENG_DENS   ,&b_cluster_SECOND_ENG_DENS   ); }
  if ( checkBranch(t,"cluster_ISOLATION"         ) && t->FindLeaf("cluster_ISOLATION"         ) != nullptr ) { t->SetBranchAddress("cluster_ISOLATION"         ,&cluster_ISOLATION         ,&b_cluster_ISOLATION         ); }
  if ( checkBranch(t,"cluster_ENG_BAD_CELLS"     ) && t->FindLeaf("cluster_ENG_BAD_CELLS"     ) != nullptr ) { t->SetBranchAddress("cluster_ENG_BAD_CELLS"     ,&cluster_ENG_BAD_CELLS     ,&b_cluster_ENG_BAD_CELLS     ); }
  if ( checkBranch(t,"cluster_N_BAD_CELLS"       ) && t->FindLeaf("cluster_N_BAD_CELLS"       ) != nullptr ) { t->SetBranchAddress("cluster_N_BAD_CELLS"       ,&cluster_N_BAD_CELLS       ,&b_cluster_N_BAD_CELLS       ); }
  if ( checkBranch(t,"cluster_N_BAD_CELLS_CORR"  ) && t->FindLeaf("cluster_N_BAD_CELLS_CORR"  ) != nullptr ) { t->SetBranchAddress("cluster_N_BAD_CELLS_CORR"  ,&cluster_N_BAD_CELLS_CORR  ,&b_cluster_N_BAD_CELLS_CORR  ); }
  if ( checkBranch(t,"cluster_BAD_CELLS_CORR_E"  ) && t->FindLeaf("cluster_BAD_CELLS_CORR_E"  ) != nullptr ) { t->SetBranchAddress("cluster_BAD_CELLS_CORR_E"  ,&cluster_BAD_CELLS_CORR_E  ,&b_cluster_BAD_CELLS_CORR_E  ); }
  if ( checkBranch(t,"cluster_BADLARQ_FRAC"      ) && t->FindLeaf("cluster_BADLARQ_FRAC"      ) != nullptr ) { t->SetBranchAddress("cluster_BADLARQ_FRAC"      ,&cluster_BADLARQ_FRAC      ,&b_cluster_BADLARQ_FRAC      ); }
  if ( checkBranch(t,"cluster_ENG_POS"           ) && t->FindLeaf("cluster_ENG_POS"           ) != nullptr ) { t->SetBranchAddress("cluster_ENG_POS"           ,&cluster_ENG_POS           ,&b_cluster_ENG_POS           ); }
  if ( checkBranch(t,"cluster_SIGNIFICANCE"      ) && t->FindLeaf("cluster_SIGNIFICANCE"      ) != nullptr ) { t->SetBranchAddress("cluster_SIGNIFICANCE"      ,&cluster_SIGNIFICANCE      ,&b_cluster_SIGNIFICANCE      ); }
  if ( checkBranch(t,"cluster_CELL_SIGNIFICANCE" ) && t->FindLeaf("cluster_CELL_SIGNIFICANCE" ) != nullptr ) { t->SetBranchAddress("cluster_CELL_SIGNIFICANCE" ,&cluster_CELL_SIGNIFICANCE ,&b_cluster_CELL_SIGNIFICANCE ); }
  if ( checkBranch(t,"cluster_CELL_SIG_SAMPLING" ) && t->FindLeaf("cluster_CELL_SIG_SAMPLING" ) != nullptr ) { t->SetBranchAddress("cluster_CELL_SIG_SAMPLING" ,&cluster_CELL_SIG_SAMPLING ,&b_cluster_CELL_SIG_SAMPLING ); }
  if ( checkBranch(t,"cluster_AVG_LAR_Q"         ) && t->FindLeaf("cluster_AVG_LAR_Q"         ) != nullptr ) { t->SetBranchAddress("cluster_AVG_LAR_Q"         ,&cluster_AVG_LAR_Q         ,&b_cluster_AVG_LAR_Q         ); }
  if ( checkBranch(t,"cluster_AVG_TILE_Q"        ) && t->FindLeaf("cluster_AVG_TILE_Q"        ) != nullptr ) { t->SetBranchAddress("cluster_AVG_TILE_Q"        ,&cluster_AVG_TILE_Q        ,&b_cluster_AVG_TILE_Q        ); }
  if ( checkBranch(t,"cluster_ENG_BAD_HV_CELLS"  ) && t->FindLeaf("cluster_ENG_BAD_HV_CELLS"  ) != nullptr ) { t->SetBranchAddress("cluster_ENG_BAD_HV_CELLS"  ,&cluster_ENG_BAD_HV_CELLS  ,&b_cluster_ENG_BAD_HV_CELLS  ); }
  if ( checkBranch(t,"cluster_N_BAD_HV_CELLS"    ) && t->FindLeaf("cluster_N_BAD_HV_CELLS"    ) != nullptr ) { t->SetBranchAddress("cluster_N_BAD_HV_CELLS"    ,&cluster_N_BAD_HV_CELLS    ,&b_cluster_N_BAD_HV_CELLS    ); }
  if ( checkBranch(t,"cluster_PTD"               ) && t->FindLeaf("cluster_PTD"               ) != nullptr ) { t->SetBranchAddress("cluster_PTD"               ,&cluster_PTD               ,&b_cluster_PTD               ); }
  if ( checkBranch(t,"cluster_MASS"              ) && t->FindLeaf("cluster_MASS"              ) != nullptr ) { t->SetBranchAddress("cluster_MASS"              ,&cluster_MASS              ,&b_cluster_MASS              ); }
  if ( checkBranch(t,"cluster_SECOND_TIME"       ) && t->FindLeaf("cluster_SECOND_TIME"       ) != nullptr ) { t->SetBranchAddress("cluster_SECOND_TIME"       ,&cluster_SECOND_TIME       ,&b_cluster_SECOND_TIME       ); }
  if ( checkBranch(t,"CalibratedE"               ) && t->FindLeaf("CalibratedE"               ) != nullptr ) { t->SetBranchAddress("CalibratedE"               ,&CalibratedE               ,&b_CalibratedE               ); }
  if ( checkBranch(t,"ResponseMeasure"           ) && t->FindLeaf("ResponseMeasure"           ) != nullptr ) { t->SetBranchAddress("ResponseMeasure"           ,&ResponseMeasure           ,&b_ResponseMeasure           ); }
  if ( checkBranch(t,"ResponsePrediction"        ) && t->FindLeaf("ResponsePrediction"        ) != nullptr ) { t->SetBranchAddress("ResponsePrediction"        ,&ResponsePrediction        ,&b_ResponsePrediction        ); }
  //  if ( checkBranch(t,"ResponseFluctuation"       ) && t->FindLeaf("ResponseFluctuation"       ) != nullptr ) { t->SetBranchAddress("ResponseFluctuation"       ,&ResponseFluctuation       ,&b_ResponseFluctuation       ); } 
  size_t inputDiff (inputBranchNames.size() -nInputBranches ); 
  size_t knownDiff (knownBranchNames.size() -nKnownBranches );
  size_t missedDiff(missedBranchNames.size()-nMissedBranches);
  printf("[ClusterTreeMerger] INFO tree <%s> type %-*.*s: number of found/missed/known %zu/%zu/%zu [+%-2zu/+%-2zu/+%-2zu]\n",t->GetName(),treeTypeWidth(),treeTypeWidth(),treeTypeToName.at(tt).c_str(),
	 inputBranchNames.size(),missedBranchNames.size(),knownBranchNames.size(),inputDiff,missedDiff,knownDiff); 
  // set particle/jet flag
  if ( tt == OriginalTree ) {
    cfgTrees.originalTree.treePtr->SetBranchStatus("*",1); 
    static std::string _particleTag = "truthPDG";
    flag_isParticle = std::find(inputBranchNames.begin(),inputBranchNames.end(),_particleTag) != inputBranchNames.end();
    printf("[ClusterTreeMerger] INFO merged data type is <%s>\n",Utils::Format::Bool::Type::toChar(flag_isParticle));
  } else if ( tt == AdditionalTree ) { 
    cfgTrees.additionalTree.treePtr->SetBranchStatus("*",0); 
    cfgTrees.additionalTree.treePtr->SetBranchStatus("CalibratedE",1);
    cfgTrees.additionalTree.treePtr->SetBranchStatus("ResponsePrediction",1);
  }
  return (inputBranchNames.size()-nInputBranches) > 0;
}
Bool_t ClusterTreeMerger::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ClusterTreeMerger::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ClusterTreeMerger::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
Bool_t ClusterTreeMerger::BookBranches(TTree* pTree) {
  TTree* oTree = originalTree();   if ( oTree == nullptr ) { printf("[ClusterTreeMerger::BookBranches(...)] ABRT invalid pointer to original tree\n"  ); return false; }
  TTree* aTree = additionalTree(); if ( aTree == nullptr ) { printf("[ClusterTreeMerger::BookBranches(...)] ABRT invalid pointer to additional tree\n"); return false; }
CLUSTERTREE_BOOK_BRANCHES( oTree, aTree, pTree, bookedBranchNames );
   return !bookedBranchNames.empty();
}

bool ClusterTreeMerger::checkBranch(TTree* ptree,const std::string& bname) {
  // check if branch name is known
  bool check(false); 
  if ( std::find(knownBranchNames.begin(),knownBranchNames.end(),bname) == knownBranchNames.end() ) { 
    knownBranchNames.push_back(bname); 
  }
  // check if branch name is already booked
  if ( std::find(inputBranchNames.begin(),inputBranchNames.end(),bname) == inputBranchNames.end() ) {
    if ( ptree->FindLeaf(bname.c_str()) != nullptr ) { 
      inputBranchNames.push_back(bname); 
      check = true;
    } else { 
      if ( std::find(missedBranchNames.begin(),missedBranchNames.end(),bname) == missedBranchNames.end() ) { 
	missedBranchNames.push_back(bname); 
      } 
    }
  }
  return check; 
}
#endif // #ifdef ClusterTreeMerger_cxx
