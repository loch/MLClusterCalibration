// -*- c++ -*-
#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include "Type.h"
#include "Variable.h"
#include "Axis.h"
#include "Utils.h"

#include "HistManager.h"

#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>

#include <string>
#include <vector>
#include <map>
#include <tuple>

#include <cstdio>
#include <cmath>

namespace Histogram {
    /// @brief Histogram types
    enum class Type { TH1D = 0x01, TH2D = 0x02, TH3D = 0x04, TProfile = 0x11, TProfile2D = 0x12, UNKNOWN = 0x00 };
    static const std::map<Type,std::string> printableType = {
        {Type::TH1D      ,"TH1D"      },
        {Type::TH2D      ,"TH2D"      },
        {Type::TH3D      ,"TH3D"      },
        {Type::TProfile  ,"TProfile"  },
        {Type::TProfile2D,"TProfile2D"},
        {Type::UNKNOWN   ,"UNKNOWN"   }
    };
    static const std::map<Type,std::string> typeFromString = {
        {"TH1D"      ,Type::TH1D      },
        {"TH2D"      ,Type::TH2D      },
        {"TH3D"      ,Type::TH3D      },
        {"TProfile"  ,Type::TProfile  },
        {"TProfile2D",Type::TProfile2D},
        {"UNKNOWN"   ,Type::UNKNOWN   }
    };
    static const std::string& typeName(Type t) { return printableType.at(t); }
    static Type namedType(const std::string& tname) { auto ft(typeFromString.find(tname)); return ft != typeToString.end() ? ft->second : Type::UNKNOWN; }
    /// @name Local types
    ///@{
    typedef Type::Variable::bitmask_t                   Key;             ///< Key type for histogram descriptor maps 
    typedef Type::Variable::name_t                      Name;            ///< Histogram name type
    typedef Type::Variable::title_t                     Title;           ///< Histogram title type
    typedef Axis::Number                                BinNumber;       ///< Histogram number of bins/bin index type
    typedef Axis::Value                                 BinValue;        ///< Histogram bin value type
    typedef TH1                                         BaseType;        ///< Histogram base type
    typedef Axis::Delimiters                            Binning;         ///< Container type for bin delimiter list 
    typedef Axis::LogFlag                               LogFlag;         ///< Flag type (mark log(value) fill)
    typedef Axis::BitMask                               BitMask;         ///< Bit mask type from @c Axis
    // typedef Axis::Descriptor                            AxisDescriptor;  ///< Axis description type
    // typedef Axis::Coordinate                            AxisCoordinate;  ///< Coordinate marker 
    typedef std::map<Axis::Coordinate,Axis::Descriptor> AxisList;        ///< List of axis descriptors type
    typedef AxisList::const_iterator                    AxisIterator;    ///< Iterator for the axis list (const version)
    ///@}
    /// @name Histogram descriptor
    ///@{
    typedef std::tuple<Type,Name,Title,AxisList> Descriptor; ///< Histogram descriptor
    /// List of histogram axis descriptors
    static const AxisList&         axisList (const Descriptor& hdescr) { return std::get<2>(hdescr); }
    /// Extract axis descriptor from list     
    static const Axis::Descriptor& axisDescr(const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) {  
        const auto& alist = axisList(hdescr); auto fc(alist.find(c)); return fc != alist.end() ? fc->second : Axis::generate();
    }
    static Type                type     (const Descriptor& hdescr) { return std::get<0>(hdescr); }
    static const Name&         name     (const Descriptor& hdescr) { return std::get<1>(hdescr); }                                                        ///< Access histogram name
    static const Title&        title    (const Descriptor& hdescr) { return std::get<2>(hdescr); }                                                        ///< Access histogram title
    static AxisList::size_type nAxis    (const Descriptor& hdescr) { return axisList(hdescr).size(); }                                                    ///< Access number of configured axis
    static BinNumber           nBins    (const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::nBins  (axisDescr(hdescr,c)); }  ///< Access number of bins on axis
    static BinValue            lower    (const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::xMin   (axisDescr(hdescr,c)); }  ///< Access lower value range boundary on axis
    static BinValue            upper    (const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::xMax   (axisDescr(hdescr,c)); }  ///< Access upper value range boundary on axis
    static LogFlag             logFlag  (const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::logFlag(axisDescr(hdescr,c)); }  ///< Access flag indicating log(value) fill
    static Binning             binning  (const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::binning(axisDescr(hdescr,c)); }  ///< Access list of bin delimiters
    static const Axis::Title&  axisTitle(const Descriptor& hdescr,Axis::Coordinate c=Axis::Coordinate::X) { return Axis::title  (axisDescr(hdescr,c)); }  ///< Access axis title
    /// Formatted messages
    static std::vector<std::string> description(const Descriptor& hdescr) {
        static char _buffer[1024];
        std::vector<std::string> msgs;
        sprintf(_buffer,"Name ........................ \042%s\042",name(hdescr).c_str() );
        sprintf(_buffer,"Title ....................... \042%s\042",title(hdescr).c_str());
        sprintf(_buffer,"Number of configured axis ... %zu",nAxis(hdescr)); msgs.push_back(std::string(_buffer));
        AxisList::size_type iax(1); Axis::Name::size_type twidth(0); Axis::Label::size_type lwidth(0); int maxBins(0);
        for ( const auto& entry: axisList(hdescr) ) { 
            twidth  = std::max(twidth,Axis::title(entry.second).length());
            lwidth  = std::max(lwidth,Axis::label(entry.second).length());
            maxBins = std::max(maxBins,Axis::nBins(entry.second));
        }
        int bwidth(static_cast<int>(std::log10(static_cast<double>(maxBins)))+1);
        for ( const auto& entry : axisList(hdescr) ) {
            sprintf(_buffer,"%1.1s-axis ...................... title %*.*s label %*.*s log flag %-3.3s number of bins %-*.*i range [%+10.3e,%+-10.3e]",
                    Utils::String::print_it(Axis::nameFromCoordinate(entry.first)),
                    twidth,twidth,Utils::String::print_it(Axis::title(entry.second)),
                    lwidth,lwidth,Utils::String::print_it(Axis::label(entry.second)),
                    Utils::Format::Bool::State::toChar(Axis::logFlag(entry.second));
                    bwidth,bwidth,Axis::nBins(entry.second),
                    Axis::xMin(entry.second),
                    Axis::xMax(entry.second)
                   );
            msgs.push_back(std::string(_buffer));
        }
        return msgs; 
    }
    ///@}
    /// @brief Histogram manager
    typedef HistManager Manager;
    /// @brief Booking
    namespace Booking {
        static BaseType* bookAny(const Descriptor& hdescr) {
            Type htype = type(hdescr); 
            if ( htype == Type::UNKNOWN ) { printf("[Histogram::Booking::book(...)] WARN cannot book histogram type %s\n",typeName(htype).c_str()); return false; }
            Axis::Coordinate axc = Axis::Coordinate::X;
            Axis::Coordinate ayc = Axis::Coordinate::Y;
            Axis::Coordinate azc = Axis::Coordinate::Z;
            switch ( htype ) {
                case Type::TH1D:
                    if ( binning(hdescr,axc).empty() ) {
                        HistTools::book<TH1D>(name(hdescr),title(hdescr),nBins(hdescr,axc),lower(hdescr,axc),upper(hdescr,axc),axisTitle(hdescr,axc)); 
                    } else { 
                        HistTools::book<TH1D>(name(hdescr),title(hdescr),binning(hdescr,axc),axisTitle(hdescr,axc));
                    }
                    break;
                case Type::TH2D:
            } 
        }
    } // Histogram::Booking
} // Histogram 
#endif