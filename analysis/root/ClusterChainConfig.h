// -*- c++ -*-
#ifndef CLUSTERCHAINCFG_H
#define CLUSTERCHAINCFG_H

#include <string>
#include <vector>

namespace Cluster {
    namespace Config {
        namespace Data {
            static const std::string              directory = "C:/Users/loch/MLClusterCalibration/data/data.04.04.2022";
            static const std::vector<std::string> files     = { "JZ1.topo-cluster.root", "JZ2.topo-cluster.root", "JZ3.topo-cluster.root" };
            static const std::vector<std::string> paths     = { 
                directory+std::string("/")+files.at(0),
                directory+std::string("/")+files.at(1),
                directory+std::string("/")+files.at(2)
            }
            static const std::string              treeName  = "ClusterTree";
        } // Data
    } // Config
    static const std::string&              dataDirectory() { return Config::Data::directory; }
    static const std::vector<std::string>& dataFiles()     { return Config::Data::files;     }
    static const std::vector<std::string>& dataPaths()     { return Config::Data::paths;     }
    static const std::string&              treeName()      { return Config::Data::treeName;  } 
    static const std::ostream& listDataFiles(std::ostream& ostr) { for ( const auto& fn : dataFiles ) { ostr << fn << std::endl; } return ostr; }
    namespace Chain {
        static ClusterChain* factory(const std::vector<std::string>& fNames=Config::Data:paths,const std::string& tName=Config::Data::treeName) {
            return new ClusterChain(fNames,tName);
        } 
    }
}
#endif