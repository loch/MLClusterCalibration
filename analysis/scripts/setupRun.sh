
REPODIR="./repo/analysis"
SRCDIR="${REPODIR}/root"
RUNDIR="${REPODIR}/run"

MODE="link"

if [ "$#" -gt 1 ]; then 
    if [ "$1" -eq "--copy" ]; then MODE="copy"; fi
fi

echo "[setupRun|repo:${REPODIR}|mode:${MODE}] setting up the run directory ${RUNDIR} using files from ${SRCDIR}"

