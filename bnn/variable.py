

import config as cfg
import features
import numerical
import units

from units import UnitDescr as unit_descr

mprint  = cfg.msgPrint          
mskip   = cfg.msgSkipLine       
msprint = cfg.msgPrintSeparator 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()
msgSvc     = cfg.msgSvc
msgDict    = cfg.msgLevelDescr

mname = 'main::variable'

class Variable():
    def __init__(self,name,unit=None,transform=None,projection=None,**kwargs):
        self._name       = name
        self._unit       = unit
        self._projection = projection
        self._transform  = transform
        for k,v in kwargs:
            setattr(self,k,v)
    def unit(self) -> unit_descr:
        return self._unit
    def name(self) -> str:
        return self._name
    def value(self,ovalue) -> float:
        return units.scale(ovalue)

