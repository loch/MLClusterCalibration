
import config as cfg
import pandas as pd
from   pandas import DataFrame as dframe
import numpy as np
import random
import time

# -- used externals
from config import msgPrint     as mprint
from config import msgSkipLine  as mskip
from config import msgSeparator as msep

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()

from type_decl import extractType

''' 
Build list of formatted lines:

This is basically a table with ncols and len(array) lines where the columns are ',' separated. If as_string is set to True, the entries in array are enclosed in "". A prefix can be specified to be attached to the beginnig of each line.

'''

module_name = 'utils::'

def fmtArray1d(array,ncols=5,as_string=True,prefix='     '):

    # -- find array length (rows)
    nrows = len(array)
    nwdth = int(0)

    # -- convert input to array of strings and measure width
    strarray = []
    for a in array:
        strarray.append(f'\042{a}\042' if as_string else f'{a}')
        nwdth = np.max([len(strarray[len(strarray)-1])+2,nwdth])
    # -- produce array of lines
    slines = []          ## array to capture result
    icol   = ncols       ## column counter
    pstr   = f'{prefix}' ## line prefix
    for irow in range(0,nrows):
        if icol == ncols:
            if irow > 0: 
                slines.append(pstr)
                pstr = f'{prefix}'
            icol = 0
        astr = f'{strarray[irow]},' if irow < nrows-1 else f'{strarray[irow]}'
        pstr += f'{astr:{nwdth}}'
        icol += 1
        if irow == nrows-1: 
            slines.append(pstr)

    # -- return list of lines
    return slines

def printArray1d(array):
    for astr in array: print(astr)
    

#################
## Data loader ##
#################

'''
Load requested number of data records from specified file type
'''
def dataLoader(nrecords=cfg.training_size,sample_fraction=-1.,filePath=None,fieldDelimiter=cfg.input_file_field_sep,filters=None,features=None):

    mname = f'{module_name}::dataLoader'

    # -- input arguments check
    if filePath == None:
        mprint(module=mname,level=msgError,message='no valid input file path specified')
        return None

    # -- read input data (or specified members)
    df = []
    if nrecords > 0:
        mprint(module=mname,level=msgInfo,message=f'attempt read {nrecords:d} data records from {filePath} (be patient)')
        df = pd.read_csv(filePath, delimiter=fieldDelimiter, nrows=nrecords)
        if df.shape[0] < nrecords: 
            mprint(module=mname,level=msgWarning,message=f'only {df.shape[0]:d} data records from {filePath} available, out of {nrecords} requested')
    else:
        mprint(module=mname,level=msgInfo,message=f'read all data records from {filePath} (be patient)')
        df = pd.read_csv(filePath, delimiter=fieldDelimiter)
        if sample_fraction > 0. and sample_fraction < 1:
            df = df.sample(frac=sample_fraction,replace=False,axis=0)

    ishape = df.shape[0]
    pshape = int(np.log10(ishape))+1
    mprint(module=mname,level=msgInfo,message=f'loaded {ishape} data records into database of shape {df.shape}')

    # -- done if no filters requested
    if filters == None:
        return df

    # -- filter input data based on features
    keys = []
    for f in features:
        keys.append(f)
    if 'cluster_ENG_CALIB_TOT' not in keys:
        keys.append('cluster_ENG_CALIB_TOT')
    fshape  = 0
    initial = True
    for f in keys:
        if f in filters:
            cstr   = filters[f]
            fcmd   = f'df[\042{f}\042] {cstr}'
            df     = df.loc[eval(fcmd)]
            fshape = df.shape[0]
            if initial:
                pshape = int(np.log10(fshape))+1
            fpct   = float(fshape)/float(ishape)*100.
            mprint(module=mname,level=msgInfo,message=f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fpct:5.1f}%)') 

    # -- filter input data based on derived quantities
    f = 'clusterResponseEM'
    if f in filters: 
        clusterResponseEM = df["clusterE"] / df["cluster_ENG_CALIB_TOT"]
        cstr              = filters[f] 
        fcmd              = f'df.loc[clusterResponseEM {cstr}]' 
        df                = eval(fcmd)
        fshape            = df.shape[0]
        mprint(module=mname,level=msgInfo,message=f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fshape/ishape*100:5.1f}%) [selection based on derived quantity]') 
 
    # return data set
    mprint(module=mname,level=msgInfo,message=f'returned {df.shape[0]} data records after filters')
    return df

'''
Load nrecords data records from a file specified by filePath and saves them into a pandas.DataFrame (df). The fileType can be specified (presently only 'csv' is supported), in addition to fieldDelimiter to be used instead of the default ','. A dictionary of filters can be provided, which removes records not passing the specified filter. The column headers of df can be specified in the features argumnent. The resulting (potentially filtered) data frame is returned.   
'''

def dataLoader_prep(nrecords=cfg.training_size,filePath=cfg.input_file_path,fileType=cfg.input_file_type,fieldDelimiter=cfg.input_file_field_sep,filters=cfg.input_filters,features=[]):
    mname = f'{module_name}::dataLoader'
    # -- not yet implemented file type
    if fileType != 'csv':
        mprint(mname,'WARN',f'file type <{fileType}> not yet implemented, try <csv> instead')
        fileType = 'csv'
    # -- read input data (all or specified number)
    df = []
    if nrecords > 0:
        if fileType == 'csv':
            mprint(mname,'INFO',f'read {nrecords:d} data records from {filePath} (be patient)')
            df = pd.read_csv(filePath, delimiter=fieldDelimiter, nrows=nrecords)
            if df.shape[0] < nrecords: 
                mprint(mname,'WARN',f'only {df.shape[0]:d} data records from {filePath} available, out of {nrecords} requested')
    else:
        if fileType == 'csv':
            mprint(mname,'INFO',f'read all data records from {filePath} (be patient)')
            df = pd.read_csv(filePath, delimiter=fieldDelimiter)

    ishape = df.shape[0]
    mprint(mname,'INFO',f'loaded {ishape} data records into database of shape {df.shape}')

    # -- filter input data based on features
    keys = []
    for f in features:
        keys.append(f)
    if 'cluster_ENG_CALIB_TOT' not in keys:
        keys.append('cluster_ENG_CALIB_TOT')
    ishape  = df.shape[0]
    fshape  = 0
    pshape  = int(np.log10(ishape))+1
    initial = True
    for f in keys:
        if f in filters:
            cstr   = filters[f]
            fcmd   = f'df[\042{f}\042] {cstr}'
            df     = df.loc[eval(fcmd)]
            fshape = df.shape[0]
            if initial:
                pshape = int(np.log10(fshape))+1
            fpct   = float(fshape)/float(ishape)*100.
            mprint(mname,'INFO',f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fpct:5.1f}%)') 

    # -- filter input data based on derived quantities
    f = 'clusterResponseEM'
    if f in filters: 
        clusterResponseEM = df["clusterE"] / df["cluster_ENG_CALIB_TOT"]
        cstr              = filters[f] 
        fcmd              = f'df.loc[clusterResponseEM {cstr}]' 
        df                = eval(fcmd)
        fshape            = df.shape[0]
        mprint(mname,'INFO',f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fshape/ishape*100:5.1f}%) [selection based on derived quantity]') 
 
    # return data set
    mprint(mname,'INFO',f'returned {df.shape[0]} data records after filters')
    return df

#####################
## Compose samples ##
#####################

def composeSamples(df,train_fraction=0.50,valid_fraction=0.20,test_fraction=-1):
    mname = f'{module_name}::composeSamples'
    #
    if test_fraction < 0:
        test_fraction = 1.-(train_fraction+valid_fraction)
###    if testFrac < 0:
###        mprint(mname,'WARN',f'invalid fractions training {train_fraction:.3f} validation {valid_fraction:.3f} testing {testFrac:.3d}')
###        return df, df, df

    nrecs    = df.shape[0]                                         ## total number of records
    # -- training sample
    trn_data = df.sample(frac=train_fraction,replace=False,axis=0) ## sample training data
    ntrn     = trn_data.shape[0]                                   ## number of sampled records
    df.drop(trn_data.index)                                        ## tag sampled records in original data
    # -- validation sample
    val_data = df.sample(frac=valid_fraction,replace=False,axis=0) ## sample validation data
    nval     = val_data.shape[0]                                   ## number of sampled records
    df.drop(val_data.index)                                        ## tag sampled records in remaining data
    # -- testing sample
    tst_data = df.sample(frac=test_fraction,replace=False,axis=0)  ## sample testing data
    ntst     = tst_data.shape[0]                                   ## number of sampled records
    df.drop(tst_data.index)                                        ## tag sampled records in remaining data
    # -- validate indices
    nchk = ntrn+nval+ntst
    if nchk != nrecs:
        mprint(module=mname,level=msgWarning,message=f'only {nchk} of {nrecs} records sampled - missing {nrecs-nchk} record(s) from input')

    # -- messages
    xtrn = float(ntrn)/float(nrecs)*100.
    xval = float(nval)/float(nrecs)*100.
    xtst = float(ntst)/float(nrecs)*100.
    ndec = int(int(np.log10(nrecs))+1)
    npct = int(int(np.log10(np.max([xtrn,xval,xtst])))+3)
    npcr = int(int(np.log10(np.max([train_fraction,valid_fraction,test_fraction])))+3)
    mprint(module=mname,level=msgInfo,message=f'training sample .....: {ntrn:{ndec}} of {nrecs:{ndec}} records ({ntrn/nrecs*100:{npct}.1f}%, requested {train_fraction*100:{npcr}.1f}%)')
    mprint(module=mname,level=msgInfo,message=f'validation sample ...: {nval:{ndec}} of {nrecs:{ndec}} records ({nval/nrecs*100:{npct}.1f}%, requested {valid_fraction*100:{npcr}.1f}%)')
    mprint(module=mname,level=msgInfo,message=f'testing sample ......: {ntst:{ndec}} of {nrecs:{ndec}} records ({ntst/nrecs*100:{npct}.1f}%, requested {test_fraction*100:{npcr}.1f}%)' )

    return trn_data, val_data, tst_data

############################
# Argument help formatter ##
############################

## https://github.com/python/cpython/blob/main/Lib/argparse.py
## 
## class HelpMsgFormat(HelpFormatter):
## 
##     def _get_help_string(self, action):
##         # -- check if help string is to printed
##         help = action.help
##         if help is None:
##             help = ''
##         # -- access default value
##         if '%(default)' not in help:
##             if action.default is not SUPPRESS:
##                 defaulting_nargs = [OPTIONAL, ZERO_OR_MORE]
##
def string_insert(astr,mode='front',inserted_string=' '):
    if mode == 'front':
        return f'{inserted_string}{astr}'
    elif mode == 'back':
        return f'{astr}{inserted_string}'
    else:
        return astr

'''
Adjust words in string to 'left', 'right', 'center', or 'justify'. 
'''
def string_adjust(astr,width=0,adjust='left',sep=' ') -> str:
    mname = 'utils::string_adjust'
    _ctrlc = { 
        'left'    : '<',
        'right'   : '>',
        'center'  : '^',
        'justify' : None,
    }
    # -- invalid/unknown input
    if astr == None or astr == '':
        return astr
    if adjust not in _ctrlc:
        mprint(module=mname,level=msgWarning,message=f'unknown mode {adjust}, use one of {_ctrlc.keys()}.')
        return astr
    # -- strip string (remove seperator character front and back) 
    tstr = astr.rstrip(sep).lstrip(sep)   # - stripped string
    tlen = len(tstr)
    wdth = np.max([width,tlen])      # - width of output string is at least the length of the stripped string
    # -- return left flushed/right flushed/centered string
    if adjust != 'justify':
        adjust = _ctrlc[adjust]
        return f'{tstr:{adjust}{wdth}}'
    # -- return fully justified string
    else:
        # -- find separators in string
        spos = []
        ipos = 0
        while ipos != -1:
            print(ipos)
            lpos = ipos + 1
            if  lpos < tlen - 1:
                ipos = tstr.find(sep,lpos)
            else:
                ipos = -1
            if ipos != -1:
                spos.append(ipos)
        print(spos)

        # -- split string into list of words
        lstr = tstr.split(sep)
        # -- number of words in string (= # entries in list)
        nwrd = len(lstr)
        # -- not enough words to insert separators
        if nwrd < 2:
            mprint(module=mname,level=msgWarning,message=f'cannot apply mode {adjust} as string only contains {nwrds} word')
            return tstr
        # -- find out how many separators to insert
        nsep = np.max([wdth-tlen,0])
        if nsep == 0:
            return tstr
        # -- fill from center
        tidx = int(nwrd / 2) - 1
        lidx = 0
        ridx = tidx
        while nsep > 0 and ridx < nwrd and lidx < tidx:
            print('indices ',lidx,' ',ridx,' ',nsep)
            lstr[lidx] = lstr[lidx] + sep
            lidx += 1
            nsep -= 1
            if nsep > 0 and ridx < nwrd and lidx < ridx:
                lstr[ridx] = sep + lstr[ridx]
                ridx += 1
                nsep -= 1
        # -- join list to string
        return ''.join(lstr)

##################################
## Argument data extractor help ##
##################################

def className(object=None):
    mname = f'{module_name}::className'
    # -- missing object/class specifier 
    if object == None:
        mprint(module=mname,level=msgWarning,message='missing argument')
        return None
    # -- invalid object/class specifier
    if not hasattr(object,'__class__'):
        mprint(module=mname,level=msgWarning,message=f'argument is not a class:')
        mprint(module=mname,level=msgWarning,message=cfg.msgExtract(f'{object}')) 
        return None
    # -- all set
    return object.__class__.__name__

def moduleName(parent=None,object=None):
    cname = className(object)
    if cname == None:
        return None
    mname = f'::{cname}'
    if parent != None:
        mname = f'{parent}{mname}'
    return mname

def numDigits(number):
    addon = 2 if number < 0 else 1
    return int(np.max([np.log10(np.abs(number)),0]))+1
        
