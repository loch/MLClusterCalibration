
import sys

import config as cfg

mprint  = cfg.msgPrint          
mskip   = cfg.msgSkipLine       
msprint = cfg.msgPrintSeparator 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()
msgSvc     = cfg.msgSvc
msgDict    = cfg.msgLevelDescr

mname = 'main::writeRootFile'

## -- this needs python3
mcolor = 'cyan'
mstart = f'msprint(module=mname,level=msgInfo,with_time=True,start=True,color=\'cyan\')'
mstop  = f'msprint(module=mname,level=msgInfo,with_time=True,color=\'cyan\')'

import test

import uproot as rootsvc
import numpy  as np

from bayesian import PVBNetwork      as trained_model
from bayesian import validation_pass as vpass

from features import featureDefaults as features

import numerical

import utils as util
loader = util.dataLoader    

'''
Capture arguments and set up validation plots
'''

dnow = cfg.date_now.replace('_','/')
tnow = cfg.time_now
znow = cfg.zone_now

module_description = 'Plotting module for validation and testing a Bayesian DNN (Peter Loch <Peter.Loch_at_cern.ch> (University of Arizona)).'
module_epilog      = f'Help printed on {dnow} at {tnow} {znow}'


################
## Setting up ##
################

eval(mstart)
# -- parse command-line arguments
if '-h' or '--help' in sys.args[1:]:
    mskip()
parser, pargs = cfg.argumentParser('writeRootFile.py',description=module_description,epilog=module_epilog,mode='test',argument_settings=cfg.argumentSettingsTest)

# -- collect the various values
retrieve_value = cfg.argumentValues(parser,pargs,argument_settings=cfg.argumentSettingsTest)
nrecs    , def_nrecs     = retrieve_value.get_value('nrecords'                , both = True)
tstf     , def_tstf      = retrieve_value.get_value('testfrac'                , both = True)
x_mlvl   , def_x_mlvl    = retrieve_value.get_value('exec-msg-level'          , both = True)
x_name   , def_x_name    = retrieve_value.get_value('exec-name'               , both = True)
mfname   , def_mfname    = retrieve_value.get_value('model-file-name'         , both = True)
mfmode   , def_mfmode    = retrieve_value.get_value('model-file-mode'         , both = True)
iftst    , def_iftst     = retrieve_value.get_value('input-tst-file-path'     , both = True)
### ifname   , def_ifname    = retrieve_value.get_value('input-file-name'         , both = True)
### ifpath   , def_ifpath    = retrieve_value.get_value('input-file-path'         , both = True)
### iftype   , def_iftype    = retrieve_value.get_value('input-file-type'         , both = True)
rfname   , def_rfname    = retrieve_value.get_value('root-file-name'          , both = True)
rfpath   , def_rfpath    = retrieve_value.get_value('root-file-path'          , both = True)
rtname   , def_rtname    = retrieve_value.get_value('root-tree-name'          , both = True)
m_nlayers, def_m_nlayers = retrieve_value.get_value('model-hidden-layers'     , both = True)
m_nbatch , def_m_nbatch  = retrieve_value.get_value('model-batch-size'        , both = True)
m_nnodes , def_m_nnodes  = retrieve_value.get_value('model-hidden-layer-nodes', both = True)
m_name   , def_m_name    = retrieve_value.get_value('model-design'            , both = True)
msgSvc.set_level(x_mlvl)

def_iffsep = ' '
iffsep     = def_iffsep

title   = f'Project \'{x_name}\' Configuration at {tnow} ({znow}) on {dnow}' 
tlength = len(title) + 6
tchar   = '#'
tbar    = f'{tchar:{tchar}^{tlength}}'

mskip()
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mprint(module=mname,level=msgInfo,message=f'## {title} ##',color='yellow')
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mskip()

N = nrecs

# -- print setup so far
if nrecs > 0:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
elif nrecs == -1:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
else:
    mprint(module=mname,level=msgWarning,message=f'number of data records to be read ....: {nrecs} unknown specification, (default: {N} => all available records)')
mprint(module=mname,level=msgInfo,message=f'input filename .......................: \'{iftst}\' (default: \'{def_iftst}\')')
### mprint(module=mname,level=msgInfo,message=f'input filepath .......................: \'{ifpath}\' (default: \'{def_ifpath}\')')
### mprint(module=mname,level=msgInfo,message=f'input filetype .......................: \'{iftype}\' (default: \'{def_iftype}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT filename ........................: \'{rfname}\' (default: \'{def_rfname}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT filepath ........................: \'{rfpath}\' (default: \'{def_rfpath}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT tree name .......................: \'{rtname}\' (default: \'{def_rtname}\')')
mprint(module=mname,level=msgInfo,message=f'model name ...........................: \'{m_name}\': \"{cfg.model_descr(m_name)}\" (default: \'{def_m_name}\')')
mprint(module=mname,level=msgInfo,message=f'model number of hidden layers ........: {m_nlayers} (default: {def_m_nlayers})'                                 )   
mprint(module=mname,level=msgInfo,message=f'model batch size .....................: {m_nbatch} (default: {def_m_nbatch})'                                   )
mprint(module=mname,level=msgInfo,message=f'model number of nodes/hidden layer ...: {m_nnodes} (default: {def_m_nnodes})'                                   )
mprint(module=mname,level=msgInfo,message=f'model number of features/inputs ......: {len(features)} (from configuration)')
mprint(module=mname,level=msgInfo,message=f'message level ........................: {x_mlvl}: \"{msgDict[x_mlvl]}\" (default: {def_x_mlvl})')
mskip()

# -- collect input data
tst_data = loader(nrecords=nrecs,filePath=iftst) if tstf <= 0 else loader(nrecords=-1,sample_fraction=np.min([tstf,1.]),filePath=iftst)

# -- calculate the response
mod_data = tst_data.copy()
mprint(module=mname,level=msgDebug,message=f'raw data shape ..........: {mod_data.shape}')
mod_data['clusterResponseEM']  = tst_data['clusterE']/tst_data['cluster_ENG_CALIB_TOT']
mod_data['clusterResponseLCW'] = tst_data['cluster_HAD_WEIGHT']*tst_data['clusterE']/tst_data['cluster_ENG_CALIB_TOT']
mod_data['clusterResponseML']  = 0 
mprint(module=mname,level=msgDebug,message=f'extended data shape .....: {mod_data.shape}')
mprint(module=mname,level=msgDebug,message='extended data columns:')
mprint(module=mname,level=msgDebug,message=cfg.msgExtract(list(mod_data.columns.values)))

# -- model provider object
model    = trained_model(tst_data.shape[0],hdn_dim=m_nnodes,num_hidden_layers=m_nlayers,n_features=len(features))
provider = test.Provider(model=model,file=mfname,mode=mfmode,msglevel=msgInfo)
model    = provider.model()
if model == None:
    mprint(module=mname,level=msgError,message=f'No model loaded, abort')
    exit(1)

# -- transform data
trans_features = [ 'clusterE', 'cluster_FIRST_ENG_DENS' ]
mod_data = transformDataToLog(mod_data,features,transfeatures)

# -- reshape and transform data




num_features        = mod_data[features].to_numpy()
num_target          = mod_data['clusterResponseEM'].to_numpy()
num_prediction      = mod_data['clusterResponseML'].to_numpy()
num_features_shaped = np.reshape(num_features,(num_features.shape[0],-1))
trans_features      = [ 'clusterE', 'cluster_FIRST_ENG_DENS' ]
eval(mstart)
mprint(module=mname,level=msgInfo,message=f'transforming data: x->log10(x) transformation of features {trans_features}')
num_features_trans = numerical.transformDataToLog(num_features_shaped,features=features,transformToLog=trans_features)
eval(mstop)
mskip()

# -- project data into ranges

### mprint(module=mname,level=msgInfo,message='Loaded model:')
### mprint(module=mname,level=msgInfo,message=cfg.msgExtract(model))



