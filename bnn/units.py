
import config as cfg
import numpy  as np
import utils  as util

mprint     = cfg.msgPrint          
mskip      = cfg.msgSkipLine

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB

import math

from quantiphy import Quantity

def num_quantity(value=0,unit=None,scale=None):
    if unit != None:
        return Quantity(value,unit)
    elif scale != None:
        return Quantity(value,scale=scale)
    else:
        return value

def str_quantity(svalue=None,scale=None):
    if svalue == None:
        return None
    if scale == None:
        return Quantity(svalue)
    else:
        return Quantity(svalue,scale=scale)
    

######################
## Units of Measure ##
######################

# Taken from GaudiKernel.SystemOfUnits. The module is not imported to avoid problems in non-Athena-based environments. 
#
# -- NOTE:
#  
#    The numerical conversions here are adapted to a default system of {GeV,ns,mm} - this is adequate for the data tuples
#    for this study but is NOT the ATLAS/Gaudi default {MeV,ns,mm}!


## -- distances
millimeter  = 1.
millimeter2 = millimeter * millimeter
millimeter3 = millimeter * millimeter2

centimeter  = 10. * millimeter
centimeter2 = centimeter * centimeter
centimeter3 = centimeter * centimeter2

meter       = 1.0e+03 * millimeter
meter2      = meter * meter
meter3      = meter * meter2

kilometer  = 1.0e+03 * meter
kilometer2 = kilometer * kilometer
kilometer3 = kilometer * kilometer2

parsec     = 3.08567757807e+16 * meter

micrometer = 1.0e-06 * meter
nanometer  = 1.0e-09 * meter
picometer  = 1.0e-12 * meter
femtometer = 1.0e-15 * meter

angstrom   = 1.0e-10 * meter
fermi      = femtometer

## -- cross sections
barn       = 1.0e-28 * meter2
millibarn  = 1.0e-03 * barn
microbarn  = 1.0e-06 * barn
nanonbarn  = 1.0e-09 * barn
picobarn   = 1.0e-12 * barn
femtobarn  = 1.0e-15 * barn
ibarn      = 1. / barn      
imillibarn = 1. / millibarn
imicrobarn = 1. / microbarn
inanonbarn = 1. / nanonbarn
ipicobarn  = 1. / picobarn 
ifemtobarn = 1. / femtobarn


## -- volumes
liter      = 1.0e+03 * centimeter3
deciliter  = 1.0e-01 * liter
centiliter = 1.0e-02 * liter
milliliter = 1.0e-03 * liter

## -- angles
radian      = 1.0
milliradian = 1.0e-03 * radian
degree      = math.pi / 180. * radian
steradian   = 1.

## -- time
nanosecond  = 1.
second      = 1.0e+09 * nanosecond     
millisecond = 1.0e-03 * second
microsecond = 1.0e-06 * second
picosecond  = 1.0e-12 * second 
femtosecond = 1.0e-15 * second
minute      = 60. * second
hour        = 60. * minute
day         = 24. * hour
week        = 7.  * day

# -- frequency 
hertz       = 1. / second
kilohertz   = 1.0e+03 * hertz  
megahertz   = 1.0e+06 * hertz  
gigahertz   = 1.0e+09 * hertz        
Hertz       = hertz    
kiloHertz   = kilohertz
MegaHertz   = megahertz
GigaHertz   = gigahertz

## -- electric charges
eplus        = 1.0
e_SI         = 1.602176487e-19
coulomb      = eplus / e_SI
millicoulomb = 1.0e-03 * coulomb
microcoulomb = 1.0e-06 * coulomb
nanocoulomb  = 1.0e-09 * coulomb
picocoulomb  = 1.0e-12 * coulomb
femtocoulomb = 1.0e-15 * coulomb

## -- energy units units
gigaelectronvolt = 1.
electronvolt     = 1.0e-09 * gigaelectronvolt
kiloelectronvolt = 1.0e-06 * gigaelectronvolt
megaelectronvolt = 1.0e-03 * gigaelectronvolt
## gigaelectronvolt = 1.0e+03 * megaelectronvolt 
teraelectronvolt = 1.0e+03 * gigaelectronvolt
petaelectronvolt = 1.0e+06 * gigaelectronvolt
joule            = electronvolt / e_SI

## -- mass
kilogram  = joule * second * second / ( meter * meter )
gram      = 1.0e-03 * kilogram
milligram = 1.0e-03 * gram

## -- power
watt      = joule / second
kilowatt  = 1.0e+03 * watt
megawatt  = 1.0e+03 * kilowatt
gigawatt  = 1.0e+03 * megawatt

## -- energy
watthour     = watt * hour
kilowatthour = 1.0e+03 * watt
megawatthour = 1.0e+06 * watt
gigawatthour = 1.0e+09 * watt

## -- force
newton       = joule / meter
kilonewton   = 1.0e+03 * newton

## -- pressure
hep_pascal  = newton / meter2
pascal      = hep_pascal
hectopascal = 1.0e+02 * pascal
kilopascal  = 1.0e+03 * pascal
millibar    = hectopascal 
bar         = 1.0e+03 * millibar
atmosphere  = 1013.25 * millibar ## [hectopascal]

## -- electric current
ampere      = coulomb / second
milliampere = 1.0e-03 * ampere
microampere = 1.0e-06 * ampere
nanoampere  = 1.0e-09 * ampere
picoampere  = 1.0e-12 * ampere 

## -- electric potential/tention
megavolt    = megaelectronvolt / eplus
volt        = 1.0e-06 * megavolt
kilovolt    = 1.0e-03 * megavolt

## -- electric resistance
ohm         = volt / ampere
kiloohm     = 1.0e+03 * ohm
megaohm     = 1.0e+06 * ohm

## -- electric capacitance
farad       = coulomb / volt
millifarad  = 1.0e-03 * farad
microfarad  = 1.0e-06 * farad
nanofarad   = 1.0e-09 * farad
picofarad   = 1.0e-12 * farad

## -- magnetic field
tesla       = volt * second / meter2    # -- field
weber       = tesla * meter2            # -- flux
gauss       = 1.0e-04 * tesla           # -- field
kilogauss   = 1.0e+03 * gauss           # -- field
henry       = weber / ampere            # -- inductance

## -- temperature
kelvin = 1.

class TempCnv(): 
    def __init__(self):
        self._absolute_null = -273.15
        self._f_offset      = 32.
        self._f_slope       = 1.8
        self._f_slope_inv   = 1. / self._f_slope
        self._r_slope       = 1.25
        self._r_slope_inv   = 1. / self._r_slope
    def KtoC(self,tvar):
        return tvar + self._absolute_null
    def CtoK(self,tvar):
        return tvar - self._absolute_null
    def CtoF(self,tvar):
        return tvar * self._f_slope + self._f_offset
    def FtoC(self,tvar):
        return ( tvar - self._f_offset ) * self._f_slope_inv
    def KtoF(self,tvar):
        return self.CtoF(self.KtoC(tvar))
    def FtoK(self,tvar):
        return self.FtoC(self.CtoK(tvar))
    def CtoR(self,tvar):
        return tvar * self._r_slope
    def RtoC(self,tvar):
        return tvar * self._r_slope_inv
    def KtoR(self,tvar):
        return self.CtoR(self.KtoC(tvar))
    def RtoK(self,tvar):
        return self.CtoK(self.RtoC(tvar))
    def FtoR(self,tvar):
        return self.CtoR(self.FtoC(tvar))
    def RtoF(self,tvar):
        return self.CtoF(self.RtoC(tvar))

tempCnv = TempCnv() 

def celsius(tvar):
    return tempCnv.KtoC(tvar)
def fahrenheit(tvar):
    return tempCnv.KtoF(tvar)

# -- radioactivity
becquerel     = 1. / second
curie         = 3.7e+10 * becquerel
kilobecquerel = 1.0e+03 * becquerel
megabecquerel = 1.0e+06 * becquerel
gigabecquerel = 1.0e+09 * becquerel
millicurie    = 1.0e-03 * curie
microcurie    = 1.0e-06 * curie

# -- absorbed dose
gray          = joule / kilogram
microgray     = 1.0e-06 * gray
milligray     = 1.0e-03 * gray
kilogray      = 1.0e+03 * gray

# -- luminous quantities
candela       = 1.                    ## -- luminous intensity
lumen         = candela * steradian   ## -- luminous flux
lux           = lumen / meter2        ## -- illuminance

# -- misc
perCent       = 0.01
perMille      = 0.001
perThousand   = perMille
perMillion    = 0.000001

#############
## Symbols ##
#############

mm  = millimeter
mm2 = millimeter2
mm3 = millimeter3

cm  = centimeter
cm2 = centimeter2
cm3 = centimeter3

m  = meter
m2 = meter2
m3 = meter3

km  = kilometer
km2 = kilometer2
km3 = kilometer3

pc  = parsec

mu  = micrometer
nm  = nanometer
pm  = picometer
fm  = femtometer
A   = angstrom

b   = barn     
mb  = millibarn
ub  = microbarn
nb  = nanonbarn
pb  = picobarn 
fb  = femtobarn

ib  = ibarn     
imb = imillibarn
iub = imicrobarn
inb = inanonbarn
ipb = ipicobarn 
ifb = ifemtobarn

L   = liter     
dL  = deciliter 
cL  = centiliter
mL  = milliliter

rad  = radian     
mrad = milliradian
deg  = degree     
sr   = steradian  

ns  = nanosecond 
s   = second     
ms  = millisecond
us  = microsecond
ps  = picosecond 
fs  = femtosecond
mn  = minute
hr  = hour  
dy  = day   
wk  = week  

Hz  = hertz      
kHz = kilohertz  
MHz = megahertz  
GHz = gigahertz

eV  = electronvolt    
keV = kiloelectronvolt
MeV = megaelectronvolt
GeV = gigaelectronvolt
TeV = teraelectronvolt
PeV = petaelectronvolt

mg  = milligram
g   = gram     
kg  = kilogram 

W   = watt
kW  = kilowatt
MW  = megawatt
GW  = gigawatt

A   = ampere
mA  = milliampere 
uA  = microampere 
nA  = nanoampere 

J   = joule
Wh  = watthour    
kWh = kilowatthour
MWh = megawatthour
GWh = gigawatthour

e   = eplus       
C   = coulomb     
mC  = millicoulomb
uC  = microcoulomb
nC  = nanocoulomb 
pC  = picocoulomb 
fC  = femtocoulomb

N   = newton    
kN  = kilonewton

V   = volt    
kV  = kilovolt
MV  = megavolt

O   = ohm
kO  = kiloohm
MO  = megaohm

F   = farad     
mF  = millifarad
uF  = microfarad
nF  = nanofarad 
pF  = picofarad 

Wb  = weber    
T   = tesla    
Gs  = gauss    
kGs = kilogauss
H   = henry

K    = kelvin
degK = K
degC = celsius
degF = fahrenheit

Bq  = becquerel
kBq = kilobecquerel
MBq = megabecquerel
GBq = gigabecquerel
Ci  = curie
mCi = millicurie
uCi = microcurie

uGy = microgray
mGy = milligray
Gy  = gray     
kGy = kilogray 

Pa   = pascal
hPa  = hectopascal
kPa  = kilopascal    
mbar = millibar  
atm  = atmosphere

cd   = candela
lm   = lumen  
lx   = lux

class UnitDescr():
    def __init__(self,scale=1.,name=None,abbr=None,symbol=None,axis=None,latex_axis=None,root_axis=None,is_base=False):
        self._scale      = scale
        self._name       = name
        self._abbr       = abbr 
        self._symbol     = symbol
        self._axis       = axis
        self._latex_axis = latex_axis
        self._root_axis  = root_axis
        self._is_base    = is_base 
        self._mname      = f'Unit::{__class__.__name__}'
    def name(self) -> str:
        return self._name
    def scale(self) -> float:
        return self._scale
    def abbr(self) -> str:
        return self._abbr
    def symbol(self) -> str:
        return self._symbol
    def axis(self,mode='text') -> str:
        if mode == 'text':
            return self._axis
        elif mode == 'root':
            return self._root_axis
        elif mode == 'latex':
            return self._latex_axis
        else:
            mprint(module=self._mname,level=msgWarning,message=f'unknow axis mode \'{mode}\' - no axis returned')
            return None
    def value(self,val) -> float:
        return val * self._scale
    def r_value(self,val) -> float:
        return val / self._scale if self._scale != 0 else 0.
    def is_base(self) -> bool:
        return self._is_base

unitDescrDict = {
    'mm'    : UnitDescr(scale=mm , name='millimeter'      ,abbr='mm'  ,symbol='mm'   ,axis='mm'        ,latex_axis='\ensuremath{\text{mm}}'       ,root_axis='mm'           ,is_base=True ),
    'mm2'   : UnitDescr(scale=mm2, name='millimeter2'     ,abbr='mm2' ,symbol='mm2'  ,axis='mm^{2}'    ,latex_axis='\ensuremath{\text{mm}^{2}}'   ,root_axis='mm^{2}'       ,is_base=False),
    'mm3'   : UnitDescr(scale=mm3, name='millimeter3'     ,abbr='mm3' ,symbol='mm3'  ,axis='mm^{3}'    ,latex_axis='\ensuremath{\text{mm}^{3}'    ,root_axis='mm^{3}'       ,is_base=False),
    'cm'    : UnitDescr(scale=cm , name='centimeter'      ,abbr='cm'  ,symbol='cm'   ,axis='cm'        ,latex_axis='\ensuremath{\text{cm}}'       ,root_axis='cm'           ,is_base=False),
    'cm2'   : UnitDescr(scale=cm2, name='centimeter2'     ,abbr='cm2' ,symbol='cm2'  ,axis='cm^{2}'    ,latex_axis='\ensuremath{\text{cm}^{2}}'   ,root_axis='cm^{2}'       ,is_base=False),
    'cm3'   : UnitDescr(scale=cm3, name='centimeter3'     ,abbr='cm3' ,symbol='cm3'  ,axis='cm^{3}'    ,latex_axis='\ensuremath{\text{cm}^{3}}'   ,root_axis='cm^{3}'       ,is_base=False),
    'm'     : UnitDescr(scale=m  , name='meter'           ,abbr='m'   ,symbol='mm'   ,axis='mm'        ,latex_axis='\ensuremath{\text{mm}}'       ,root_axis='mm'           ,is_base=False),
    'm2'    : UnitDescr(scale=m2 , name='meter2'          ,abbr='m2'  ,symbol='mm2'  ,axis='m^{2}'     ,latex_axis='\ensuremath{\text{m}^{2}}'    ,root_axis='m^{2}'        ,is_base=False),
    'm3'    : UnitDescr(scale=m3 , name='meter3'          ,abbr='m3'  ,symbol='mm3'  ,axis='m^{3}'     ,latex_axis='\ensuremath{\text{m}^{3}}'    ,root_axis='m^{3}'        ,is_base=False),
    'km'    : UnitDescr(scale=km , name='centimeter'      ,abbr='km'  ,symbol='km'   ,axis='km'        ,latex_axis='\ensuremath{\text{km}}'       ,root_axis='km'           ,is_base=False),
    'km2'   : UnitDescr(scale=km2, name='centimeter2'     ,abbr='km2' ,symbol='km2'  ,axis='km^{2}'    ,latex_axis='\ensuremath{\text{km}^{2}}'   ,root_axis='km^{2}'       ,is_base=False),
    'km3'   : UnitDescr(scale=km3, name='centimeter3'     ,abbr='km3' ,symbol='km3'  ,axis='km^{3}'    ,latex_axis='\ensuremath{\text{km}^{3}}'   ,root_axis='km^{3}'       ,is_base=False),
    'pc'    : UnitDescr(scale=pc,  name='parsec'          ,abbr='pc'  ,symbol='pc'   ,axis='pc'        ,latex_axis='\ensuremath{\text{pc}}'       ,root_axis='pc'           ,is_base=False),
    'mu'    : UnitDescr(scale=mu,  name='micrometer'      ,abbr='mu'  ,symbol='μm'   ,axis='\mu m'     ,latex_axis='\ensuremath{\mu\text{m}}'     ,root_axis='#mum'         ,is_base=False),
    'nm'    : UnitDescr(scale=nm,  name='nanometer'       ,abbr='nm'  ,symbol='nm'   ,axis='nm'        ,latex_axis='\ensuremath{\text{nm}}'       ,root_axis='nm'           ,is_base=False),
    'pm'    : UnitDescr(scale=pm,  name='picometer'       ,abbr='pm'  ,symbol='pm'   ,axis='pm'        ,latex_axis='\ensuremath{\text{pm}}'       ,root_axis='pm'           ,is_base=False),
    'fm'    : UnitDescr(scale=fm,  name='femtometer'      ,abbr='fm'  ,symbol='fm'   ,axis='fm'        ,latex_axis='\ensuremath{\text{fm}}'       ,root_axis='fm'           ,is_base=False),
    'fermi' : UnitDescr(scale=fm,  name='femtometer'      ,abbr='fm'  ,symbol='fm'   ,axis='fm'        ,latex_axis='\ensuremath{\text{fm}}'       ,root_axis='fm'           ,is_base=False),
    'A'     : UnitDescr(scale=A,   name='angstrom'        ,abbr='A'   ,symbol='A'    ,axis='\AA'       ,latex_axis='\ensuremath{\AA}'             ,root_axis='A'            ,is_base=False),
    'b'     : UnitDescr(scale=b,   name='barn'            ,abbr='b'   ,symbol='b'    ,axis='b'         ,latex_axis='\ensuremath{\text{b}}'        ,root_axis='b'            ,is_base=False),
    'mb'    : UnitDescr(scale=mb,  name='millibarn'       ,abbr='mb'  ,symbol='mb'   ,axis='mb'        ,latex_axis='\ensuremath{\text{mb}}'       ,root_axis='mb'           ,is_base=False),
    'ub'    : UnitDescr(scale=ub,  name='microbarn'       ,abbr='ub'  ,symbol='μb'   ,axis='\mu b'     ,latex_axis='\ensuremath{\mu\text{b}}'     ,root_axis='#mub'         ,is_base=False),
    'nb'    : UnitDescr(scale=nb,  name='nanobarn'        ,abbr='nb'  ,symbol='nb'   ,axis='nb'        ,latex_axis='\ensuremath{\text{nb}}'       ,root_axis='nb'           ,is_base=False),
    'pb'    : UnitDescr(scale=pb,  name='picobarn'        ,abbr='pb'  ,symbol='pb'   ,axis='pb'        ,latex_axis='\ensuremath{\text{pb}}'       ,root_axis='pb'           ,is_base=False),
    'fb'    : UnitDescr(scale=fb,  name='femtobarn'       ,abbr='fb'  ,symbol='fb'   ,axis='fb'        ,latex_axis='\ensuremath{\text{fb}}'       ,root_axis='fb'           ,is_base=False),
    'ib'    : UnitDescr(scale=b,   name='ibarn'           ,abbr='ib'  ,symbol='b^-1' ,axis='b^{-1}'    ,latex_axis='\ensuremath{\text{b}^{-1}}'   ,root_axis='b^{-1}'       ,is_base=False),
    'imb'   : UnitDescr(scale=mb,  name='imillibarn'      ,abbr='imb' ,symbol='mb^-1',axis='mb^{-1}'   ,latex_axis='\ensuremath{\text{mb}^{-1}}'  ,root_axis='mb^{-1}'      ,is_base=False),
    'iub'   : UnitDescr(scale=ub,  name='imicrobarn'      ,abbr='iub' ,symbol='μb^-1',axis='\mu b^{-1}',latex_axis='\ensuremath{\mu\text{b}^{-1}}',root_axis='#mub^{-1}'    ,is_base=False),
    'inb'   : UnitDescr(scale=nb,  name='inanobarn'       ,abbr='inb' ,symbol='nb^-1',axis='nb^{-1}'   ,latex_axis='\ensuremath{\text{nb}^{-1}}'  ,root_axis='nb^{-1}'      ,is_base=False),
    'ipb'   : UnitDescr(scale=pb,  name='ipicobarn'       ,abbr='ipb' ,symbol='pb^-1',axis='pb^{-1}'   ,latex_axis='\ensuremath{\text{pb}^{-1}}'  ,root_axis='pb^{-1}'      ,is_base=False),
    'ifb'   : UnitDescr(scale=fb,  name='ifemtobarn'      ,abbr='ifb' ,symbol='fb^-1',axis='fb^{-1}'   ,latex_axis='\ensuremath{\text{fb}^{-1}}'  ,root_axis='fb^{-1}'      ,is_base=False),
    'L'     : UnitDescr(scale=L,   name='liter'           ,abbr='L'   ,symbol='l'    ,axis='l'         ,latex_axis='\ensuremath{\text{l}}'        ,root_axis='l'            ,is_base=False),
    'dL'    : UnitDescr(scale=dL,  name='deciliter'       ,abbr='dL'  ,symbol='dl'   ,axis='dl'        ,latex_axis='\ensuremath{10^{-1}\text{l}}' ,root_axis='10^{#minus1}l',is_base=False),
    'cL'    : UnitDescr(scale=cL,  name='centiliter'      ,abbr='cL'  ,symbol='cl'   ,axis='cl'        ,latex_axis='\ensuremath{10^{-2}\text{l}}' ,root_axis='10^{#minus2}l',is_base=False),
    'mL'    : UnitDescr(scale=mL,  name='milliliter'      ,abbr='mL'  ,symbol='ml'   ,axis='ml'        ,latex_axis='\ensuremath{10^{-3}\text{l}}' ,root_axis='10^{#minus3}l',is_base=False),
    'rad'   : UnitDescr(scale=rad, name='radian'          ,abbr='rad' ,symbol='rad'  ,axis='rad'       ,latex_axis='\ensuremath{\text{rad}}'      ,root_axis='rad'          ,is_base=True ),
    'mrad'  : UnitDescr(scale=mrad,name='mradian'         ,abbr='mrad',symbol='mrad' ,axis='mrad'      ,latex_axis='\ensuremath{\text{mrad}}'     ,root_axis='mrad'         ,is_base=False),
    'deg'   : UnitDescr(scale=deg ,name='degree'          ,abbr='deg' ,symbol='°'    ,axis='^{\circ}'  ,latex_axis='\ensuremath{^{\circ}}'        ,root_axis='^{#circ}'     ,is_base=False),
    'sr'    : UnitDescr(scale=sr  ,name='steradian'       ,abbr='sr'  ,symbol='sr'   ,axis='sr'        ,latex_axis='\ensuremath{\text{sr}}'       ,root_axis='sr'           ,is_base=False),
    'ns'    : UnitDescr(scale=ns  ,name='nanosecond'      ,abbr='ns'  ,symbol='ns'   ,axis='ns'        ,latex_axis='\ensuremath{\text{ns}}'       ,root_axis='ns'           ,is_base=True ),
    's'     : UnitDescr(scale=s   ,name='second'          ,abbr='s'   ,symbol='s'    ,axis='s'         ,latex_axis='\ensuremath{\text{s}}'        ,root_axis='s'            ,is_base=False),
    'ms'    : UnitDescr(scale=ms  ,name='millisecond'     ,abbr='ms'  ,symbol='ms'   ,axis='ms'        ,latex_axis='\ensuremath{\text{ms}}'       ,root_axis='ms'           ,is_base=False),
    'us'    : UnitDescr(scale=us  ,name='microsecond'     ,abbr='us'  ,symbol='μs'   ,axis='\mu s'     ,latex_axis='\ensuremath{\mu\text{s}}'     ,root_axis='#mus'         ,is_base=False),
    'ps'    : UnitDescr(scale=ps  ,name='picosecond'      ,abbr='ps'  ,symbol='ps'   ,axis='ps'        ,latex_axis='\ensuremath{\text{ps}}'       ,root_axis='ps'           ,is_base=False),
    'fs'    : UnitDescr(scale=fs  ,name='femtosecond'     ,abbr='fs'  ,symbol='fs'   ,axis='fs'        ,latex_axis='\ensuremath{\text{fs}}'       ,root_axis='fs'           ,is_base=False),
    'mn'    : UnitDescr(scale=mn  ,name='minute'          ,abbr='mn'  ,symbol='mn'   ,axis='mn'        ,latex_axis='\ensuremath{\text{mn}}'       ,root_axis='mn'           ,is_base=False),
    'hr'    : UnitDescr(scale=hr  ,name='hour'            ,abbr='hr'  ,symbol='hr'   ,axis='hrs'       ,latex_axis='\ensuremath{\text{hrs}}'      ,root_axis='hrs'          ,is_base=False), 
    'dy'    : UnitDescr(scale=dy  ,name='day'             ,abbr='dy'  ,symbol='dy'   ,axis='days'      ,latex_axis='\ensuremath{\text{days}}'     ,root_axis='days'         ,is_base=False),
    'wk'    : UnitDescr(scale=wk  ,name='week'            ,abbr='wk'  ,symbol='wk'   ,axis='weeks'     ,latex_axis='\ensuremath{\text{weeks}}'    ,root_axis='weeks'        ,is_base=False),
    'Hz'    : UnitDescr(scale=Hz  ,name='hertz'           ,abbr='Hz'  ,symbol='Hz'   ,axis='Hz'        ,latex_axis='\ensuremath{\text{Hz}}'       ,root_axis='Hz'           ,is_base=False),      
    'kHz'   : UnitDescr(scale=kHz ,name='kilohertz'       ,abbr='kHz' ,symbol='kHz'  ,axis='kHz'       ,latex_axis='\ensuremath{\text{kHz}}'      ,root_axis='kHz'          ,is_base=False),
    'MHz'   : UnitDescr(scale=MHz ,name='megahertz'       ,abbr='MHz' ,symbol='MHz'  ,axis='MHz'       ,latex_axis='\ensuremath{\text{MHz}}'      ,root_axis='MHz'          ,is_base=False),
    'GHz'   : UnitDescr(scale=GHz ,name='gigahertz'       ,abbr='GHz' ,symbol='GHz'  ,axis='GHz'       ,latex_axis='\ensuremath{\text{GHz}}'      ,root_axis='GHz'          ,is_base=False),
    'eV'    : UnitDescr(scale=eV  ,name='electronvolt'    ,abbr='eV'  ,symbol='eV'   ,axis='eV'        ,latex_axis='\ensuremath{\text{eV}}'       ,root_axis='eV'           ,is_base=False),      
    'keV'   : UnitDescr(scale=keV ,name='kiloelectronvolt',abbr='keV' ,symbol='keV'  ,axis='keV'       ,latex_axis='\ensuremath{\text{keV}}'      ,root_axis='keV'          ,is_base=False),  
    'MeV'   : UnitDescr(scale=MeV ,name='megaelectronvolt',abbr='MeV' ,symbol='MeV'  ,axis='MeV'       ,latex_axis='\ensuremath{\text{MeV}}'      ,root_axis='MeV'          ,is_base=True ),  
    'GeV'   : UnitDescr(scale=GeV ,name='gigaelectronvolt',abbr='GeV' ,symbol='GeV'  ,axis='GeV'       ,latex_axis='\ensuremath{\text{GeV}}'      ,root_axis='GeV'          ,is_base=False),  
    'TeV'   : UnitDescr(scale=TeV ,name='teraelectronvolt',abbr='TeV' ,symbol='TeV'  ,axis='TeV'       ,latex_axis='\ensuremath{\text{TeV}}'      ,root_axis='TeV'          ,is_base=False),  
    'PeV'   : UnitDescr(scale=PeV ,name='petaelectronvolt',abbr='PeV' ,symbol='PeV'  ,axis='PeV'       ,latex_axis='\ensuremath{\text{PeV}}'      ,root_axis='PeV'          ,is_base=False),  
    'mg'    : UnitDescr(scale=mg  ,name='milligram'       ,abbr='mg'  ,symbol='mg'   ,axis='mg'        ,latex_axis='\ensuremath{\text{mg}}'       ,root_axis='mg'           ,is_base=False),
    'g'     : UnitDescr(scale=g   ,name='gram'            ,abbr='g'   ,symbol='g'    ,axis='g'         ,latex_axis='\ensuremath{\text{g}}'        ,root_axis='g'            ,is_base=False),
    'kg'    : UnitDescr(scale=kg  ,name='kilogram'        ,abbr='kg'  ,symbol='kg'   ,axis='kg'        ,latex_axis='\ensuremath{\text{kg}}'       ,root_axis='kg'           ,is_base=False), 
    'W'     : UnitDescr(scale=W   ,name='watt'            ,abbr='W'   ,symbol='W'    ,axis='W'         ,latex_axis='\ensuremath{\text{W}}'        ,root_axis='W'            ,is_base=False),
    'kW'    : UnitDescr(scale=kW  ,name='kilowatt'        ,abbr='kW'  ,symbol='kW'   ,axis='kW'        ,latex_axis='\ensuremath{\text{kW}}'       ,root_axis='kW'           ,is_base=False),
    'MW'    : UnitDescr(scale=MW  ,name='megawatt'        ,abbr='MW'  ,symbol='MW'   ,axis='MW'        ,latex_axis='\ensuremath{\text{MW}}'       ,root_axis='MW'           ,is_base=False),
    'GW'    : UnitDescr(scale=GW  ,name='gigawatt'        ,abbr='GW'  ,symbol='GW'   ,axis='GW'        ,latex_axis='\ensuremath{\text{GW}}'       ,root_axis='GW'           ,is_base=False),
    'A'     : UnitDescr(scale=A   ,name='ampere'          ,abbr='A'   ,symbol='A'    ,axis='A'         ,latex_axis='\ensuremath{\text{A}}'        ,root_axis='A'            ,is_base=False),
    'mA'    : UnitDescr(scale=mA  ,name='milliampere'     ,abbr='mA'  ,symbol='mA'   ,axis='mA'        ,latex_axis='\ensuremath{\text{mA}}'       ,root_axis='mA'           ,is_base=False),
    'uA'    : UnitDescr(scale=uA  ,name='microampere'     ,abbr='uA'  ,symbol='μA'   ,axis='\mu A'     ,latex_axis='\ensuremath{\mu\text{A}}'     ,root_axis='#muA'         ,is_base=False),
    'nA'    : UnitDescr(scale=nA  ,name='nanoampere'      ,abbr='nA'  ,symbol='nA'   ,axis='nA'        ,latex_axis='\ensuremath{\text{nA}}'       ,root_axis='nA'           ,is_base=False),
    'J'     : UnitDescr(scale=J   ,name='joule'           ,abbr='J'   ,symbol='J'    ,axis='J'         ,latex_axis='\ensuremath{\text{J}} '       ,root_axis='J'            ,is_base=False),
    'Wh'    : UnitDescr(scale=Wh  ,name='watthour'        ,abbr='Wh'  ,symbol='Wh'   ,axis='Wh'        ,latex_axis='\ensuremath{\text{Wh}}'       ,root_axis='Wh'           ,is_base=False),
    'kWh'   : UnitDescr(scale=kWh ,name='kilowatthour'    ,abbr='kWh' ,symbol='kWh'  ,axis='kWh'       ,latex_axis='\ensuremath{\text{kWh}}'      ,root_axis='kWh'          ,is_base=False),
    'MWh'   : UnitDescr(scale=MWh ,name='megawatthour'    ,abbr='MWh' ,symbol='MWh'  ,axis='MWh'       ,latex_axis='\ensuremath{\text{MWh}}'      ,root_axis='MWh'          ,is_base=False),
    'GWh'   : UnitDescr(scale=GWh ,name='gigawatthour'    ,abbr='GWh' ,symbol='GWh'  ,axis='GWh'       ,latex_axis='\ensuremath{\text{GWh}}'      ,root_axis='GWh'          ,is_base=False),
    'e'     : UnitDescr(scale=e   ,name='eplus'           ,abbr='e'   ,symbol='e'    ,axis='e'         ,latex_axis='\ensuremath{q_{\text{e}}}'    ,root_axis='q_{e}'        ,is_base=True ),     
    'C'     : UnitDescr(scale=C   ,name='coulomb'         ,abbr='C'   ,symbol='C'    ,axis='C'         ,latex_axis='\ensuremath{\text{C}}'        ,root_axis='C'            ,is_base=False),
    'mC'    : UnitDescr(scale=mC  ,name='millicoulomb'    ,abbr='mC'  ,symbol='mC'   ,axis='mC'        ,latex_axis='\ensuremath{\text{mC}}'       ,root_axis='mC'           ,is_base=False),
    'uC'    : UnitDescr(scale=uC  ,name='microcoulomb'    ,abbr='uC'  ,symbol='μC'   ,axis='\mu C'     ,latex_axis='\ensuremath{\mu\text{C}}'     ,root_axis='#muC'         ,is_base=False),
    'nC'    : UnitDescr(scale=nC  ,name='nanocoulomb'     ,abbr='nC'  ,symbol='nC'   ,axis='nC'        ,latex_axis='\ensuremath{\text{nC}}'       ,root_axis='nC'           ,is_base=False),
    'pC'    : UnitDescr(scale=pC  ,name='picocoulomb'     ,abbr='pC'  ,symbol='pC'   ,axis='pC'        ,latex_axis='\ensuremath{\text{pC}}'       ,root_axis='pC'           ,is_base=False),
    'fC'    : UnitDescr(scale=fC  ,name='femtocoulomb'    ,abbr='fC'  ,symbol='fC'   ,axis='fC'        ,latex_axis='\ensuremath{\text{fC}}'       ,root_axis='fC'           ,is_base=False),
    'N'     : UnitDescr(scale=N   ,name='newton'          ,abbr='N'   ,symbol='N'    ,axis='N'         ,latex_axis='\ensuremath{\text{N}}'        ,root_axis='N'            ,is_base=False),               
    'kN'    : UnitDescr(scale=kN  ,name='kilonewton'      ,abbr='kN'  ,symbol='kN'   ,axis='kN'        ,latex_axis='\ensuremath{\text{kN}}'       ,root_axis='kN'           ,is_base=False),   
    'V'     : UnitDescr(scale=V   ,name='volt'            ,abbr='V'   ,symbol='V'    ,axis='V'         ,latex_axis='\ensuremath{\text{V}}'        ,root_axis='V'            ,is_base=False),     
    'kV'    : UnitDescr(scale=kV  ,name='kilovolt'        ,abbr='kV'  ,symbol='kV'   ,axis='kV'        ,latex_axis='\ensuremath{\text{kV}}'       ,root_axis='kV'           ,is_base=False),    
    'MV'    : UnitDescr(scale=MV  ,name='megavolt'        ,abbr='MV'  ,symbol='MV'   ,axis='MV'        ,latex_axis='\ensuremath{\text{MV}}'       ,root_axis='MV'           ,is_base=False),
    'O'     : UnitDescr(scale=O   ,name='ohm'             ,abbr='O'   ,symbol='Ω'    ,axis='\Omega'    ,latex_axis='\ensuremath{\Omega}'          ,root_axis='#Omega'       ,is_base=False),     
    'kO'    : UnitDescr(scale=kO  ,name='kiloohm'         ,abbr='kO'  ,symbol='kΩ'   ,axis='k\Omega'   ,latex_axis='\ensuremath{\text{k}\Omega}'  ,root_axis='k#Omega'      ,is_base=False), 
    'MO'    : UnitDescr(scale=MO  ,name='megaohm'         ,abbr='MO'  ,symbol='MΩ'   ,axis='M\Omega'   ,latex_axis='\ensuremath{\text{M}\Omega}'  ,root_axis='M#Omega'      ,is_base=False), 
    'F'     : UnitDescr(scale=F   ,name='farad'           ,abbr='F'   ,symbol='F'    ,axis='F'         ,latex_axis='\ensuremath{\text{F}}'        ,root_axis='F'            ,is_base=False), 
    'mF'    : UnitDescr(scale=mF  ,name='millifarad'      ,abbr='mF'  ,symbol='mF'   ,axis='mF'        ,latex_axis='\ensuremath{\text{mF}}'       ,root_axis='mF'           ,is_base=False),
    'uF'    : UnitDescr(scale=uF  ,name='microfarad'      ,abbr='uF'  ,symbol='μF'   ,axis='\mu F'     ,latex_axis='\ensuremath{\mu\text{F}}'     ,root_axis='#muF'         ,is_base=False),
    'nF'    : UnitDescr(scale=nF  ,name='nanofarad'       ,abbr='nF'  ,symbol='nF'   ,axis='nF'        ,latex_axis='\ensuremath{\text{nF}}'       ,root_axis='nF'           ,is_base=False),
    'pF'    : UnitDescr(scale=pF  ,name='picofarad'       ,abbr='pF'  ,symbol='pF'   ,axis='pF'        ,latex_axis='\ensuremath{\text{pF}}'       ,root_axis='pF'           ,is_base=False),
    'Wb'    : UnitDescr(scale=Wb  ,name='weber'           ,abbr='Wb'  ,symbol='Wb'   ,axis='Wb'        ,latex_axis='\ensuremath{\text{Wb}}'       ,root_axis='Wb'           ,is_base=False),    
    'T'     : UnitDescr(scale=T   ,name='tesla'           ,abbr='T'   ,symbol='T'    ,axis='T'         ,latex_axis='\ensuremath{\text{T}}'        ,root_axis='T'            ,is_base=False),
    'Gs'    : UnitDescr(scale=Gs  ,name='gauss'           ,abbr='Gs'  ,symbol='Gs'   ,axis='Gs'        ,latex_axis='\ensuremath{\text{Gs}}'       ,root_axis='Gs'           ,is_base=False),
    'kGs'   : UnitDescr(scale=kGs ,name='kilogauss'       ,abbr='kGs' ,symbol='kGs'  ,axis='kGs'       ,latex_axis='\ensuremath{\text{kGs}}'      ,root_axis='kGs'          ,is_base=False),
    'H'     : UnitDescr(scale=H   ,name='henry'           ,abbr='H'   ,symbol='H'    ,axis='H'         ,latex_axis='\ensuremath{\text{H}}'        ,root_axis='H'            ,is_base=False),
    'degK'  : UnitDescr(scale=1   ,name='kelvin'          ,abbr='K'   ,symbol='°K'   ,axis='^{\circ}K' ,latex_axis='\ensuremath{^{\circ}\text{K}}',root_axis='^{#circ}K'    ,is_base=True ),
    'degC'  : UnitDescr(scale=None,name='celsius'         ,abbr='C'   ,symbol='°C'   ,axis='^{\circ}C' ,latex_axis='\ensuremath{^{\circ}\text{C}}',root_axis='^{#circ}C'    ,is_base=False),
    'degF'  : UnitDescr(scale=None,name='fahrenheit'      ,abbr='F'   ,symbol='°F'   ,axis='^{\circ}F' ,latex_axis='\ensuremath{^{\circ}\text{F}}',root_axis='^{#circ}F'    ,is_base=False),
    'Bq'    : UnitDescr(scale=Bq  ,name='becquerel'       ,abbr='Bq'  ,symbol='Bq'   ,axis='Bq'        ,latex_axis='\ensuremath{\text{Bq}}'       ,root_axis='Bq'           ,is_base=False),    
    'kBq'   : UnitDescr(scale=kBq ,name='kilobecquerel'   ,abbr='kBq' ,symbol='kBq'  ,axis='kBq'       ,latex_axis='\ensuremath{\text{kBq}}'      ,root_axis='kBq'          ,is_base=False),
    'MBq'   : UnitDescr(scale=MBq ,name='megabecquerel'   ,abbr='MBq' ,symbol='MBq'  ,axis='MBq'       ,latex_axis='\ensuremath{\text{MBq}}'      ,root_axis='MBq'          ,is_base=False),
    'GBq'   : UnitDescr(scale=GBq ,name='gigabecquerel'   ,abbr='GBq' ,symbol='GBq'  ,axis='GBq'       ,latex_axis='\ensuremath{\text{GBq}}'      ,root_axis='GBq'          ,is_base=False),
    'Ci'    : UnitDescr(scale=Ci  ,name='curie'           ,abbr='Ci'  ,symbol='Ci'   ,axis='Ci'        ,latex_axis='\ensuremath{\text{Ci}}'       ,root_axis='Ci'           ,is_base=False),
    'mCi'   : UnitDescr(scale=mCi ,name='millicurie'      ,abbr='mCi' ,symbol='mCi'  ,axis='mCi'       ,latex_axis='\ensuremath{\text{mCi}}'      ,root_axis='mCi'          ,is_base=False),
    'uCi'   : UnitDescr(scale=uCi ,name='microcurie'      ,abbr='uCi' ,symbol='μCi'  ,axis='\mu Ci'    ,latex_axis='\ensuremath{\mu\text{Ci}}'    ,root_axis='#muCi'        ,is_base=False),
    'uGy'   : UnitDescr(scale=uGy ,name='microgray'       ,abbr='uGy' ,symbol='μGy'  ,axis='\mu Gy'    ,latex_axis='\ensuremath{\mu\text{Gy}}'    ,root_axis='#muGy'        ,is_base=False), 
    'mGy'   : UnitDescr(scale=mGy ,name='milligray'       ,abbr='mGy' ,symbol='mGy'  ,axis='mGy'       ,latex_axis='\ensuremath{\text{mGy}}'      ,root_axis='mGy'          ,is_base=False),
    'Gy'    : UnitDescr(scale=Gy  ,name='gray'            ,abbr='Gy'  ,symbol='Gy'   ,axis='Gy'        ,latex_axis='\ensuremath{\text{Gy}}'       ,root_axis='Gy'           ,is_base=False),
    'kGy'   : UnitDescr(scale=kGy ,name='kilogray'        ,abbr='kGy' ,symbol='kGy'  ,axis='kGy'       ,latex_axis='\ensuremath{\text{kGy}}'      ,root_axis='kGy'          ,is_base=False),
    'Pa'    : UnitDescr(scale=Pa  ,name='pascal'          ,abbr='Pa'  ,symbol='Pa'   ,axis='Pa'        ,latex_axis='\ensuremath{\text{Pa}}'       ,root_axis='Pa'           ,is_base=False),   
    'hPa'   : UnitDescr(scale=hPa ,name='hectoPascal'     ,abbr='hPa' ,symbol='hPa'  ,axis='hPa'       ,latex_axis='\ensuremath{\text{hPa}}'      ,root_axis='hPa'          ,is_base=False),
    'kPa'   : UnitDescr(scale=kPa ,name='kilopascal'      ,abbr='kPa' ,symbol='kPa'  ,axis='kPa'       ,latex_axis='\ensuremath{\text{kPa}}'      ,root_axis='kPa'          ,is_base=False),
    'mbar'  : UnitDescr(scale=mbar,name='millibar'        ,abbr='mbar',symbol='mbar' ,axis='mbar'      ,latex_axis='\ensuremath{\text{mbar}}'     ,root_axis='mbar'         ,is_base=False),
    'atm'   : UnitDescr(scale=atm ,name='atmosphere'      ,abbr='atm' ,symbol='atm'  ,axis='atm'       ,latex_axis='\ensuremath{\text{atm}}'      ,root_axis='atm'          ,is_base=False),
    'cd'    : UnitDescr(scale=cd  ,name='candela'         ,abbr='cd'  ,symbol='cd'   ,axis='cd'        ,latex_axis='\ensuremath{\text{cd}}'       ,root_axis='cd'           ,is_base=True ),
    'lm'    : UnitDescr(scale=lm  ,name='lumen'           ,abbr='lm'  ,symbol='lm'   ,axis='lm'        ,latex_axis='\ensuremath{\text{lm}}'       ,root_axis='lm'           ,is_base=False),
    'lx'    : UnitDescr(scale=lx  ,name='lux'             ,abbr='lx'  ,symbol='lx'   ,axis='lx'        ,latex_axis='\ensuremath{\text{lx}}'       ,root_axis='lx'           ,is_base=False),
}

def find(key) -> bool:
    return key in unitDescrDict
def name(key) -> str:
    if find(key):
        return unitDescrDict[key].name() 
    else:
        mprint(module="units::name",level=msgWarning,message=f'unknown unit \'{key}\' - no name found')
        return None
def symbol(key) -> str:
    if find(key):
        return unitDescrDict[key].symbol()
    else:
        mprint(module="units::symbol",level=msgWarning,message=f'unknown unit \'{key}\' - no symbol found')
        return None 
def scale(key) -> float:
    if find(key):
        return unitDescrDict[key].scale()
    else:
        mprint(module="units::scale",level=msgWarning,message=f'unknown unit \'{key}\' - no scaling (scale = 1)')
        return 1.
def scale(key,value) -> float:
    fcts = { 'degC' : celsius, 'C' : celsius, 'degF' : fahrenheit, 'F' : fahrenheit }
    if key in fcts:
        return fcts[key](value)
    else:
        mprint(module="units::scale",level=msgWarning,message=f'unknown temperature \'{key}\' requested - unit of value is not changed')
        return value
def axis(key,mode='text') -> str:
    if find(key):
        return userDescrDict[key].axis(mode)
    else:
        mprint(module="units::axis",level=msgWarning,message=f'unknown unit \'{key}\' - no axis returned')
        return None
def is_base(key) -> bool:
    if find(key):
        return userDescrDict[key].is_base()
    else:
        mprint(module="units::is_base",level=msgWarning,message=f'unknown unit \'{key}\' - not a base unit')
        return False
def print_unit_keys(module=None,level=msgInfo) -> int:
    key_list = list(unitDesrcDict.keys())
    num_keys = len(key_list)
    if num_keys <= 0:
        mprint(module=module,level=msgWarning,message=f'found {num_keys} in known units dictionary, no print')
        return num_keys
    wid_cntr = unit.numDigits(num_keys+1)
    wid_keys = 0
    wid_name = 0
    for key in key_list:
        wid_keys = np.max([wid_keys,len(key)])
        wid_name = np.max([wid_name,name(key)])
    wid_keys += 2 ## account for decorations 
    wid_name += 2 ## account for decorations 
    wid_stat = cfg.msgFromBoolWidth(spec='primary')
    mprint(module=module,level=level,message=f'found {num_keys:{wid_cntr}} known units:')
    ikey = 1
    for key in key_list:
        ukey  = f'[{key}]'
        uname = f'\'{name(key)}\''
        fstat = is_base(key)
        ustat = msgFromBool(fstat,spec='primary')
        if key not in ['degC','degF']:
            mprint(module=module,level=msgLevel,message=f'     {ikey:>{wid_cntr}}. {ukey:{wid_key}} full name {uname:{wid_name}} status {ustat:{wid_stat}} scale factor {scale(key):<10.3e}')
        else:             
            mprint(module=module,level=msgLevel,message=f'     {ikey:>{wid_cntr}}. {ukey:{wid_key}} full name {uname:{wid_name}} status {ustat:{wid_stat}} scale factor N/A (conversion function available)')
    return num_keys

def print_banner():
    mname = 'unit::print_banner'
    msg = [ ' ',
            'The system of units provided by this module assumes that data is stored in the',
            '{GeV,ns,mm} system of units. This is not the ATLAS standard {MeV,ns,mm} frame',
            'but corresponds to the units used in the raw data for the ML studies. Nearly',
            'all units are extracted from the Gaudi SystemOfUnits module, with some',
            'extensions and adaptations to more modern naming conventions and abbreviations.', 
        ]
    title = 'Attention'
    mw = 0
    for m in msg:
        mw = np.max([len(m),mw])
    bl = mw + 6
    bc = '#'
    bb = f'{bc:{bc}^{bl}}'
    bu = '='
    bu = f'{bu:{bu}^{len(title)}}'

    mskip()
    mprint(module=mname,level=msgInfo,message=bb,color='yellow')
    mprint(module=mname,level=msgInfo,message=f'## {title:^{mw}} ##',color='yellow')
    mprint(module=mname,level=msgInfo,message=f'## {bu:^{mw}} ##',color='yellow')
    for m in msg:
        mprint(module=mname,level=msgInfo,message=f'## {m:<{mw}} ##',color='yellow')
    mprint(module=mname,level=msgInfo,message=bb,color='yellow')
    mskip()


