
'''
This module provides functionality to test/validate a trained network.
'''

import config as cfg

mprint  = cfg.msgPrint          
mskip   = cfg.msgSkipLine       
msprint = cfg.msgPrintSeparator 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()

import utils

import torch as nn

##from bayesian import  

module_name = 'test'

class Provider():
    # -- at object construction
    def __init__(self,model=None,file=None,mode='state',msglevel=msgVerbose):
        ## -- model load state
        self._known_modes = [ 'full', 'state' ]
        ## -- collect class properties
        self._model      = model
        self._model_dict = model.state_dict() if model != None else None
        self._file       = file
        self._mode       = mode
        self._msglevel   = msglevel
        self._name       = utils.moduleName(module_name,self)
        ## -- object state
        self._is_active = self._file != None and ( self._mode == 'full' or ( self._mode == 'state' and self._model != None ) )
        ## -- load model
        if self._is_active:
            if self._mode == 'state':
                self._model_dict = nn.load(self._file)
                self._model.load_state_dict(self._model_dict)
                self._model.eval()
            else:
                self._model = nn.load(self._file)
                self._model.eval()
            ## -- print object configuration and state
            mprint(module=self._name,level=self._msglevel,message=f'instantiated with object state {cfg.mbool(self._is_active)} and model read from file \'{self._file}\' in mode \'{self._mode}\':')
            mprint(module=self._name,level=self._msglevel,message=cfg.msgExtract(f'{self._model}'))
        else:
            ffile  = cfg.msgFromBool((self._file!=None) ,spec='valid' )
            fmode  = cfg.msgFromBool((self._mode!=None) ,spec='valid' )
            fmodel = cfg.msgFromBool((self._model!=None),spec='valid' ) 
            factiv = cfg.msgFromBool(self._is_active    ,spec='active')
            mprint(module=self._name,level=msgWarning,message=f'instantiated with object state {factiv}, {fmodel} model, {ffile} file \'{self._file}\' and {fmode} mode \'{self._mode}\'')
    
    ## -- retrieve model
    def __call__(self):
        return self._model
    def model(self):
        return self._model

    ## -- representation
    def __repr__(self):
        return f'{self._name}'  


            

