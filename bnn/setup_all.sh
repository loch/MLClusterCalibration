# activate the miniconda environment for python - DO THIS FIRST!
conda activate ~/raid01/tensorflow0
# set up the ATLAS environment
setupATLAS -3
# set up git
lsetup git
# set up root (via LCG) -- python version mismatch, don't do this!
# lsetup "views LCG_102 x86_64-centos7-gcc11-opt"
