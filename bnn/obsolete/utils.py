
import config as cfg
import pandas as pd
from   pandas import DataFrame as dframe
import numpy as np
import random
import time

# -- used externals
from config import msgPrint     as mprint
from config import msgSkipLine  as mskip
from config import msgSeparator as msep

''' 
Build list of formatted lines:

This is basically a table with ncols and len(array) lines where the columns are ',' separated. If as_string is set to True, the entries in array are enclosed in "". A prefix can be specified to be attached to the beginnig of each line.

'''

def fmtArray1d(array,ncols=5,as_string=True,prefix='     '):

    # -- find array length (rows)
    nrows = len(array)
    nwdth = int(0)

    # -- convert input to array of strings and measure width
    strarray = []
    for a in array:
        strarray.append(f'\042{a}\042' if as_string else f'{a}')
        nwdth = np.max([len(strarray[len(strarray)-1])+2,nwdth])
    # -- produce array of lines
    slines = []          ## array to capture result
    icol   = ncols       ## column counter
    pstr   = f'{prefix}' ## line prefix
    for irow in range(0,nrows):
        if icol == ncols:
            if irow > 0: 
                slines.append(pstr)
                pstr = f'{prefix}'
            icol = 0
        astr = f'{strarray[irow]},' if irow < nrows-1 else f'{strarray[irow]}'
        pstr += f'{astr:{nwdth}}'
        icol += 1
        if irow == nrows-1: 
            slines.append(pstr)

    # -- return list of lines
    return slines

def printArray1d(array):
    for astr in array: print(astr)
    

#################
## Data loader ##
#################

'''
Load nrecords data records from a file specified by filePath and saves them into a pandas.DataFrame (df). The fileType can be specified (presently only 'csv' is supported), in addition to fieldDelimiter to be used instead of the default ','. A dictionary of filters can be provided, which removes records not passing the specified filter. The column headers of df can be specified in the features argumnent. The resulting (potentially filtered) data frame is returned.   
'''

def dataLoader(nrecords=cfg.training_size,filePath=cfg.input_file_path,fileType=cfg.input_file_type,fieldDelimiter=cfg.input_file_field_sep,filters=cfg.input_filters,features=[]):
    mname = 'utils::dataLoader'
    # -- not yet implemented file type
    if fileType != 'csv':
        mprint(mname,'WARN',f'file type <{fileType}> not yet implemented, try <csv> instead')
        fileType = 'csv'
    # -- read input data (all or specified number)
    df = []
    if nrecords > 0:
        if fileType == 'csv':
            mprint(mname,'INFO',f'read {nrecords:d} data records from {filePath} (be patient)')
            df = pd.read_csv(filePath, delimiter=fieldDelimiter, nrows=nrecords)
            if df.shape[0] < nrecords: 
                mprint(mname,'WARN',f'only {df.shape[0]:d} data records from {filePath} available, out of {nrecords} requested')
    else:
        if fileType == 'csv':
            mprint(mname,'INFO',f'read all data records from {filePath} (be patient)')
            df = pd.read_csv(filePath, delimiter=fieldDelimiter)

    ishape = df.shape[0]
    mprint(mname,'INFO',f'loaded {ishape} data records into database of shape {df.shape}')

    # -- filter input data based on features
    keys = []
    for f in features:
        keys.append(f)
    if 'cluster_ENG_CALIB_TOT' not in keys:
        keys.append('cluster_ENG_CALIB_TOT')
    ishape  = df.shape[0]
    fshape  = 0
    pshape  = int(np.log10(ishape))+1
    initial = True
    for f in keys:
        if f in filters:
            cstr   = filters[f]
            fcmd   = f'df[\042{f}\042] {cstr}'
            df     = df.loc[eval(fcmd)]
            fshape = df.shape[0]
            if initial:
                pshape = int(np.log10(fshape))+1
            fpct   = float(fshape)/float(ishape)*100.
            mprint(mname,'INFO',f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fpct:5.1f}%)') 

    # -- filter input data based on derived quantities
    f = 'clusterResponseEM'
    if f in filters: 
        clusterResponseEM = df["clusterE"] / df["cluster_ENG_CALIB_TOT"]
        cstr              = filters[f] 
        fcmd              = f'df.loc[clusterResponseEM {cstr}]' 
        df                = eval(fcmd)
        fshape            = df.shape[0]
        mprint(mname,'INFO',f'filter {fcmd:36} applied, {fshape:{pshape}}/{ishape:{pshape}} records kept ({fshape/ishape*100:5.1f}%) [selection based on derived quantity]') 
 
    # return data set
    mprint(mname,'INFO',f'returned {df.shape[0]} data records after filters')
    return df

#####################
## Compose samples ##
#####################

def composeSamples(df,train_fraction=0.50,valid_fraction=0.20,test_fraction=-1):
    mname = 'utils::composeSamples'
    #
    if test_fraction < 0:
        test_fraction = 1.-(train_fraction+valid_fraction)
###    if testFrac < 0:
###        mprint(mname,'WARN',f'invalid fractions training {train_fraction:.3f} validation {valid_fraction:.3f} testing {testFrac:.3d}')
###        return df, df, df

    nrecs    = df.shape[0]                                         ## total number of records
    # -- training sample
    trn_data = df.sample(frac=train_fraction,replace=False,axis=0) ## sample training data
    ntrn     = trn_data.shape[0]                                   ## number of sampled records
    df.drop(trn_data.index)                                        ## tag sampled records in original data
    # -- validation sample
    val_data = df.sample(frac=valid_fraction,replace=False,axis=0) ## sample validation data
    nval     = val_data.shape[0]                                   ## number of sampled records
    df.drop(val_data.index)                                        ## tag sampled records in remaining data
    # -- testing sample
    tst_data = df.sample(frac=test_fraction,replace=False,axis=0)  ## sample testing data
    ntst     = tst_data.shape[0]                                   ## number of sampled records
    df.drop(tst_data.index)                                        ## tag sampled records in remaining data
    # -- validate indices
    nchk = ntrn+nval+ntst
    if nchk != nrecs:
        mprint(mname,'WARN',f'only {nchk} of {nrecs} records sampled - missing {nrecs-nchk} record(s) from input')

    # -- messages
    xtrn = float(ntrn)/float(nrecs)*100.
    xval = float(nval)/float(nrecs)*100.
    xtst = float(ntst)/float(nrecs)*100.
    ndec = int(int(np.log10(nrecs))+1)
    npct = int(int(np.log10(np.max([xtrn,xval,xtst])))+3)
    npcr = int(int(np.log10(np.max([train_fraction,valid_fraction,test_fraction])))+3)
    mprint(mname,'INFO',f'training sample .....: {ntrn:{ndec}} of {nrecs:{ndec}} records ({ntrn/nrecs*100:{npct}.1f}%, requested {train_fraction*100:{npcr}.1f}%)')
    mprint(mname,'INFO',f'validation sample ...: {nval:{ndec}} of {nrecs:{ndec}} records ({nval/nrecs*100:{npct}.1f}%, requested {valid_fraction*100:{npcr}.1f}%)')
    mprint(mname,'INFO',f'testing sample ......: {ntst:{ndec}} of {nrecs:{ndec}} records ({ntst/nrecs*100:{npct}.1f}%, requested {test_fraction*100:{npcr}.1f}%)' )

    return trn_data, val_data, tst_data

############################
# Argument help formatter ##
############################

## https://github.com/python/cpython/blob/main/Lib/argparse.py
## 
## class HelpMsgFormat(HelpFormatter):
## 
##     def _get_help_string(self, action):
##         # -- check if help string is to printed
##         help = action.help
##         if help is None:
##             help = ''
##         # -- access default value
##         if '%(default)' not in help:
##             if action.default is not SUPPRESS:
##                 defaulting_nargs = [OPTIONAL, ZERO_OR_MORE]
##         

##################################
## Argument data extractor help ##
##################################

def extractType(var,rtype=int):
    scalar_types = [ int, float, str, bool ]
    list_types   = [ list ]
    # -- invalid input
    if var == None:
        return None
    # -- variable is of requested type, no action
    if type(var) == rtype: 
        return var
    # -- requested type is not a scalar 
    if rtype not in scalar_types:
        return None
    # -- input variable type is a known indexable type  
    if type(var) in list_types:
        return rtype(var[0]) if len(var) > 0 else None
    elif type(var) in scalar_types:
        return rtype(var)
    # -- everything else 
    return None


        
