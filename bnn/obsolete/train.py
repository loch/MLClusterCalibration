
import config as cfg

cfg.msgPrefixWidth = 36
from config import msgSvc

from config import msgPrint          as mprint
from config import msgSkipLine       as mskip
from config import msgPrintSeparator as msprint 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()


import utils as util
loader      = util.dataLoader    
samples     = util.composeSamples
fmt         = util.fmtArray1d    
array_print = util.printArray1d  

import numerical

from numerical import transformDataToLog
from numerical import projectAroundMean
from numerical import projectIntoRange
from numerical import projectFromRange
from numerical import valueRangeDict
from numerical import findMinMax
from numerical import findQuantiles

### DNN fitter
import torch
from   torch import Tensor as tensor

### import bayesian
from bayesian import PVBLinear   as layer   
from bayesian import PVBNetwork  as network
from bayesian import run_network as training 

from datamanager import DataProvider as dataprovider
from datamanager import DataManager  as dataloader

from features  import featurePrint
from features  import featureDefaults as features

import argparse
import sys

import numpy as np

mname = 'main::train'

#######################
## Capture arguments ##
#######################

dnow = cfg.date_now.replace('_','/')
tnow = cfg.time_now
znow = cfg.zone_now

module_description = 'Training a configurable Dense Neural Network (DNN), using linear Variable Bayesian (VB) or linear Parameterized Variable Bayesian (PVB) network designs. This code is based on code received in a jupyter notebook from the University of Heidelberg (Plehn, Luchmann et al., August 2022). Adaptations for batch running and other non-jupyter environments by Peter Loch <Peter.Loch_at_cern.ch> (University of Arizona).'
module_epilog      = f'Help printed on {dnow} at {tnow} {znow}'

# -- parse command-line arguments
if '-h' or '--help' in sys.args[1:]:
    mskip()
parser, pargs = cfg.argumentParser('train.py',description=module_description,epilog=module_epilog)
## targs = vars(pargs)

# -- collect the various values
retrieve_value = cfg.argumentValues(parser,pargs)
nrecs    , def_nrecs     = retrieve_value.get_value('nrecords'                , both = True)
trnf     , def_trnf      = retrieve_value.get_value('trainfrac'               , both = True)
valf     , def_valf      = retrieve_value.get_value('validfrac'               , both = True)
tstf     , def_tstf      = retrieve_value.get_value('testfrac'                , both = True)
pmethod  , def_pmethod   = retrieve_value.get_value('projection-method'       , both = True)
prange   , def_prange    = retrieve_value.get_value('projection-range'        , both = True)
piqr     , def_piqr      = retrieve_value.get_value('projection-iqr'          , both = True)
plimits  , def_plimits   = retrieve_value.get_value('projection-limits'       , both = True)
m_nlayers, def_m_nlayers = retrieve_value.get_value('model-hidden-layers'     , both = True)
m_nbatch , def_m_nbatch  = retrieve_value.get_value('model-batch-size'        , both = True)
m_nnodes , def_m_nnodes  = retrieve_value.get_value('model-hidden-layer-nodes', both = True)
m_epochs , def_m_epochs  = retrieve_value.get_value('model-epochs'            , both = True)
m_lr     , def_m_lr      = retrieve_value.get_value('model-learning-rate'     , both = True)
m_name   , def_m_name    = retrieve_value.get_value('model-design'            , both = True)
x_mode   , def_x_mode    = retrieve_value.get_value('exec-run-mode'           , both = True)
x_mlvl   , def_x_mlvl    = retrieve_value.get_value('exec-msg-level'          , both = True)
x_name   , def_x_name    = retrieve_value.get_value('exec-name'               , both = True)
ifname   , def_ifname    = retrieve_value.get_value('input-file-path'         , both = True)
iftype   , def_iftype    = retrieve_value.get_value('input-file-type'         , both = True)
ofpref   , def_ofpref    = retrieve_value.get_value('output-file-prefix'      , both = True)
ofextn   , def_ofextn    = retrieve_value.get_value('output-file-ext'         , both = True)
ofvers   , def_ofvers    = retrieve_value.get_value('output-file-version'     , both = True)
offreq   , def_offreq    = retrieve_value.get_value('output-file-frequency'   , both = True)
msgSvc.set_level(x_mlvl)
mprint(mname,level=msgVerbose,message=','.join(['nrecords                ', str(type(nrecs    )), str(type(def_nrecs    )), str(type(pargs.nrecords                )), str(type(parser.get_default('nrecords'                )))]))
mprint(mname,level=msgVerbose,message=','.join(['trainfrac               ', str(type(trnf     )), str(type(def_trnf     )), str(type(pargs.trainfrac               )), str(type(parser.get_default('trainfrac'               )))]))
mprint(mname,level=msgVerbose,message=','.join(['validfrac               ', str(type(valf     )), str(type(def_valf     )), str(type(pargs.validfrac               )), str(type(parser.get_default('validfrac'               )))]))
mprint(mname,level=msgVerbose,message=','.join(['testfrac                ', str(type(tstf     )), str(type(def_tstf     )), str(type(pargs.testfrac                )), str(type(parser.get_default('testfrac'                )))]))
mprint(mname,level=msgVerbose,message=','.join(['projection_method       ', str(type(pmethod  )), str(type(def_pmethod  )), str(type(pargs.projection_method       )), str(type(parser.get_default('projection_method'       )))]))
mprint(mname,level=msgVerbose,message=','.join(['projection_range        ', str(type(prange   )), str(type(def_prange   )), str(type(pargs.projection_range        )), str(type(parser.get_default('projection_range'        )))]))
mprint(mname,level=msgVerbose,message=','.join(['projection_iqr          ', str(type(piqr     )), str(type(def_piqr     )), str(type(pargs.projection_iqr          )), str(type(parser.get_default('projection_iqr'          )))]))
mprint(mname,level=msgVerbose,message=','.join(['projection_limits       ', str(type(plimits  )), str(type(def_plimits  )), str(type(pargs.projection_limits       )), str(type(parser.get_default('projection_limits'       )))]))
mprint(mname,level=msgVerbose,message=','.join(['model_hidden_layers     ', str(type(m_nlayers)), str(type(def_m_nlayers)), str(type(pargs.model_hidden_layers     )), str(type(parser.get_default('model_hidden_layers'     )))]))
mprint(mname,level=msgVerbose,message=','.join(['model_batch_size        ', str(type(m_nbatch )), str(type(def_m_nbatch )), str(type(pargs.model_batch_size        )), str(type(parser.get_default('model_batch_size'        )))]))
mprint(mname,level=msgVerbose,message=','.join(['model_hidden_layer_nodes', str(type(m_nnodes )), str(type(def_m_nnodes )), str(type(pargs.model_hidden_layer_nodes)), str(type(parser.get_default('model_hidden_layer_nodes')))]))
mprint(mname,level=msgVerbose,message=','.join(['model_epochs            ', str(type(m_epochs )), str(type(def_m_epochs )), str(type(pargs.model_epochs            )), str(type(parser.get_default('model_epochs'            )))]))
mprint(mname,level=msgVerbose,message=','.join(['model_learning_rate     ', str(type(m_lr     )), str(type(def_m_lr     )), str(type(pargs.model_learning_rate     )), str(type(parser.get_default('model_learning_rate'     )))]))
mprint(mname,level=msgVerbose,message=','.join(['model_design            ', str(type(m_name   )), str(type(def_m_name   )), str(type(pargs.model_design            )), str(type(parser.get_default('model_design'            )))]))
mprint(mname,level=msgVerbose,message=','.join(['exec_run_mode           ', str(type(x_mode   )), str(type(def_x_mode   )), str(type(pargs.exec_run_mode           )), str(type(parser.get_default('exec_run_mode'           )))]))
mprint(mname,level=msgVerbose,message=','.join(['exec_msg_level          ', str(type(x_mlvl   )), str(type(def_x_mlvl   )), str(type(pargs.exec_msg_level          )), str(type(parser.get_default('exec_msg_level'          )))]))
mprint(mname,level=msgVerbose,message=','.join(['exec_name               ', str(type(x_name   )), str(type(def_x_name   )), str(type(pargs.exec_name               )), str(type(parser.get_default('exec_name'               )))]))
mprint(mname,level=msgVerbose,message=','.join(['input_file_path         ', str(type(ifname   )), str(type(def_ifname   )), str(type(pargs.input_file_path         )), str(type(parser.get_default('input_file_path'         )))]))
mprint(mname,level=msgVerbose,message=','.join(['input_file_type         ', str(type(iftype   )), str(type(def_iftype   )), str(type(pargs.input_file_type         )), str(type(parser.get_default('input_file_type'         )))]))
mprint(mname,level=msgVerbose,message=','.join(['output_file_prefix      ', str(type(ofpref   )), str(type(def_ofpref   )), str(type(pargs.output_file_prefix      )), str(type(parser.get_default('output_file_prefix'      )))]))
mprint(mname,level=msgVerbose,message=','.join(['output_file_ext         ', str(type(ofextn   )), str(type(def_ofextn   )), str(type(pargs.output_file_ext         )), str(type(parser.get_default('output_file_ext'         )))]))
mprint(mname,level=msgVerbose,message=','.join(['output_file_version     ', str(type(ofvers   )), str(type(def_ofvers   )), str(type(pargs.output_file_version     )), str(type(parser.get_default('output_file_version'     )))]))
mprint(mname,level=msgVerbose,message=','.join(['output_file_frequency   ', str(type(offreq   )), str(type(def_offreq   )), str(type(pargs.output_file_frequency   )), str(type(parser.get_default('output_file_frequency'   )))]))

def_iffsep = ' '
iffsep     = def_iffsep

#if x_mlvl != def_x_mlvl: 
#    cfg.msgInit(level=x_mlvl[0])
#
# -- legacy, deprecated
N = def_nrecs

# -- adjust selected values
###print(m_nlayers)
m_nlayers = np.max([m_nlayers,   1]) ## model number of hidden layers
m_nbatch  = np.max([m_nbatch ,  10]) ## size of batch
m_nnodes  = np.max([m_nnodes ,   4]) ## model number of nodes/hidden layer  
m_epochs  = np.max([m_epochs ,   1]) ## model number of epochs to train
m_lr      = np.min([m_lr     ,0.01]) ## model learning rate

# -- switches and configurations
doMeanStd    = cfg.doNorm(pmethod)
doLinear     = cfg.doLinearProjection(pmethod)
doLinearIQR  = cfg.doLinearProjectionIQRange(pmethod,plimits,piqr)
doLinearFull = cfg.doLinearProjectionFullRange(pmethod,plimits)
if doMeanStd:
    if doLinear:
        mskip()
        mprint(mname,level=msgError,message=f'Configuration mismatch: can either do \042{tag_projection_norm}\042 or \042{tag_projection_lpro}\042 in data projection, not both as requested')
        exit(-1)
stopAfterInit  = cfg.exec_stop_after_init(x_mode)
stopAfterLoad  = cfg.exec_stop_after_load(x_mode)
stopAfterSetup = cfg.exec_stop_after_setup_training(x_mode)
stopAfterRun   = cfg.exec_stop_after_run(x_mode)
stopAfterPlot  = cfg.exec_stop_after_plot(x_mode) 

title   = f'Project \'{x_name}\' Configuration at {tnow} ({znow}) on {dnow}' 
tlength = len(title) + 6
tchar   = '#'
tbar    = f'{tchar:{tchar}^{tlength}}'

mskip()
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mprint(module=mname,level=msgInfo,message=f'## {title} ##',color='yellow')
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mskip()

if nrecs > 0:
    mprint(mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
elif nrecs == -1:
    mprint(mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
else:
    mprint(mname,level=msgWarning,message=f'number of data records to be read ....: {nrecs} unknown specification, (default: {N} => all available records)')

maxp = np.max([trnf,valf,tstf,def_trnf,def_valf,def_tstf])
fixp = int(np.max([maxp+1,1]))+3
fmtp = f'{fixp}.1f'
    
mprint(mname,level=msgInfo,message=f'fraction of data for training ........: {trnf*100:>{fmtp}}% (default: {def_trnf*100:>{fmtp}}%)')
mprint(mname,level=msgInfo,message=f'fraction of data for validation ......: {valf*100:>{fmtp}}% (default: {def_valf*100:>{fmtp}}%)')
mprint(mname,level=msgInfo,message=f'fraction of data for testing .........: {tstf*100:>{fmtp}}% (default: {def_tstf*100:>{fmtp}}%)')
msprint(True)
mprint(mname,level=msgInfo,message=f'projection method ....................: {pmethod} (default: {def_pmethod})')
if doLinear:
    mprint(mname,level=msgInfo,message=f'projection limits definition .........: \'{plimits}\' (default: \'{def_plimits}\')')
    mprint(mname,level=msgInfo,message=f'projection range .....................: {prange} (default: {def_prange})')
    if plimits == cfg.projection_limits_iqr:
        mprint(mname,level=msgInfo,message=f'projection IQR percentage ............; {piqr*100:>{fmtp}}% (default: {def_piqr*100:>{fmtp}}%)')
msprint(True)
mprint(mname,level=msgInfo,message=f'model name ...........................: \'{m_name}\': \"{cfg.model_descr(m_name)}\" (default: \'{def_m_name}\')')
mprint(mname,level=msgInfo,message=f'model number of hidden layers ........: {m_nlayers} (default: {def_m_nlayers})'                                 )   
mprint(mname,level=msgInfo,message=f'model batch size .....................: {m_nbatch} (default: {def_m_nbatch})'                                   )
mprint(mname,level=msgInfo,message=f'model number of nodes/hidden layer ...: {m_nnodes} (default: {def_m_nnodes})'                                   )
mprint(mname,level=msgInfo,message=f'model number of training epochs ......: {m_epochs} (default: {def_m_epochs})'                                   )
mprint(mname,level=msgInfo,message=f'model learning rate ..................: {m_lr:>7.1e} (default: {def_m_lr:<7.1e})'                               )
mprint(mname,level=msgInfo,message=f'model number of features/inputs ......: {len(features)} (from configuration)')
msprint(True)       
mprint(mname,level=msgInfo,message=f'execution stops after step ...........: {x_mode}: \"{cfg.exec_run_mode_dict[x_mode]}\" (default: {def_x_mode}: \"{cfg.exec_run_mode_dict[def_x_mode]}\")')
mprint(mname,level=msgInfo,message=f'message level ........................: {x_mlvl}: \"{cfg.msgGlobalInfo}\" (default: {def_x_mlvl})')
msprint(True)
mprint(mname,level=msgInfo,message=f'input file path ......................: \'{ifname}\' (default: \'{def_ifname}\')')
mprint(mname,level=msgInfo,message=f'input file type ......................: \'{iftype}\' (default: \'{def_iftype}\')')
mprint(mname,level=msgInfo,message=f'input file field separator ...........: \'{iffsep}\' (default: \'{def_iffsep}\')')
mprint(mname,level=msgInfo,message=f'output filename prefix ...............: \'{ofpref}\' (default: \'{def_ofpref}\')')
mprint(mname,level=msgInfo,message=f'output filename extension ............: \'{ofextn}\' (default: \'{def_ofextn}\')')
mprint(mname,level=msgInfo,message=f'output filename version tag ..........: {ofvers} (default: {def_ofvers})')
mprint(mname,level=msgInfo,message=f'output file write frequency ..........: after {offreq} epochs (default: {def_offreq} epochs)')

sumf = float(trnf) + float(valf) + float(tstf)
exco = 0
if sumf > 1.0001:
    mprint(mname,level=msgWarning,message=f'configuration: fraction of data for training ({trnf*100:.1f}%), validation ({valf*100:.1f}%) and testing ({tstf*100:.1f}%) sums to {sumf*100:.1f} -> potentially overlapping samples')
###    if not stopAfterInit:
###        exit(-2)
###    else:
###        exco = -2

if stopAfterInit:
    mskip()
    if exco != 0:
        mprint(mname,level=msgInfo,message=f'End of processing after initialization (with errors) requested - STOP!')
    else: 
        mprint(mname,level=msgInfo,message=f'End of processing after initialization requested - STOP!')
    mskip()
    exit(exco)

mskip()
mprint(module=mname,level=msgInfo,message='###########################################################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Enter training: define features/inputs and observables for evaluation ##',color='yellow')
mprint(module=mname,level=msgInfo,message='###########################################################################',color='yellow')
mskip()

##############################
## Defining features/inputs ##
##############################

mprint(mname,level=msgInfo,message=f'Found {len(features)} features:')
featurePrint(features=features,print_name='main::train',print_level=msgInfo)
mskip()

#####################
## Load input data ##
#####################

if nrecs > 0:
    mprint(mname,level=msgInfo,message=f'Loading input data - maximum number of records to be read is {nrecs}')
elif nrecs == -1:
    mprint(mname,level=msgInfo,message='Loading input data - read all available data records')
else:
    mprint(mname,level=msgError,message=f'Loading input data - invalid number of data records specified ({nrecs}) -stop')
    exit(-3)
mskip()

df = loader(features=features,nrecords=nrecs)

###############################################################
## Define labels (trained variables), references and samples ##
###############################################################

mskip()
mprint(mname,level=msgInfo,message='#######################################################################')
mprint(mname,level=msgInfo,message='## Create training, validation and testing data, labels & references ##')
mprint(mname,level=msgInfo,message='#######################################################################')
mskip()

# -- randomly split pandas data into samples
trn_data, val_data, tst_data = samples(df,train_fraction=trnf,valid_fraction=valf,test_fraction=tstf) ### => test sample 1-(train_fraction+valid_fraction)

# -- references and labels for training sample
trn_e_dep = trn_data["cluster_ENG_CALIB_TOT"]          # energy deposited in cells of topo-cluster
trn_e_ems = trn_data["clusterE"]                       # raw (EM scale) signal
trn_e_had = trn_data["cluster_HAD_WEIGHT"] * trn_e_ems # signal after hadronic calibration (first calibration in LCW procedure, after classification) 
trn_label = trn_e_ems / trn_e_dep                      # training goal/label

# -- references and labels for validation sample
val_e_dep = val_data["cluster_ENG_CALIB_TOT"]          # energy deposited in cells of topo-cluster
val_e_ems = val_data["clusterE"]                       # raw (EM scale) signal
val_e_had = val_data["cluster_HAD_WEIGHT"] * val_e_ems # signal after hadronic calibration (first calibration in LCW procedure, after classification) 
val_label = val_e_ems / val_e_dep                      # training goal/label

# -- references and labels for testing sample
tst_e_dep = tst_data["cluster_ENG_CALIB_TOT"]          # energy deposited in cells of topo-cluster
tst_e_ems = tst_data["clusterE"]                       # raw (EM scale) signal
tst_e_had = tst_data["cluster_HAD_WEIGHT"] * tst_e_ems # signal after hadronic calibration (first calibration in LCW procedure, after classification) 
tst_label = tst_e_ems / tst_e_dep                      # training goal/label
 
# -- convert to numpy arrays
trn_data  = trn_data[features].to_numpy()
trn_e_dep = trn_e_dep.to_numpy()
trn_e_ems = trn_e_ems.to_numpy()
trn_e_had = trn_e_had.to_numpy()
trn_label = trn_label.to_numpy()

val_data  = val_data[features].to_numpy()
val_e_dep = val_e_dep.to_numpy()
val_e_ems = val_e_ems.to_numpy()
val_e_had = val_e_had.to_numpy()
val_label = val_label.to_numpy()

tst_data  = tst_data[features].to_numpy()
tst_e_dep = tst_e_dep.to_numpy()
tst_e_ems = tst_e_ems.to_numpy()
tst_e_had = tst_e_had.to_numpy()
tst_label = tst_label.to_numpy()

mskip()
mprint(mname,level=msgDebug,message=f'training data .......[shape]: {trn_data.shape}' )
mprint(mname,level=msgDebug,message=f'training e_dep ......[shape]: {trn_e_dep.shape}')
mprint(mname,level=msgDebug,message=f'training e_ems ......[shape]: {trn_e_ems.shape}')
mprint(mname,level=msgDebug,message=f'training e_had ......[shape]: {trn_e_had.shape}')
mprint(mname,level=msgDebug,message=f'training labels .....[shape]: {trn_label.shape}')
mskip()

mprint(mname,level=msgDebug,message=f'validation data .....[shape]: {val_data.shape}' )
mprint(mname,level=msgDebug,message=f'validation e_dep ....[shape]: {val_e_dep.shape}')
mprint(mname,level=msgDebug,message=f'validation e_ems ....[shape]: {val_e_ems.shape}')
mprint(mname,level=msgDebug,message=f'validation e_had ....[shape]: {val_e_had.shape}')
mprint(mname,level=msgDebug,message=f'validation labels ...[shape]: {val_label.shape}')
mskip()

mprint(mname,level=msgDebug,message=f'testing data ........[shape]: {tst_data.shape}' )
mprint(mname,level=msgDebug,message=f'testing e_dep .......[shape]: {tst_e_dep.shape}')
mprint(mname,level=msgDebug,message=f'testing e_ems .......[shape]: {tst_e_ems.shape}')
mprint(mname,level=msgDebug,message=f'testing e_had .......[shape]: {tst_e_had.shape}')
mprint(mname,level=msgDebug,message=f'testing labels ......[shape]: {tst_label.shape}')

if stopAfterLoad:
    mskip()
    mprint(mname,level=msgInfo,message=f'End of processing after loading data requested - STOP!') 
    mskip()
    exit(0)

####################
## Transform data ##
####################

mskip()
mprint(mname,level=msgInfo,message='#################################')
mprint(mname,level=msgInfo,message='## Transform data and features ##')
mprint(mname,level=msgInfo,message='#################################')
mskip()

# -- reshape data
trn_data_shaped = np.reshape( trn_data, (trn_data.shape[0],-1) ) if trn_data.shape[0] > 0 else trn_data
val_data_shaped = np.reshape( val_data, (val_data.shape[0],-1) ) if val_data.shape[0] > 0 else val_data
tst_data_shaped = np.reshape( tst_data, (tst_data.shape[0],-1) ) if tst_data.shape[0] > 0 else tst_data
                    
# -- transform data

'''
Data is transformed using ranges (absolute or interquantile) or scaled to standard deviatoins with a mean of 0
'''
## -- transform using log10 if desired
trans_features = [ 'clusterE', 'cluster_FIRST_ENG_DENS' ]
mprint(mname,level=msgInfo,message=f'Transforming data: x->log10(x) transformation of features {trans_features}')
trn_data_trans = transformDataToLog(trn_data_shaped,features=features,transformToLog=trans_features)
val_data_trans = transformDataToLog(val_data_shaped,features=features,transformToLog=trans_features)
tst_data_trans = transformDataToLog(tst_data_shaped,features=features,transformToLog=trans_features)
mskip()

## -- project according to configuration
mprint(mname,level=msgInfo,message=f'Attempt to projecting data with method \042{pmethod}\042:')

### doTrnProj = trn_data_trans.shape[0] > 0
### doValProj = val_data_trans.shape[0] > 0
### doTstProj = tst_data_trans.shape[0] > 0

if doMeanStd:
    trn_data_trans = projectAroundMean(trn_data_trans,features=features,do_print=True,print_name="training data"  ,transformToLog=trans_features) if trn_data_shaped.shape[0] > 0 else trn_data_shaped
    mprint(mname,level=msgDebug,message=f'Training data .....[shape after applying {pmethod}]: {trn_data_trans.shape}')
    val_data_trans = projectAroundMean(val_data_trans,features=features,do_print=True,print_name="validation data",transformToLog=trans_features) if val_data_shaped.shape[0] > 0 else val_data_shaped
    mprint(mname,level=msgDebug,message=f'Validation data ...[shape after applying {pmethod}]: {val_data_trans.shape}')
    tst_data_trans = projectAroundMean(tst_data_trans,features=features,do_print=True,print_name="test data"      ,transformToLog=trans_features) if tst_data_shaped.shape[0] > 0 else tst_data_shaped
    mprint(mname,level=msgDebug,message=f'Test data .........[shape after applying {pmethod}]: {tst_data_trans.shape}')
elif doLinear:
    doProjDict = {
        'train'    : False,
        'validate' : False,
        'test'     : False,
    }
    # -- get dictionary templates
    trn_range_dict = valueRangeDict
    val_range_dict = valueRangeDict
    tst_range_dict = valueRangeDict
    # -- fill dictionaries with ranges
    vetoProjection = False
    if doLinearFull:
        # -- training range
        trnDict = findMinMax(trn_data_trans,features=features)
        if trnDict != None:
            doProjDict['train'] = True
            for key, value in trnDict.items():
                trn_range_dict[key] = value
        # -- validation range
        valDict = findMinMax(val_data_trans,features=features)
        if valDict != None:
            doProjDict['validate'] = True
            for key, value in valDict.items():
                val_range_dict[key] = value
        # -- test range
        tstDict = findMinMax(tst_data_trans,features=features)
        if tstDict != None:
            doProjDict['test'] = True
            for key, value in tstDict.items():
                tst_range_dict[key] = value
    elif doLinearIQR:
        # -- training range
        trnDict = findQuantiles(trn_data_trans,features=features,iqr=piqr)
        if trnDict != None:
            doProjDict['train'] = True
            for key, value in trnDict.items():
                trn_range_dict[key] = value
        # -- validation range
        valDict = findQuantiles(val_data_trans,features=features,iqr=piqr)
        if valDict != None:
            doProjDict['validate'] = True
            for key, value in valDict.items():
                val_range_dict[key] = value
        # -- test range
        tstDict = findQuantiles(tst_data_trans,features=features,iqr=piqr)
        if tstDict != None:
            doProjDict['test'] = True
            for key, value in tstDict.items():
                tst_range_dict[key] = value
    else:
        mskip()
        mprint(mname,level=msgWarning,message=f'unknown range definition \042{plimits}\042 for projection method \042{pmethod}\042 - no projection of features')
        vetoProjection = True
    # -- transform all data sets
    if not vetoProjection:
        if doProjDict['train']:
            trn_data_trans = projectIntoRange(trn_data_trans,features=features,range_dict=trn_range_dict,projection_range=prange,transformToLog=trans_features,print_name='training')
            mprint(mname,level=msgDebug,message=f'Training data .....[shape after applying {pmethod}]: {trn_data_trans.shape}')
        if doProjDict['validate']:
            val_data_trans = projectIntoRange(val_data_trans,features=features,range_dict=val_range_dict,projection_range=prange,transformToLog=trans_features,print_name='validation')
            mprint(mname,level=msgDebug,message=f'Validation data ...[shape after applying {pmethod}]: {val_data_trans.shape}')
        if doProjDict['test']:
            tst_data_trans = projectIntoRange(tst_data_trans,features=features,range_dict=tst_range_dict,projection_range=prange,transformToLog=trans_features,print_name='testing')
            mprint(mname,level=msgDebug,message=f'Test data .........[shape after applying {pmethod}]: {tst_data_trans.shape}')
else:
    mprint(mname,level=msgWarning,message=f'Unknown projection method \042{pmethod}\042, no projection of features - remaining shapes:')
    mprint(mname,level=msgDebug  ,message=f'Training data ......................: {trn_data_trans.shape}')
    mprint(mname,level=msgDebug  ,message=f'Validation data ....................: {val_data_trans.shape}')
    mprint(mname,level=msgDebug  ,message=f'Test data ..........................: {tst_data_trans.shape}')

#####################
## Data prepration ##
#####################

mskip()
mprint(mname,level=msgInfo,message='######################')
mprint(mname,level=msgInfo,message='## Data preparation ##')
mprint(mname,level=msgInfo,message='######################')
mskip()

## -- convert all data sets to (py)torch tensors
mprint(mname,level=msgInfo,message='Convert all data to torch tensors')
trn_data_tensor = tensor(trn_data_trans)
val_data_tensor = tensor(val_data_trans)
tst_data_tensor = tensor(tst_data_trans)
mprint(mname,level=msgInfo,message='Convert all labels/targets to torch tensors')
trn_label_tensor = tensor(trn_label)
val_label_tensor = tensor(val_label)
tst_label_tensor = tensor(tst_label)

## -- define data providers
mprint(mname,level=msgInfo,message='Defined data sets merging data and labels/targets')
trn_dataset = dataprovider( trn_data_tensor, trn_label_tensor.unsqueeze(-1) )
val_dataset = dataprovider( val_data_tensor, val_label_tensor.unsqueeze(-1) )
tst_dataset = dataprovider( tst_data_tensor, trn_label_tensor.unsqueeze(-1) )

## -- define data loaders
mprint(mname,level=msgInfo,message='Set up data loaders for the data sets')
trn_dataloader = dataloader( trn_dataset, batch_size=64, shuffle=True )
val_dataloader = dataloader( val_dataset, batch_size=64, shuffle=True )
tst_dataloader = dataloader( tst_dataset, batch_size=64, shuffle=True )

## -- check the batch shapes
dshape = next(iter(trn_dataloader))[0].shape
lshape = next(iter(trn_dataloader))[1].shape
mprint(mname,level=msgInfo,message=f'shape of training data .....: {dshape}')
mprint(mname,level=msgInfo,message=f'shape of training labels ...: {lshape}')

############################
## Setting up the network ##
############################

mskip()
mprint(mname,level=msgInfo,message='#####################################################################')
mprint(mname,level=msgInfo,message='## Setting up the Parameterized Variational Bayesian Network (BNN) ##') 
mprint(mname,level=msgInfo,message='#####################################################################')
mskip()

# -- run time environment/computing resources
device = 'cuda' if torch.cuda.is_available() else 'cpu'
mprint(mname,level=msgInfo,message=f'Using a <{device}> device')

# -- BNN setup
n_features = len(features)
n_sample   = len(trn_dataset)
trn_model  = network( n_sample, n_features=n_features, hdn_dim=m_nnodes, num_hidden_layers=m_nlayers ).to( device )
mprint(mname,level=msgInfo,message=f'BNN model configuration:')
##msgs : list
msgs = cfg.msgExtract(trn_model)
mprint(mname,level=msgInfo,message=msgs)

### for m in msgs:
###     mprint(mname,level=msgInfo,message=m)
### 
### print(msgs)
### 
if stopAfterSetup:
    mskip()
    mprint(mname,level=msgInfo,message=f'End of processing after setting up the training requested - STOP!') 
    mskip()
    exit(0)


##########################
## Training the network ##
##########################

mskip()
mprint(mname,level=msgInfo,message='##########################')
mprint(mname,level=msgInfo,message='## Running the training ##')
mprint(mname,level=msgInfo,message='##########################')
mskip()

# -- invoke the training
if training(trn_model,training_data_loader=trn_dataloader,validation_data_loader=val_dataloader,batch_size=m_nbatch,n_epochs=m_epochs,learning_rate=m_lr,file_name_prefix=ofpref,file_name_version=ofvers,file_write_freq=offreq): 
    mprint(mname,level=msgInfo,message='Successfull training run')
else:
    mprint(mname,level=msgWarning,message='Problems with training data set')

