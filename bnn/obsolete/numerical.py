
import config as cfg
from config import msgPrint as mprint

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB

defaultProjectionRange = cfg.projection_range
defaultValueRange      = cfg.projection_value_range

import numpy as np

'''
Transform data to (x-mean)/stdvar - more flexibity later! Data input is a numpy array.  
'''
def transformDataToLog(data,features=[],transformToLog=['clusterE','cluster_FIRST_ENG_DENS'],print_name=''):

    mname = 'numerical::transformDataToLog'

    # -- output array
    tdata = np.ndarray([data.shape[0],data.shape[1]],dtype=float)

    # -- find the features 
    for fidx, fname in enumerate(features):
        rdata = data[:,fidx]
        ndata = len(rdata)
        if ndata > 0 and fname in transformToLog:
            mprint(mname,msgInfo,f'Feature \042{fname}\042 in column {fidx} has {ndata} data words transformed to log10')
            tdata[:,fidx] = np.log10(rdata)
        else:
            tdata[:,fidx] = rdata
    #
    return tdata
'''
Project data around mean - this  modifies the data in place!
'''
def projectAroundMean(data,left=1.,right=1.,features=[],do_print=True,print_name=None,transformToLog=None):

    mname = 'numerical::projectAroundMean'

    # -- get mean and standard deviation
    p_mean = np.mean(data,axis=0,dtype=np.float64)   ## mean of all data/features
    p_stds = np.std (data,axis=0,dtype=np.float64)   ## standard derivation of all data

    # -- apply projection
    pdata = (data-p_mean)/p_stds

    # -- some quick visual checks
    if do_print:
        if print_name != '':
            mprint(mname,msgInfo,f'[data:{print_name}] Mean values and standard deviation before and after rescaling:')
        else:
            mprint(mname,msgInfo,'Mean values and standard deviation before and after rescaling:')
        lwdth = int(0)
        flist = [None] * len(features)
        for idx, fn in enumerate(features):
            fname = fn  
            if transformToLog != None and fn in transformToLog:
                fname = f'log10({fn})'
            lwdth = np.max([lwdth,len(fname)])
            flist[idx] = fname
            
        n_mean = np.mean(pdata,axis=0,dtype=np.float64)
        n_stds = np.std (pdata,axis=0,dtype=np.float64)
        if print_name == None:
            for fidx, fname in enumerate(features):
                mprint(mname,msgInfo,f'{flist[fidx]:{lwdth}} original mean +/- rms: {p_mean[fidx]:10.3e} +/- {p_stds[fidx]:<10.3e} projection mean +/- rms: {n_mean[fidx]:>10.3e} +/- {n_stds[fidx]:<10.3e}')
        else:
            for fidx, fname in enumerate(features):
                mprint(mname,msgInfo,f'[data:{print_name}] {flist[fidx]:{lwdth}} original mean +/- rms: {p_mean[fidx]:>10.3e} +/- {p_stds[fidx]:<10.3e} projection mean +/- rms: {n_mean[fidx]:>10.3e} +/- {n_stds[fidx]:<10.3e}')

    # -- return         
    return pdata

'''
Project data into given range
'''            
valueRangeDict = {
    'clusterEtaCalib'       : defaultValueRange,
    'clusterE'              : defaultValueRange,
    'cluster_CENTER_LAMBDA' : defaultValueRange,
    'cluster_LONGITUDINAL'  : defaultValueRange,
    'cluster_LATERAL'       : defaultValueRange,
    'cluster_ENG_FRAC_EM'   : defaultValueRange,
    'cluster_FIRST_ENG_DENS': defaultValueRange,
    'cluster_SIGNIFICANCE'  : defaultValueRange,
    'cluster_PTD'           : defaultValueRange,
    'cluster_SECOND_TIME'   : defaultValueRange,
    'avgMu'                 : defaultValueRange,
    'nPrimVtx'              : defaultValueRange,
}
'''
Undo range projection
'''
def projectIntoRange(data,features=[],range_dict=valueRangeDict,projection_range=defaultProjectionRange,transformToLog=None,do_print=True,print_name=None):

    mname = 'numerical::projectIntoRange'

    # -- output
    tdata : type(data)
    tdata = np.zeros_like(data)

    # -- transform features
    lwdth  = int(0)
    vdict  = { 'xmin'   : 0, 'xmax'     : 0, 'pmin'     : 0, 'pmax'     : 0, 'lower'    : 0, 'upper'    : 0, 'is_valid' : False, }
    lookup = { }
    for fiter, fn in enumerate(features):
        doLog = transformToLog != None and fn in transformToLog
        fname = fn if not doLog else f'log10({fn})'
        lwdth = np.max([lwdth,len(fname)])
        tdata[:,fiter] = data[:,fiter]
        lookup[fname] = vdict;
        if fn in range_dict:
            vmin, vmax = float(range_dict[fn][0]), float(range_dict[fn][1])
###            print(f'{fn} range {vmin}, {vmax}')
            if vmin > float('-inf') and vmax < float('+inf') and vmax > vmin:
                a, b = float(projection_range[0]) , float(projection_range[1])
                slope = (b-a)/(vmax-vmin)
                tdata[:,fiter] = tdata[:,fiter] - vmin
                tdata[:,fiter] = tdata[:,fiter] * slope
                tdata[:,fiter] = tdata[:,fiter] + a
                if do_print:
                    lookup[fname] = { 'xmin' : np.min(data[:,fiter]), 'xmax' : np.max(data[:,fiter]), 'pmin' : np.min(tdata[:,fiter]), 'pmax' : np.max(tdata[:,fiter]), 'lower' : a, 'upper' : b, 'is_valid' : vmax > vmin }

    # -- print transformation ranges etc.
    if do_print:
        prefix = f'[data:{print_name}] ' if print_name != None else ''
        mprint(mname,msgInfo,f'{prefix}Projection into fixed range, minimum/maximum before and after projection:')
        for fname, data in lookup.items():
            if data['is_valid']:
                vmin, vmax, pmin, pmax, a, b  = data['xmin'], data['xmax'], data['pmin'], data['pmax'], data['lower'], data['upper']
                mprint(mname,msgInfo,f'{prefix}{fname:{lwdth}} original min/max: {vmin:10.3e}/{vmax:<10.3e} projected min/max: {pmin:>10.3e} +/- {pmax:<10.3e} set range boundaries: {a:10.3e}/{b:10.3e}')

    # -- return 
    return tdata

def projectFromRange(data,features=[],range_dict=valueRangeDict,projection_range=defaultProjectionRange):
    
    # -- output
    tdata = np.zero_like(data)

    # -- convert features back to original data
    for fiter, fname in enumerate(features):
        tdata[:,fiter] = data[:,fiter]
        if fname in range_dict:
            a, b       = float(projection_range[0] ), float(projection_range[1] )
            vmin, vmax = float(range_dict[fname][0]), float(range_dict[fname][1]) 
            if b > a and vmin > float('-inf') and vmax < float('+inf'):
                slope = (vmax-vmin)/(b-a)
                tdata[:,fiter] = tdata[:,fiter] - a
                tdata[:,fiter] = tdata[:,fiter] * slope
                tdata[:,fiter] = tdata[:,fiter] + vmin

    # -- return
    return tdata

'''
Find quantiles
'''
def findQuantiles(data,features=[],iqr=1.):

    # -- invalid input
    if data.shape[0] == 0:
        return None

    # -- build output structure
    iqrs = { }

    # -- extract features 
    for fiter, fname in enumerate(features):
        tdata  = sorted(data[:,fiter])
        qleft  = (1.-iqr)/2.
        qright = 1.-qleft
        iqrs[fname] = [ np.quantile(tdata,qleft,axis=0), np.quantile(tdata,qright,axis=0) ]

    # -- return
    return iqrs

'''
Find minimum/maximum at the same time
'''
def findMinMax(data):
    
    # -- preset variables
    xmin = cfg.flt_lowest 
    xmax = cfg.flt_highest

    # -- loop data
    for value in data:
        if value > xmax:
            xmax = value
        if value < xmin:
            xmin = value

    # -- return 
    return xmin, xmax

'''
Find minimum/maximum of feature
'''
def findMinMax(data,features):

    # -- build output structure
    mmDict = { }

    # -- find values
    for fiter, fname in enumerate(features):
        tdata = sorted(data[:,fiter])
        mmDict[fname] = findMinMax(tdata)

    # -- return 
    return mmDict

        
        

    
            
            
        
    
