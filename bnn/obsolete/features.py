
import sys

import numpy as np

from config import msgPrint as mprint


signalFeaturesDict = { 
    'clusterEtaCalib'           : 'topo-cluster rapidity (LCW scale)'                                                             ,
    'clusterE'                  : 'topo-cluster enegry at EM scale'                                                               ,
    'cluster_CENTER_LAMBDA'     : 'distance of topo-cluster center-of-gravity from calorimeter front face'                        ,
    'cluster_LONGITUDINAL'      : 'normalized longitudinal dispersion of cell energies in topo-cluster'                           ,
    'cluster_LATERAL'           : 'normalized lateral (radial) dispersion of cell energies in topo-cluster'                       ,
    'cluster_ENG_FRAC_EM'       : 'topo-cluster energy fraction in electromagnetic calorimeter'                                   ,
    'cluster_FIRST_ENG_DENS'    : 'first (energy-weighted) moment of cell energy density distribution in topo-cluster'            ,
    'cluster_SIGNIFICANCE'      : 'significance of topo-cluster signal (signal-over-noise)'                                       ,
    'cluster_PTD'               : 'compactness of cell energy distribution in topo-cluster'                                       ,
    'cluster_SECOND_TIME'       : 'second (energy-squared-weighted) moment of cell time distribution in topo-cluster'             ,
    'avgMu'                     : 'average number of interactions per bunch crossing'                                             ,
    'nPrimVtx'                  : 'number of reconstructed primary vertices'                                                      ,
    'clusterResponseEM'         : 'topo-cluster response at EM scale (MC only)'                                                   ,
    'clusterResponseLCW'        : 'topo-cluster response at LCW scale (hadronic calibration only, MC only)'                       ,
    'clusterResponseML'         : 'topo-cluster response at ML scale (MC only)'                                                   ,
    'cluster_CENTER_MAG'        : 'distance of topo-cluster from nominal vertex'                                                  ,
    'cluster_FIRST_PHI'         : 'first (energy-weighted) moment of cell azimuth distribution'                                   ,
    'cluster_FIRST_ETA'         : 'first (energy-weighted) moment of cell rapidity distribution'                                  ,
    'cluster_SECOND_R'          : 'second (energy-weighted) moment of radial cell distance from cluster axis distribution'        ,
    'cluster_SECOND_LAMBDA'     : 'second (energy-weighted) moment of longitudinal cell distance from cluster center distribution',
    'cluster_DELTA_PHI'         : 'azimuthal distance between principal cluster axis and nominal direction from vertex'           ,
    'cluster_DELTA_THETA'       : 'polar angle distance between principal cluster axis and nominal direction from vertex'         ,
    'cluster_DELTA_ALPHA'       : 'angular distance between principal cluster axis and nominal direction from vertex'             ,
    'cluster_CENTER_X'          : 'x-coordinate topo-cluster center of gravity'                                                   ,
    'cluster_CENTER_Y'          : 'y-coordinate topo-cluster center of gravity'                                                   ,
    'cluster_CENTER_Z'          : 'z-coordinate topo-cluster center of gravity'                                                   ,
    'cluster_ENG_FRAC_MAX'      : 'maximum fraction of energy in one topo-cluster cell'                                           ,
    'cluster_ENG_FRAC_CORE'     : 'fraction of enegry in core of topo-cluster'                                                    ,
    'cluster_SECOND_ENG_DENS'   : 'second (energy-weighted) moment of cell energy density distribution in topo-cluster'           ,
    'cluster_ISOLATION'         : 'topo-cluster isolation measure'                                                                ,
    'cluster_ENG_BAD_CELLS'     : 'energy in bad cells in topo-cluster'                                                           ,
    'cluster_N_BAD_CELLS'       : 'number of bad cells in too-cluster'                                                            ,
    'cluster_N_BAD_CELLS_CORR'  : 'number of corrected bad cells in topo-cluster'                                                 ,
    'cluster_BAD_CELLS_CORR_E'  : 'corrected energy in bad cells in topo-cluster'                                                 ,
    'cluster_BADLARQ_FRAC'      : 'bad LAr signal quality fraction in topo-cluster'                                               ,
    'cluster_ENG_POS'           : 'energy sum of cells with E > 0 in topo-cluster'                                                ,
    'cluster_CELL_SIGNIFICANCE' : 'highest cell signal significance in topo-cluster'                                              ,
    'cluster_CELL_SIG_SAMPLING' : 'sampling id of cell with highest signal significance'                                          ,
    'cluster_AVG_LAR_Q'         : 'average LAr cell signal quality in topo-cluster'                                               ,
    'cluster_AVG_TILE_Q'        : 'average Tile cell signal quality in topo-cluster'                                              ,
    'cluster_ENG_BAD_HV_CELLS'  : 'energy in cells with bad HV in topo-cluster'                                                   ,
    'cluster_N_BAD_HV_CELLS'    : 'number of cells with bad HV in topo-cluster'                                                   ,
    'cluster_PTD'               : 'topo-cluster signal compactness measure'                                                       ,
    'cluster_MASS'              : 'topo-cluster mass from cells with E > 0'                                                       ,
    'cluster_EM_PROBABILITY'    : 'EM probability of topo-cluster from LCW calibration'                                           ,
} 

calibFeaturesDict = { 
    'cluster_ENG_CALIB_TOT'      : 'energy deposited in cells of topo-cluster'                       ,
    'cluster_ENG_CALIB_OUT_T'    : 'energy deposited outside of the topo-cluster'                    ,
    'cluster_ENG_CALIB_DEAD_TOT' : 'energy lost in dead material around the topo-cluster'            ,
    'cluster_ENG_FRAC_EM'        : 'energy deposited in topo-cluster by electrons/positrons/photons' ,
    'cluster_ENG_FRAC_HAD'       : 'energy deposited in topo-cluster by hadrons (mostly ionizations)',
    'cluster_ENG_FRAC_REST'      : 'energy deposited in topo-cluster any other process'              ,
    'cluster_HAD_WEIGHT'         : 'hadronic resonse correction factor'                              ,
    'cluster_OOC_WEIGHT'         : 'out-of-cluster correction factor'                                ,
    'cluster_DM_WEIGHT'          : 'dead material correction factor'                                 ,
} 

eventFeaturesDict = {
    'avgMu'                     : 'average number of interactions per bunch crossing',
    'nPrimVtx'                  : 'number of reconstructed primary vertices'         ,
}

allFeaturesDict = { }
if sys.version_info >= ( 3, 9 ):
    allFeaturesDict = signalFeaturesDict | calibFeaturesDict | eventFeaturesDict 
elif sys.version_info > ( 3, 4 ):
    allFeaturesDict = { **signalFeaturesDict, **calibFeaturesDict, **eventFeaturesDict }
else:
    allFeaturesDict.update(signalFeaturesDict) 
    allFeaturesDict.update(calibFeaturesDict) 
    allFeaturesDict.update(eventFeaturesDict) 

featureDefaults = [
    'clusterEtaCalib',          # input: cluster rapidity          
    'clusterE',                 # input: cluster EM-scale energy
    'cluster_CENTER_LAMBDA',    # input: cluster depth in calorimeter
    'cluster_LONGITUDINAL',     # input: normalized longitudinal compactness measure
    'cluster_LATERAL',          # input: normalized lateral compactness measure
    'cluster_ENG_FRAC_EM',      # input: fraction of energy in EM calorimeter
    'cluster_FIRST_ENG_DENS',   # input: first moment of cell energy densities
    'cluster_SIGNIFICANCE',     # input: overall cluster signal significance
    'cluster_PTD',              # input: cluster fragmentation function
    'cluster_SECOND_TIME',      # input: second moment of cell time distribution
    'avgMu',                    # average number of pile-up collisions contributing to recorded event
    'nPrimVtx',
]

featureProjection = {
    'scale'      : 1.,            # scale
    'bias'       : 0.,            # bias/offset
    'lower'      : float('-inf'), # lower range limit
    'upper'      : float('+inf'), # upper range limit
    'IQR'        : 1.,            # percentage interquantile range
    'Qleft'      : 0.,            # percentage left quantile
    'Qright'     : 1.,            # percentage right quantile
}

featureSpecs = {
    'title'      : None,          # (LaTeX) formatted feature title for plots
    'space'      : None,          # axis space: 'linear', 'log', 'geom'
    'transform'  : None,          # trasnformation: 'none', 'log', 'scaled'
    'projection' : None,          # (x-<x>)/std(x), x -> [
}

featureDict = {
    'clusterEtaCalib'       : 'topo-cluster rapidity (LCW scale)'                                                 ,
    'clusterE'              : 'topo-cluster enegry at EM scale'                                                   ,
    'cluster_CENTER_LAMBDA' : 'distance of topo-cluster center-of-gravity from calorimeter front face'            ,
    'cluster_LONGITUDINAL'  : 'normalized longitudinal dispersion of cell energies in topo-cluster'               ,
    'cluster_LATERAL'       : 'normalized lateral (radial) dispersion of cell energies in topo-cluster'           ,
    'cluster_ENG_FRAC_EM'   : 'topo-cluster energy fraction in electromagnetic calorimeter'                       ,
    'cluster_FIRST_ENG_DENS': 'first (energy-weighted) moment of cell energy density distribution in topo-cluster',
    'cluster_SIGNIFICANCE'  : 'significance of topo-cluster signal (signal-over-noise)'                           ,
    'cluster_PTD'           : 'compactness of cell energy distribution in topo-cluster'                           ,
    'cluster_SECOND_TIME'   : 'second (energy-squared-weighted) moment of cell time distribution in topo-cluster' ,
    'avgMu'                 : 'average number of interactions per bunch crossing'                                 ,
    'nPrimVtx'              : 'number of reconstructed primary vertices'                                          ,
    'cluster_ENG_CALIB_TOT' : 'energy deposited in cells of topo-cluster (MC only)'                               ,
    'clusterResponseEM'     : 'topo-cluster response at EM scale'                                                 ,
    'clusterResponseLCW'    : 'topo-cluster response at LCW scale (hadronic calibration only)'                    ,
    'clusterResponseML'     : 'topo-cluster response at ML scale'                                                 ,
}

def featurePrintWidth(dictionary=featureDict):
    kwidth = int(0)
    fwidth = int(0)
    for k, f in dictionary.items():
        kwidth = np.max([kwidth,len(k)])
        fwidth = np.max([fwidth,len(f)])
    return kwidth, fwidth
    
def featureFormat(features=[],dictionary=featureDict):
    
    # -- list for formatted strings
    lines = []

    # -- check input
    if len(features) == 0:
        return lines

    # -- get description from dictionary and produce formatted list
    kwidth, fwidth = featurePrintWidth(dictionary)
    kwidth += 2  ## account for ""
    for f in features:
        if f in dictionary:
            lstr = f'\042{f}\042'
            dstr = dictionary[f]
            lines += [ f'Feature {lstr:{kwidth}} - {dstr:{fwidth}}' ]

    # -- return list 
    return lines 

def featurePrint(features=[],print_name='',print_level='INFO'):
    lines = featureFormat(features)
    if print_name != '':
        for lstr in lines:
            mprint(module=print_name,level=print_level,message=f'{lstr}')
    else:
        for lstr in lines:
            mprint(module='features::featurePrint',level=print_level,message=f'{lstr}')
