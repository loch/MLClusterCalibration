
import torch
from   torch    import nn
from   torch    import Tensor
from   torch.nn import Module
from   torch.nn import Parameter, UninitializedParameter

ReLU = nn.ReLU

import numpy as np

import config as cfg
mformat = cfg.msgFormat          
mprint  = cfg.msgPrint           
mskip   = cfg.msgSkipLine        
msprint = cfg.msgPrintSeparator  
mbool   = cfg.msgFromBool        
mcolor  = cfg.setColor           
mreset  = cfg.resetColor         

msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgInfo    = cfg.msgLevelINFO
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB

msgTermGreen = mcolor('green',fore=True,bold=False)
msgTermRed   = mcolor('red'  ,fore=True,bold=False)
msgTermReset = mreset()
msgToTerm    = cfg.is_terminal()

module_name = 'bayesian::'


##################################################
## Variational Bayesian Linear Layer (VBLinear) ##
##################################################

## -- simplest variational Bayesian layer (illustrative, use the parameterized version for efficiency!)
class VBLinear( Module ):
    
    __constants__ = [ 'in_features', 'out_features' ]
    in_features  : int                                    ## size of input features
    out_features : int                                    ## size of output features
    weight       : Tensor

    def __init__( self, in_features, out_features ):
        ## -- initialize the base class
        super().__init__() 
        ## -- layer parameters (in_features -> out_features)
        self.in_features  = in_features                                        ## number of input features
        self.out_features = out_features                                       ## number of output features 
        self.resample     = True                                               ## flag controls re-sampling random numbers for weights (True -> yes, False -> no)
        self.bias         = Parameter( Tensor( out_features ) )                ## controls if layer learns additive bias(es)
        self.mu_w         = Parameter( Tensor( out_features, in_features ) )   ## component of KL divergence of this layer (mean value of weighst w)
        self.logsig2_w    = Parameter( Tensor( out_features, in_features ) )   ## component of KL divergence of this layer (log(sigma^2) of weights w) (used to get sigma^2)
        self.random       = torch.randn_like( self.logsig2_w )                 ## random number 
        self.reset_parameters()

    def forward( self, input ):
        if self.resample:
            self.random = torch.randn_like( self.logsig2_w )
        s2_w   = self.logsig2_w.exp()                        ## sigma^2
        weight = self.mu_w + s2_w.sqrt() * self.random       ## sampled weight from Gaussian with width s2_w.sqrt() around mean mu_w
        return nn.functional.linear( input, weight, self.bias )

    def reset_parameters( self ):
        stdv = 1. / np.sqrt( self.mu_w.size(1) )
        self.mu_w.data.normal_( 0, stdv )
        self.logsig2_w.data.zero_().normal_( -9, 0.001 )
        self.bias.data.zero_()

    def KL( self ):
        kl = 0.5 * ( self.mu_w.pow(2) + self.logsig2_w.exp() - self.logsig2_w - 1).sum()
        return kl
    def __repr__(self):
        return f'{self.__class__.__name__:<12}: (input) -> (output) mapping: ({self.in_features}) -> ({self.out_features})'
### mformat(f'{module_name}{self.__class__.__name__}','INFO',f'Mapping: ({self.in_features}) -> ({self.out_features})')
    
################################################################
## Parameterized Variational Bayesian Liner Layer (PVBLinear) ##
################################################################

# local reparameterization trick is more efficient and leads to
# an estimate of the gradient with smaller variance.
# https://arxiv.org/pdf/1506.02557.pdf

class PVBLinear( Module ):

    def __init__(self, in_features, out_features, prior_prec=1.0, _map=False):
        super().__init__()
        self.n_in       = in_features
        self.n_out      = out_features
        self.map        = _map
        self.prior_prec = prior_prec
        self.random     = None
        self.bias       = Parameter( torch.Tensor( out_features ) )
        self.mu_w       = Parameter( torch.Tensor( out_features, in_features ) )
        self.logsig2_w  = Parameter( torch.Tensor( out_features, in_features ) )
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / np.sqrt( self.mu_w.size(1) )
        self.mu_w.data.normal_( 0, stdv )
        self.logsig2_w.data.zero_().normal_(- 9, 0.001 )
        self.bias.data.zero_()

    def reset_random(self):
        self.random = None

    def KL(self):
        logsig2_w = self.logsig2_w.clamp(-20, 11)
        kl = 0.5 * ( self.prior_prec * ( self.mu_w.pow(2) + logsig2_w.exp() ) - logsig2_w - 1 - np.log( self.prior_prec) ).sum()
        return kl

    def forward(self, input):
        if self.training:
            mu_out    = nn.functional.linear(input, self.mu_w, self.bias)
            logsig2_w = self.logsig2_w.clamp( -20, 11 )   ## -- avoid numerical overflows
            s2_w      = logsig2_w.exp()
            var_out   = nn.functional.linear( input.pow(2), s2_w )
            return mu_out + var_out.sqrt() * torch.randn_like( mu_out )
        else:
            # maximum a posteriori (MAP)
            if self.map:
                return nn.functional.linear(input, self.mu_w, self.bias)
            logsig2_w = self.logsig2_w.clamp( -20, 11 )
            if self.random is None:
                self.random = torch.randn_like( self.logsig2_w )
            s2_w = logsig2_w.exp()
            weight = self.mu_w + s2_w.sqrt() * self.random
            return nn.functional.linear( input, weight, self.bias )

    def __repr__(self):
        return f'{self.__class__.__name__:<12}: (input) -> (output) mapping: ({self.n_in}) -> ({self.n_out})'
####        return f'{mname}{self.__class__.__name__} ({self.n_in}) -> ({self.n_out})'
    
############################
## Bayesian Loss Function ##
############################

## -- negative log(Gauss) component (not obvious this is used as external function ??
def neg_log_gauss( outputs, targets ):
    mu        = outputs[:,0]
    logsigma2 = outputs[:,1]
    loss      = torch.pow( mu - targets, 2 ) / ( 2 * logsigma2.exp() ) + 1./2. * logsigma2
    return torch.mean( loss )

#########################
## Activation Function ##
#########################

class ActivateReLU( ReLU ):
    def __repr__(self):
        return f'{self.__class__.__name__}: implements torch.{nn.ReLU().__class__.__name__}'

#########################################################################
## Variational Bayesian Network with parameterized layers (PVBNetwork) ##
#########################################################################

class PVBNetwork( Module ):
    ## -- initialization
    def __init__( self, training_size, hdn_dim=50, num_hidden_layers=3, n_features=1 ):
        # -- initialize base
        super( PVBNetwork, self ).__init__()
        # -- the loss function depends on the amount of training data we have, so we need to store this
        self.training_size = training_size
        # -- the activation layers of the network are not bayesian and we need to be able to access the bayesian layers separately
        self.vb_layers  = []   ## Bayesian layers
        self.all_layers = []   ## all layers
        # -- define the input layer (maps n_features to hdn_dim nodes)
        vb_layer = PVBLinear( n_features, hdn_dim )
        self.vb_layers.append ( vb_layer  )
        self.all_layers.append( vb_layer  )
        self.all_layers.append( ActivateReLU() )
        # -- set up hidden layers
        for i in range( num_hidden_layers ):
            vb_layer = PVBLinear( hdn_dim, hdn_dim )
            self.vb_layers.append ( vb_layer  )
            self.all_layers.append( vb_layer  )
            self.all_layers.append( ActivateReLU() )
        # -- define the output layer
        vb_layer = VBLinear( hdn_dim, 2 )
        self.vb_layers.append ( vb_layer )
        self.all_layers.append( vb_layer )
        
        # -- define the model as a Sequential net over all layers
        self.model = nn.Sequential(*self.all_layers)

    # -- forward function
    def forward( self, x ):
        out = self.model( x )
        return out
    
    # --- we need the KL from the bayesian layers to compute the loss function
    def KL( self ):
        kl = 0
        for vb_layer in self.vb_layers:
            kl += vb_layer.KL()
        return kl / self.training_size
    
    # -- let's put the neg_log_gauss in the network class aswell since it is key to bayesian networks
    def neg_log_gauss( self, outputs, targets ):
        mu        = outputs[:, 0]
        logsigma2 = outputs[:, 1]
        out       = torch.pow( mu - targets, 2 ) / ( 2 * logsigma2.exp() ) + 1./2. * logsigma2
        return torch.mean(out)

#############################
## Action during one epoch ##
#############################

# -- measure the loss during training 
def train_epoch( data_loader=None, model=None, optimizer=None, update_print=100,prev_loss=1000.,prev_kl=1000.,prev_nl=1000.):
    
    mname = f'{module_name}train_epoch'

    #-- check inputs
    if data_loader == None or model == None or optimizer == None:
        mprint(mname,level=msgWarning,message=f'Invalid input: data_loader = {data_loader} model = {model} optimizer = {optimizer}')
        return None
    size    = len( data_loader.dataset )
    ndigits = int(np.log10(size))+1

    #-- train the model
    model.train()
    loss, kl, nl = 0., 0.,0.

    # -- get the loss
    for batch, (X, y) in enumerate( data_loader ):
        # -- pass through network
        pred = model( X )
        # -- compute loss
        nl = model.neg_log_gauss( pred, y.reshape(-1) )
        kl = model.KL()
        loss = nl +kl
        # -- reset gradients in optimizer (from previous epoch)
        optimizer.zero_grad()
        # -- compute gradients
        loss.backward()
        # -- update weights with optimizer
        optimizer.step()
        # -- print the training loss every update_print updates
        if batch % 100 == 0:
            current = batch * len( X )
            if not msgToTerm:
                mprint(mname,level=msgInfo,message=f'current loss per batch: {loss:>+8f} KL: {kl:>+8f} Negative logarithmic: {nl:>+8f} [{current:>{ndigits}d}/{size:>{ndigits}d}]')
            else:
                tlcol = msgTermGreen if loss <= prev_loss else msgTermRed
                klcol = msgTermGreen if kl   <= prev_kl   else msgTermRed
                nlcol = msgTermGreen if nl   <= prev_nl   else msgTermRed
                mprint(mname,level=msgInfo,message=f'current loss per batch: {tlcol}{loss:>+8f}{msgTermReset} KL: {klcol}{kl:>+8f}{msgTermReset} Negative logarithmic: {nlcol}{nl:>+8f}{msgTermReset} [{current:>{ndigits}d}/{size:>{ndigits}d}]')
                prev_loss = loss
                prev_kl   = kl  
                prev_nl   = nl  
    # -- return
    prev_loss = loss
    prev_kl   = kl  
    prev_nl   = nl  
    return prev_loss,prev_kl,prev_nl

# -- pass any (validation, test) data through network and measure loss
def data_pass( data_loader=None, model=None, step_name=None ):

    mname = f'{module_name}data_pass'

    # -- check inputs
    if data_loader == None or model == None:
        if step_name == None:
            mprint(mname,level=msgWarning,message=f'Invalid input: data_loader = {data_loader} model = {model}')
        else:
            mprint(mname,level=msgWarning,message=f'Step [{step_name}] - invalid input: data_loader = {data_loader} model = {model}') 
        return None
    # -- parameters
    size     = len( data_loader.dataset )
    nbatches = len( data_loader )
    print(nbatches)
    # -- loss types: nls - neg log, kls - KL, vls - total loss, mse - MSE loss (for reference)
    nls, kls, vls, mse = 0.0, 0.0, 0.0, 0.0 
    # -- no gradients, only forward pass is used
    with torch.no_grad():
        for X, y in data_loader:
            pred = model( X )
            nl   = model.neg_log_gauss( pred, y.reshape(-1) )
            kl   = model.KL()
            vl   = nl.item() + kl.item()
            mse += torch.mean((pred[:, 0] - y.reshape(-1))**2) ## MSE loss
            nls += nl
            kls += kl
            vls += vl
    # -- averaging 
    nls /= nbatches
    kls /= nbatches
    vls /= nbatches
    mse /= nbatches
    # -- message
    mprint(mname,level=msgInfo,message=f"average loss per batch: {vls:>8f} KL: {kls:>8f} Neg-log {nls:>8f} MSE {mse:>8f} [{step_name}]" )
    
    return nls, kls, vls, mse

# -- pre-define validation and training passes
def training_pass( data_loader=None, model=None ):
    return data_pass(data_loader,model,'training')
def validation_pass( data_loader=None, model=None ):
    return data_pass(data_loader,model,'validation')

########################################
## Main function running the training ##
########################################

# -- arguments are:
#
# batch_size        - size of batch, defaults to 1024
# n_epochs          - number of n_epochs to train, defaults to 8
# learning_rate     - learning rate of network, defaults to 0.0001
# file_name_prefix  - prefix for output file name
# file_name_version - training version attached to file name 
def run_network(model,training_data_loader=None,validation_data_loader=None,batch_size=cfg.model_batch,n_epochs=cfg.model_epochs,learning_rate=cfg.model_learning_rate,file_name_prefix=None,file_name_version=None,file_name_ext=None,file_write_freq=10):
    # -- module name
    mname = f'{module_name}run_training'

    # -- check inputs
    if training_data_loader == None:
        mprint(mname,level=msgLevelError,message=f'invalid training data loader spec {training_loader}, need a valid object to run training')
        return False
    trn_size = len(training_data_loader.dataset)
    if trn_size <= 0:
        mprint(mname,level=msgLevelError,message=f'no training data in loader, N_train = {trn_size}, insufficient data to run training')
        return False


    # -- build file name base
    writeFiles = file_name_prefix != None and file_name_version != None and file_name_ext != None and file_write_freq > 0

    fname_base = f'{file_name_prefix}_e{n_epochs}_b{batch_size}.{file_name_version}' if writeFiles else None
    fname_ext  = file_name_ext

    # -- figure out what else to do
    doValidation = validation_data_loader != None 
    val_size     = len(validation_data_loader.dataset) if doValidation else 0
    doValidation = True if val_size > 0 and doValidation else False

    # -- reinitialize the optimizer
    optimizer = torch.optim.Adam(model.parameters(),lr=learning_rate) 

    # -- tell the world what is done
    mskip()
    mprint(mname,level=msgInfo,message=f'number of epochs to train .....................: {n_epochs}'     )
    mprint(mname,level=msgInfo,message=f'training data size (events, clusters, etc.) ...: {trn_size}'     )
    mprint(mname,level=msgInfo,message=f'batch size ....................................: {batch_size}'   )
    mprint(mname,level=msgInfo,message=f'learning rate .................................: {learning_rate}')
    
    # -- print architecture
    mprint(mname,level=msgInfo,message='model architecture:')
    for m in cfg.msgExtract(model):
        mprint(mname,level=msgInfo,message=m)
    
    # -- track train and validation losses in categories
    trn_nl_losses  = []
    trn_kl_losses  = []
    trn_mse_losses = []
    trn_losses     = []
    val_nl_losses  = []
    val_kl_losses  = []
    val_mse_losses = []
    val_losses     = []

    # -- loop epochs
    ploss = 1000.
    pkl   = 1000.
    pnl   = 1000.
    for t in range(n_epochs):
        msprint(True)
        mprint(mname,level=msgInfo,message=f'Epoch {t+1}')
        msprint(True)
        ploss, pkl, pnl = train_epoch(training_data_loader,model,optimizer,prev_loss=ploss,prev_kl=pkl,prev_nl=pnl)
        msprint(True)
        trn_nl_loss, trn_kl_loss, trn_loss, trn_mse_loss = training_pass(training_data_loader,model) 
        trn_nl_losses.append( trn_nl_loss)
        trn_kl_losses.append( trn_kl_loss)
        trn_mse_losses.append(trn_mse_loss)
        trn_losses.append(    trn_loss)
        msprint()
        if doValidation:
            val_nl_loss, val_kl_loss, val_loss, val_mse_loss = validation_pass(validation_data_loader,model)
            val_nl_losses.append( val_nl_loss)
            val_kl_losses.append( val_kl_loss)
            val_mse_losses.append(val_mse_loss)
            val_losses.append(    val_loss)
            msprint()
        # -- intermittent file saving
        if writeFiles:
            if t == 0 or (t+1) % file_write_freq == 0:
                fname = '{0}_{1:03d}.model.{2}'.format(fname_base,t+1,fname_ext)
                torch.save(model,fname)
                sname = '{0}_{1:03d}.state.{2}'.format(fname_base,t+1,fname_ext)
                torch.save(model.state_dict(),sname)
    
    # -- done
    if writeFiles:
        fname = '{0}_final.model.{1}'.format(fname_base,fname_ext)
        torch.save(model,fname)
        sname = '{0}_final.state.{2}'.format(fname_base,fname_ext)
        torch.save(model.state_dict(),sname)
    mprint(mname,level=msgInfo,message='Training done')
    return True

