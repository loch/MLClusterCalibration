
import sys

import config as cfg

mprint  = cfg.msgPrint          
mskip   = cfg.msgSkipLine       
msprint = cfg.msgPrintSeparator 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()
## -- this needs python3
mcolor = 'cyan'
mstart = f'msprint(module=mname,level=msgInfo,with_time=True,start=True,color=\'cyan\')'
mstop  = f'msprint(module=mname,level=msgInfo,with_time=True,color=\'cyan\')'

import test

import uproot as rootsvc

from bayesian import PVBNetwork as trained_model

'''
Capture arguments and set up validation plots
'''

dnow = cfg.date_now.replace('_','/')
tnow = cfg.time_now
znow = cfg.zone_now

module_description = 'Plotting module for validation and testing a Bayesian DNN (Peter Loch <Peter.Loch_at_cern.ch> (University of Arizona)).'
module_epilog      = f'Help printed on {dnow} at {tnow} {znow}'


################
## Setting up ##
################

title   = f'Project \'{x_name}\' Configuration at {tnow} ({znow}) on {dnow}' 
tlength = len(title) + 6
tchar   = '#'
tbar    = f'{tchar:{tchar}^{tlength}}'

mskip()
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mprint(module=mname,level=msgInfo,message=f'## {title} ##',color='yellow')
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mskip()

eval(mstart)
# -- parse command-line arguments
if '-h' or '--help' in sys.args[1:]:
    mskip()
parser, pargs = cfg.argumentParser('train.py',description=module_description,epilog=module_epilog)

# -- collect the various values
retrieve_value = cfg.argumentValues(parser,pargs,cfg.argumentSettingsTest)
nrecs    , def_nrecs     = retrieve_value.get_value('nrecords'                , both = True)
tstf     , def_tstf      = retrieve_value.get_value('testfrac'                , both = True)
x_mlvl   , def_x_mlvl    = retrieve_value.get_value('exec-msg-level'          , both = True)
x_name   , def_x_name    = retrieve_value.get_value('exec-name'               , both = True)
mfname   , def_mfname    = retrieve_value.get_value('model-file-name'         , both = True)
mfmode   , def_mfmode    = retreive_value.get_value('model-file-mode'         , both = True)
ifname   , def_ifname    = retrieve_value.get_value('input-file-name'         , both = True)
ifpath   , def_ifpath    = retrieve_value.get_value('input-file-path'         , both = True)
iftype   , def_iftype    = retrieve_value.get_value('input-file-type'         , both = True)
rfname   , def_rfname    = retrieve_value.get_value('root-file-name'          , both = True)
rfpath   , def_rfpath    = retrieve_value.get_value('root-file-path'          , both = True)
rtname   , def_rtname    = retrieve_value.get_value('root-tree-name'          , both = True)
msgSvc.set_level(x_mlvl)

def_iffsep = ' '
iffsep     = def_iffsep

# -- print setup so far
if nrecs > 0:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
elif nrecs == -1:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
else:
    mprint(module=mname,level=msgWarning,message=f'number of data records to be read ....: {nrecs} unknown specification, (default: {N} => all available records)')
mprint(module=mname,level=msgInfo,message=f'input filename .......................: \'{ifname}\' (default: \'{def_ifname}\')')
mprint(module=mname,level=msgInfo,message=f'input filepath .......................: \'{ifpath}\' (default: \'{def_ifpath}\')')
mprint(module=mname,level=msgInfo,message=f'input filetype .......................: \'{iftype}\' (default: \'{def_iftype}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT filename ........................: \'{rfname}\' (default: \'{def_rfname}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT filepath ........................: \'{rfpath}\' (default: \'{def_rfpath}\')')
mprint(module=mname,level=msgInfo,message=f'ROOT tree name .......................: \'{rtname}\' (default: \'{def_rtname}\')')
mprint(module=mname,level=msgInfo,message=f'message level ........................: {x_mlvl}: \"{cfg.msgGlobalInfo}\" (default: {def_x_mlvl})')

# -- model provider object
provider = test.Provider(file=mfname,mode=mfmode,msglevel=msgInfo)
model : trained_model
model = provider.model()
mprint(module=mname,level=msgInfo,message='Loaded model:')
mprint(module=mname,level=msgInfo,message=cfg.msgExtract(model))




