
def extractType(var,rtype=int):
    scalar_types = [ int, float, str, bool ]
    list_types   = [ list ]
    # -- invalid input
    if var == None:
        return None
    # -- variable is of requested type, no action
    if type(var) == rtype: 
        return var
    # -- requested type is not a scalar 
    if rtype not in scalar_types:
        return None
    # -- input variable type is a known indexable type  
    if type(var) in list_types:
        return rtype(var[0]) if len(var) > 0 else None
    elif type(var) in scalar_types:
        return rtype(var)
    # -- everything else 
    return None
