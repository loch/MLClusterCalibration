
import torch
from torch.utils.data import Dataset    
from torch.utils.data import DataLoader 

#########################
## Dataprovider object ##
#########################

# This is a (py)torch data set holding the data and labels (targets)

class DataProvider( Dataset ):

    def __init__(self,data,targets):
        self._data    = data
        self._targets = targets 

    def __len__(self):
        return len(self._targets)

    def __getitem__(self,idx):
        if idx < self.__len__():
            return self._data[idx], self._targets[idx]
        else:
            return None

################
## Dataloader ##
################

class DataManager( DataLoader ):
    pass



