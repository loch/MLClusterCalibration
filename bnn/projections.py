
import config as cfg
import features
import numerical

mprint  = cfg.msgPrint          
mskip   = cfg.msgSkipLine       
msprint = cfg.msgPrintSeparator 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()
msgSvc     = cfg.msgSvc
msgDict    = cfg.msgLevelDescr

mname = 'main::writeRootFile'


import numerical

dataState = {
    'original'    : 'original data'   ,
    'transformed' : 'transformed data',
    'projected'   : 'projected data'  ,
}

def DataDescriptor():
    def __init__(self,data,dolog=False,projection_algo=None):
        parmDict = {
            'xmin'  : 0.,
            'xmax'  : 0.,
            'mean'  : 0.,
            'std'   : 0.,
            'iqr'   : 1.,
            'islog' : dolog,
        }
        self._state_dict = {
            'original'    : parmDict,
            'transformed' : parmDict,
            'projected'   : parmDict,
        }
    def __call__(self,data):
        if data,shape[0] == 0: 
            return False
        pdata = sorted(data.to_numpy()) if not isinstance(data,np.ndarray) else sorted(data)
        if projection_algo == None:
            self._state_dict['original'] = { 'xmin' : pdata[0] ,
                                             'xmax' : pdata[-1],
                                             'mean' : np.mean(pdata),
                                             'std'  : np.std (pdata),
                                             'islog': parmDict['dolog'],
                                         }
        
            
                                         



        self._state = 
        ndata = data.reshape(data.shape[0],-1).to_numpy()
        self._xmin, self._xmax = numerical.findMinMax(data)
        self._pmin, self._pmax = 0, 0 if projections == None else numerical.findMinMax(projections)
        self._mean             = np.nmean   ## mean value
        self._std    = std    ## standard deviation
        self._iqr    = iqr    ## inter-quantile range
        self._do_log = dolog  ## transform with log10 if True
    def __call__(self,data):
        
        
        
    def range(self):
        return self._xmin,self._xmax
    def projected_range(self):
        return self._pmin,self._pmax

class DataProjection():
    # -- instance
    def __init__(self,method=None,features=features.featuresDefault,transformToLog=None,print_name=None,**kwargs):
        # -- initial setup
        self._mname                = f'{self.__class__.__name__}' if print_name == None else f'{print_name}::{self.__class__.__name__}'
        self._do_norm              = cfg.doNorm(method)                   ## do mean and std normalization
        self._do_linear            = cfg.doLinearProjection(method)       ## do a linear projection onto a given range
        self._do_quantiles         = None                                 ## use quantiles to define range
        self._do_min_max           = None                                 ## use absolute minimum/maximum range
        self._features             = features                             ## features subjected to transformation and subjection
        self._features_transformed = transformToLog                       ## features transformed to log10(x) before projection
        self._do_print             = self._print_name != None             ## print control
        # -- internalk dictionary
        self._lookup = { }     ## feature -> data descriptor
        self._entry  = { }
        for f in self._features:
            self._lookup[f] = 
        # -- check on method
        if not self._doNorm and not self._do_linear:
            mprint(module=self._mname,level=msgWarning,message=f'unknown method \"{self._method}\", module is inactive')
            return
        # -- collection of additional parameters for linear projections
        if self._do_linear:
            self._value_dict = { 
                'projection_range'  : cfg.projection_range,
                'projection_limits' : cfg.projection_limits,
                'projection_iqr'    : cfg.projection_iqr_percent
            }
            self._limit_extractor = cfg.quantile
            for k, v in kwargs.items():
                if k in self._value_dict:
                    self._value_dict[k] = v
            if self._value_dict['projection_limits'] == cfg.projection_limits_all:
                self._range_calc = numerical.findMinMax
            elif self._value_dict['projection_limits'] == cfg.projection_limits_iqr:
                self._range_calc = nmerical.findQuantiles
    
    def __call__(self,data):
        if self._do_norm:
            return numerical.projectAroundMean(data,features=self._features,do_print=self._do_print,print_name=self._print_name,transformToLog=self._features_transformed) if data.shape[0] > 0 else data
        if self._do_linear:
            


