## 
# @mainpage Topo-cluster calibration using machine-learning
# 
# @author  Peter Loch <Peter.Loch@cern.ch>
# @version 1.0
## 
import sys
import io

import time
import datetime as dtime

import numpy as np
 
from quantiphy import Quantity

def num_quantity(value=0,unit=None,scale=None):
    if unit != None:
        return Quantity(value,unit)
    elif scale != None:
        return Quantity(value,scale=scale)
    else:
        return value

def str_quantity(svalue=None,scale=None):
    if svalue == None:
        return None
    if scale == None:
        return Quantity(svalue)
    else:
        return Quantity(svalue,scale=scale)

quantity = num_quantity

class TimeServer():
    def __init__(self):
        self._start = time.clock_gettime_ns(time.CLOCK_PROCESS_CPUTIME_ID)
        self._stop  = self._start
    def __call__(self,reset=True) -> int:
        self._stop = time.clock_gettime_ns(time.CLOCK_PROCESS_CPUTIME_ID)
        tdiff = self._stop-self._start
        if reset:
            self._start = self._stop
        return tdiff
    def reset(self):
        self._start = time.clock_gettime_ns(time.CLOCK_PROCESS_CPUTIME_ID)
        self._stop  = self._start 

timeSvc = TimeServer()

termForeColors = {
    'default'     : ('\033[0;39m','\033[1;39m'),
    'black'       : ('\033[0;30m','\033[1;30m'),
    'darkred'     : ('\033[0;31m','\033[1;31m'),
    'darkgreen'   : ('\033[0;32m','\033[1;32m'),
    'darkyellow'  : ('\033[0;33m','\033[1;33m'),
    'yellow'      : ('\033[0;33m','\033[1;33m'),
    'darkblue'    : ('\033[0;34m','\033[1;34m'),
    'darkmagenta' : ('\033[0;35m','\033[1;35m'),
    'darkcyan'    : ('\033[0;36m','\033[1;36m'),
    'lightgray'   : ('\033[0;37m','\033[1;37m'),
    'darkgray'    : ('\033[0;90m','\033[1;90m'),
    'red'         : ('\033[0;91m','\033[1;91m'),
    'green'       : ('\033[0;92m','\033[1;92m'),
    'orange'      : ('\033[0;93m','\033[1;93m'),
    'blue'        : ('\033[0;94m','\033[1;94m'),
    'magneta'     : ('\033[0;95m','\033[1;95m'),
    'cyan'        : ('\033[0;96m','\033[1;96m'),
    'white'       : ('\033[0;97m','\033[1;97m'),
    'reset'       : ('\033[0m'   ,'\033[0m'   ),
}

termBackColors = {
    'default'     : '\033[49m' ,
    'black'       : '\033[40m' ,
    'darkred'     : '\033[41m' ,
    'darkgreen'   : '\033[42m' ,
    'darkyellow'  : '\033[43m' ,
    'darkblue'    : '\033[44m' ,
    'darkmagenta' : '\033[45m' ,
    'darkcyan'    : '\033[46m' ,
    'lightgray'   : '\033[47m' ,
    'darkgray'    : '\033[100m',
    'red'         : '\033[101m',
    'green'       : '\033[102m',
    'orange'      : '\033[103m',
    'blue'        : '\033[104m',
    'magneta'     : '\033[105m',
    'cyan'        : '\033[106m',
    'white'       : '\033[107m',
    'reset'       : '\033[0m'  ,
}

def setColor(color,fore=True,bold=False):
    if fore:
        if color in termForeColors:
            if not bold:
                return termForeColors[color][0]
            else:
                return termForeColors[color][1]
    else:
        if color in termBackColors:
            return termBackColors[color]

def resetColor():
    return termForeColors['reset'][0]


class constants():
    def __init__(self):
        self._min_value    = 0.
        self._max_value    = float('+inf')
        self._lowest_value = float('-inf')

    def lowest_value(self):
        return self._lowest_value
    def highest_value(self):
        return self._max_value
    def max_value(self):
        return self._max_value
    def min_value(self,full_range=True):
        return self._lowest_value if full_range else self._min_value

flt_limits = constants()
flt_lowest  = flt_limits.lowest_value()
flt_highest = flt_limits.highest_value()
flt_max     = flt_limits.max_value()
flt_min     = flt_limits.min_value()

##############
## Printing ##
##############

global msgLevelABRT
global msgLevelWARN
global msgLevelINFO
global msgLevelDEBG
global msgLevelVERB

msgLevelABRT = 'ABRT'
msgLevelWARN = 'WARN'
msgLevelINFO = 'INFO'
msgLevelDEBG = 'DEBG'
msgLevelVERB = 'VERB'

global msgLevels

msgLevels = [ msgLevelABRT, msgLevelWARN, msgLevelINFO, msgLevelDEBG, msgLevelVERB ] 

msgLevelDict = {
    msgLevelABRT :  [ [ msgLevelABRT ]                                                       , 'error - fatal error aborts execution'], 
    msgLevelWARN :  [ [ msgLevelABRT, msgLevelWARN ]                                         , 'warning - improper/missing settings/data (recovverable but may change further processing in an unexpected way)'],
    msgLevelINFO :  [ [ msgLevelABRT, msgLevelWARN, msgLevelINFO ]                           , 'info - informative messages'],
    msgLevelDEBG :  [ [ msgLevelABRT, msgLevelWARN, msgLevelINFO, msgLevelDEBG ]             , 'debug - extended messages for debugging'],
    msgLevelVERB :  [ [ msgLevelABRT, msgLevelWARN, msgLevelINFO, msgLevelDEBG, msgLevelVERB], 'verbose - verbose messaging'],
}

msgLevelDescr = {
    msgLevelABRT : msgLevelDict[msgLevelABRT][1],   
    msgLevelWARN : msgLevelDict[msgLevelWARN][1],
    msgLevelINFO : msgLevelDict[msgLevelINFO][1],
    msgLevelDEBG : msgLevelDict[msgLevelDEBG][1],
    msgLevelVERB : msgLevelDict[msgLevelVERB][1],
}

global msgGlobalDefault 
global msgGlobalLevel   
global msgGlobalSteps   
global msgGlobalInfo    

msgGlobalDefault = msgLevelINFO
msgGlobalLevel   = msgGlobalDefault
msgGlobalSteps   = msgLevelDict[msgGlobalLevel][0]
msgGlobalInfo    = msgLevelDict[msgGlobalLevel][1]

global msgPrefixWidth
msgPrefixWidth = 30

def msgInit(level=msgGlobalLevel,module_width=msgPrefixWidth):
    global msgGlobalLevel
    global msgGlobalSteps
    global msgGlobalInfo
    global msgPrefixWidth
###    print(level,type(level))
    if level in msgLevelDict: 
        msgGlobalLevel = level
        msgGlobalSteps = msgLevelDict[level][0] 
        msgGlobalInfo  = msgLevelDict[level][1]
    msgPrefixWidth = module_width

# -- deprecated
def msgSetGlobalLevel(mlevel=msgGlobalDefault) -> None:
    global msgGlobalLevel
    global msgGlobalSteps
    global msgGlobalInfo
    if mlevel in msgLevelDict:
        msgGlobalLevel = mlevel
        msgGlobalSteps = msgLevelDict[msgGlobalLevel][0] 
        msgGlobalInfo  = msgLevelDict[msgGlobalLevel][1]

# -- message server
class msgServer():
    def __init__(self,level=msgGlobalDefault):
        self._level = level
        self._dict  = msgLevelDict

    def __call__(self,level=msgGlobalDefault):
        if level in self._dict:
            self._level = level
        return level

    def set_level(self,level=msgGlobalDefault):
        return self.__call__(level)

    def check_level(self,level):
        if level in self._dict[self._level][0]:
            return True
        else:
            return False

    def __repr__(self):
        return f'actual level {self._level}: {self._dict[self._level][1]}'

msgInit()
msgSvc = msgServer()

def msgSkipLine() -> None:
    print('')

def msgFormat(module='',level=msgGlobalDefault,message='',module_width=msgPrefixWidth,level_width=np.max([len(msgLevels)]),color=None) -> str:

    # -- field width 
    mwidth = module_width  ## 32
    lwidth = level_width   ## np.max([len(msgLevels)])

    # -- adjust module string
    mstr = f'[{module}]'
    if len(mstr) > mwidth:
        mstr = f'[{module[0:mwidth-5]}...]'
###        mstr += '...]'
    
    # -- formatted content
    fstr = None
    lstr = None
    if msgSvc.check_level(level):
        if is_terminal():
            lcol = setColor('default',fore=True,bold=False)
            lres = resetColor()
            if level == msgLevelINFO:
                lcol = setColor('green',fore=True,bold=False)
            elif level == msgLevelABRT:
                lcol = setColor('red',fore=True,bold=False)
            elif level ==  msgLevelDEBG:
                lcol = setColor('cyan',fore=True,bold=False)
            elif level == msgLevelWARN:
                lcol = setColor('yellow',fore=True,bold=False)
            lstr = f'{lcol}{level:<{lwidth}}{lres}'
        else:
            lstr = f'{level:<{lwidth}}'

        fstr = f'{mstr:{mwidth}} {lstr} {message}'
        if is_terminal() and color != None:
            mcolor = setColor(color,fore=True,bold=False)
            mreset = resetColor()
            if mcolor != None:
                fstr = f'{mstr:{mwidth}} {lstr} {mcolor}{message}{mreset}'

    # -- return formatted line
    return fstr

def msgPrint(module,level='INFO',message='',color=None,flush=True) -> None:
    if 'list' in str(type(message)) or 'tuple' in str(type(message)):
        for m in message:
            fstr = msgFormat(module=module,level=level,message=m,color=color)
            if fstr != None:
                print(fstr)
                if flush:
                    sys.stdout.flush()
    else:
        fstr = msgFormat(module=module,level=level,message=message,color=color)
        if fstr != None:
            print(fstr)
            if flush:
                sys.stdout.flush()


def msgExtract(print_this) -> list:
    if print_this == None:               ## nothing to extract, return None
        return []
    ostr = str(print_this).splitlines() if type(print_this) != list else print_this
### old_stdout = sys.stdout              ## remember the stdout
### new_stdout = io.StringIO()           ## assign a new stdout redirecting messages to  
### print(print_this)                    ## print the argument to a string buffer 
### ostr = new_stdout.getvalue()         ## retrieve the buffer content
### sys.stdout = old_stdout              ## reset to the previous stdout
    return ostr                          ## return printed content

def msgSeparator(with_time=False,start=False) -> str:
    if with_time:
        if not start:
            ttime = quantity(value=float(timeSvc(True))*1e-09,unit='s')
###             dexp  = 0
###             while xtime >= 1000:
###                 dexp += 3
###                 xtime /= 1000.
###             ttime = f'{xtime:.3f} ns' if dexp == 0 else f'{xtime:.3f} x 10^{dexp} ns' 
            return f'----------------------------------------------- {time.ctime()} [{ttime}]'
        else:
            timeSvc.reset()
            return f'----------------------------------------------- {time.ctime()}'
    else:
        return f'-----------------------------------------------'

def msgPrintSeparator(module,level='INFO',color=None,with_time=False,start=False):
    msgPrint(module,level=level,message=msgSeparator(with_time=with_time,start=start),color=color)

def is_terminal():
    return sys.stdout.isatty()

mprint  = msgPrint
msep    = msgSeparator
mskip   = msgSkipLine
msprint = msgPrintSeparator 

##################
## Dictionaries ##
##################

msgDict = {
    'yes'       : { True : 'yes'      , False : 'no'           , 'width' :  3 },
    'pass'      : { True : 'pass'     , False : 'fail'         , 'width' :  4 },
    'true'      : { True : 'true'     , False : 'false'        , 'width' :  5 },
    'active'    : { True : 'active'   , False : 'inactive'     , 'width' :  8 },
    'valid'     : { True : 'valid'    , False : 'invalid'      , 'width' :  6 },
    'available' : { True : 'available', False : 'not available', 'width' : 14 }, 
    'primary'   : { True : 'primary'  , False : 'derived'      , 'width' :  7 },
}

# -- backward compatibility -- to be removed soon!
msgSpecTranslate = {
    'y/n' : 'yes'   , 
    'p/f' : 'pass'  ,
    'a/p' : 'active',
    'h/n' : 'available',
    'p/d' : 'primary',
}

def msgFromBoolKey(spec='true',msgdict=msgDict) -> str:
    arg = spec
    if arg not in msgdict:
        if arg in mspSpecTranslate:
            arg = msgSpecTranslate[arg]
        else:
            arg = None
    return arg

def msgFromBool(bool_flag : bool,spec='true',msgdict=msgDict) -> str:
    arg = msgFromBoolKey(spec,msgdict)
    return msgdict[arg][bool_flag] if arg != None else arg

def msgFromBoolWidth(spec='true',msgdict=msgDict) -> int:
    arg = msgFromBoolKey(spec,msgdict)
    return np.max([msgFromBool(True,arg,msgdict),msgFromBool(False,arg,msgdict)]) if arg != None else 0 

mbool = msgFromBool

##
# @section config_applications Application imports

# @name System and general services
# @{
import os
import sys 
import random
import warnings
# @}

# @name Numerical and scientific libraries
# @{
import scipy
# @}

# @name Machine learning (@c pytorch )
# @{
### import torch
### from torch import nn
### from torch.utils.data import Dataset
### from torch.utils.data import DataLoader
### from torch import Tensor
### from torch.nn.parameter import Parameter, UninitializedParameter
### from torch.nn import functional as F
### from torch.nn import init
### from torch.nn import Module
### from torch import jit as jtorch
### # @}

# @name I/O support
# @{
import pandas as pd
# @}

# @name Plotting & graphs
# @{
import colorsys
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.font_manager import FontProperties
import matplotlib.colors as mcolors 
# @}

import argparse
### arg_fmt_with_defaults = argparse.ArgumentDefaultsHelpFormatter
### arg_parser            = argparse.ArgumentParser

class LocalArgParser(argparse.ArgumentParser):
    def error(self,message):
        mprint('config::LocalArgParser',level=msgLevelABRT,message=f'Error - {message} (abort module, usage documentation printed below)')
        mskip()
        self.print_help()
        mskip()
        sys.exit(2)


class LocalHelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    def __init_(self,hwidth=32,width=128,**kwargs):
        super().__init__(max_help_position=hwidth,width=width,**kwargs) ## max_help_position

## -- this reads the configurations from a file specified by '--file'
class LocalArgFile(argparse.Action):
    def __call__(self,parser,namespace,values,option_string=None):
        with values as f:
            parser.parse_args(f.read().split(),namespace)

arg_fmt_with_defaults  = LocalHelpFormatter
arg_parser             = LocalArgParser
arg_parser_file_action = LocalArgFile 
arg_parser_file_tag    = '@'
arg_parser_argv_tag    = '--'


##
# @section config_settings Settings and default parameters

# filter warning messages
warnings.filterwarnings('ignore')

# -- graphics and plotting
labelfont = FontProperties()
labelfont.set_family('serif')
labelfont.set_name('Times New Roman')
labelfont.set_size(14)

axislabelfont = FontProperties()
axislabelfont.set_family('serif')
axislabelfont.set_name('Times New Roman')
axislabelfont.set_size(18)

tickfont = FontProperties()
tickfont.set_family('serif')
tickfont.set_name('Times New Roman')
tickfont.set_size(16)

axisfontsize  = 16
labelfontsize = 16

plt.rcParams['font.family'     ] = 'Times New Roman'
plt.rcParams['mathtext.default'] = 'rm'
plt.rcParams['text.usetex'     ] = True

date_now = time.strftime('%m_%d_%Y')   ## MM_DD_YYYY
time_now = time.strftime('%H:%M:%S')   ## HH:MM:SS
zone_now = time.tzname[time.daylight]  ## time zone

# -- ML  model and file settings
project_name         = 'MLTopoClusterCalibration'            ## project name
dataprep_name        = 'MLTopoClusterDataPreparation'        ## data preparation name
output_file_model_ext       = 'pt'                           ## extension for saved model file names
output_file_model_prefix    = 'bayes_regression'             ## prefix for saved model file names
output_file_model_version   = f'{date_now}_0'                ## model version for use in file names
output_file_write_frequency = 10                             ## write frequency (length of period in count of epochs)

# -- inputs                                             
input_file_name      = 'EM_all.csv'                          ## input file name
input_dir_name       = 'data'                                ## input directory name
input_file_path      = f'{input_dir_name}/{input_file_name}' ## full path to input file
input_file_field_sep = ' '                                   ## input file field separator 
input_file_type      = 'csv'                                 ## input file type (only allowed type!!

# -- input filter
input_filters = { 'cluster_ENG_CALIB_TOT' : '> 0.3',
                  'clusterE'              : '> 0'  ,
                  'cluster_FIRST_ENG_DENS': '> 0'  ,
                  'clusterResponseEM'     : '< 3'  ,
}

#########################
## Training parameters ##
#########################

# -- data sizes
training_size        = -1                                    ## size of an epoch (=<0 -> all input data)
training_fraction    = 0.7                                   ## fraction of data for training
validation_fraction  = 0.3                                   ## fraction of data for validation
testing_fraction     = 0.                                    ## fraction of data for testing

# -- data projections/transformations (all)
#
# projection_method.....: 'none'        - no transformation of projection of features
#                         'MeanStd'     - transform data like (x-mean)/std
#                         'Compression' - project data  [xmin,xmax] -> [vmin,vmax]
# projection_range .....: [-1,1]        - target range for compression [vmin,vmax]
# projection_limits ....: 'MinMax'      - value limits for compression, 'MinMax' = absolute or given value range, 'IQR' = interquantile range
# projection_value_range: [-inf,+inf]   - client-provided value range for compression
# projection_iqr_percent: 0.98          - fraction of sample in IQR for compression   
#         
projection_method_norm = 'Normalize'
projection_method_lpro = 'LinearProjection'

projection_method_dict = {
    projection_method_norm : 'Yields a mean of 0 and a standard deviation of 1 for the dataset by subtracting the mean followed by a division by the standard deviation',
    projection_method_lpro : 'Data is projected into a given numerical range',
}

projection_limits_all = 'MinMax'
projection_limits_iqr = 'IQR'

projection_limits_dict = {
    projection_limits_all : 'Use the minimum and maximum of the data set (all data included)',
    projection_limits_iqr : 'Use only the data within the given interquantile range (IQR) of the data set',
}

## -- configurable
projection_method      = projection_method_norm   ## no transformation by default
projection_range       = [-1,1]                   ## 'LinearProjection': target range
projection_limits      = projection_limits_iqr    ## 'LinearProjection': definition of value range limits
projection_iqr_percent = 0.98                     ## 'LinearProjection': percentage of interquantile range 

## -- not configurable
projection_value_range = [flt_lowest,flt_highest] ## 'LinearProjection': data value range

def checkProjectionMethod(method):
    return method in projection_method_dict

def checkProjectionLimits(limits):
    return limits in projection_limits_dict

def doNorm(method):
    return str(method) == projection_method_norm

def doLinearProjection(method):
    return str(method) == projection_method_lpro

def doLinearProjectionIQRange(method,limit_definition='',iqr_percentage=0):
    return doLinearProjection(method) and limit_definition == projection_limits_iqr and iqr_percentage < 1

def doLinearProjectionFullRange(method,limit_definition='',iqr_percentage=0):
    return doLinearProjection(method) and limit_definition == projection_limits_all

## -- model parameters
model_hidden_layers = 3         ## number of hidden layers
model_nodes         = 128       ## number of hidden layer nodes
model_epochs        = 8         ## number of epochs
model_batch         = 10        ## batch size
model_learning_rate = 0.0001    ## model learning rate
model_network       = 'PVB'     ## default network 

model_network_dict = {
    'PVB'     : 'parameterized variable Bayesian DNN',
    'VB'      : 'variable Bayesian DNN',
}

def model_descr(nname):
    if nname in model_network_dict:
        return model_network_dict[nname]
    else:
        return 'N/A'

######################
## Script execution ##
######################

class RunMode():
    # -- initial setup
    def __init__(self,rmode=None):
        # -- known modes
        self._mode_init           = 'init'          
        self._mode_load           = 'load'          
        self._mode_setup_training = 'setup_training'
        self._mode_run            = 'run'
        self._mode_plot           = 'plot'
        self._mode_all            = 'all'
        self._known_modes = (
            self._mode_init          ,        
            self._mode_load          ,
            self._mode_setup_training,
            self._mode_run           ,
            self._mode_plot          ,
            self._mode_all           ,
        )
        # -- configuration
        if rmode in self._known_modes:
            self.mode_actual = rmode
        else:
            self._mode_actual = None
###            print(f'[config::RunMode] WARN unknown mode \042{rmode}\042 requested, request ignored - actual mode is \042{self._mode_actual}\042')
###            print(f'[config::RunMode] INFO possible modes are {self._known_modes}')
    # -- find mode
    def find(self,rmode):
        if rmode in self._known_modes:
            return True
        else:
###            print(f'[config::RunMode] WARN unknown mode \042{rmode}\042 requested, request ignored - actual mode is \042{self._mode_actual}\042')
###            print(f'[config::RunMode] INFO possible modes are {self._known_modes}')
            return False
        return rmode in self._known_modes

    # -- access mode names
    def init(self):
        return self._mode_init
    def load(self):
        return self._mode_load
    def setup_training(self):
        return self._mode_setup_training
    def run(self):
        return self._mode_run
    def plot(self):
        return self._mode_plot
    def all(self):
        return self._mode_all
    def actual(self):
        return self._mode_actual
    def options(self):
        return self._known_modes

    # -- check mode
    def check(self,rmode,request=None):
        if request == None or not self.find(request):
            return False
        return rmode == request

exec_run_mode_provider = RunMode()
exec_run_mode_default  = exec_run_mode_provider.run()
exec_msg_level         = 'INFO' 

# -- check on mode
def exec_stop_after_load(rmode):
    return exec_run_mode_provider.check(rmode,exec_run_mode_provider.load())
def exec_stop_after_init(rmode):
    return exec_run_mode_provider.check(rmode,exec_run_mode_provider.init())
def exec_stop_after_setup_training(rmode):
    return exec_run_mode_provider.check(rmode,exec_run_mode_provider.setup_training())
def exec_stop_after_run(rmode):
    return exec_run_mode_provider.check(rmode,exec_run_mode_provider.run())
def exec_stop_after_plot(rmode):
    return exec_run_mode_provider.check(rmode,exec_run_mode_provider.plot())

exec_run_mode_dict = {
    'init'           : 'only initialization - checks arguments and run time steering'                      ,
    'load'           : 'loads data into memory and prepares the samples - encompasses the init step'       ,
    'setup_training' : 'sets up the training process but does not run it - encompasses init and load steps',
    'run'            : 'run the training and validation - encompasses all previous steps'                  ,
    'plot'           : 'produce plots - encompasses all previous steps'                                    , 
    'all'            : 'run all steps'                                                                     ,
}

class Range():
    def __init__(self,xmin,xmax):
        self._xmin   = None
        self._xmax   = None
        self._active = xmin < xmax
        if self._active:
            self._xmin = xmin
            self._xmax = xmax

    def __call__(self):
        return [ self._xmin, self._xmax ]
    def __repr__(self):
        return f'[{self._xmin},{self._xmax})'
    def __str__(self): 
        tstr = __repr__(self)
        return f'Range(): {tstr}' 

    def lower(self):
        return self._xmin
    def upper(self):
        return self._xmax
    def in_range(self,x):
        return x >= self.lower() and x < self.upper()
    def descr(self):
        return __repr__(self)
    def range(self):
        return self.lower(), self.upper()

defRangeStr = f'{Range(-1.,1.).range()[0]},{Range(-1.,1.).range()[1]}'
    

#####################
## Argument parser ##
#####################

class ArgumentDescr():
    def __init__(self,arg_default=None,arg_name=None,arg_descr=None,arg_type=None,nargs=1):
        self._default  = arg_default
        self._typename = arg_name
        self._descr    = arg_descr
        self._type     = arg_type
        self._nargs    = nargs

    def default(self):
        return self._default
    def typename(self):
        return self._typename
    def description(self):
        return self._descr
    def type(self):
        return self._type
    def nargs(self):
        return self._nargs

###########################################################
## Old-style configuration with dynamic dataset creation ##
###########################################################

argumentSettingsTrain = {
    'nrecords'                : ArgumentDescr(arg_default=int(training_size)               ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Number of records to be read - all in input file if < 0'),
    'trainfrac'               : ArgumentDescr(arg_default=float(training_fraction)         ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for training'),
    'validfrac'               : ArgumentDescr(arg_default=float(validation_fraction)       ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for validation'),
    'testfrac'                : ArgumentDescr(arg_default=float(testing_fraction)          ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for testing'),
    'projection-method'       : ArgumentDescr(arg_default=str(projection_method_norm)      ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr=f'Feature projection method, {projection_method_dict.keys()}'.replace('dict_keys','choices: ').replace('([','[').replace('])',']')),
    'projection-range'        : ArgumentDescr(arg_default=f'({defRangeStr})'               ,arg_name='(<float>,<float>)',arg_type=str  ,nargs=1,arg_descr='Feature projection range'),
    'projection-iqr'          : ArgumentDescr(arg_default=float(projection_iqr_percent)    ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr=f'Feature projection inter-quantile-range (IQR) fraction-of-sample for method/limit definition {projection_limits_iqr}/{projection_method_lpro}'),
    'projection-limits'       : ArgumentDescr(arg_default=str(projection_limits)           ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Feature projection function defining projection range'),
    'model-design'            : ArgumentDescr(arg_default=str(model_network)               ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Network model design/short name'),
    'model-hidden-layers'     : ArgumentDescr(arg_default=int(model_hidden_layers)         ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of hidden layers'),
    'model-hidden-layer-nodes': ArgumentDescr(arg_default=int(model_nodes)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of nodes in hidden layer'),
    'model-epochs'            : ArgumentDescr(arg_default=int(model_epochs)                ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of epochs to train'),
    'model-batch-size'        : ArgumentDescr(arg_default=int(model_batch)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model batch size'),
    'model-learning-rate'     : ArgumentDescr(arg_default=float(model_learning_rate)       ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Network model learning rate'),
    'exec-run-mode'           : ArgumentDescr(arg_default=str(RunMode().all())             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr=f'Run control - execute up to the specified steps, known steps: {RunMode().options()}'),  
    'exec-msg-level'          : ArgumentDescr(arg_default=str(exec_msg_level)              ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - message level'),
    'exec-name'               : ArgumentDescr(arg_default=str(project_name)                ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - project name'),
    'input-file-path'         : ArgumentDescr(arg_default=str(input_file_path)             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path'),
    'input-file-type'         : ArgumentDescr(arg_default=str(input_file_type)             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file type (only csv/DataFrame available)'),
    'output-file-prefix'      : ArgumentDescr(arg_default=str(output_file_model_prefix)    ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output filename prefix'),
    'output-file-ext'         : ArgumentDescr(arg_default=str(output_file_model_ext)       ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output filename extension'),
    'output-file-version'     : ArgumentDescr(arg_default=str(output_file_model_version)   ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output file version'),
    'output-file-frequency'   : ArgumentDescr(arg_default=int(output_file_write_frequency) ,arg_name='<string>'         ,arg_type=int  ,nargs=1,arg_descr='Output file write frequency (length of period in number of epochs)'),
    'file'                    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Configuration file name')
}

################################################################################################
## Standard training configuration using separate training, validiation and testing data sets ##  
################################################################################################

argumentSettingsTrainFromFiles = {
    'nrecords'                : ArgumentDescr(arg_default=int(training_size)               ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Number of records to be read - all in input file if < 0'),
    'trainfrac'               : ArgumentDescr(arg_default=-1.0                             ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for training (all in file if < 0)'),
    'validfrac'               : ArgumentDescr(arg_default=-1.0                             ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for validation (all in file if < 0)'),
    'testfrac'                : ArgumentDescr(arg_default=-1.0                             ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for testing (all in file if < 0)'),
    'projection-method'       : ArgumentDescr(arg_default=str(projection_method_norm)      ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr=f'Feature projection method, {projection_method_dict.keys()}'.replace('dict_keys','choices: ').replace('([','[').replace('])',']')),
    'projection-range'        : ArgumentDescr(arg_default=f'({defRangeStr})'               ,arg_name='(<float>,<float>)',arg_type=str  ,nargs=1,arg_descr='Feature projection range'),
    'projection-iqr'          : ArgumentDescr(arg_default=float(projection_iqr_percent)    ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr=f'Feature projection inter-quantile-range (IQR) fraction-of-sample for method/limit definition {projection_limits_iqr}/{projection_method_lpro}'),
    'projection-limits'       : ArgumentDescr(arg_default=str(projection_limits)           ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Feature projection function defining projection range'),
    'model-design'            : ArgumentDescr(arg_default=str(model_network)               ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Network model design/short name'),
    'model-hidden-layers'     : ArgumentDescr(arg_default=int(model_hidden_layers)         ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of hidden layers'),
    'model-hidden-layer-nodes': ArgumentDescr(arg_default=int(model_nodes)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of nodes in hidden layer'),
    'model-epochs'            : ArgumentDescr(arg_default=int(model_epochs)                ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of epochs to train'),
    'model-batch-size'        : ArgumentDescr(arg_default=int(model_batch)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model batch size'),
    'model-learning-rate'     : ArgumentDescr(arg_default=float(model_learning_rate)       ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Network model learning rate'),
    'exec-run-mode'           : ArgumentDescr(arg_default=str(RunMode().all())             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr=f'Run control - execute up to the specified steps, known steps: {RunMode().options()}'),  
    'exec-msg-level'          : ArgumentDescr(arg_default=str(exec_msg_level)              ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - message level'),
    'exec-name'               : ArgumentDescr(arg_default=str(project_name)                ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - project name'),
    'input-trn-file-path'     : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path for training dataset'),
    'input-val-file-path'     : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path for validation dataset'),
    'input-tst-file-path'     : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path for test dataset'),
    'output-file-prefix'      : ArgumentDescr(arg_default=str(output_file_model_prefix)    ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output filename prefix'),
    'output-file-ext'         : ArgumentDescr(arg_default=str(output_file_model_ext)       ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output filename extension'),
    'output-file-version'     : ArgumentDescr(arg_default=str(output_file_model_version)   ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output file version'),
    'output-file-frequency'   : ArgumentDescr(arg_default=int(output_file_write_frequency) ,arg_name='<string>'         ,arg_type=int  ,nargs=1,arg_descr='Output file write frequency (length of period in number of epochs)'),
    'file'                    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Configuration file name')
}

argumentSettingsDP = {
    'nrecords'                : ArgumentDescr(arg_default=int(training_size)               ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Number of records to be read - all in input file if < 0'),
    'trainfrac'               : ArgumentDescr(arg_default=float(training_fraction)         ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for training'),
    'validfrac'               : ArgumentDescr(arg_default=float(validation_fraction)       ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for validation'),
    'testfrac'                : ArgumentDescr(arg_default=float(testing_fraction)          ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for testing'),
    'exec-msg-level'          : ArgumentDescr(arg_default=str(exec_msg_level)              ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - message level'),
    'exec-name'               : ArgumentDescr(arg_default=str(dataprep_name)               ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - project name'),
    'input-file-path'         : ArgumentDescr(arg_default=str(input_file_path)             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path'),
    'input-file-type'         : ArgumentDescr(arg_default=str(input_file_type)             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file type (only csv/DataFrame available)'),
    'file'                    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Configuration file name'),
    'output-trn-file-path'    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output file path for training dataset'),
    'output-val-file-path'    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output file path for validation dataset'),
    'output-tst-file-path'    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Output file path for test dataset'),
    'test-all-features'       : ArgumentDescr(arg_default=False                            ,arg_name=''                 ,arg_type=bool ,nargs=1,arg_descr='Write all features to test dataset if flag is present, else selected features only'),
    'train-all-features'      : ArgumentDescr(arg_default=False                            ,arg_name=''                 ,arg_type=bool ,nargs=1,arg_descr='Write all features to training dataset if flag is present, else selected features only'),
    'valid-all-features'      : ArgumentDescr(arg_default=False                            ,arg_name=''                 ,arg_type=bool ,nargs=1,arg_descr='Write all features to validation dataset if flag is present, else selected features only'),
}

argumentSettingsTest = {
    'nrecords'                : ArgumentDescr(arg_default=int(training_size)               ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Number of records to be read - all in input file if < 0'),
    'testfrac'                : ArgumentDescr(arg_default=float(testing_fraction)          ,arg_name='<float>'          ,arg_type=float,nargs=1,arg_descr='Fraction of data used for testing'),
    'exec-msg-level'          : ArgumentDescr(arg_default=str(exec_msg_level)              ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - message level'),
    'exec-name'               : ArgumentDescr(arg_default=str(dataprep_name)               ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Run control - project name'),
    'model-file-name'         : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Model file name'),
    'model-file-mode'         : ArgumentDescr(arg_default='state'                          ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Model file state (mode), choices: \'state\' - state dictionary, \'full\' - full model'), 
    'model-design'            : ArgumentDescr(arg_default=str(model_network)               ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Network model design/short name'),
    'model-hidden-layers'     : ArgumentDescr(arg_default=int(model_hidden_layers)         ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of hidden layers'),
    'model-hidden-layer-nodes': ArgumentDescr(arg_default=int(model_nodes)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model number of nodes in hidden layer'),
    'model-batch-size'        : ArgumentDescr(arg_default=int(model_batch)                 ,arg_name='<integer>'        ,arg_type=int  ,nargs=1,arg_descr='Network model batch size'),
    'input-tst-file-path'     : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Input file path for test dataset'),
    'root-file-name'          : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='ROOT file name (output file)'),
    'root-file-path'          : ArgumentDescr(arg_default='./'                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='ROOT file directory (output file)'),
    'root-tree-name'          : ArgumentDescr(arg_default='ClusterTree'                    ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='ROOT tree name'),
    'file'                    : ArgumentDescr(arg_default=None                             ,arg_name='<string>'         ,arg_type=str  ,nargs=1,arg_descr='Configuration file name'),
}

argumentSettings = { }
if sys.version_info >= ( 3, 9 ):
    argumentSettings = argumentSettingsTrain | argumentSettingsDP | argumentSettingsTrainFromFiles | argumentSettingsTest
elif sys.version_info > ( 3, 4 ):
    argumentSettings = { **argumentSettingsTrain, **argumentSettingsDP, **argumentSettingsTrainFromFiles, **argumentsSettingsTest }
else:
    argumentSettings = argumentSettingsTrain.copy()
    argumentSettings.update(argumentSettingsDP)
    argumentSettings.update(argumentSettingsTrainFromFiles)
    argumentSettings.update(argumentSettingsTest)

class argumentPair():
    def __init__(self,a,b):
        self._a = a
        self._b = b
    def __call__(self):
        return self._a, self._b

from type_decl import extractType as convertToType

class argumentValues():
    # -- attribute name construction
    def _fmt_strip_pref(self,a):
        if a == None or a[0] != '-':
            return a
        return a[1:] if a[1] != '-' else a[2:]
    def _fmt_attr_def(self,a) -> str:
        b = self._fmt_strip_pref(a)
        c = b.replace('-','_')
        return f'_def_{c}'
    def _fmt_attr_val(self,a) -> str:
        b = self._fmt_strip_pref(a)
        c = b.replace('-','_')
        return f'_{c}'
    # -- attribute pair construction
    def _fmt_attr_pair(self,a) -> [ str, str ]:
        return self._fmt_attr_def(a), self._fmt_attr_val(a)
    # -- extract correct type
    def extract_value(self,a,v):
        if a not in self._dict:
            return None
        if type(v) == str and '(' in v and ')' in v:
            vstr = v.replace('(','').replace(')','').split(',')
            return [ float(vstr[0]), float(vstr[1]) ]
        else:
            return convertToType(v,rtype=self._dict[a].type())
    # -- set up attributes dynamically 
    def __init__(self,parser,parsed_args,argument_settings=None):
        self._mname = f'{self.__class__.__name__}'
        self._dict = argumentSettings if argument_settings == None else argument_settings
        for k, v in vars(parsed_args).items():
            mkey = k.replace('_','-')
            if mkey in self._dict:
                dstr, vstr = self._fmt_attr_pair(k)
                etype = self._dict[mkey].type()
                setattr( self, dstr, parser.get_default(k)  )
                setattr( self, vstr, self.extract_value(mkey,v) )
                mprint(f'{self}.__class__.__name__::__init__',level=msgLevelDEBG,message=f'key \'{mkey}\' expected type {etype} found type {type(v)} set type {type(getattr(self,dstr))}')
            else:
                mprint(module=f'{self._mname}::__init__()',level=msgLevelWARN,message=f'key \'{mkey}\' not in argument dictionary, added new key only within scope of this class.')
                dstr, vstr = self._fmt_attr_pair(k)
                setattr( self, dstr, parser.get_default(k) )
                setattr( self, vstr, self.extract_value(mkey,v) )
        
    # -- retrieve default value
    def __call__(self,a,default=False,both=True):
        if not both:
            batt = self._fmt_attr_val(a) if not default else self._fmt_attr_def(a)
            if hasattr(self,batt):
                return self.extract_value(getattr(self,batt))
            else:
                return None
        else:
            batt = self._fmt_attr_val(a)
            bval = None 
            if hasattr(self,batt):
                bval = self.extract_value(a,getattr(self,batt)) 
            datt = self._fmt_attr_def(a)
            dval = None
            if hasattr(self,datt):
                dval = self.extract_value(a,getattr(self,datt))
            return bval, dval
    # -- return a list of all (formatted) attributes
    def get_attr(self):
        return dir(self)
    # -- find value of a specific attribute
    def get_value(self,a=None,default=False,both=True):
        if a == None:
            return None if not both else [ None, None ]
        else:
            return self.__call__(a,default,both)
    # -- set value
    def set_value(self,a=None,v=None,default=False):
        if a != None: 
            b = self._fmt_attr_val(a) if not default else self._fmt_attr_def(a)
            if hasattr(self,b):
                setattr(self,b,v)

def argumentParser(prog_name,description=None,epilog=None,formatter=LocalHelpFormatter,mode='train',argument_settings=None,**kwargs):

    mname = 'config::argumentParser'
    if argument_settings == None:
        if mode == 'train':
            argument_settings = argumentSettingsTrainFromFiles
            mprint(module=mname,level=msgLevelDEBG,message=f'argument parser in \'{mode}\'')
        elif mode == 'dataprep':
            argument_settings = argumentSettingsDP
            mprint(module=mname,level=msgLevelDEBG,message=f'argument parser in \'{mode}\'')
        elif mode == 'test':
            argument_settings = argumentSettingsTest
            mprint(module=mname,level=msgLevelDEBG,message=f'argument parser in \'{mode}\'')
        else:
            argument_settings = argumentSettings
            mprint(module=mname,level=msgLevelWARN,message=f'argument parser in unknown \'{mode}\', use full argument dictionary')
    else:
        mprint(module=mname,level=msgLevelDEBG,message=f'argument parser in \'{mode}\' uses given dictionary')

    # -- optional arguments
    rejected_opts    = [ 'file' ] ## -- list of options to be ignored
    opt_file_name    = None       ## -- file name for options file - this can also be set by --file <file_name>
    do_opt_from_file = False      ## -- flag to read file with options (True means a valid file name has been found
    for k, opt in kwargs.items():
        if k == 'rejected_option':
            rejected_opts += [ opt ]
        if k == 'options_file':
            opt_file_name = opt
        if k == 'do_options_from_file':
            do_opt_from_file = opt
    # -- check if command line includes '--file' directive
    ostream = sys.stdout
    fopt  = 'file'
    ftag  = f'{arg_parser_argv_tag}{fopt}'
    sstr  = [ ]
    if ftag in sys.argv[1:]:
        findx = sys.argv.index(ftag)
        fname = sys.argv[findx+1]
        ofile = open( fname, 'rt' )
        lctr = 1
        for line in ofile.readlines():
            cpos = line.find('#')
            line = ' '.join(line.replace('\n','').split())
            rstr = f'{arg_parser_argv_tag}{line}'
            if cpos != 0:
                if cpos > 0:
                    rstr = f'{arg_parser_argv_tag}{line[:cpos-1]}'
                vstr = None 
                fapos  = rstr.find('\'')
                if fapos >= 0:
                    lapos  = rstr[fapos+1:].find('\'') if fapos >=0 and fapos+1 < len(rstr) else fapos
                    vstr   = rstr[fapos+1:lapos-1] if lapos-1 >= fapos+1 else None
                record = rstr.replace('[','(').replace(']',')').replace('\'','').replace('=','').split()
                mprint(module=mname,level=msgLevelDEBG,message=f'record length: {len(record)}, type {type(record)}, record: {record}')
                if len(record) == 1 or len(record) == 2:
                    if vstr == None:
                        sstr  += record
                    else:
                        sstr  += record
                        sstr  += vstr
                elif len(record) > 0:
                    sstr += record
        do_opt_from_file = True
    else:
        sstr = sstr.append( sys.argv )
    parse_tag = arg_parser_argv_tag # if not do_opt_from_file else arg_parser_file_tag 
    if 'file' in argument_settings:
        ftyp = argument_settings['file'].typename()
        fdef = argument_settings['file'].default()
        if sys.stdout.isatty():
            dcol = 'white'
            fcol = 'lightgray'
            ocol = 'yellow'
            tdco = setColor(dcol,fore=True,bold=False)
            tcol = setColor(fcol,fore=True,bold=False)
            topt = setColor(ocol,fore=True,bold=False)
            tres = resetColor()
            description = f'{tdco}{description} {tcol}All options listed below can also be provided in a file which will be read if the {topt}{parse_tag}file {ftyp}{tcol} option specifies the filename (default value is {topt}\'{fdef}\'{tcol}, meaning no file) and the file exists with the specified name/path. The file should contain one line per option, with the options specified without the leading {topt}{parse_tag}{tcol}.{tres}'
        else:
            description = f'{description} All options listed below can also be provided in a file which will be read if the {parse_tag}file {ftyp} option specifies the filename (default value is \'{fdef}\', meaning no file) and the file exists with the specified name/path. The file should contain one line per option, with the options specified without the leading {parse_tag}.'

    p = arg_parser(prog=prog_name,description=description,epilog=epilog,formatter_class=formatter)
    # -- add arguments
    for k, descr in argument_settings.items():
        if k not in rejected_opts:
            vdef : descr.type()
            vdef = descr.default()
            # -- default is an iterable item
            kopts = f'{parse_tag}{k}'
            p.add_argument(kopts,default=descr.default(),type=descr.type(),nargs=descr.nargs(),metavar=descr.typename(),help=descr.description())
    # -- return parser and parsed arguments
    pargs = p.parse_args(sstr)
    return p, pargs

