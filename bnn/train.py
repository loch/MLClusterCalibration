
mname = 'main::train'

import config as cfg

cfg.msgPrefixWidth = 36
from config import msgSvc

from config import msgPrint          as mprint
from config import msgSkipLine       as mskip
from config import msgPrintSeparator as msprint

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgReset   = cfg.resetColor()

## -- this needs python3
mcolor = 'cyan'
mstart = f'msprint(module=mname,level=msgInfo,with_time=True,start=True,color=\'cyan\')'
mstop  = f'msprint(module=mname,level=msgInfo,with_time=True,color=\'cyan\')'

import utils as util
loader      = util.dataLoader    
samples     = util.composeSamples
fmt         = util.fmtArray1d    
array_print = util.printArray1d  

import numerical

from numerical import transformDataToLog
from numerical import projectAroundMean
from numerical import projectIntoRange
from numerical import projectFromRange
from numerical import valueRangeDict
from numerical import findMinMax
from numerical import findQuantiles

### DNN fitter
import torch
from   torch import Tensor as tensor

### import bayesian
from bayesian import PVBLinear   as layer   
from bayesian import PVBNetwork  as network
from bayesian import run_network as training 

from datamanager import DataProvider as dataprovider
from datamanager import DataManager  as dataloader

from features  import featurePrint
from features  import featureDefaults as features

import argparse
import sys

import numpy as np

#######################
## Capture arguments ##
#######################

dnow = cfg.date_now.replace('_','/')
tnow = cfg.time_now
znow = cfg.zone_now

module_description = 'Training a configurable Dense Neural Network (DNN), using linear Variable Bayesian (VB) or linear Parameterized Variable Bayesian (PVB) network designs. This code is based on code received in a jupyter notebook from the University of Heidelberg (Plehn, Luchmann et al., August 2022). Adaptations for batch running and other non-jupyter environments by Peter Loch <Peter.Loch_at_cern.ch> (University of Arizona).'
module_epilog      = f'Help printed on {dnow} at {tnow} {znow}'

# -- parse command-line arguments
if '-h' or '--help' in sys.args[1:]:
    mskip()
parser, pargs = cfg.argumentParser('train.py',description=module_description,epilog=module_epilog)
## targs = vars(pargs)

# -- collect the various values
retrieve_value = cfg.argumentValues(parser,pargs)
nrecs    , def_nrecs     = retrieve_value.get_value('nrecords'                , both = True)
pmethod  , def_pmethod   = retrieve_value.get_value('projection-method'       , both = True)
prange   , def_prange    = retrieve_value.get_value('projection-range'        , both = True)
piqr     , def_piqr      = retrieve_value.get_value('projection-iqr'          , both = True)
plimits  , def_plimits   = retrieve_value.get_value('projection-limits'       , both = True)
m_nlayers, def_m_nlayers = retrieve_value.get_value('model-hidden-layers'     , both = True)
m_nbatch , def_m_nbatch  = retrieve_value.get_value('model-batch-size'        , both = True)
m_nnodes , def_m_nnodes  = retrieve_value.get_value('model-hidden-layer-nodes', both = True)
m_epochs , def_m_epochs  = retrieve_value.get_value('model-epochs'            , both = True)
m_lr     , def_m_lr      = retrieve_value.get_value('model-learning-rate'     , both = True)
m_name   , def_m_name    = retrieve_value.get_value('model-design'            , both = True)
x_mode   , def_x_mode    = retrieve_value.get_value('exec-run-mode'           , both = True)
x_mlvl   , def_x_mlvl    = retrieve_value.get_value('exec-msg-level'          , both = True)
x_name   , def_x_name    = retrieve_value.get_value('exec-name'               , both = True)
iftrn    , def_iftrn     = retrieve_value.get_value('input-trn-file-path'     , both = True)
ifval    , def_ifval     = retrieve_value.get_value('input-val-file-path'     , both = True)
iftst    , def_iftst     = retrieve_value.get_value('input-tst-file-path'     , both = True)
trnf     , def_trnf      = retrieve_value.get_value('trainfrac'               , both = True)
valf     , def_valf      = retrieve_value.get_value('validfrac'               , both = True)
ofpref   , def_ofpref    = retrieve_value.get_value('output-file-prefix'      , both = True)
ofextn   , def_ofextn    = retrieve_value.get_value('output-file-ext'         , both = True)
ofvers   , def_ofvers    = retrieve_value.get_value('output-file-version'     , both = True)
offreq   , def_offreq    = retrieve_value.get_value('output-file-frequency'   , both = True)
msgSvc.set_level(x_mlvl)

def_iffsep = ' '
iffsep     = def_iffsep

# -- legacy, deprecated
N = def_nrecs

# -- adjust selected values
m_nlayers = np.max([m_nlayers,   1]) ## model number of hidden layers
m_nbatch  = np.max([m_nbatch ,  10]) ## size of batch
m_nnodes  = np.max([m_nnodes ,   4]) ## model number of nodes/hidden layer  
m_epochs  = np.max([m_epochs ,   1]) ## model number of epochs to train
m_lr      = np.min([m_lr     ,0.01]) ## model learning rate

# -- switches and configurations
doMeanStd    = cfg.doNorm(pmethod)
doLinear     = cfg.doLinearProjection(pmethod)
doLinearIQR  = cfg.doLinearProjectionIQRange(pmethod,plimits,piqr)
doLinearFull = cfg.doLinearProjectionFullRange(pmethod,plimits)
if doMeanStd:
    if doLinear:
        mskip()
        mprint(module=mname,level=msgError,message=f'Configuration mismatch: can either do \042{tag_projection_norm}\042 or \042{tag_projection_lpro}\042 in data projection, not both as requested')
        exit(-1)
stopAfterInit  = cfg.exec_stop_after_init(x_mode)
stopAfterLoad  = cfg.exec_stop_after_load(x_mode)
stopAfterSetup = cfg.exec_stop_after_setup_training(x_mode)
stopAfterRun   = cfg.exec_stop_after_run(x_mode)
stopAfterPlot  = cfg.exec_stop_after_plot(x_mode) 

title   = f'Project \'{x_name}\' Configuration at {tnow} ({znow}) on {dnow}' 
tlength = len(title) + 6
tchar   = '#'
tbar    = f'{tchar:{tchar}^{tlength}}'

mskip()
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mprint(module=mname,level=msgInfo,message=f'## {title} ##',color='yellow')
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mskip()
    
## msprint(module=mname,with_time=True,start=True,color='cyan')
eval(mstart)
if nrecs > 0:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
elif nrecs == -1:
    mprint(module=mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
else:
    mprint(module=mname,level=msgWarning,message=f'number of data records to be read ....: {nrecs} unknown specification, (default: {N} => all available records)')
mprint(module=mname,level=msgInfo,message=f'input filename (training) ............: \'{iftrn}\' (default: \'{def_iftrn}\')')
mprint(module=mname,level=msgInfo,message=f'input filename (validation) ..........: \'{ifval}\' (default: \'{def_ifval}\')')
mprint(module=mname,level=msgInfo,message=f'input filename (testing) .............: \'{iftst}\' (default: \'{def_iftst}\')')

str = None
vstr = None
if trnf > 0 and valf > 0:
    maxp = np.max([trnf,valf])
    fmtp = f'{int(np.max([maxp+1,1]))+3}.1f'
    tstr = f'{trnf*100:>{fmtp}}% (default: {def_trnf} means all records)'
    vstr = f'{valf*100:>{fmtp}}% (default: {def_valf} means all records)'
elif trnf > 0 and valf < 0:
    maxp = trnf
    fmtp = f'{int(np.max([maxp+1,1]))+3}.1f'
    tstr = f'{trnf*100:>{fmtp}}% (default: {def_trnf} means all records)'
    vstr = f'{valf:.1f} -> all records (default: {def_valf} means all records)'
elif trnf < 0 and valf > 0:
    maxp = valf
    fmtp = f'{int(np.max([maxp+1,1]))+3}.1f'
    tstr = f'{trnf:.1f} -> all records (default: {def_trnf} means all records)'
    vstr = f'{valf*100:>{fmtp}}% (default: {def_valf} means all records)'
else:
    tstr = f'{trnf:.1f} -> all records (default: {def_trnf} means all records)'
    vstr = f'{valf:.1f} -> all records (default: {def_valf} means all records)'

mprint(module=mname,level=msgInfo,message=f'fraction of data for training ........: {tstr}')
mprint(module=mname,level=msgInfo,message=f'fraction of data for validation ......: {vstr}')

mprint(module=mname,level=msgInfo,message=f'input file field separator ...........: \'{iffsep}\' (default: \'{def_iffsep}\')')
mprint(module=mname,level=msgInfo,message=f'projection method ....................: {pmethod} (default: {def_pmethod})')
if doLinear:
    mprint(module=mname,level=msgInfo,message=f'projection limits definition .........: \'{plimits}\' (default: \'{def_plimits}\')')
    mprint(module=mname,level=msgInfo,message=f'projection range .....................: {prange} (default: {def_prange})')
    if plimits == cfg.projection_limits_iqr:
        mprint(module=mname,level=msgInfo,message=f'projection IQR percentage ............: {piqr*100:>.1f}% (default: {def_piqr*100:>.1f}%)')
mprint(module=mname,level=msgInfo,message=f'model name ...........................: \'{m_name}\': \"{cfg.model_descr(m_name)}\" (default: \'{def_m_name}\')')
mprint(module=mname,level=msgInfo,message=f'model number of hidden layers ........: {m_nlayers} (default: {def_m_nlayers})'                                 )   
mprint(module=mname,level=msgInfo,message=f'model batch size .....................: {m_nbatch} (default: {def_m_nbatch})'                                   )
mprint(module=mname,level=msgInfo,message=f'model number of nodes/hidden layer ...: {m_nnodes} (default: {def_m_nnodes})'                                   )
mprint(module=mname,level=msgInfo,message=f'model number of training epochs ......: {m_epochs} (default: {def_m_epochs})'                                   )
mprint(module=mname,level=msgInfo,message=f'model learning rate ..................: {m_lr:>7.1e} (default: {def_m_lr:<7.1e})'                               )
mprint(module=mname,level=msgInfo,message=f'model number of features/inputs ......: {len(features)} (from configuration)')
mprint(module=mname,level=msgInfo,message=f'execution stops after step ...........: {x_mode}: \"{cfg.exec_run_mode_dict[x_mode]}\" (default: {def_x_mode}: \"{cfg.exec_run_mode_dict[def_x_mode]}\")')
mprint(module=mname,level=msgInfo,message=f'message level ........................: {x_mlvl}: \"{cfg.msgGlobalInfo}\" (default: {def_x_mlvl})')
mprint(module=mname,level=msgInfo,message=f'output filename prefix ...............: \'{ofpref}\' (default: \'{def_ofpref}\')')
mprint(module=mname,level=msgInfo,message=f'output filename extension ............: \'{ofextn}\' (default: \'{def_ofextn}\')')
mprint(module=mname,level=msgInfo,message=f'output filename version tag ..........: {ofvers} (default: {def_ofvers})')
mprint(module=mname,level=msgInfo,message=f'output file write frequency ..........: after {offreq} epochs (default: {def_offreq} epochs)')
eval(mstop)

## -- check configuration 
if iftrn == None:
    mskip()
    mprint(module=mname,level=msgError,message=f'module requires a valid filename for the training data set but was configured with \'{iftrn}\'')
    mskip()
    exit(-10)

do_valid = ifval != None
do_test  = iftst != None

if stopAfterInit:
    mskip()
    mprint(module=mname,level=msgInfo,message=f'End of processing after initialization requested - STOP!',color='red')
    mskip()
    exit(0)

mskip()
mprint(module=mname,level=msgInfo,message='###########################################################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Enter training: define features/inputs and observables for evaluation ##',color='yellow')
mprint(module=mname,level=msgInfo,message='###########################################################################',color='yellow')
mskip()

##############################
## Defining features/inputs ##
##############################

mprint(module=mname,level=msgInfo,message=f'Found {len(features)} features:')
featurePrint(features=features,print_name=mname,print_level=msgInfo)
mskip()

###############################################################
## Define labels (trained variables), references and samples ##
###############################################################

mskip()
mprint(module=mname,level=msgInfo,message='#######################################################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Create training, validation and testing data, labels & references ##',color='yellow')
mprint(module=mname,level=msgInfo,message='#######################################################################',color='yellow')
mskip()

#####################
## Load input data ##
#####################

if nrecs > 0:
    mprint(module=mname,level=msgInfo,message=f'Loading input data - maximum number of records to be read is {nrecs}')
elif nrecs == -1:
    mprint(module=mname,level=msgInfo,message='Loading input data - read all available data records for each dataset')
else:
    mprint(module=mname,level=msgError,message=f'Loading input data - invalid number of data records specified ({nrecs}) -stop')
    exit(-3)

eval(mstart)
mprint(module=mname,level=msgInfo,message='loading training data (be patient)')
trn_data = loader(nrecords=nrecs,filePath=iftrn) if trnf <= 0 else loader(nrecords=-1,sample_fraction=np.min([trnf,1.]),filePath=iftrn)
eval(mstop)
if trn_data.empty:
    mprint(module=mname,level=msgError,message=f'no training dataset found at path {iftrn}, abort')
    exit(-11)
else:
    mprint(module=mname,level=msgInfo,message=f'training dataset with shape {trn_data.shape} loaded')

val_data = []
if do_valid:
    eval(mstart)
    mprint(module=mname,level=msgInfo,message='loading validation data (be patient)')
    val_data = loader(nrecords=nrecs,filePath=ifval) if valf <= 0 else loader(nrecords=-1,sample_fraction=np.min([valf,1.]),filePath=ifval)
    eval(mstop)
    do_valid = not val_data.empty
    if do_valid:
        mprint(module=mname,level=msgInfo,message=f'validation dataset with shape {val_data.shape} loaded')
    else:
        mprint(module=mname,level=msgWarning,message=f'no or empty validation dataset at path {ifval}')
        
# -- references and labels for training sample
trn_e_dep = trn_data["cluster_ENG_CALIB_TOT"]          # energy deposited in cells of topo-cluster
trn_e_ems = trn_data["clusterE"]                       # raw (EM scale) signal
trn_label = trn_e_ems / trn_e_dep                      # training goal/label

# -- references and labels for validation sample
val_e_dep = val_data["cluster_ENG_CALIB_TOT"]          # energy deposited in cells of topo-cluster
val_e_ems = val_data["clusterE"]                       # raw (EM scale) signal
val_label = val_e_ems / val_e_dep                      # training goal/label
 
# -- convert to numpy arrays
trn_data  = trn_data[features].to_numpy()
trn_e_dep = trn_e_dep.to_numpy()
trn_e_ems = trn_e_ems.to_numpy()
trn_label = trn_label.to_numpy()

val_data  = val_data[features].to_numpy()
val_e_dep = val_e_dep.to_numpy()
val_e_ems = val_e_ems.to_numpy()
val_label = val_label.to_numpy()

mskip()
mprint(module=mname,level=msgDebug,message=f'training data .......[shape]: {trn_data.shape}' )
mprint(module=mname,level=msgDebug,message=f'training e_dep ......[shape]: {trn_e_dep.shape}')
mprint(module=mname,level=msgDebug,message=f'training e_ems ......[shape]: {trn_e_ems.shape}')
mprint(module=mname,level=msgDebug,message=f'training labels .....[shape]: {trn_label.shape}')
mskip()

mprint(module=mname,level=msgDebug,message=f'validation data .....[shape]: {val_data.shape}' )
mprint(module=mname,level=msgDebug,message=f'validation e_dep ....[shape]: {val_e_dep.shape}')
mprint(module=mname,level=msgDebug,message=f'validation e_ems ....[shape]: {val_e_ems.shape}')
mprint(module=mname,level=msgDebug,message=f'validation labels ...[shape]: {val_label.shape}')
mskip()

if stopAfterLoad:
    mskip()
    mprint(module=mname,level=msgInfo,message=f'End of processing after loading data requested - STOP!') 
    mskip()
    exit(0)

####################
## Transform data ##
####################

mskip()
mprint(module=mname,level=msgInfo,message='#################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Transform data and features ##',color='yellow')
mprint(module=mname,level=msgInfo,message='#################################',color='yellow')
mskip()

# -- reshape data
trn_data_shaped = np.reshape( trn_data, (trn_data.shape[0],-1) ) if trn_data.shape[0] > 0 else trn_data
val_data_shaped = np.reshape( val_data, (val_data.shape[0],-1) ) if val_data.shape[0] > 0 else val_data
                    
# -- transform data

'''
Data is transformed using ranges (absolute or interquantile) or scaled to standard deviatoins with a mean of 0
'''
## -- transform using log10 if desired
trans_features = [ 'clusterE', 'cluster_FIRST_ENG_DENS' ]
mprint(module=mname,level=msgInfo,message=f'transforming data: x->log10(x) transformation of features {trans_features}')
eval(mstart)
mprint(module=mname,level=msgInfo,message='working on training data')
trn_data_trans = transformDataToLog(trn_data_shaped,features=features,transformToLog=trans_features)
eval(mstop)
mskip()
eval(mstart)
mprint(module=mname,level=msgInfo,message='working on validation data')
val_data_trans = transformDataToLog(val_data_shaped,features=features,transformToLog=trans_features)
eval(mstop)
mskip()

## -- project according to configuration
mprint(module=mname,level=msgInfo,message=f'attempt to projecting data with method \042{pmethod}\042:')

## -- around mean, scaled by standard variation: (x-<x>)/RMS
if doMeanStd:
    eval(mstart)
    trn_data_trans = projectAroundMean(trn_data_trans,features=features,do_print=True,print_name="training data"  ,transformToLog=trans_features) if trn_data_shaped.shape[0] > 0 else trn_data_shaped
    mprint(module=mname,level=msgDebug,message=f'Training data .....[shape after applying {pmethod}]: {trn_data_trans.shape}')
    val_data_trans = projectAroundMean(val_data_trans,features=features,do_print=True,print_name="validation data",transformToLog=trans_features) if val_data_shaped.shape[0] > 0 else val_data_shaped
    mprint(module=mname,level=msgDebug,message=f'Validation data ...[shape after applying {pmethod}]: {val_data_trans.shape}')
    eval(mstop)
## -- projected (compressed) into a given range
elif doLinear:
    eval(mstart)
    doProjDict = {
        'train'    : False,
        'validate' : False,
    }
    # -- get dictionary templates
    trn_range_dict = valueRangeDict
    val_range_dict = valueRangeDict
    # -- fill dictionaries with ranges
    vetoProjection = False
    if doLinearFull:                                                     ###########################################
        # -- training range                                              ## Project data into [data_min,data_max] ##
        trnDict = findMinMax(trn_data_trans,features=features)           ###########################################
        if trnDict != None:
            doProjDict['train'] = True
            for key, value in trnDict.items():
                trn_range_dict[key] = value
        # -- validation range
        valDict = findMinMax(val_data_trans,features=features)
        if valDict != None:
            doProjDict['validate'] = True
            for key, value in valDict.items():
                val_range_dict[key] = value
    elif doLinearIQR:                                                         ###########################################
        # -- training range                                                   ## Project data into interquantile range ##
        trnDict = findQuantiles(trn_data_trans,features=features,iqr=piqr)    ###########################################
        if trnDict != None:
            doProjDict['train'] = True
            for key, value in trnDict.items():
                trn_range_dict[key] = value
        # -- validation range
        valDict = findQuantiles(val_data_trans,features=features,iqr=piqr)
        if valDict != None:
            doProjDict['validate'] = True
            for key, value in valDict.items():
                val_range_dict[key] = value
    else:
        mskip()
        mprint(module=mname,level=msgWarning,message=f'unknown range definition \042{plimits}\042 for projection method \042{pmethod}\042 - no projection of features')
        vetoProjection = True
    # -- transform all data sets
    if not vetoProjection:
        if doProjDict['train']:
            trn_data_trans = projectIntoRange(trn_data_trans,features=features,range_dict=trn_range_dict,projection_range=prange,transformToLog=trans_features,print_name='training')
            mprint(module=mname,level=msgDebug,message=f'Training data .....[shape after applying {pmethod}]: {trn_data_trans.shape}')
        if doProjDict['validate']:
            val_data_trans = projectIntoRange(val_data_trans,features=features,range_dict=val_range_dict,projection_range=prange,transformToLog=trans_features,print_name='validation')
            mprint(module=mname,level=msgDebug,message=f'Validation data ...[shape after applying {pmethod}]: {val_data_trans.shape}')
    eval(mstop)
else:
    mprint(module=mname,level=msgWarning,message=f'Unknown projection method \042{pmethod}\042, no projection of features - remaining shapes:')
    mprint(module=mname,level=msgDebug  ,message=f'Training data ......................: {trn_data_trans.shape}')
    mprint(module=mname,level=msgDebug  ,message=f'Validation data ....................: {val_data_trans.shape}')

#####################
## Data prepration ##
#####################

mskip()
mprint(module=mname,level=msgInfo,message='######################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Data preparation ##',color='yellow')
mprint(module=mname,level=msgInfo,message='######################',color='yellow')
mskip()

## -- convert all data sets to (py)torch tensors
mprint(module=mname,level=msgInfo,message='Convert all data to torch tensors')
trn_data_tensor = tensor(trn_data_trans)
val_data_tensor = tensor(val_data_trans)
mprint(module=mname,level=msgInfo,message='Convert all labels/targets to torch tensors')
trn_label_tensor = tensor(trn_label)
val_label_tensor = tensor(val_label)

## -- define data providers
mprint(module=mname,level=msgInfo,message='Defined data sets merging data and labels/targets')
trn_dataset = dataprovider( trn_data_tensor, trn_label_tensor.unsqueeze(-1) )
val_dataset = dataprovider( val_data_tensor, val_label_tensor.unsqueeze(-1) )

## -- define data loaders
mprint(module=mname,level=msgInfo,message='Set up data loaders for the data sets')
trn_dataloader = dataloader( trn_dataset, batch_size=64, shuffle=True )
val_dataloader = dataloader( val_dataset, batch_size=64, shuffle=True )

## -- check the batch shapes
dshape = next(iter(trn_dataloader))[0].shape
lshape = next(iter(trn_dataloader))[1].shape
mprint(module=mname,level=msgInfo,message=f'shape of training data .....: {dshape}')
mprint(module=mname,level=msgInfo,message=f'shape of training labels ...: {lshape}')

############################
## Setting up the network ##
############################

mskip()
mprint(module=mname,level=msgInfo,message='#####################################################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Setting up the Parameterized Variational Bayesian Network (BNN) ##',color='yellow') 
mprint(module=mname,level=msgInfo,message='#####################################################################',color='yellow')
mskip()

# -- run time environment/computing resources
device = 'cuda' if torch.cuda.is_available() else 'cpu'
mprint(module=mname,level=msgInfo,message=f'Using a <{device}> device')

# -- BNN setup
eval(mstart)
n_features = len(features)
n_sample   = len(trn_dataset)
trn_model  = network( n_sample, n_features=n_features, hdn_dim=m_nnodes, num_hidden_layers=m_nlayers ).to( device )
mprint(module=mname,level=msgInfo,message=f'BNN model configuration:')
##msgs : list
msgs = cfg.msgExtract(trn_model)
mprint(module=mname,level=msgInfo,message=msgs)
eval(mstop)

if stopAfterSetup:
    mskip()
    mprint(module=mname,level=msgInfo,message=f'End of processing after setting up the training requested - STOP!') 
    mskip()
    exit(0)


##########################
## Training the network ##
##########################

mskip()
mprint(module=mname,level=msgInfo,message='##########################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Running the training ##',color='yellow')
mprint(module=mname,level=msgInfo,message='##########################',color='yellow')
mskip()

# -- invoke the training
eval(mstop)
if training(trn_model,training_data_loader=trn_dataloader,validation_data_loader=val_dataloader,batch_size=m_nbatch,n_epochs=m_epochs,learning_rate=m_lr,file_name_prefix=ofpref,file_name_version=ofvers,file_name_ext=ofextn,file_write_freq=offreq): 
    mprint(module=mname,level=msgInfo,message='Successfull training run')
else:
    mprint(module=mname,level=msgWarning,message='Problems with training data set')
eval(mstop)

