
import config as cfg

cfg.msgPrefixWidth = 36
from config import msgSvc

from config import msgPrint          as mprint
from config import msgSkipLine       as mskip
from config import msgPrintSeparator as msprint 

msgInfo    = cfg.msgLevelINFO
msgError   = cfg.msgLevelABRT
msgDebug   = cfg.msgLevelDEBG
msgWarning = cfg.msgLevelWARN
msgVerbose = cfg.msgLevelVERB
msgEmph    = cfg.setColor('yellow',fore=True,bold=False)
msgTime    = cfg.setColor('cyan',fore=True,bold=False)
msgReset   = cfg.resetColor()


import utils as util
loader      = util.dataLoader_prep    
samples     = util.composeSamples

from features  import featurePrint
from features  import featureDefaults as features

import argparse
import sys

import numpy as np

mname = 'main::dataprep'

#######################
## Capture arguments ##
#######################

dnow = cfg.date_now.replace('_','/')
tnow = cfg.time_now
znow = cfg.zone_now

module_description = 'Preparing the training, validation and testing datasets for the training of networks for topo-cluster calibration.'
module_epilog      = f'Help printed on {dnow} at {tnow} {znow}'

# -- parse command-line arguments
if '-h' or '--help' in sys.args[1:]:
    mskip()
parser, pargs = cfg.argumentParser('dataprep.py',description=module_description,epilog=module_epilog,mode='dataprep')

# -- collect the various values
retrieve_value = cfg.argumentValues(parser,pargs)
nrecs    , def_nrecs     = retrieve_value.get_value('nrecords'            , both = True)
trnf     , def_trnf      = retrieve_value.get_value('trainfrac'           , both = True)
valf     , def_valf      = retrieve_value.get_value('validfrac'           , both = True)
tstf     , def_tstf      = retrieve_value.get_value('testfrac'            , both = True)
ifname   , def_ifname    = retrieve_value.get_value('input-file-path'     , both = True)
iftype   , def_iftype    = retrieve_value.get_value('input-file-type'     , both = True)
oftrn    , def_oftrn     = retrieve_value.get_value('output-trn-file-path', both = True)
ofval    , def_ofval     = retrieve_value.get_value('output-val-file-path', both = True)
oftrn    , def_oftrn     = retrieve_value.get_value('output-trn-file-path', both = True)
oftst    , def_oftst     = retrieve_value.get_value('output-tst-file-path', both = True)
x_mlvl   , def_x_mlvl    = retrieve_value.get_value('exec-msg-level'      , both = True)
x_name   , def_x_name    = retrieve_value.get_value('exec-name'           , both = True)
f_atrn   , def_f_atrn    = retrieve_value.get_value('train-all-features'  , both = True)
f_aval   , def_f_aval    = retrieve_value.get_value('valid-all-features'  , both = True)
f_atst   , def_f_atst    = retrieve_value.get_value('test-all-features'   , both = True)
msgSvc.set_level(x_mlvl)
### mprint(mname,level=msgVerbose,message=','.join(['nrecords            ', str(type(nrecs    )), str(type(def_nrecs )), str(type(pargs.nrecords            )), str(type(parser.get_default('nrecords'            )))]))
### mprint(mname,level=msgVerbose,message=','.join(['trainfrac           ', str(type(trnf     )), str(type(def_trnf  )), str(type(pargs.trainfrac           )), str(type(parser.get_default('trainfrac'           )))]))
### mprint(mname,level=msgVerbose,message=','.join(['validfrac           ', str(type(valf     )), str(type(def_valf  )), str(type(pargs.validfrac           )), str(type(parser.get_default('validfrac'           )))]))
### mprint(mname,level=msgVerbose,message=','.join(['testfrac            ', str(type(tstf     )), str(type(def_tstf  )), str(type(pargs.testfrac            )), str(type(parser.get_default('testfrac'            )))]))
### mprint(mname,level=msgVerbose,message=','.join(['exec_msg_level      ', str(type(x_mlvl   )), str(type(def_x_mlvl)), str(type(pargs.exec_msg_level      )), str(type(parser.get_default('exec_msg_level'      )))]))
### mprint(mname,level=msgVerbose,message=','.join(['exec_name           ', str(type(x_name   )), str(type(def_x_name)), str(type(pargs.exec_name           )), str(type(parser.get_default('exec_name'           )))]))
### mprint(mname,level=msgVerbose,message=','.join(['input_file_path     ', str(type(ifname   )), str(type(def_ifname)), str(type(pargs.input_file_path     )), str(type(parser.get_default('input_file_path'     )))]))
### mprint(mname,level=msgVerbose,message=','.join(['input_file_type     ', str(type(iftype   )), str(type(def_iftype)), str(type(pargs.input_file_type     )), str(type(parser.get_default('input_file_type'     )))]))
### mprint(mname,level=msgVerbose,message=','.join(['output_trn_file_path', str(type(oftrn    )), str(type(def_oftrn )), str(type(pargs.output_trn_file_path)), str(type(parser.get_default('output_trn_file_path')))]))
### mprint(mname,level=msgVerbose,message=','.join(['output_val_file_path', str(type(ofval    )), str(type(def_ofval )), str(type(pargs.output_val_file_path)), str(type(parser.get_default('output_val_file_path')))]))
### mprint(mname,level=msgVerbose,message=','.join(['output_tst_file_path', str(type(oftst    )), str(type(def_oftst )), str(type(pargs.output_tst_file_path)), str(type(parser.get_default('output_tst_file_path')))]))

def_iffsep = ' '
iffsep     = def_iffsep

#if x_mlvl != def_x_mlvl: 
#    cfg.msgInit(level=x_mlvl[0])
#
# -- legacy, deprecated
N = def_nrecs

title   = f'Project \'{x_name}\' Configuration at {tnow} ({znow}) on {dnow}' 
tlength = len(title) + 6
tchar   = '#'
tbar    = f'{tchar:{tchar}^{tlength}}'

mskip()
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mprint(module=mname,level=msgInfo,message=f'## {title} ##',color='yellow')
mprint(module=mname,level=msgInfo,message=tbar,color='yellow')
mskip()

if nrecs > 0:
    mprint(mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
elif nrecs == -1:
    mprint(mname,level=msgInfo,message=f'number of data records to be read ....: {nrecs} (default: {N} => all available records)')
else:
    mprint(mname,level=msgWarning,message=f'number of data records to be read ....: {nrecs} unknown specification, (default: {N} => all available records)')

maxp = np.max([trnf,valf,tstf,def_trnf,def_valf,def_tstf])
fixp = int(np.max([maxp+1,1]))+3
fmtp = f'{fixp}.1f'
    
mprint(mname,level=msgInfo,message=f'fraction of data for training ........: {trnf*100:>{fmtp}}% (default: {def_trnf*100:>{fmtp}}%)')
mprint(mname,level=msgInfo,message=f'fraction of data for validation ......: {valf*100:>{fmtp}}% (default: {def_valf*100:>{fmtp}}%)')
mprint(mname,level=msgInfo,message=f'fraction of data for testing .........: {tstf*100:>{fmtp}}% (default: {def_tstf*100:>{fmtp}}%)')
mprint(mname,level=msgInfo,message=f'message level ........................: {x_mlvl}: \"{cfg.msgGlobalInfo}\" (default: {def_x_mlvl})')
mprint(mname,level=msgInfo,message=f'input file path ......................: \'{ifname}\' (default: \'{def_ifname}\')')
mprint(mname,level=msgInfo,message=f'input file type ......................: \'{iftype}\' (default: \'{def_iftype}\')')
mprint(mname,level=msgInfo,message=f'input file field separator ...........: \'{iffsep}\' (default: \'{def_iffsep}\')')
mprint(mname,level=msgInfo,message=f'output filename (training) ...........: \'{oftrn}\' (default: \'{def_oftrn}\')')
mprint(mname,level=msgInfo,message=f'output filename (validation) .........: \'{ofval}\' (default: \'{def_ofval}\')')
mprint(mname,level=msgInfo,message=f'output filename (testing) ............: \'{oftst}\' (default: \'{def_oftst}\')')
mprint(mname,level=msgInfo,message=f'save all features (training) .........: {cfg.msgFromBool(f_atrn)} (default {cfg.msgFromBool(def_f_atrn)})')
mprint(mname,level=msgInfo,message=f'save all features (validation) .......: {cfg.msgFromBool(f_aval)} (default {cfg.msgFromBool(def_f_aval)})')
mprint(mname,level=msgInfo,message=f'save all features (testing) ..........: {cfg.msgFromBool(f_atst)} (default {cfg.msgFromBool(def_f_atst)})')


sumf = float(trnf) + float(valf) + float(tstf)
exco = 0
if sumf > 1.0:
    mprint(mname,level=msgWarning,message=f'configuration: fraction of data for training ({trnf*100:.1f}%), validation ({valf*100:.1f}%) and testing ({tstf*100:.1f}%) sums to {sumf*100:.1f} -> potentially overlapping samples')

mskip()
mprint(module=mname,level=msgInfo,message='########################################################',color='yellow')
mprint(module=mname,level=msgInfo,message='## Data preparation: input/output features and labels ##',color='yellow')
mprint(module=mname,level=msgInfo,message='########################################################',color='yellow')
mskip()

##############################
## Defining features/inputs ##
##############################

add_col = 'cluster_ENG_CALIB_TOT'
if add_col not in features:
    features.append(add_col)

mprint(mname,level=msgInfo,message=f'Found {len(features)} features:')
featurePrint(features=features,print_name='main::dataprep',print_level=msgInfo)
mskip()

#####################
## Load input data ##
#####################

if nrecs > 0:
    mprint(mname,level=msgInfo,message=f'Loading input data - maximum number of records to be read is {nrecs}')
elif nrecs == -1:
    mprint(mname,level=msgInfo,message='Loading input data - read all available data records')
else:
    mprint(mname,level=msgError,message=f'Loading input data - invalid number of data records specified ({nrecs}) -stop')
    exit(-3)
mskip()

msprint(mname,level=msgInfo,with_time=True,start=True,color='cyan')
df = loader(features=features,nrecords=nrecs)
msprint(mname,level=msgInfo,with_time=True,color='cyan')

###############################################################
## Define labels (trained variables), references and samples ##
###############################################################

mskip()
mprint(mname,level=msgInfo,message='###################################################################',color='yellow')
mprint(mname,level=msgInfo,message='## Create training, validation and testing data including labels ##',color='yellow')
mprint(mname,level=msgInfo,message='###################################################################',color='yellow')
mskip()

# -- randomly split pandas data into samples
msprint(module=mname,with_time=True,start=True,color='cyan')
trn_data, val_data, tst_data = samples(df,train_fraction=trnf,valid_fraction=valf,test_fraction=tstf)
msprint(module=mname,with_time=True,color='cyan')

# -- write out samples
if trnf > 0 and oftrn != None:
    nrows = trn_data.shape[0]
    ncols = len(features)
    hcols = features
    if f_atrn:
        ncols = trn_data.shape[1]
        hcols = None
    mprint(module=mname,level=msgInfo,message=f'Writing training data set with shape ({nrows}, {ncols}) to file \"{oftrn}\"')
    trn_data.to_csv(path_or_buf=oftrn,sep=iffsep,columns=hcols,index=False)
if valf > 0 and ofval != None:
    nrows = val_data.shape[0]
    ncols = len(features) 
    if f_aval:
        ncols = val_data.shape[1]
        hcols = None
    mprint(module=mname,level=msgInfo,message=f'Writing validation data set with shape ({nrows}, {ncols}) to file \"{ofval}\"')
    val_data.to_csv(path_or_buf=ofval,sep=iffsep,columns=hcols,index=False)
if tstf > 0 and oftst != None:
    nrows = tst_data.shape[0]
    ncols = len(features) 
    if f_atst:
        ncols = tst_data.shape[1]
        hcols = None
    mprint(module=mname,level=msgInfo,message=f'Writing test data set with shape ({nrows}, {ncols}) to file \"{oftst}\"')
    tst_data.to_csv(path_or_buf=oftst,sep=iffsep,columns=hcols,index=False)
mskip()


