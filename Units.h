// -*- c++ -*-
#ifndef UNITS_H
#define UNITS_H

#include <string>
#include <map> 

#include <cmath>

#include "Types.h"

#ifndef M_PI
#  define M__PI 3.14159265358979323846
#endif

#ifdef UNITS_USE_GEV
#  define UNITS_E_BASE 1.0E-03
#  define UNITS_E_NAME "GeV"
#else
#  define UNITS_E_BASE 1.0
#  define UNITS_E_NAME "MeV"   
#endif

///@brief Units
///
/// This code is cased on @c GaudiKernel/SystemOfUnits. The basic units are:
///
/// millimeter              (millimeter)
/// nanosecond              (nanosecond)
/// Mega electron Volt      (MeV)
/// positron charge         (eplus)
/// degree Kelvin           (kelvin)
/// the amount of substance (mole)
/// luminous intensity      (candela)
/// radian                  (radian)
/// steradian               (steradian)
///
/// The compiler directive @c UNITS_USE_GEV changes the MeV to GeV as the basic energy unit. 
namespace Units {
  ///@name Length, area, volume 
  ///@{
  constexpr double millimeter  = 1.;
  constexpr double millimeter2 = millimeter * millimeter;
  constexpr double millimeter3 = millimeter * millimeter * millimeter;
  constexpr double centimeter  = 10. * millimeter;
  constexpr double centimeter2 = centimeter * centimeter;
  constexpr double centimeter3 = centimeter * centimeter * centimeter;
  constexpr double meter       = 1000. * millimeter;
  constexpr double meter2      = meter * meter;
  constexpr double meter3      = meter * meter * meter;
  constexpr double kilometer   = 1000. * meter;
  constexpr double kilometer2  = kilometer * kilometer;
  constexpr double kilometer3  = kilometer * kilometer * kilometer;
  constexpr double parsec      = 3.0856775807e+16 * meter;
  constexpr double micrometer  = 1.0e-06 * meter;
  constexpr double nanometer   = 1.0e-09 * meter;
  constexpr double angstrom    = 1.0e-10 * meter;
  constexpr double femtometer  = 1.0e-15 * meter; 
  constexpr double fermi       = femtometer;
  constexpr double barn        = 1.0e-28 * meter2;
  constexpr double millibarn   = 1.0e-03 * barn;
  constexpr double microbarn   = 1.0e-06 * barn;
  constexpr double nanobarn    = 1.0e-09 * barn;
  constexpr double picobarn    = 1.0e-12 * barn;
  constexpr double femtobarn   = 1.0e-15 * barn;
  constexpr double liter       = 1.0e+03 * centimeter3;
  constexpr double deciliter   = 1.0e-01 * liter;
  constexpr double centiliter  = 1.0e-02 * liter;
  constexpr double milliliter  = 1.0e-03 * liter;
  constexpr double fm          = femtometer;
  constexpr double nm          = nanometer;
  constexpr double um          = micrometer;
  constexpr double mm          = millimeter;
  constexpr double mm2         = millimeter2;
  constexpr double mm3         = millimeter3;
  constexpr double cm          = centimeter;
  constexpr double cm2         = centimeter2;
  constexpr double cm3         = centimeter3;
  constexpr double m           = meter;
  constexpr double m2          = meter2;
  constexpr double m3          = meter3;
  constexpr double km          = kilometer;
  constexpr double km2         = kilometer2;
  constexpr double km3         = kilometer3;
  constexpr double pc          = parsec;
  constexpr double L           = liter;
  constexpr double dL          = deciliter;
  constexpr double cL          = centiliter; 
  constexpr double mL          = milliliter;
  constexpr double b           = barn;
  constexpr double mb          = millibarn;
  constexpr double ub          = microbarn;
  constexpr double nb          = nanobarn; 
  constexpr double pb          = picobarn; 
  constexpr double fb          = femtobarn;
  ///@}
  ///@name Angles
  ///@{
  constexpr double radian      = 1.0;
  constexpr double milliradian = 1.0e-03 * radian;
  constexpr double degree      = ( M_PI / 180. ) * radian;
  constexpr double steradian   = 1.;
  constexpr double rad         = radian;
  constexpr double mrad        = milliradian;
  constexpr double sr          = steradian;
  constexpr double deg         = degree;
  ///@}
  ///@name Time
  ///@{
  constexpr double nanosecond  = 1.;
  constexpr double second      = 10.e+09 * nanosecond;
  constexpr double millisecond = 10.e-03 * second;
  constexpr double microsecond = 10.e-06 * second;
  constexpr double picosecond  = 1.0e-12 * second;
  constexpr double femtosecond = 1.0e-15 * second;
  constexpr double minute      = 60.0 * second;
  constexpr double hour        = 60.0 * minute;
  constexpr double day         = 24.0 * hour;
  constexpr double week        = 7.0 * day;
  constexpr double hertz       = 1. / second;
  constexpr double kilohertz   = 1.0e+03 * hertz;
  constexpr double megahertz   = 1.0e+06 * hertz;
  constexpr double gigahertz   = 1.0e+09 * hertz;
  constexpr double ps          = picosecond;
  constexpr double us          = microsecond;
  constexpr double ns          = nanosecond;
  constexpr double s           = second;
  constexpr double ms          = millisecond;
  constexpr double min         = minute;
  constexpr double hr          = hour;
  constexpr double dy          = day;
  constexpr double wk          = week;
  constexpr double Hz          = hertz;
  constexpr double kHz         = kilohertz;
  constexpr double MHz         = megahertz;
  constexpr double GHz         = gigahertz;
  ///@}
  ///@name Electric charge
  ///@{
  constexpr double eplus       = 1.;              // positron charge
  constexpr double e_SI        = 1.602176487e-19; // positron charge in coulomb
  constexpr double coulomb     = eplus / e_SI;    // coulomb = 6.24150 e+18 * eplus
  ///@}
  ///@name Energy 
  ///@{
  constexpr double megaelectronvolt = UNITS_E_BASE ;
  constexpr double electronvolt     = 1.0e-06 * megaelectronvolt;
  constexpr double kiloelectronvolt = 1.0e-03 * megaelectronvolt;
  constexpr double gigaelectronvolt = 1.0e+03 * megaelectronvolt;
  constexpr double teraelectronvolt = 1.0e+06 * megaelectronvolt;
  constexpr double petaelectronvolt = 1.0e+09 * megaelectronvolt;
  constexpr double joule            = electronvolt / e_SI; // joule = 6.24150 e+12 * MeV
  constexpr double MeV              = megaelectronvolt;
  constexpr double eV               = electronvolt;
  constexpr double keV              = kiloelectronvolt;
  constexpr double GeV              = gigaelectronvolt;
  constexpr double TeV              = teraelectronvolt;
  constexpr double PeV              = petaelectronvolt;
  ///@}
  ///@name Mass
  ///@{
  constexpr double kilogram         = joule * second * second / ( meter * meter );
  constexpr double gram             = 1.0e-03 * kilogram;
  constexpr double milligram        = 1.0e-03 * gram;
  constexpr double mircogram        = 1.0e-03 * milligram;
  constexpr double kg               = kilogram;
  constexpr double g                = gram;
  constexpr double mg               = milligram;
  constexpr double ug               = microgram;
  ///@}
  ///@name Power, force and pressure
  ///@{
  constexpr double watt             = joule / second; // watt = 6.24150 e+3 * MeV/ns
  constexpr double kilowatt         = 1.0e+03 * watt;
  constexpr double megawatt         = 1.0e+03 * kilowatt;
  constexpr double milliwatt        = 1.0e-03 * watt;
  constexpr double newton           = joule / meter;   // newton = 6.24150 e+9 * MeV/mm
  constexpr double pascal           = newton / m2;     // pascal = 6.24150 e+3 * MeV/mm3
  constexpr double hep_pascal       = pascal;          // to match CLHEP
  constexpr double bar              = 1.0e+05 * pascal; // bar    = 6.24150 e+8 * MeV/mm3
  constexpr double millibar         = 1.0e-03 * bar;
  constexpr double atmosphere       = 101325  * pascal; // atm    = 6.32420 e+8 * MeV/mm3
  constexpr double hectopascal      = 1.0e+02 * pascal;
  constexpr double W                = watt;
  constexpr double kW               = kilowatt;
  constexpr double MW               = megawatt;
  constexpr double mW               = milliwatt;
  constexpr double Pa               = pascal;
  constexpr double hPa              = hectopascal;
  constexpr double mbar             = millibar;
  constexpr double atm              = atmosphere;  
  ///@}
  ///@name Electric current
  ///@{
  constexpr double ampere           = coulomb / second; // ampere = 6.24150 e+9 * eplus/ns
  constexpr double milliampere      = 1.0e-03 * ampere;
  constexpr double microampere      = 1.0e-06 * ampere;
  constexpr double nanoampere       = 1.0e-09 * ampere;
  constexpr double A                = ampere; 
  constexpr double mA               = milliampere; 
  constexpr double uA               = microampere; 
  constexpr double nA               = nanoampere; 
  ///@}
  ///@name ELectric potential
  ///@{
  constexpr double megavolt         = megaelectronvolt / eplus;
  constexpr double kilovolt         = 1.0e-03 * megavolt;
  constexpr double volt             = 1.0e-03 * kilovolt;
  constexpr double V                = volt;
  constexpr double mV               = millivolt;
  constexpr double MV               = megavolt;
  ///@}
  ///@name Electric resistance
  ///@{
  constexpr double ohm              = volt / ampere; // ohm = 1.60217e-16*(MeV/eplus)/(eplus/ns)
  constexpr double kiloohm          = 1.0e+03 * ohm;
  constexpr double megaohm          = 1.0e+03 * kiloohm;
  constexpr double milliohm         = 1.0e+03 * ohm;
  ///@}
  ///@name Electric capacitance
  ///@{ 
  constexpr double farad            = coulomb / volt; // farad = 6.24150e+24 * eplus/Megavolt
  constexpr double millifarad       = 1.0e-03 * farad;
  constexpr double microfarad       = 1.0e-06 * farad;
  constexpr double nanofarad        = 1.0e-09 * farad;
  constexpr double picofarad        = 1.0e-12 * farad;
  constexpr double F                = farad;
  constexpr double mF               = millifarad;
  constexpr double uF               = microfarad;
  constexpr double nF               = nanofarad; 
  constexpr double pF               = picofarad;
  ///@}
  ///@name Magnetic flux, field and inductance
  ///@{
  constexpr double weber            = volt * second; // weber = 1000*megavolt*ns
  constexpr double tesla            = volt * second / meter2; // tesla =0.001*megavolt*ns/mm2
  constexpr double gauss            = 1.0e-04 * tesla;
  constexpr double kilogauss        = 1.0e+03 * gauss;
  constexpr double henry            = weber / ampere; // henry = 1.60217e-7*MeV*(ns/eplus)**2
  constexpr double Wb               = weber;
  constexpr double T                = tesla;
  constexpr double G                = gauss;
  constexpr double kG               = kilogauss;
  constexpr double Gs               = gauss;
  constexpr double kGs              = kilogauss;
  constexpr double H                = henry;
  ///@}  
  ///@name Temperature, amount of substance 
  ///@{
  constexpr double kelvin          = 1.0;
  constexpr double mole            = 1.0;
  ///@}
  ///@name Radioactivity and absorbed dose
  ///@{
  constexpr double becquerel       = 1. / second;
  constexpr double curie           = 3.7e+10 * becquerel;
  constexpr double kilobecquerel   = 1.0e+03 * becquerel;
  constexpr double megabecquerel   = 1.0e+06 * becquerel;
  constexpr double gigabecquerel   = 1.0e+09 * becquerel;
  constexpr double millicurie      = 1.0e-03 * curie;
  constexpr double microcurie      = 1.0e-06 * curie;
  constexpr double gray            = joule / kilogram;
  constexpr double kilogray        = 1.0e+03 * gray;
  constexpr double milligray       = 1.0e-03 * gray;
  constexpr double microgray       = 1.0e-06 * gray;
  constexpr double Bq              = becquerel;
  constexpr double kBq             = kilobecquerel;
  constexpr double MBq             = megabecquerel;
  constexpr double GBq             = gigabecquerel;
  constexpr double Ci              = curie;
  constexpr double mCi             = millicurie;
  constexpr double uCi             = microcurie;
  constexpr double Gy              = gray;      
  constexpr double kGy             = kilogray; 
  constexpr double mGy             = milligray;
  constexpr double uBy             = microgray;
  ///@}
  ///@name Luminous intensity, flux and illuminance
  ///@{
  constexpr double candela         = 1.;
  constexpr double lumen           = candela * steradian;
  constexpr double lux             = lumen / meter2;
  constexpr double cd              = candela;
  constexpr double lm              = lumen;
  constexpr double lx              = lux;
  ///@}
  ///@name Miscellaneous
  constexpr double perCent        = 0.01;
  constexpr double perThousand    = 0.001;
  constexpr double perMillion     = 0.000001;
  ///@}
  ///@brief Unit descriptor
  struct Descriptor {
    double      scale      = { 1. };    ///< Unit scale factor
    std::string name       = { "" };    ///< Unit name
    std::string abbr       = { "" };    ///< Unit abbreviation
    std::string symbol     = { "" };    ///< Unit screen symbol
    std::string axis       = { "" };    ///< Unit axis title (general)
    std::string latex_axis = { "" };    ///< Unit axis title (LaTeX)
    std::string root_axis  = { "" };    ///< Unit axis title (ROOT)
    bool        isBase     = { false }; ///< Unit is a base unit if @c true
    ///@brief Default (invalid) unit description
    Descriptor() { }
    ///@brief General (loaded) unit description
    Descriptor(double sfactor,const std::string& nm,const std::string& ab,const std::string& sy,const std::string& ax,const std::string& lx,const std::string& rx,bool fb=false) 
      : scale(sfactor),name(nm), abbr(ab), symbol(sy), axis(ax), latex_axis(lx), root_axis(rx), isBase(fb || ab == UNIT_E_NAME ) { }
    ///@brief Copy constructor
    Descriptor(const Descriptor& udescr) : scale(udescr.scale), name(udescr.name), abbr(udescr.abbr), symbol(udescr.symbol), axis(udescr.axis), latex_axis(udescr.latex_axis), root_axis(udescr.root_axis), isBase(udescr.isBase) { }
    ///@brief Assignment operator
    Descriptor& operator=(const Descriptor& udescr) {
      if ( this == &udescr ) { return *this; }
      scale      = udescr.scale;
      name       = udescr.name; 
      abbr       = udescr.abbr; 
      symbol     = udescr.symbol; 
      axis       = udescr.axis; 
      latex_axis = udescr.latex_axis; 
      root_axis  = udescr.root_axis; 
      isBase     = udescr.isBase;
      return *this;
    }
  };
  ///@brief Unit description dictionary
  ///
  /// The key to this disctionary is the abbreviated unit name which is identical to the short name of the scale factor (if any, else it is the full name).
  static const std::map<std::string,Descriptor> Descriptions = {
    { "mm"    , Descriptor(mm   ,"millimeter"      ,"mm"  ,"mm"   ,"mm"        ,"\ensuremath{\text{mm}}"       ,"mm"           ,true )},
    { "mm2"   , Descriptor(mm2  ,"millimeter2"     ,"mm2" ,"mm2"  ,"mm^{2}"    ,"\ensuremath{\text{mm}^{2}}"   ,"mm^{2}"       ,false)},
    { "mm3"   , Descriptor(mm3  ,"millimeter3"     ,"mm3" ,"mm3"  ,"mm^{3}"    ,"\ensuremath{\text{mm}^{3}"    ,"mm^{3}"       ,false)},
    { "cm"    , Descriptor(cm   ,"centimeter"      ,"cm"  ,"cm"   ,"cm"        ,"\ensuremath{\text{cm}}"       ,"cm"           ,false)},
    { "cm2"   , Descriptor(cm2  ,"centimeter2"     ,"cm2" ,"cm2"  ,"cm^{2}"    ,"\ensuremath{\text{cm}^{2}}"   ,"cm^{2}"       ,false)},
    { "cm3"   , Descriptor(cm3  ,"centimeter3"     ,"cm3" ,"cm3"  ,"cm^{3}"    ,"\ensuremath{\text{cm}^{3}}"   ,"cm^{3}"       ,false)},
    { "m"     , Descriptor(m    ,"meter"           ,"m"   ,"mm"   ,"mm"        ,"\ensuremath{\text{mm}}"       ,"mm"           ,false)},
    { "m2"    , Descriptor(m2   ,"meter2"          ,"m2"  ,"mm2"  ,"m^{2}"     ,"\ensuremath{\text{m}^{2}}"    ,"m^{2}"        ,false)},
    { "m3"    , Descriptor(m3   ,"meter3"          ,"m3"  ,"mm3"  ,"m^{3}"     ,"\ensuremath{\text{m}^{3}}"    ,"m^{3}"        ,false)},
    { "km"    , Descriptor(km   ,"centimeter"      ,"km"  ,"km"   ,"km"        ,"\ensuremath{\text{km}}"       ,"km"           ,false)},
    { "km2"   , Descriptor(km2  ,"centimeter2"     ,"km2" ,"km2"  ,"km^{2}"    ,"\ensuremath{\text{km}^{2}}"   ,"km^{2}"       ,false)},
    { "km3"   , Descriptor(km3  ,"centimeter3"     ,"km3" ,"km3"  ,"km^{3}"    ,"\ensuremath{\text{km}^{3}}"   ,"km^{3}"       ,false)},
    { "pc"    , Descriptor(pc   ,"parsec"          ,"pc"  ,"pc"   ,"pc"        ,"\ensuremath{\text{pc}}"       ,"pc"           ,false)},
    { "mu"    , Descriptor(mu   ,"micrometer"      ,"mu"  ,"μm"   ,"\mu m"     ,"\ensuremath{\mu\text{m}}"     ,"#mum"         ,false)},
    { "nm"    , Descriptor(nm   ,"nanometer"       ,"nm"  ,"nm"   ,"nm"        ,"\ensuremath{\text{nm}}"       ,"nm"           ,false)},
    { "pm"    , Descriptor(pm   ,"picometer"       ,"pm"  ,"pm"   ,"pm"        ,"\ensuremath{\text{pm}}"       ,"pm"           ,false)},
    { "fm"    , Descriptor(fm   ,"femtometer"      ,"fm"  ,"fm"   ,"fm"        ,"\ensuremath{\text{fm}}"       ,"fm"           ,false)},
    { "fermi" , Descriptor(fm   ,"femtometer"      ,"fm"  ,"fm"   ,"fm"        ,"\ensuremath{\text{fm}}"       ,"fm"           ,false)},
    { "A"     , Descriptor(A,   ,"angstrom"        ,"A"   ,"A"    ,"\AA"       ,"\ensuremath{\AA}"             ,"A"            ,false)},
    { "b"     , Descriptor(b,   ,"barn"            ,"b"   ,"b"    ,"b"         ,"\ensuremath{\text{b}}"        ,"b"            ,false)},
    { "mb"    , Descriptor(mb   ,"millibarn"       ,"mb"  ,"mb"   ,"mb"        ,"\ensuremath{\text{mb}}"       ,"mb"           ,false)},
    { "ub"    , Descriptor(ub   ,"microbarn"       ,"ub"  ,"μb"   ,"\mu b"     ,"\ensuremath{\mu\text{b}}"     ,"#mub"         ,false)},
    { "nb"    , Descriptor(nb   ,"nanobarn"        ,"nb"  ,"nb"   ,"nb"        ,"\ensuremath{\text{nb}}"       ,"nb"           ,false)},
    { "pb"    , Descriptor(pb   ,"picobarn"        ,"pb"  ,"pb"   ,"pb"        ,"\ensuremath{\text{pb}}"       ,"pb"           ,false)},
    { "fb"    , Descriptor(fb   ,"femtobarn"       ,"fb"  ,"fb"   ,"fb"        ,"\ensuremath{\text{fb}}"       ,"fb"           ,false)},
    { "ib"    , Descriptor(1./b ,"ibarn"           ,"ib"  ,"b^-1" ,"b^{-1}"    ,"\ensuremath{\text{b}^{-1}}"   ,"b^{-1}"       ,false)},
    { "imb"   , Descriptor(1./mb,"imillibarn"      ,"imb" ,"mb^-1","mb^{-1}"   ,"\ensuremath{\text{mb}^{-1}}"  ,"mb^{-1}"      ,false)},
    { "iub"   , Descriptor(1./ub,"imicrobarn"      ,"iub" ,"μb^-1","\mu b^{-1}","\ensuremath{\mu\text{b}^{-1}}","#mub^{-1}"    ,false)},
    { "inb"   , Descriptor(1./nb,"inanobarn"       ,"inb" ,"nb^-1","nb^{-1}"   ,"\ensuremath{\text{nb}^{-1}}"  ,"nb^{-1}"      ,false)},
    { "ipb"   , Descriptor(1./pb,"ipicobarn"       ,"ipb" ,"pb^-1","pb^{-1}"   ,"\ensuremath{\text{pb}^{-1}}"  ,"pb^{-1}"      ,false)},
    { "ifb"   , Descriptor(1./fb,"ifemtobarn"      ,"ifb" ,"fb^-1","fb^{-1}"   ,"\ensuremath{\text{fb}^{-1}}"  ,"fb^{-1}"      ,false)},
    { "L"     , Descriptor(L    ,"liter"           ,"L"   ,"l"    ,"l"         ,"\ensuremath{\text{l}}"        ,"l"            ,false)},
    { "dL"    , Descriptor(dL   ,"deciliter"       ,"dL"  ,"dl"   ,"dl"        ,"\ensuremath{10^{-1}\text{l}}" ,"10^{#minus1}l",false)},
    { "cL"    , Descriptor(cL   ,"centiliter"      ,"cL"  ,"cl"   ,"cl"        ,"\ensuremath{10^{-2}\text{l}}" ,"10^{#minus2}l",false)},
    { "mL"    , Descriptor(mL   ,"milliliter"      ,"mL"  ,"ml"   ,"ml"        ,"\ensuremath{10^{-3}\text{l}}" ,"10^{#minus3}l",false)},
    { "rad"   , Descriptor(rad  ,"radian"          ,"rad" ,"rad"  ,"rad"       ,"\ensuremath{\text{rad}}"      ,"rad"          ,true )},
    { "mrad"  , Descriptor(mrad ,"mradian"         ,"mrad","mrad" ,"mrad"      ,"\ensuremath{\text{mrad}}"     ,"mrad"         ,false)},
    { "deg"   , Descriptor(deg  ,"degree"          ,"deg" ,"°"    ,"^{\circ}"  ,"\ensuremath{^{\circ}}"        ,"^{#circ}"     ,false)},
    { "sr"    , Descriptor(sr   ,"steradian"       ,"sr"  ,"sr"   ,"sr"        ,"\ensuremath{\text{sr}}"       ,"sr"           ,true )},
    { "ns"    , Descriptor(ns   ,"nanosecond"      ,"ns"  ,"ns"   ,"ns"        ,"\ensuremath{\text{ns}}"       ,"ns"           ,true )},
    { "s"     , Descriptor(s    ,"second"          ,"s"   ,"s"    ,"s"         ,"\ensuremath{\text{s}}"        ,"s"            ,false)},
    { "ms"    , Descriptor(ms   ,"millisecond"     ,"ms"  ,"ms"   ,"ms"        ,"\ensuremath{\text{ms}}"       ,"ms"           ,false)},
    { "us"    , Descriptor(us   ,"microsecond"     ,"us"  ,"μs"   ,"\mu s"     ,"\ensuremath{\mu\text{s}}"     ,"#mus"         ,false)},
    { "ps"    , Descriptor(ps   ,"picosecond"      ,"ps"  ,"ps"   ,"ps"        ,"\ensuremath{\text{ps}}"       ,"ps"           ,false)},
    { "fs"    , Descriptor(fs   ,"femtosecond"     ,"fs"  ,"fs"   ,"fs"        ,"\ensuremath{\text{fs}}"       ,"fs"           ,false)},
    { "min"   , Descriptor(min  ,"minute"          ,"min" ,"min"  ,"min"       ,"\ensuremath{\text{mn}}"       ,"mn"           ,false)},
    { "hr"    , Descriptor(hr   ,"hour"            ,"hr"  ,"hr"   ,"hrs"       ,"\ensuremath{\text{hrs}}"      ,"hrs"          ,false)}, 
    { "dy"    , Descriptor(dy   ,"day"             ,"dy"  ,"dy"   ,"days"      ,"\ensuremath{\text{days}}"     ,"days"         ,false)},
    { "wk"    , Descriptor(wk   ,"week"            ,"wk"  ,"wk"   ,"weeks"     ,"\ensuremath{\text{weeks}}"    ,"weeks"        ,false)},
    { "Hz"    , Descriptor(Hz   ,"hertz"           ,"Hz"  ,"Hz"   ,"Hz"        ,"\ensuremath{\text{Hz}}"       ,"Hz"           ,false)},      
    { "kHz"   , Descriptor(kHz  ,"kilohertz"       ,"kHz" ,"kHz"  ,"kHz"       ,"\ensuremath{\text{kHz}}"      ,"kHz"          ,false)},
    { "MHz"   , Descriptor(MHz  ,"megahertz"       ,"MHz" ,"MHz"  ,"MHz"       ,"\ensuremath{\text{MHz}}"      ,"MHz"          ,false)},
    { "GHz"   , Descriptor(GHz  ,"gigahertz"       ,"GHz" ,"GHz"  ,"GHz"       ,"\ensuremath{\text{GHz}}"      ,"GHz"          ,false)},
    { "eV"    , Descriptor(eV   ,"electronvolt"    ,"eV"  ,"eV"   ,"eV"        ,"\ensuremath{\text{eV}}"       ,"eV"           ,false)},      
    { "keV"   , Descriptor(keV  ,"kiloelectronvolt","keV" ,"keV"  ,"keV"       ,"\ensuremath{\text{keV}}"      ,"keV"          ,false)},  
    { "MeV"   , Descriptor(MeV  ,"megaelectronvolt","MeV" ,"MeV"  ,"MeV"       ,"\ensuremath{\text{MeV}}"      ,"MeV"          ,false)},  
    { "GeV"   , Descriptor(GeV  ,"gigaelectronvolt","GeV" ,"GeV"  ,"GeV"       ,"\ensuremath{\text{GeV}}"      ,"GeV"          ,false)},  
    { "TeV"   , Descriptor(TeV  ,"teraelectronvolt","TeV" ,"TeV"  ,"TeV"       ,"\ensuremath{\text{TeV}}"      ,"TeV"          ,false)},  
    { "PeV"   , Descriptor(PeV  ,"petaelectronvolt","PeV" ,"PeV"  ,"PeV"       ,"\ensuremath{\text{PeV}}"      ,"PeV"          ,false)},  
    { "mg"    , Descriptor(mg   ,"milligram"       ,"mg"  ,"mg"   ,"mg"        ,"\ensuremath{\text{mg}}"       ,"mg"           ,false)},
    { "g"     , Descriptor(g    ,"gram"            ,"g"   ,"g"    ,"g"         ,"\ensuremath{\text{g}}"        ,"g"            ,false)},
    { "kg"    , Descriptor(kg   ,"kilogram"        ,"kg"  ,"kg"   ,"kg"        ,"\ensuremath{\text{kg}}"       ,"kg"           ,false)}, 
    { "W"     , Descriptor(W    ,"watt"            ,"W"   ,"W"    ,"W"         ,"\ensuremath{\text{W}}"        ,"W"            ,false)},
    { "kW"    , Descriptor(kW   ,"kilowatt"        ,"kW"  ,"kW"   ,"kW"        ,"\ensuremath{\text{kW}}"       ,"kW"           ,false)},
    { "MW"    , Descriptor(MW   ,"megawatt"        ,"MW"  ,"MW"   ,"MW"        ,"\ensuremath{\text{MW}}"       ,"MW"           ,false)},
    { "GW"    , Descriptor(GW   ,"gigawatt"        ,"GW"  ,"GW"   ,"GW"        ,"\ensuremath{\text{GW}}"       ,"GW"           ,false)},
    { "A"     , Descriptor(A    ,"ampere"          ,"A"   ,"A"    ,"A"         ,"\ensuremath{\text{A}}"        ,"A"            ,false)},
    { "mA"    , Descriptor(mA   ,"milliampere"     ,"mA"  ,"mA"   ,"mA"        ,"\ensuremath{\text{mA}}"       ,"mA"           ,false)},
    { "uA"    , Descriptor(uA   ,"microampere"     ,"uA"  ,"μA"   ,"\mu A"     ,"\ensuremath{\mu\text{A}}"     ,"#muA"         ,false)},
    { "nA"    , Descriptor(nA   ,"nanoampere"      ,"nA"  ,"nA"   ,"nA"        ,"\ensuremath{\text{nA}}"       ,"nA"           ,false)},
    { "J"     , Descriptor(J    ,"joule"           ,"J"   ,"J"    ,"J"         ,"\ensuremath{\text{J}} "       ,"J"            ,false)},
    { "Wh"    , Descriptor(Wh   ,"watthour"        ,"Wh"  ,"Wh"   ,"Wh"        ,"\ensuremath{\text{Wh}}"       ,"Wh"           ,false)},
    { "kWh"   , Descriptor(kWh  ,"kilowatthour"    ,"kWh" ,"kWh"  ,"kWh"       ,"\ensuremath{\text{kWh}}"      ,"kWh"          ,false)},
    { "MWh"   , Descriptor(MWh  ,"megawatthour"    ,"MWh" ,"MWh"  ,"MWh"       ,"\ensuremath{\text{MWh}}"      ,"MWh"          ,false)},
    { "GWh"   , Descriptor(GWh  ,"gigawatthour"    ,"GWh" ,"GWh"  ,"GWh"       ,"\ensuremath{\text{GWh}}"      ,"GWh"          ,false)},
    { "e"     , Descriptor(e    ,"eplus"           ,"e"   ,"e"    ,"e"         ,"\ensuremath{q_{\text{e}}}"    ,"q_{e}"        ,true )},     
    { "C"     , Descriptor(C    ,"coulomb"         ,"C"   ,"C"    ,"C"         ,"\ensuremath{\text{C}}"        ,"C"            ,false)},
    { "mC"    , Descriptor(mC   ,"millicoulomb"    ,"mC"  ,"mC"   ,"mC"        ,"\ensuremath{\text{mC}}"       ,"mC"           ,false)},
    { "uC"    , Descriptor(uC   ,"microcoulomb"    ,"uC"  ,"μC"   ,"\mu C"     ,"\ensuremath{\mu\text{C}}"     ,"#muC"         ,false)},
    { "nC"    , Descriptor(nC   ,"nanocoulomb"     ,"nC"  ,"nC"   ,"nC"        ,"\ensuremath{\text{nC}}"       ,"nC"           ,false)},
    { "pC"    , Descriptor(pC   ,"picocoulomb"     ,"pC"  ,"pC"   ,"pC"        ,"\ensuremath{\text{pC}}"       ,"pC"           ,false)},
    { "fC"    , Descriptor(fC   ,"femtocoulomb"    ,"fC"  ,"fC"   ,"fC"        ,"\ensuremath{\text{fC}}"       ,"fC"           ,false)},
    { "N"     , Descriptor(N    ,"newton"          ,"N"   ,"N"    ,"N"         ,"\ensuremath{\text{N}}"        ,"N"            ,false)},               
    { "kN"    , Descriptor(kN   ,"kilonewton"      ,"kN"  ,"kN"   ,"kN"        ,"\ensuremath{\text{kN}}"       ,"kN"           ,false)},   
    { "V"     , Descriptor(V    ,"volt"            ,"V"   ,"V"    ,"V"         ,"\ensuremath{\text{V}}"        ,"V"            ,false)},     
    { "kV"    , Descriptor(kV   ,"kilovolt"        ,"kV"  ,"kV"   ,"kV"        ,"\ensuremath{\text{kV}}"       ,"kV"           ,false)},    
    { "MV"    , Descriptor(MV   ,"megavolt"        ,"MV"  ,"MV"   ,"MV"        ,"\ensuremath{\text{MV}}"       ,"MV"           ,false)},
    { "O"     , Descriptor(O    ,"ohm"             ,"O"   ,"Ω"    ,"\Omega"    ,"\ensuremath{\Omega}"          ,"#Omega"       ,false)},     
    { "kO"    , Descriptor(kO   ,"kiloohm"         ,"kO"  ,"kΩ"   ,"k\Omega"   ,"\ensuremath{\text{k}\Omega}"  ,"k#Omega"      ,false)}, 
    { "MO"    , Descriptor(MO   ,"megaohm"         ,"MO"  ,"MΩ"   ,"M\Omega"   ,"\ensuremath{\text{M}\Omega}"  ,"M#Omega"      ,false)}, 
    { "F"     , Descriptor(F    ,"farad"           ,"F"   ,"F"    ,"F"         ,"\ensuremath{\text{F}}"        ,"F"            ,false)}, 
    { "mF"    , Descriptor(mF   ,"millifarad"      ,"mF"  ,"mF"   ,"mF"        ,"\ensuremath{\text{mF}}"       ,"mF"           ,false)},
    { "uF"    , Descriptor(uF   ,"microfarad"      ,"uF"  ,"μF"   ,"\mu F"     ,"\ensuremath{\mu\text{F}}"     ,"#muF"         ,false)},
    { "nF"    , Descriptor(nF   ,"nanofarad"       ,"nF"  ,"nF"   ,"nF"        ,"\ensuremath{\text{nF}}"       ,"nF"           ,false)},
    { "pF"    , Descriptor(pF   ,"picofarad"       ,"pF"  ,"pF"   ,"pF"        ,"\ensuremath{\text{pF}}"       ,"pF"           ,false)},
    { "Wb"    , Descriptor(Wb   ,"weber"           ,"Wb"  ,"Wb"   ,"Wb"        ,"\ensuremath{\text{Wb}}"       ,"Wb"           ,false)},    
    { "T"     , Descriptor(T    ,"tesla"           ,"T"   ,"T"    ,"T"         ,"\ensuremath{\text{T}}"        ,"T"            ,false)},
    { "Gs"    , Descriptor(Gs   ,"gauss"           ,"Gs"  ,"Gs"   ,"Gs"        ,"\ensuremath{\text{Gs}}"       ,"Gs"           ,false)},
    { "kGs"   , Descriptor(kGs  ,"kilogauss"       ,"kGs" ,"kGs"  ,"kGs"       ,"\ensuremath{\text{kGs}}"      ,"kGs"          ,false)},
    { "H"     , Descriptor(H    ,"henry"           ,"H"   ,"H"    ,"H"         ,"\ensuremath{\text{H}}"        ,"H"            ,false)},
    { "mole"  , Descriptor(mole ,"mole"            ,"mole","mole" ,"mole"      ,"\ensuremath{\text{mole}}"     ,"mole"         ,true )},														       
    { "degK"  , Descriptor(k    ,"kelvin"          ,"K"   ,"°K"   ,"^{\circ}K" ,"\ensuremath{^{\circ}\text{K}}","^{#circ}K"    ,true )},
    { "degC"  , Descriptor(-1.0 ,"celsius"         ,"C"   ,"°C"   ,"^{\circ}C" ,"\ensuremath{^{\circ}\text{C}}","^{#circ}C"    ,false)},
    { "degF"  , Descriptor(-1.0 ,"fahrenheit"      ,"F"   ,"°F"   ,"^{\circ}F" ,"\ensuremath{^{\circ}\text{F}}","^{#circ}F"    ,false)},
    { "Bq"    , Descriptor(Bq   ,"becquerel"       ,"Bq"  ,"Bq"   ,"Bq"        ,"\ensuremath{\text{Bq}}"       ,"Bq"           ,false)},    
    { "kBq"   , Descriptor(kBq  ,"kilobecquerel"   ,"kBq" ,"kBq"  ,"kBq"       ,"\ensuremath{\text{kBq}}"      ,"kBq"          ,false)},
    { "MBq"   , Descriptor(MBq  ,"megabecquerel"   ,"MBq" ,"MBq"  ,"MBq"       ,"\ensuremath{\text{MBq}}"      ,"MBq"          ,false)},
    { "GBq"   , Descriptor(GBq  ,"gigabecquerel"   ,"GBq" ,"GBq"  ,"GBq"       ,"\ensuremath{\text{GBq}}"      ,"GBq"          ,false)},
    { "Ci"    , Descriptor(Ci   ,"curie"           ,"Ci"  ,"Ci"   ,"Ci"        ,"\ensuremath{\text{Ci}}"       ,"Ci"           ,false)},
    { "mCi"   , Descriptor(mCi  ,"millicurie"      ,"mCi" ,"mCi"  ,"mCi"       ,"\ensuremath{\text{mCi}}"      ,"mCi"          ,false)},
    { "uCi"   , Descriptor(uCi  ,"microcurie"      ,"uCi" ,"μCi"  ,"\mu Ci"    ,"\ensuremath{\mu\text{Ci}}"    ,"#muCi"        ,false)},
    { "uGy"   , Descriptor(uGy  ,"microgray"       ,"uGy" ,"μGy"  ,"\mu Gy"    ,"\ensuremath{\mu\text{Gy}}"    ,"#muGy"        ,false)}, 
    { "mGy"   , Descriptor(mGy  ,"milligray"       ,"mGy" ,"mGy"  ,"mGy"       ,"\ensuremath{\text{mGy}}"      ,"mGy"          ,false)},
    { "Gy"    , Descriptor(Gy   ,"gray"            ,"Gy"  ,"Gy"   ,"Gy"        ,"\ensuremath{\text{Gy}}"       ,"Gy"           ,false)},
    { "kGy"   , Descriptor(kGy  ,"kilogray"        ,"kGy" ,"kGy"  ,"kGy"       ,"\ensuremath{\text{kGy}}"      ,"kGy"          ,false)},
    { "Pa"    , Descriptor(Pa   ,"pascal"          ,"Pa"  ,"Pa"   ,"Pa"        ,"\ensuremath{\text{Pa}}"       ,"Pa"           ,false)},   
    { "hPa"   , Descriptor(hPa  ,"hectoPascal"     ,"hPa" ,"hPa"  ,"hPa"       ,"\ensuremath{\text{hPa}}"      ,"hPa"          ,false)},
    { "kPa"   , Descriptor(kPa  ,"kilopascal"      ,"kPa" ,"kPa"  ,"kPa"       ,"\ensuremath{\text{kPa}}"      ,"kPa"          ,false)},
    { "mbar"  , Descriptor(mbar ,"millibar"        ,"mbar","mbar" ,"mbar"      ,"\ensuremath{\text{mbar}}"     ,"mbar"         ,false)},
    { "atm"   , Descriptor(atm  ,"atmosphere"      ,"atm" ,"atm"  ,"atm"       ,"\ensuremath{\text{atm}}"      ,"atm"          ,false)},
    { "cd"    , Descriptor(cd   ,"candela"         ,"cd"  ,"cd"   ,"cd"        ,"\ensuremath{\text{cd}}"       ,"cd"           ,true )},
    { "lm"    , Descriptor(lm   ,"lumen"           ,"lm"  ,"lm"   ,"lm"        ,"\ensuremath{\text{lm}}"       ,"lm"           ,false)},
    { "lx"    , Descriptor(lx   ,"lux"             ,"lx"  ,"lx"   ,"lx"        ,"\ensuremath{\text{lx}}"       ,"lx"           ,false)},
    { "UNK"   , Descriptor(0.   ,"unit not known"  ,"UNK" ,""     ,""          ,""                             ,""             ,false)}
  };
  ///@brief Access functions
  static const Descriptor&  description(const std::string& key) { auto fmap = Descriptions.find(key); return fmap != Descriptions.end() ? fmap->second : Descriptions.at("UNK"); } 
  static const std::string& name       (const std::string& key) { return description(key).name;  }
  static double             scale      (const std::string& key) { return description(key).scale; }
  static const std::string& axis       (const std::string& key,Types::Display::Mode m=Type::Display::Mode::Standard) {
    const auto& udescr = description(key);
    switch ( m ) {
    case Types::Display::Mode::Latex:
      return udescr.latex_axis;
    case Types::Display::Mode::ROOT:
      return udescr.root_axis;
    case Types::Display::Mode::Standard:
      return udescr.axis;
    default:
      return udescr.axis;
    }
  }
} // namespace Units
#endif
