// -*- c++ -*-
#ifndef HISTCONFIG_H
#define HISTCONFIG_H

#include "Types.h"
#include "Math.h"
#include "Units.h"

#include "HistAxis.h"
#include "HistBins.h"
#include "HistUtils.h"

#include <TH1.h>
#include <TDirectory.h>

#include <vector>
#include <string>
#include <array>
#include <tuple>

#include <limits>
#include <memory>

#include <cmath>

///@brief Histogram configuration, management and handling 
namespace Hist {
  //@name Static const references
  ///@{
  static const Bins::Descriptor invalidBinDescriptor      = Hist::Bins::Descriptor()             ; ///< Invalid bin descriptor reference
  static const Bins::Delimiters invalidDelimiters         = Hist::Bins::Delimiters()             ; ///< Invalid bin delimiters reference
  static const Axis::Descriptor invalidAxisDescriptor     = Hist::Axis::invalidDescriptor;       ; ///< Invalid axis descriptor
  static const double           invalidLowerBoundary      = std::numeric_limits<double>::lowest(); ///< Invalid lower boundary reference
  static const double           invalidUpperBoundary      = std::numeric_limits<double>::max()   ; ///< Invalid upper boundary reference
  static const int              invalidNumberBins         = Hist::Bins::Binning().nbins()        ; ///< Invalid number of bins reference
  static const std::string      unknownRegistryDirectory  = "REG_UNK_DIR"                        ; ///< Unknown directory name
  static const std::string      unknownRegistryKey        = "REG_UNK_KEY"                        ; ///< Unknown key for registry
  static const std::string      unknownTitle              = "REG_UNK_TTL"                        ; ///< Unknown title
  static const std::string      emptyString               = "";                                  ; ///< Empty string
  static const std::string      defaultDirectoryName      = "NO_DIR";                            ; ///< Default directory name
  ///@}
  ///@brief Histogram manager types
  namespace Manager {
    typedef std::tuple<std::string,std::string> key_t;        ///< Key type for lookup (pair of directory name + histogram name)
    typedef TH1                                 base_t;       ///< Histogram base type
    typedef base_t*                             load_t;       ///< Registry load type    
    typedef std::map<key_t,load_t>              store_t;      ///< Store type
    typedef store_t::value_type                 entry_t;      ///< Store entry type
    ///@brief Histogram registry
    class Registry : public store_t {
    public:
      static Registry* instance();                          ///< Retrieve single instance object pointer
      ///@brief Add a histogram
      ///@param hptr pointer to histogram with base type @c ROOT::TH1
      bool add(base_t* hptr);
      ///@brief Add a histogram in a named directory
      ///@param dname directory name
      ///@param hptr  pointer to histogram with base type @c ROOT::TH1
      bool add(const std::string& dname,base_t* hptr);
      ///@copydoc add(Registry::base_t* hptr) 
      /// Synonym.
      bool registerHist(base_t* hptr);     
      ///@copydoc add(const Registry::directory_t& dname,Registry::base_t* hptr)
      /// Synonym.                    
      bool registerHist(const std::string& dname,base_t* hptr);
      ///@brief Write histograms
      ///@param noEmpties do not write empty histograms in @c true (default)
      int write(bool noEmpties=true);
      ///@brief Number of registered histograms
      size_t size() const;
      ///@brief Retrieve histogram
      ///@param hname histogram name 
      base_t* retrieve(const std::string& hname);
      ///@brief Retrieve a histogram in a named directory
      ///@param dname directory name
      ///@param hname histogram name
      base_t* retrieve(const std::string& dname,const std::string& hname);
      ///@brief Retrieve a histogram from a given key
      ///@param key  registry key
      base_t* retrieve(const key_t& key);
      ///@brief Print database
      ///void print() const;
      ///@brief Default directory
      const std::string& defaultDir() const;
      ///@brief Key formatting
      ///@param hname histogram name
      ///@return Key in registry
      key_t makeKey(const std::string& hname) const;
      ///@brief Key formatting with named directory
      ///@copydetails makeKey(const std::string& hname) 
      key_t makeKey(const std::string& dname,const std::string& hname) const;
      ///@brief Extract histogram name from key
      ///@param key registry key
      ///@return Histogram name
      const std::string& histName(const key_t& key) const;
      ///@brief Extract directory name from key
      ///@param key registry key
      ///@return Directory name
      const std::string& directoryName(const key_t& key) const; 
      ///@brief check if directory should be created in output file
      bool makeDir(const std::string& dname) const;
    private:     
      ///@brief Pointer to only instance
      static Registry* m_instance;
      ///@brief Private constructor
      Registry();
      ///@brief Default directory name
      static std::string m_defdir;
      ///@brief 
    };
    /// @name Interactions with the manager
    ///@{
    Registry* registry();                                                             ///< Get registry instance
    template<class H> bool registerHist(H* hptr);                                     ///< Register a histogram with registry (no directory) 
    template<class H> bool registerHist(const std::string& dname,H* hptr);            ///< Register a histogram with registry (with directory)
    template<class H> H* retrieve(const std::string& hname);                          ///< Retrieve a histogram from registry (no directory) 
    template<class H> H* retrieve(const std::string& dname,const std::string& hname); ///< Retrieve a histogram from registry (with directory)
    int write(bool noEmpties=true);                                                   ///< Write all registered histograms (@c noEmpties=false ) or only those with entries (@c noEmpties=true , default)
    int write(const std::string& fname,bool noEmpties=true);                          ///< Open file and write  all registered histograms (@c noEmpties=false ) or only those with entries (@c noEmpties=true , default)
    const key_t&         key          (const entry_t& entry);                         ///< Get the key from an entry in the registry
    const std::string&   directoryName(const entry_t& entry);                         ///< Get the directory name from an entry in the registry
    const std::string&   directoryName(const key_t&   key  );                         ///< Get the directory name from the key
    const std::string&   histName     (const entry_t& entry);                         ///< Get the histogram name from an entry in the registry
    const std::string&   histName     (const key_t&   key  );                         ///< Get the histogram name from the key
    base_t*              histBasePtr  (entry_t& entry);                               ///< Get the histogram base class pointer from an entry in the registry
    base_t*              histBasePtr  (const key_t& key);                             ///< Get the histogram base class pointer for a registry key
    template<class H> H* histPtr      (entry_t& entry);                               ///< Get the histogram pointer from an entry in the registry
    template<class H> H* histPtr      (const key_t& key);                             ///< Get the histogram pointer for a registry key               
    ///@}
  } // Hist::Manager
  ///@name Utilities
  ///@tparam H       histogram type derived from base @c ROOT::TH1
  ///@param  hptr    pointer to histogram object
  ///@param  axt     x-axis title (no default or default @c emptyString )
  ///@param  ayt     y-axis title (default @c emptyString )
  ///@param  azt     z-axis title (default @c emptyString )
  ///@param  dname   name of directory
  ///@{
  /// Add axis titles to histogram  
  template<class H> bool addAxisTitle(H* hptr,const std::string& axt,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Configure histogram - add axis titles, set statistic options and register with @c @Hist::Manager::Registry
  template<class H> bool configure   (H* hptr,const std::string& axt,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Configure histogram: add axis titles, set statistic options and register with @c @Hist::Manager
  template<class H> bool configure   (const std::string& dname,H* hptr,const std::string& axt,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  ///@}
  ///@name Useful types for descriptors
  ///@{
  typedef Axis::Coord                                 AxisCoordinate;      ///< Axis coordinate
  typedef Types::Variable::Id                         AxisVariable;        ///< Identifier of variable associated with axis
  typedef std::tuple<AxisCoordinate,Axis::Descriptor> AxisDescriptor;      ///< Axis descriptor
  typedef std::map<AxisVariable,AxisDescriptor>       AxisVariableLinks;   ///< Lookup of axis description by variable id
  typedef std::map<AxisCoordinate,AxisVariable>       AxisCoordinateLinks; ///< Lookup of variable by coordinate
  ///@}        
  ///@brief Histogram descriptor
  class Descriptor {
  private:
    std::string         m_name       = { emptyString };       ///< Histogram name
    std::string         m_title      = { emptyString };       ///< Histogram title
    AxisVariableLinks   m_axis       = AxisVariableLinks();   ///< Axis descriptions
    AxisCoordinateLinks m_coord      = AxisCoordinateLinks(); ///< Axis variables by cordinates
    size_t              m_dimension  = invalidNumberBins;     ///< Histogram dimensions
    bool                m_isProfile  = { false };             ///< Histogram descriptor defines a profile if @c true 
  public:
    ///@brief Default constructor instantiates invalid descriptor object 
    Descriptor();
    ///@brief Loaded constructor from variable-to-axis descriptor link table
    ///@param name      histogram name
    ///@param title     histogram title
    ///@param avlinks   map storing link between variable id and axis descriptor 
    Descriptor(const std::string& name,const std::string& title,const AxisVariableLinks& avlinks,bool isProfile=false);
    ///@brief Loaded constructor from list of variables and descriptors
    ///@param name      histogram name
    ///@param title     histogram title
    ///@param avars     vector of variable ids
    ///@param acoords   vector of axis coordinate ids
    ///@param adescrs   vector of axis descriptors
    ///@warning The vectors @c avars , @c accords and @c adescr are expected to be index-parallel.
    Descriptor(const std::string& name,const std::string& title,const std::vector<AxisVariable>& avars,const std::vector<AxisCoordinate>& acoords,const std::vector<Axis::Descriptor>& adescrs,bool isProfile=false);
    ///@brief Copy constructor
    Descriptor(const Descriptor& hdescr);
    ///@brief Assignment operator
    Descriptor& operator=(const Descriptor& hdescr);
    ///@name Accessors
    ///@{
    const std::string&      name()                                        const;  ///< Histogram name
    const std::string&      title()                                       const;  ///< Histogram title
    const Axis::Descriptor& axisDescr(AxisCoordinate c=AxisCoordinate::X) const;  ///< Axis descriptor for given coordinate
    const Axis::Descriptor& axisDescr(AxisVariable vid)                   const;  ///< Axis descriptor for given variable
    int                     dimension()                                   const;  ///< Dimension of histogram
    bool                    isProfile()                                   const;  ///< Histogram is a profile
    Axis::Coord             coordinate(AxisVariable vid)                  const;  ///< Coordinate for given variable
    AxisVariable            variable(AxisCoordinate c=AxisCoordinate::X)  const;  ///< Variable for given coordinate
    int                     nbins(AxisCoordinate c=AxisCoordinate::X)     const;  ///< Number of bins of axis for given coordinate 
    int                     nbins(AxisVariable vid)                       const;  ///< Number of bins of axis for given variable  
    double                  xmin(AxisCoordinate c=AxisCoordinate::X)      const;  ///< Lower boundary of value range of axis for given coordinate
    double                  xmin(AxisVariable vid)                        const;  ///< Lower boundary of value range of axis for given variable  
    double                  xmax(AxisCoordinate c=AxisCoordinate::X)      const;  ///< Upper boundary of value range of axis for given coordinate
    double                  xmax(AxisVariable vid)                        const;  ///< Upper boundary of value range of axis for given variable  
    const Bins::Delimiters& bins(AxisCoordinate c=AxisCoordinate::X)      const;  ///< Binning of axis for given coordinate
    const Bins::Delimiters& bins(AxisVariable vid)                        const;  ///< Binning of axis for given variable  
    bool                    logValue(AxisCoordinate c=AxisCoordinate::X)  const;  ///< Axis for given coordinate has @f \log_{10}(x) @f values if @c true
    bool                    logValue(AxisVariable vd)                     const;  ///< Axis for given variable has @f \log_{10}(x) @f values if @c true  
    bool                    logEqui(AxisCoordinate c=AxisCoordinate::X)   const;  ///< Axis for given coordinate has equidistant bins in @f \log_{10}(x) @f if @c true
    bool                    logEqui(AxisVariable vd)                      const;  ///< Axis for given variable has equidistant bins in @f \log_{10}(x) @f if @c true  
    const std::string&      axisTitle(AxisCoordinate c=AxisCoordinate::X) const;  ///< Title of axis for given coordinate
    const std::string&      axisTitle(AxisVariable vid)                   const;  ///< Title of axis for given variable  
    ///@}
    ///@brief Booking support
    /// Books a histogram according to the cached axis descriptors. Typically, a @c TH1D typed object is returned for dimension 1, a @c TH2D typed object is
    /// returned for dimension 2, and a @c TH3 typed object is returned for dimension 3. 
    ///@note The number of internally cached axis descriptors is expected to be equal or greater then the dimension @c N. The @c N axis descriptors are considered 
    ///      for the value axes of the the histogram, while only the title is used for a possible descriptor at @c N+1 .  
    ///@tparam H histogram implementation class
    ///@tparam N dimension to be booked
    ///@return The booked histogram is returned by its base class pointer. If the cached axisd descriptors are invalid, a @c nullptr is returned.  
    ///    template<class H,int N> H* book(); 
  private:
    AxisVariable f_var(AxisCoordinate c);                           ///< Map coordinate to variable, add mapping if not yet existing
    AxisVariable f_var(AxisCoordinate c) const;                     ///< Map coordinate to variable, but do not add if not yet existing
    void         f_setDimension();                                  ///< Set dimensions
    void         f_messageInit(const std::string& module="") const; ///< Initial message
  };
  ///@name Booking histograms
  ///@tparam H       histogram type
  ///@param  hname   histogram name
  ///@param  htitle  histogram title
  ///@param  dname   directory name
  ///@param  nxbins  number of x-axis bins
  ///@param  xmin    lower boundary x-axis value range
  ///@param  xmax    upper boundary x-axis value range
  ///@param  xbins   vector of bin limits on x-axis
  ///@param  nybins  number of y-axis bins 
  ///@param  ymin    lower boundary y-axis value range
  ///@param  ymax    upper boundary y-axis value range
  ///@param  ybins   vector of bin limits on y-axis
  ///@param  nzbins  number of z-axis bins 
  ///@param  zmin    lower boundary z-axis value range
  ///@param  zmax    upper boundary z-axis value range
  ///@param  zbins   vector of bin limits on z-axis
  ///@param  axt     x-axis title
  ///@param  ayt     y-axis title
  ///@param  azt     z-axis title
  ///@{
  /// Book 1-dim histogram with equidistant bins
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::string& axt=emptyString,const std::string& ayt=emptyString);
  /// Book 1-dim histogram with non-equidistant bins
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString);
  // /// Book 1-dim histogram using a @c Bins::Binning object
  // template<class H> H* book(const std::string& hname,const std::string& htitle,const Bins::Binning& xbinning     ,const std::string& axt=emptyString,const std::string& ayt=emptyString);
  /// Book any histogram using a @c Hist::Descriptor object
  template<class H> H* book(const Descriptor& hdescr);
  /// Book any histogram using a @c Hist::Descriptor object in a directory object
  template<class H> H* book(TDirectory* pdir,const Descriptor& hdescr);
  /// Book any histogram using a @c Hist::Descriptor object in a named directory
  template<class H> H* book(const std::string& dname,const Descriptor& hdescr);
  /// Book 1-dim histogram with equidistant bins in a directory                           
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::string& axt=emptyString,const std::string& ayt=emptyString); 
  /// Book 1-dim histogram with non-equidistant bins in a directory 
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString); 
  /// Book 2-dim histogram with equidistant bins on both axes
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with non-equidistant bins on x-axis and equidistant bins on y-axis
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,int nybins,double ymin,double ymax,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with non-equidistant bins on x-axis and non-equidistant bins on y-axis
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,const std::vector<double>& ybins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with equidistant bins on x-axis and non-equidistant bins on y-axis
  template<class H> 
  H* book(const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::vector<double>& ybins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with equidistant bins on both axes in a directory
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with non-equidistant bins on x-axis and equidistant bins on y-axis in a directory
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,int nybins,double ymin,double ymax,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with non-equidistant bins on x-axis and non-equidistant bins on y-axis in a directory
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,const std::vector<double>& ybins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  /// Book 2-dim histogram with equidistant bins on x-axis and non-equidistant bins on y-axis in a directory
  template<class H> 
  H* book(const std::string& dname,const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::vector<double>& ybins  ,const std::string& axt=emptyString,const std::string& ayt=emptyString,const std::string& azt=emptyString);
  ///@}
} // Hist
									       
// --------//
// Manager //
// ------- //

inline Hist::Manager::Registry::Registry() : Hist::Manager::store_t() { }
inline Hist::Manager::Registry* Hist::Manager::Registry::instance()                                                        { if ( m_instance == nullptr ) { m_instance = new Registry(); } return m_instance; }
inline size_t                   Hist::Manager::Registry::size()                                                      const { return size();                 }
inline bool                     Hist::Manager::Registry::add(base_t* hptr)                                                 { return add(defaultDir(),hptr); }
inline bool                     Hist::Manager::Registry::registerHist(base_t* hptr)                                        { return add(hptr);              }
inline bool                     Hist::Manager::Registry::registerHist(const std::string& dname,base_t* hptr)               { return add(dname,hptr);        }
inline Hist::Manager::base_t*   Hist::Manager::Registry::retrieve(const std::string& hname)                                { return retrieve(defaultDir(),hname); }
inline Hist::Manager::base_t*   Hist::Manager::Registry::retrieve(const std::string& dname,const std::string& hname)       { auto key(makeKey(dname,hname)); auto fhptr(find(key)); return fhptr != end() ? fhptr->second : nullptr; }
inline Hist::Manager::key_t     Hist::Manager::Registry::makeKey(const std::string& dname,const std::string& hname)  const { return { dname, hname }; }
inline Hist::Manager::key_t     Hist::Manager::Registry::makeKey(const std::string& hname)                           const { return { defaultDir(), hname }; }
inline const std::string&       Hist::Manager::Registry::histName(const key_t& key)                                  const { return std::get<1>(key); }
inline const std::string&       Hist::Manager::Registry::directoryName(const key_t& key)                             const { return std::get<0>(key); }
inline const std::string&       Hist::Manager::Registry::defaultDir()                                                const { return m_defdir; }
inline bool                     Hist::Manager::Registry::makeDir(const std::string& dname)                           const { return dname != defaultDir() && dname != Hist::emptyString; } 

// --------- //
// Functions //
// --------- //

inline Hist::Manager::Registry*    Hist::Manager::registry     ()                                    { return Hist::Manager::Registry::instance(); }
inline const Hist::Manager::key_t& Hist::Manager::key          (const Hist::Manager::entry_t& entry) { return entry.first; }
inline const std::string&          Hist::Manager::directoryName(const Hist::Manager::entry_t& entry) { return Hist::Manager::directoryName(entry.first); }       
inline const std::string&          Hist::Manager::directoryName(const Hist::Manager::key_t& key)     { return Hist::Manager::registry()->directoryName(key); }                       
inline const std::string&          Hist::Manager::histName     (const Hist::Manager::entry_t& entry) { return Hist::Manager::histName(entry.first); }       
inline const std::string&          Hist::Manager::histName     (const Hist::Manager::key_t& key)     { return Hist::Manager::registry()->histName(key); }
inline Hist::Manager::base_t*      Hist::Manager::histBasePtr  (Hist::Manager::entry_t& entry)       { return std::get<1>(entry); }
inline Hist::Manager::base_t*      Hist::Manager::histBasePtr  (const Hist::Manager::key_t& key)     { return registry()->retrieve(key); }               

// ---------- //
// Descriptor //
// ---------- //

inline const std::string&            Hist::Descriptor::name()                       const { return m_name     ; }
inline const std::string&            Hist::Descriptor::title()                      const { return m_title    ; }
inline int                           Hist::Descriptor::dimension()                  const { return m_dimension; }
inline bool                          Hist::Descriptor::isProfile()                  const { return m_isProfile; }

inline const Hist::Axis::Descriptor& Hist::Descriptor::axisDescr(AxisVariable vid)  const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? std::get<1>(fdescr->second)                   : invalidAxisDescriptor  ; }
inline Hist::AxisCoordinate          Hist::Descriptor::coordinate(AxisVariable vid) const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? std::get<0>(fdescr->second)                   : AxisCoordinate::Unknown; }
inline int                           Hist::Descriptor::nbins(AxisVariable vid)      const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::nbins(std::get<1>(fdescr->second))      : invalidNumberBins      ; }
inline double                        Hist::Descriptor::xmin(AxisVariable vid)       const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::xmin(std::get<1>(fdescr->second))       : invalidLowerBoundary   ; }
inline double                        Hist::Descriptor::xmax(AxisVariable vid)       const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::xmax(std::get<1>(fdescr->second))       : invalidUpperBoundary   ; }
inline const Hist::Bins::Delimiters& Hist::Descriptor::bins(AxisVariable vid)       const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::delimiters(std::get<1>(fdescr->second)) : invalidDelimiters      ; }
inline bool                          Hist::Descriptor::logValue(AxisVariable vid)   const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::logValue(std::get<1>(fdescr->second))   : false                  ; }
inline bool                          Hist::Descriptor::logEqui(AxisVariable vid)    const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::logEqui(std::get<1>(fdescr->second))    : false                  ; }
inline const std::string&            Hist::Descriptor::axisTitle(AxisVariable vid)  const { auto fdescr(m_axis.find(vid)); return fdescr != m_axis.end() ? Axis::title(std::get<1>(fdescr->second))      : unknownTitle           ; }

inline const Hist::Axis::Descriptor& Hist::Descriptor::axisDescr(AxisCoordinate c) const { return axisDescr(f_var(c)); }
inline Hist::AxisVariable            Hist::Descriptor::variable(AxisCoordinate c)  const { return f_var(c)           ; }
inline int                           Hist::Descriptor::nbins(AxisCoordinate c)     const { return nbins(f_var(c))    ; }
inline double                        Hist::Descriptor::xmin(AxisCoordinate c)      const { return xmin(f_var(c))     ; }
inline double                        Hist::Descriptor::xmax(AxisCoordinate c)      const { return xmax(f_var(c))     ; }
inline const Hist::Bins::Delimiters& Hist::Descriptor::bins(AxisCoordinate c)      const { return bins(f_var(c))     ; }
inline bool                          Hist::Descriptor::logValue(AxisCoordinate c)  const { return logValue(f_var(c)) ; }
inline bool                          Hist::Descriptor::logEqui(AxisCoordinate c)   const { return logEqui(f_var(c))  ; }
inline const std::string&            Hist::Descriptor::axisTitle(AxisCoordinate c) const { return axisTitle(f_var(c)); }

#include "HistConfig.icc"
#endif 
