
#include "HistSelector.h"

Hist::Interface::Selector::Selector() : Base::Config() { }
Hist::Interface::Selector::Selector(const std::string& name,bool isActive) : Base::Config(name,isActive) { }

bool Hist::Select::greater(double value,double threshold)        { return value > threshold; }
bool Hist::Select::greater(int    value,int    threshold)        { return value > threshold; }
bool Hist::Select::greater(float  value,float  threshold)        { return value > threshold; }
bool Hist::Select::smaller(double value,double threshold)        { return value < threshold; }
bool Hist::Select::smaller(int    value,int    threshold)        { return value < threshold; }
bool Hist::Select::smaller(float  value,float  threshold)        { return value < threshold; }
bool Hist::Select::inRange(double value,double rmin,double rmax) { return value > rmin && value < rmax; }
bool Hist::Select::inRange(int    value,int    rmin,int    rmax) { return value > rmin && value < rmax; }
bool Hist::Select::inRange(float  value,float  rmin,float  rmax) { return value > rmin && value < rmax; }
