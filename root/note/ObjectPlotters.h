// -*- c++ -*-
#ifndef OBJECTPLOTTERS_H
#define OBJECTPLOTTERS_H

#include "Types.h"
#include "Base.h"
#include "HistPlotter.h"

#include <string>
#include <map>

namespace Object {
  namespace Plotter {
    using VariableId  = Types::Variable::Id;
    using HistPlotter = Hist::Plotter::Base;
    using PlotMap     = std::map<VariableId,HistPlotter>;
    namespace Interface {
      class Base {
      public:
	virtual ~Base() { }
	virtual bool plot(double weight) = 0; 
	virtual bool configure(VariableId vid,const HistPlotter& hplotter) = 0;
      };
    } // Object::Plotter::Interface
    class Base : public ::Base::Config, virtual public Interface::Base {
    public:
      typedef ::Base::Config base_t;
      Base(const std::string& name);
      virtual ~Base();
      virtual bool plot(double weight=1.0);
      virtual bool configure(VariableId vid,const HistPlotter& hplotter);
    protected:
      PlotMap m_plotMap;
    }; 
  } // Object::Plotter
} // Object

#endif
