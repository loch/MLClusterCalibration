// -*- c++ -*-

#define HIST_MSG_MAP( MAP )			\
  static const std::map<std::tuple<bool,bool>,std::string> MAP = {	\
    { { false, false }, "at least one bin and bins are ordered"             }, \
    { { true , false }, "at least one bin but bins are not ordered"         }, \
    { { true , true  }, "not enough bin limits and bins are not ordered"    }, \
    { { false, true  }, "not enough bins but binning is defined as ordered" } \
  }

#define HIST_CHK_BINS_1D( BINS, MAP )	\
  HIST_MSG_MAP( MAP );							\
  bool notOrdered(!Bins::isOrdered( BINS )); bool tooFewBins( BINS.size() < 2 ); \
  if ( notOrdered || tooFewBins ) { printf("[Hist::book(...)] WARN cannot book histogram due to \042%s\042\n", MAP.at({notOrdered,tooFewBins}).c_str()); return nullptr; }

#define HIST_CHK_BINS_2D( XBINS, YBINS, MAP )			       \
  HIST_MSG_MAP( MAP );						       \
  bool notOrdered(!Bins::isOrdered( XBINS ) || !Bins::isOrdered( YBINS ) ); bool tooFewBins( XBINS.size() < 2 || YBINS.size() < 2 ); \
  if ( notOrdered || tooFewBins ) { printf("[Hist::book(...)] WARN cannot book histogram due to \042%s\042\n", MAP .at({notOrdered,tooFewBins}).c_str()); return nullptr; }

// +------------------------------+
// | 1-dim Booking implementation |
// +------------------------------+
  
#define HIST_BOOK_PARM( PTR, NAME, TITLE, NBIN, XMIN, XMAX, AXT, AYT ) \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NBIN, XMIN, XMAX );	\
  configure< H >( PTR, AXT, AYT );				\
  return PTR

#define HIST_BOOK_BINS( PTR, NAME, TITLE, BINS, MAP, AXT, AYT )	\
  HIST_CHK_BINS_1D( BINS, MAP )			     \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( BINS.size()-1 ), &BINS.at(0) ); \
  configure< H >( PTR, AXT, AYT );					\
  return PTR

#define HIST_BOOK_PARM_DIR( DIR, PTR, NAME, TITLE, NBIN, XMIN, XMAX, AXT, AYT ) \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NBIN, XMIN, XMAX );	\
  configure< H >( DIR, PTR, AXT, AYT );				\
  return PTR

#define HIST_BOOK_BINS_DIR( DIR, PTR, NAME, TITLE, BINS, MAP, AXT, AYT ) \
  HIST_CHK_BINS_1D( BINS, MAP )			     \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( BINS.size()-1 ), &BINS.at(0) ); \
  configure< H >( DIR, PTR, AXT, AYT );					\
  return PTR

// +------------------------------+
// | 2-dim Booking implementation |
// +------------------------------+

#define HIST_BOOK_BINS_BINS( PTR, NAME, TITLE, XBINS, YBINS, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_2D( XBINS, YBINS, MAP )					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( XBINS.size()-1 ), &XBINS.at(0), static_cast<int>( YBINS.size()-1 ), &YBINS.at(0) ); \
  configure< H >( PTR, AXT, AYT, AZT );					\
  return PTR
  
#define HIST_BOOK_PARM_PARM( PTR, NAME, TITLE, NXBIN, XMIN, XMAX, NYBINS, YMIN, YMAX, AXT, AYT, AZT ) \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NXBIN, XMIN, XMAX, NYBINS, YMIN, YMAX ); \
  PTR->Sumw2();							\
  configure< H >( PTR, AXT, AYT, AZT );				\
  return PTR
  
#define HIST_BOOK_BINS_PARM( PTR, NAME, TITLE, XBINS, NYBINS, YMIN, YMAX, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_1D( XBINS, MAP );					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( XBINS.size()-1), &XBINS.at(0), NYBINS, YMIN, YMAX ); \
  PTR->Sumw2();								\
  configure< H >( PTR, AXT, AYT, AZT );					\
  return PTR
  
#define HIST_BOOK_PARM_BINS( PTR, NAME, TITLE, NXBINS, XMIN, XMAX, YBINS, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_1D( YBINS, MAP );					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NXBINS, XMIN, XMAX, static_cast<int>( YBINS.size()-1 ), &YBINS.at(0) ); \
  PTR->Sumw2();								\
  configure< H >( PTR, AXT, AYT, AZT );					\
  return PTR

#define HIST_BOOK_BINS_BINS_DIR( DIR, PTR, NAME, TITLE, XBINS, YBINS, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_2D( XBINS, YBINS, MAP )					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( XBINS.size()-1 ), &XBINS.at(0), static_cast<int>( YBINS.size()-1 ), &YBINS.at(0) ); \
  configure< H >( DIR, PTR, AXT, AYT, AZT );				\
  return PTR
  
#define HIST_BOOK_PARM_PARM_DIR( DIR, PTR, NAME, TITLE, NXBIN, XMIN, XMAX, NYBINS, YMIN, YMAX, AXT, AYT, AZT ) \
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NXBIN, XMIN, XMAX, NYBINS, YMIN, YMAX ); \
  PTR->Sumw2();							\
  configure< H >( DIR, PTR, AXT, AYT, AZT );			\
  return PTR
  
#define HIST_BOOK_BINS_PARM_DIR( DIR, PTR, NAME, TITLE, XBINS, NYBINS, YMIN, YMAX, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_1D( XBINS, MAP );					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), static_cast<int>( XBINS.size()-1), &XBINS.at(0), NYBINS, YMIN, YMAX ); \
  PTR->Sumw2();								\
  configure< H >( DIR, PTR, AXT, AYT, AZT );				\
  return PTR
  
#define HIST_BOOK_PARM_BINS_DIR( DIR, PTR, NAME, TITLE, NXBINS, XMIN, XMAX, YBINS, MAP, AXT, AYT, AZT ) \
  HIST_CHK_BINS_1D( YBINS, MAP );					\
  H* PTR = new H( NAME.c_str(), TITLE.c_str(), NXBINS, XMIN, XMAX, static_cast<int>( YBINS.size()-1 ), &YBINS.at(0) ); \
  PTR->Sumw2();								\
  configure< H >( DIR, PTR, AXT, AYT, AZT );				\
  return PTR

// +-------------------------+
// | Histogram configuration |
// +-------------------------+

// -- add titles to histogram axes
template<class H> bool Hist::addAxisTitle(H* hptr,const std::string& axt,const std::string& ayt,const std::string& azt) {
  if ( hptr == nullptr ) { 
    printf("[Hist::Hist::addAxisTitle(...)] WARN invalid pointer to histogram %p, axis titles cannot be added\n",hptr);
    return false; 
  }
  bool fSetTitle(false);
  if ( axt != emptyString )                                { hptr->GetXaxis()->SetTitle(axt.c_str()); fSetTitle = true; }
  if ( ayt != emptyString )                                { hptr->GetYaxis()->SetTitle(ayt.c_str()); fSetTitle = true; }
  if ( azt != emptyString && hptr->GetZaxis() != nullptr ) { hptr->GetZaxis()->SetTitle(azt.c_str()); fSetTitle = true; }
  return fSetTitle; 
}

// -- configure booked histogram
template<class H> bool Hist::configure(H* hptr,const std::string& axt,const std::string& ayt,const std::string& azt) {
  if ( hptr == nullptr ) { 
    printf("[Hist::configure(...)] WARN invalid pointer to histogram %p, histogram cannot be configured\n",hptr);
    return false; 
  }
  hptr->Sumw2();
  addAxisTitle< H >(hptr,axt,ayt,azt);
  return Hist::Manager::registerHist(hptr);
}

template<class H> bool Hist::configure(const std::string& dname,H* hptr,const std::string& axt,const std::string& ayt,const std::string& azt) {
  if ( hptr == nullptr ) { 
    printf("[Hist::configure(...)] WARN invalid pointer to histogram %p, histogram cannot be configured\n",hptr);
    return false; 
  }
  hptr->Sumw2();
  addAxisTitle< H >(hptr,axt,ayt,azt);
  return Hist::Manager::registerHist(dname,hptr);
}

// +-------------------------------------------+
// | 1-dim Histogram booking without directory |
// +-------------------------------------------+

template<class H> H* Hist::book(const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::string& axt,const std::string& ayt) { HIST_BOOK_PARM( hptr, hname, htitle, nxbins, xmin, xmax, axt, ayt ); }
template<class H> H* Hist::book(const std::string& hname,const std::string& htitle,const std::vector<double>& xbins  ,const std::string& axt,const std::string& ayt) { HIST_BOOK_BINS( hptr, hname, htitle, xbins, _lmap,       axt, ayt ); }

// +----------------------------------------+
// | 1-dim Histogram booking with directory |
// +----------------------------------------+

template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string& htitle,int nxbins,double xmin,double xmax,const std::string& axt,const std::string& ayt) { HIST_BOOK_PARM_DIR( dname, hptr, hname, htitle, nxbins, xmin, xmax, axt, ayt ); }
template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string& htitle,const std::vector<double>& xbins,const std::string& axt,const std::string& ayt) { HIST_BOOK_BINS_DIR( dname, hptr, hname, htitle, xbins, _lmap, axt, ayt ); }

// +-------------------------------------------+
// | 2-dim Histogram booking without directory |
// +-------------------------------------------+

template<class H> 
H* Hist::book(const std::string& hname,const std::string htitle,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_PARM_PARM( hptr, hname, htitle, nxbins, xmin, xmax, nybins, ymin, ymax, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& hname,const std::string htitle,const std::vector<double>& xbins,int nybins,double ymin,double ymax,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_BINS_PARM( hptr, hname, htitle, xbins, nybins, ymin, ymax, _lmap, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& hname,const std::string htitle,const std::vector<double>& xbins,const std::vector<double>& ybins,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_BINS_BINS( hptr, hname, htitle, xbins, ybins, _lmap, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& hname,const std::string htitle,int nxbins,double xmin,double xmax,const std::vector<double>& ybins,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_PARM_BINS( hptr, hname, htitle, nxbins, xmin, xmax, ybins, _lmap, axt, ayt, azt );
}

// +----------------------------------------+
// | 2-dim Histogram booking with directory |
// +----------------------------------------+

template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string htitle,int nxbins,double xmin,double xmax,int nybins,double ymin,double ymax,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_PARM_PARM_DIR( dname, hptr, hname, htitle, nxbins, xmin, xmax, nybins, ymin, ymax, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string htitle,const std::vector<double>& xbins,int nybins,double ymin,double ymax,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_BINS_PARM_DIR( dname, hptr, hname, htitle, xbins, nybins, ymin, ymax, _lmap, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string htitle,const std::vector<double>& xbins,const std::vector<double>& ybins,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_BINS_BINS_DIR( dname, hptr, hname, htitle, xbins, ybins, _lmap, axt, ayt, azt );
}
template<class H> 
H* Hist::book(const std::string& dname,const std::string& hname,const std::string htitle,int nxbins,double xmin,double xmax,const std::vector<double>& ybins,const std::string& axt,const std::string& ayt,const std::string& azt) {
  HIST_BOOK_PARM_BINS_DIR( dname, hptr, hname, htitle, nxbins, xmin, xmax, ybins, _lmap, axt, ayt, azt );
}

// +-----------------+
// | General booking |
// +-----------------+

template<class H> H* Hist::book(const Hist::Descriptor& hdescr) {
  H* hptr = nullptr;
  switch ( hdescr.dimension() ) {
  case 1:
    if ( hdescr.bins().empty() ) {
      hptr = book< H >(hdescr.name(),hdescr.title(),hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
    } else {
      hptr = book< H >(hdescr.name(),hdescr.title(),hdescr.bins(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
    }
    break;
  case 2:
    if ( hdescr.bins(AxisCoordinate::X).empty() ) {
      if ( hdescr.bins(AxisCoordinate::Y).empty() ) {
	hptr = book< H >(hdescr.name(),hdescr.title(),
			 hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),
			 hdescr.nbins(AxisCoordinate::Y),hdescr.xmin(AxisCoordinate::Y),hdescr.xmax(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      } else {
	hptr = book< H >(hdescr.name(),hdescr.title(),
			 hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),
			 hdescr.bins(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
      }
    } else {
      if ( hdescr.bins(AxisCoordinate::Y).empty() ) {
	hptr = book< H >(hdescr.name(),hdescr.title(),
			 hdescr.bins(AxisCoordinate::X),
			 hdescr.nbins(AxisCoordinate::Y),hdescr.xmin(AxisCoordinate::Y),hdescr.xmax(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      } else {
	hptr = book< H >(hdescr.name(),hdescr.title(),
			 hdescr.bins(AxisCoordinate::X),
			 hdescr.bins(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      }
    }
  case 3:
    printf("Hist::book()] WARN functionality not yet implemented for histogram dimension %i, no histogram booked\n",hdescr.dimension());
    break;
  default:
    printf("Hist::book()] WARN histogram descriptor has invalid dimension %i, no histogram booked\n",hdescr.dimension());
    break;
  }
  return hptr;
}

template<class H> H* Hist::book(TDirectory* pdir,const Hist::Descriptor& hdescr) {
  if ( pdir == nullptr ) { return book< H >(hdescr); }
  TDirectory* thisDir = TDirectory::CurrentDirectory();
  pdir->cd();
  std::string dname(pdir->GetName());
  H* hptr = book< H >(dname,hdescr); 
  thisDir->cd();
  return hptr;
}

template<class H> H* Hist::book(const std::string& dname,const Hist::Descriptor& hdescr) { 
  H* hptr = nullptr;
  switch ( hdescr.dimension() ) {
  case 1:
    if ( hdescr.bins().empty() ) {
      hptr = book< H >(dname,hdescr.name(),hdescr.title(),hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
    } else {
      hptr = book< H >(dname,hdescr.name(),hdescr.title(),hdescr.bins(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
    }
    break;
  case 2:
    if ( hdescr.bins(AxisCoordinate::X).empty() ) {
      if ( hdescr.bins(AxisCoordinate::Y).empty() ) {
	hptr = book< H >(dname,hdescr.name(),hdescr.title(),
			 hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),
			 hdescr.nbins(AxisCoordinate::Y),hdescr.xmin(AxisCoordinate::Y),hdescr.xmax(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      } else {
	hptr = book< H >(dname,hdescr.name(),hdescr.title(),
			 hdescr.nbins(AxisCoordinate::X),hdescr.xmin(AxisCoordinate::X),hdescr.xmax(AxisCoordinate::X),
			 hdescr.bins(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y));
      }
    } else {
      if ( hdescr.bins(AxisCoordinate::Y).empty() ) {
	hptr = book< H >(dname,hdescr.name(),hdescr.title(),
			 hdescr.bins(AxisCoordinate::X),
			 hdescr.nbins(AxisCoordinate::Y),hdescr.xmin(AxisCoordinate::Y),hdescr.xmax(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      } else {
	hptr = book< H >(dname,hdescr.name(),hdescr.title(),
			 hdescr.bins(AxisCoordinate::X),
			 hdescr.bins(AxisCoordinate::Y),
			 hdescr.axisTitle(AxisCoordinate::X),hdescr.axisTitle(AxisCoordinate::Y),hdescr.axisTitle(AxisCoordinate::Z));
      }
    }
  case 3:
    printf("Hist::book()] WARN functionality not yet implemented for histogram dimension %i, no histogram booked\n",hdescr.dimension());
    break;
  default:
    printf("Hist::book()] WARN histogram descriptor has invalid dimension %i, no histogram booked\n",hdescr.dimension());
    break;
  }
  return hptr;
}

// +-------------------+
// | Histogram manager |
// +-------------------+

template<class H> H* Hist::Manager::histPtr(Hist::Manager::entry_t& entry)   { return Hist::Manager::histBasePtr(entry) != nullptr ? dynamic_cast< H* >(Hist::Manager::histBasePtr(entry)) : nullptr; }
template<class H> H* Hist::Manager::histPtr(const Hist::Manager::key_t& key) { return Hist::Manager::histBasePtr(key)   != nullptr ? dynamic_cast< H* >(Hist::Manager::histBasePtr(key))   : nullptr; }

// -- registration
template<class H> bool Hist::Manager::registerHist(H* hptr)                          { return registry()->add((Hist::Manager::base_t*)hptr)      ; }              
template<class H> bool Hist::Manager::registerHist(const std::string& dname,H* hptr) { return registry()->add(dname,(Hist::Manager::base_t*)hptr); }
// -- retrieval
template<class H> H* Hist::Manager::retrieve(const std::string& hname)                          { base_t* hptr(registry()->retrieve(hname))      ; return hptr != nullptr ? dynamic_cast< H* >(hptr) : nullptr; }
template<class H> H* Hist::Manager::retrieve(const std::string& dname,const std::string& hname) { base_t* hptr(registry()->retrieve(dname,hname)); return hptr != nullptr ? dynamic_cast< H* >(hptr) : nullptr; }  
