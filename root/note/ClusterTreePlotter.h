// -*- c++ -*-
// Mon Sep 20 10:05:15 2021 by ROOT version 6.20/06 from TTree ClusterTree/ClusterTree
#ifndef ClusterTreePlotter_h
#define ClusterTreePlotter_h

#include "config.h"

#include <TROOT.h>

#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TBranch.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Types.h"
#include "AnalysisUtils.h"
#include "HistPlotter.h"

#include <vector>
#include <map>
#include <tuple>
#include <string>

#include <iostream>

#include <algorithm>

#include <cstdio>
#include <cmath>

#define CT_SET_BRANCH( NAME, SVAR )					\
  SVAR = std::string( #NAME ); if ( acceptBranch( SVAR ) && fChain->FindLeaf( SVAR.c_str() ) != nullptr ) { m_listOfLeaves[ SVAR ] = true; fChain->SetBranchAddress( SVAR.c_str(), &NAME, &b_##NAME ); }

/// @brief Masks for bit patterns identifying object types and calibration status
namespace Mask {
  static constexpr Types::Data::mask_t cluster  = static_cast<Types::Data::mask_t>(Types::Object::Type::Cluster);   ///< Bit pattern identifying topo-clusters
  static constexpr Types::Data::mask_t jet      = static_cast<Types::Data::mask_t>(Types::Object::Type::Jet);       ///< Bit pattern identifying jets
  static constexpr Types::Data::mask_t particle = static_cast<Types::Data::mask_t>(Types::Object::Type::Particle);  ///< Bit pattern identifying particles
} // Mask
/// @brief Analysis object
///
/// This class contains the description of features and stores implemented into an analysis object working on input data stored in a flat @c ROOT tuple structure.
/// It provides all functionality to read, filter and process the data, and produce several groups of distributions.
class ClusterTreePlotter {
public :
  /// @name Forwarded and local type defintions
  ///@{
  using uint_t       = Types::Data::uint_t;
  using int_t        = Int_t;
  using float_t      = Float_t;
  using bool_t       = Bool_t;
  using mask_t       = Types::Data::mask_t;
  using HistSelector = Hist::InterFace::Selector;
  ///@}
  ///@name Public data members related to I/O
  ///@{
  TTree* fChain   = { nullptr };  ///<! Pointer to the analyzed TTree or TChain
  int_t  fCurrent = { -1      };  ///< Current Tree number in a TChain
  ///@}
  ///
  ///@name Public data members storing event level input data
  ///@{
  int_t    seqNumber              = { 0  }; ///< Data sequence number for ID purposes
  int_t    runNumber              = { 0  }; ///< Run number used in MC production
  int_t    eventNumber            = { 0  }; ///< Event number used in MC production
  int_t    nPrimVtx               = { 0  }; ///< Number of reconstructed primary vertices (jet tuples only)
  float_t  avgMu                  = { 0. }; ///< Average number of interactions per bunch crossings (jet tuples only)
  ///@}
  ///@name Particle truth 
  /// This block is only filled in single particle tuples.
  ///@{
  float_t  truthE                 = { 0. }; ///< Truth particle energy
  float_t  truthPt                = { 0. }; ///< Truth particle pT
  float_t  truthEta               = { 0. }; ///< Truth particle rapidity (!)
  float_t  truthPhi               = { 0. }; ///< Truth particle azimuth
  int_t    truthPDG               = { 0  }; ///< Truth particle PDG identifier
  ///@}
  ///@name Jet truth (
  /// This block is only filled in jet tuples.
  ///@{
  float_t  truthJetMatchingRadius      = { 0. }; ///< Distance to reco jet 
  float_t  truthJetE                   = { 0. }; ///< Truth jet energy
  float_t  truthJetPt 	               = { 0. }; ///< Truth jet pT
  float_t  truthJetEta 	               = { 0. }; ///< Truth jet rapidity (!)
  float_t  truthJetPhi 	               = { 0. }; ///< Truth jet azimuth
  ///@}
  ///@name Reconstructed jets
  /// This block is only filled in jet tuples.
  ///@{
  int_t    jetCnt                      = { 0  }; ///< Jet per event counter
  float_t  jetCalE                     = { 0. }; ///< Jet energy (JES) @f E_{\rm jet}^{\rm JES} @f	    
  float_t  jetCalPt                    = { 0. }; ///< Jet pT (JES)	@f p_{\rm T,jet}^{\rm JES} @f    
  float_t  jetCalEta                   = { 0. }; ///< Jet rapidity (JES) @f y_{\rm jet}^{\rm JES} @f
  float_t  jetCalPhi                   = { 0. }; ///< Jet azimuth (JES) @f \phi_{\rm jet}^{\rm JES} @f    
  float_t  jetRawE                     = { 0. }; ///< Jet energy (constituent scale)	@f E_{\rm jet}^{\rm constscale} @f    
  float_t  jetRawPt                    = { 0. }; ///< Jet pT (constituent scale) @f p_{\rm T,jet}^{\rm constscale} @f	    
  float_t  jetRawEta                   = { 0. }; ///< Jet rapidity (constituent scale) @f y_{\rm jet}^{\rm constscale} @f
  float_t  jetRawPhi                   = { 0. }; ///< Jet azimuth (constituent scale) @f \phi_{\rm jet}^{\rm constscale} @f   
  int_t    jetNConst                   = { 0  }; ///< Number of constituents in jet @f N_{\rm const}^{\rm jet} @f
  ///@}
  ///@name Topo-cluster counter
  ///@{    
  int_t    nCluster                    = { 0  }; ///< Number of clusters for this jet or particle @f N_{\rm clus} @f. For jets, @f N_{\rm clus} \leq N_{\rm const}^{\rm jet} @f due to filters.
  int_t    clusterIndex                = { 0  }; ///< Index of cluster @f 1\ldots N_{\rm clus} @f
  ///@}
  ///@name Topo-cluster composition
  ///@{
  int_t    cluster_nCells              = { 0  }; ///< Number of cells with @f E_{\rm cell} > 0 @f in topo-cluster
  int_t    cluster_nCells_tot          = { 0  }; ///< Number of all cells in topo-cluster
  ///@}     
  ///@name Topo-cluster kinematics
  /// For details, see <a href="https://docs.google.com/document/d/1D4haSVnKQNqIuqz5BINUZn7HbIatXEjKqeiDYe3ATac/edit?usp=sharing">this document</a>.
  /// For this version, there is a problem with @c clusterEta (the topo-cluster rapidity at EM scale). We need to use clusterEtaCalib for the
  /// clusters in the jets. (PL, December 12, 2022)  
  ///@{		    
  float_t  clusterECalib               = { 0. }; ///< Topo-cluster energy (fully calibrated at LCW scale)	@f E_{|rm clus}^{\rm LCW} @f      
  float_t  clusterPtCalib              = { 0. }; ///< Topo-cluster pT (fully calibrated at LCW scale)	@f p_{\rm T,clus}^{\rm LCW} @f      
  float_t  clusterEtaCalib	       = { 0. }; ///< Topo-cluster rapidity (fully calibrated at LCW scale) @f y_{\rm clus}^{\rm LCW} @f       
  float_t  clusterPhiCalib	       = { 0. }; ///< Topo-cluster azimuth (fully calibrated at LCW scale) @f \phi_{\rm clus}^{\rm LCW} @f       
  float_t  cluster_sumCellECalib       = { 0. }; ///< Topo-cluster energy sum of cells with energies @f E_{\rm cell} > 0 @f  
  float_t  cluster_fracECalib	       = { 0. }; ///< Fraction of calibrated energy of all clusters @f E_{\rm clus}^{\rm LCW}/\sum_{i=1}^{N_{\rmclus}} E_{\rm clus}^{\rm LCW} @f	       
  float_t  cluster_fracECalib_ref      = { 0. }; ///< Fraction of calibrated energy of jet                     
  float_t  clusterE		       = { 0. }; ///< Topo-cluster energy (at EM scale)	       
  float_t  clusterPt		       = { 0. }; ///< Topo-cluster pT (at EM scale)	       
  float_t  clusterEta		       = { 0. }; ///< Topo-cluster rapidity (at EM scale)        
  float_t  clusterPhi		       = { 0. }; ///< Topo-cluster azimuth (at EM scale)	       
  float_t  cluster_sumCellE            = { 0. }; ///< Topo-cluster energy sum of cells with EM-scale energies > 0
  float_t  cluster_time 	       = { 0. }; ///< Topo-cluster time
  float_t  cluster_fracE	       = { 0. }; ///< fraction of calibrated energy of all clusters           
  float_t  cluster_fracE_ref	       = { 0. }; ///< fraction of calibrated energy of jet
  ///@}
  ///@name Topo-cluster moments: truth and LCW calibration scale factors
  /// For details, see <a href="https://docs.google.com/document/d/1D4haSVnKQNqIuqz5BINUZn7HbIatXEjKqeiDYe3ATac/edit?usp=sharing">this document</a>
  ///@{
  float_t  cluster_HAD_WEIGHT          = { 0. }; ///< scale factor from hadronic calibration
  float_t  cluster_OOC_WEIGHT	       = { 0. }; ///< scale factor from OOC correction
  float_t  cluster_DM_WEIGHT	       = { 0. }; ///< scale factor from DM correction
  float_t  cluster_ENG_CALIB_TOT       = { 0. }; ///< energy deposited in cells in topo-cluster 
  float_t  cluster_ENG_CALIB_OUT_T     = { 0. }; ///< OOC energy deposits associated with topo-cluster
  float_t  cluster_ENG_CALIB_DEAD_TOT  = { 0. }; ///< DM energy deposits associated with topo-cluster
  float_t  cluster_ENG_CALIB_FRAC_EM   = { 0. }; ///< fraction of cluster_ENG_CALIB_TOT deposited by EM processes
  float_t  cluster_ENG_CALIB_FRAC_HAD  = { 0. }; ///< fraction of cluster_ENG_CALIB_TOT deposited by HAD processes
  float_t  cluster_ENG_CALIB_FRAC_REST = { 0. }; ///< fraction of cluster_ENG_CALIB_TOT deposited by other processes
  ///@}
  ///@name Topo-cluster moments: reconstructed moments
  /// For details, see <a href="https://docs.google.com/document/d/1D4haSVnKQNqIuqz5BINUZn7HbIatXEjKqeiDYe3ATac/edit?usp=sharing">this document</a>
  ///@{ 
  float_t  cluster_EM_PROBABILITY      = { 0. }; ///< EM likelihood assigned by LCW
  float_t  cluster_CENTER_MAG	       = { 0. }; ///< Distance of cluster center-of-gravity from nominal vertex
  float_t  cluster_FIRST_ENG_DENS      = { 0. }; ///< First moment of energy-weighted cell signal density distribution in topo-cluster
  float_t  cluster_FIRST_PHI	       = { 0. }; ///< First moment of energy-weighted cell azimuth distribution in topo-cluster
  float_t  cluster_FIRST_ETA	       = { 0. }; ///< First moment of energy-weighted cell repidity distribution in topo-cluster
  float_t  cluster_SECOND_R            = { 0. }; ///< Decond moment of cell radial distance from shower center distribution in topo-cluster
  float_t  cluster_SECOND_LAMBDA       = { 0. }; ///< Decond moment of cell longitudinal distance from shower center distribution in topo-cluster
  float_t  cluster_DELTA_PHI	       = { 0. }; ///< Azimuthal distance between principal topo-cluster axis and direction from nominal vertex 
  float_t  cluster_DELTA_THETA	       = { 0. }; ///< Polar angle distance between principal topo-cluster axis and direction from nominal vertex 
  float_t  cluster_DELTA_ALPHA	       = { 0. }; ///< Angular distance between principal topo-cluster axis and direction from nominal vertex  
  float_t  cluster_CENTER_X            = { 0. }; ///< x-coordinate of topo-cluster center-of-gravity
  float_t  cluster_CENTER_Y	       = { 0. }; ///< y-coordinate of topo-cluster center-of-gravity
  float_t  cluster_CENTER_Z	       = { 0. }; ///< z-coordinate of topo-cluster center-of-gravity
  float_t  cluster_CENTER_LAMBDA       = { 0. }; ///< Topo-cluster center-of-gravity distance from calorimter front face
  float_t  cluster_LATERAL	       = { 0. }; ///< Normalized second moment of distribution of cell lateral distances from shower axis
  float_t  cluster_LONGITUDINAL        = { 0. }; ///< Normalized second moment of distribution of cell longitudinal distances from shower center
  float_t  cluster_ENG_FRAC_EM	       = { 0. }; ///< Energy fraction in EMC in topo-cluster
  float_t  cluster_ENG_FRAC_MAX        = { 0. }; ///< Energy fraction of cell with largest signal in topo-cluster
  float_t  cluster_ENG_FRAC_CORE       = { 0. }; ///< Energy fraction contained in four hottest cells in topo-cluster
  float_t  cluster_SECOND_ENG_DENS     = { 0. }; ///< Eecond moment of cell energy density distribution in topo-cluster
  float_t  cluster_ISOLATION           = { 0. }; ///< Isolation moment 
  float_t  cluster_ENG_BAD_CELLS       = { 0. }; ///< Energy in bad cells in topo-cluster
  float_t  cluster_N_BAD_CELLS         = { 0. }; ///< Number of bad cells in topo-cluster
  float_t  cluster_N_BAD_CELLS_CORR    = { 0. }; ///< Number of corrected bad cells in topo-cluster
  float_t  cluster_BAD_CELLS_CORR_E    = { 0. }; ///< Corrected energy of bad cells in topo-cluster
  float_t  cluster_BADLARQ_FRAC        = { 0. }; ///< Fraction of LAr cells with bad quality in topo-cluster
  float_t  cluster_ENG_POS	       = { 0. }; ///< Sum of cell energies E > 0 in topo-cluster
  float_t  cluster_SIGNIFICANCE        = { 0. }; ///< Cluster signal significance 
  float_t  cluster_CELL_SIGNIFICANCE   = { 0. }; ///< Signal significance of highest significant cell signal
  float_t  cluster_CELL_SIG_SAMPLING   = { 0. }; ///< Sampling containing cell with highest significant signal
  float_t  cluster_AVG_LAR_Q           = { 0. }; ///< Average LAr signal quality in topo-cluster
  float_t  cluster_AVG_TILE_Q	       = { 0. }; ///< Average Tile signal quality in topo-cluster
  float_t  cluster_ENG_BAD_HV_CELLS    = { 0. }; ///< Energy in cells with bad HV in topo-cluster
  float_t  cluster_N_BAD_HV_CELLS      = { 0. }; ///< Number of cells with bad HV in topo-cluster
  float_t  cluster_PTD		       = { 0. }; ///< Compactness measure for topo-cluster
  float_t  cluster_MASS                = { 0. }; ///< Topo-cluster mass moment
  float_t  cluster_SECOND_TIME	       = { 0. }; ///< Second moment of cell time distribution in topo-cluster
  ///@}
  ///@name Topo-cluster response
  /// These variables are not necessarily stored in branches of the input data @c ROOT @c TTree .
  ///@{
  float_t  clusterResponse             = { 0. }; ///< cluster response at EM scale
  float_t  clusterResponseLCW          = { 0. }; ///< cluster response at LCW scale
  float_t  clusterResponseHAD          = { 0. }; ///< cluster response at hadronic scale only
  float_t  clusterResponsePred         = { 0. }; ///< cluster response at EM scale predicted by ML
  ///@}
  ///@name List of branches
  ///@{
  TBranch* b_seqNumber                   = { nullptr }; ///<! Event: branch pointer to sequence number
  TBranch* b_runNumber   	         = { nullptr }; ///<! Event: branch pointer to run number
  TBranch* b_eventNumber 	         = { nullptr }; ///<! Event: branch pointer to event number
  TBranch* b_nPrimVtx                    = { nullptr }; ///<! Event: branch pointer to number of primary vertices
  TBranch* b_avgMu       	         = { nullptr }; ///<! Event: branch pointer to average number of pile-up interactions
  // particles	 		          
  TBranch* b_truthE                      = { nullptr }; ///<! Truth particles: branch pointer to energy
  TBranch* b_truthPt		         = { nullptr }; ///<! Truth particles: branch pointer to transverse momentum
  TBranch* b_truthEta		         = { nullptr }; ///<! Truth particles: branch pointer to rapidity
  TBranch* b_truthPhi		         = { nullptr }; ///<! Truth particles: branch pointer to azimuth
  TBranch* b_truthPDG		         = { nullptr }; ///<! Truth particles: branch pointer to PDG identifier
  // truth jets				          
  TBranch* b_truthJetMatchingRadius      = { nullptr }; ///<! Truth jets: branch pointer to jet matching distance
  TBranch* b_truthJetE                   = { nullptr }; //!/< Truth jets: branch pointer to energy
  TBranch* b_truthJetPt 	         = { nullptr }; //!/< Truth jets: branch pointer to transverse momentum
  TBranch* b_truthJetEta 	         = { nullptr }; //!/< Truth jets: branch pointer to rapidity 
  TBranch* b_truthJetPhi 	         = { nullptr }; //!/< Truth jets: branch pointer to azimuth 
  // reco jets  
  TBranch* b_jetCnt                      = { nullptr }; ///<! Reco jets: branch pointer to jet counter in event
  TBranch* b_jetCalE                     = { nullptr }; ///<! Reco jets: branch pointer to JES energy
  TBranch* b_jetCalPt		         = { nullptr }; ///<! Reco jets: branch pointer to JES transverse momentum
  TBranch* b_jetCalEta		         = { nullptr }; ///<! Reco jets: branch pointer to JES rapidity
  TBranch* b_jetCalPhi		         = { nullptr }; ///<! Reco jets: branch pointer to JES azimuth
  TBranch* b_jetRawE		         = { nullptr }; ///<! Reco jets: branch pointer to constituent scale energy
  TBranch* b_jetRawPt                    = { nullptr }; ///<! Reco jets: branch pointer to constituent scale transverse momentum
  TBranch* b_jetRawEta		         = { nullptr }; ///<! Reco jets: branch pointer to constituent scale rapidity
  TBranch* b_jetRawPhi		         = { nullptr }; ///<! Reco jets: branch pointer to constituent scale azimuth
  TBranch* b_jetNConst		         = { nullptr }; ///<! Reco jets: branch pointer to number of constituents
  // clusters				          
  TBranch* b_nCluster                    = { nullptr }; ///<! Topo-cluster: branch pointer to number of clusters in particle or jet
  TBranch* b_clusterIndex                = { nullptr }; ///<! Topo-cluster: branch pointer to index of cluster in particle or jet
  TBranch* b_cluster_nCells              = { nullptr }; ///<! Topo-cluster: branch pointer to number of cells with @f E_{\rm cell} > 0 @f in cluster
  TBranch* b_cluster_nCells_tot          = { nullptr }; ///<! Topo-cluster: branch pointer to number of all cells  in cluster
  TBranch* b_clusterECalib               = { nullptr }; ///<! Topo-cluster: branch pointer to LCW scale energy
  TBranch* b_clusterPtCalib	         = { nullptr }; ///<! Topo-cluster: branch pointer to LCW scale transverse momentum
  TBranch* b_clusterEtaCalib	         = { nullptr }; ///<! Topo-cluster: branch pointer to LCW scale rapidity
  TBranch* b_clusterPhiCalib	         = { nullptr }; ///<! Topo-cluster: branch pointer to LCW scale azimuth
  TBranch* b_cluster_sumCellECalib       = { nullptr }; ///<! Topo-cluster: branch pointer to sum of LCW scale energy in clusters 
  TBranch* b_cluster_fracECalib	         = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of LCW scale cluster energy in total energy sum over all clusters
  TBranch* b_cluster_fracECalib_ref      = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of LCW scale cluster energy in jet energy at JES scale 
  TBranch* b_clusterE		         = { nullptr }; ///<! Topo-cluster: branch pointer to EM scale energy
  TBranch* b_clusterPt                   = { nullptr }; ///<! Topo-cluster: branch pointer to EM scale transverse momentum
  TBranch* b_clusterEta		         = { nullptr }; ///<! Topo-cluster: branch pointer to EM scale rapidity
  TBranch* b_clusterPhi		         = { nullptr }; ///<! Topo-cluster: branch pointer to EM scale azimuth
  TBranch* b_cluster_sumCellE	         = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of EM scale cluster energy in total enegry sum over all clusters
  TBranch* b_cluster_time                = { nullptr }; ///<! Topo-cluster: branch pointer to cluster time
  TBranch* b_cluster_fracE	         = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of EM scale cluster energy in total energy sum over all clusters
  TBranch* b_cluster_fracE_ref	         = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of EM scale cluster energy in jet energy at constituent scale
  TBranch* b_cluster_HAD_WEIGHT          = { nullptr }; ///<! Topo-cluster: branch pointer to hadronic calibration weight
  TBranch* b_cluster_OOC_WEIGHT	         = { nullptr }; ///<! Topo-cluster: branch pointer to out-of-cluster calibration weight
  TBranch* b_cluster_DM_WEIGHT	         = { nullptr }; ///<! Topo-cluster: branch pointer to dead material calibration weight
  TBranch* b_cluster_ENG_CALIB_TOT       = { nullptr }; ///<! Topo-cluster: branch pointer to true energy deposited in cluster
  TBranch* b_cluster_ENG_CALIB_OUT_T     = { nullptr }; ///<! Topo-cluster: branch pointer to true energy deposited in calorimeter cells outside of cluster
  TBranch* b_cluster_ENG_CALIB_DEAD_TOT	 = { nullptr }; ///<! Topo-cluster: branch pointer to true energy deposited in dead material
  TBranch* b_cluster_ENG_CALIB_FRAC_EM   = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of true energy inside cluster deposited by electromagnetic processes 
  TBranch* b_cluster_ENG_CALIB_FRAC_HAD  = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of true energy inside cluster deposited by hadronic processes
  TBranch* b_cluster_ENG_CALIB_FRAC_REST = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of true energy inside cluster deposited by unclassified processes (decays, stopped particles)
  TBranch* b_cluster_EM_PROBABILITY      = { nullptr }; ///<! Topo-cluster: branch pointer to EM probability assigned by LCW procedure 
  TBranch* b_cluster_CENTER_MAG		 = { nullptr }; ///<! Topo-cluster: branch pointer to distance of cluster center-of-gravity from nominal vertex
  TBranch* b_cluster_FIRST_ENG_DENS	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted first moment of cell signal density distribution in cluster
  TBranch* b_cluster_FIRST_PHI           = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted first moment of cell azimuth distribution in cluster
  TBranch* b_cluster_FIRST_ETA		 = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted first moment pf cell rapidity distribution in cluster
  TBranch* b_cluster_SECOND_R		 = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted second moment of cell radial distance from cluster axis distribution 
  TBranch* b_cluster_SECOND_LAMBDA	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted second moment of cell longitudinal distance from cluster center-of-gravity distribution
  TBranch* b_cluster_DELTA_PHI           = { nullptr }; ///<! Topo-cluster: branch pointer to azimuthal distance between principal cluster axis and direction of cluster from nominal vertex
  TBranch* b_cluster_DELTA_THETA	 = { nullptr }; ///<! Topo-cluster: branch pointer to polar angle distance between principal cluster axis and direction of cluster from nominal vertex
  TBranch* b_cluster_DELTA_ALPHA	 = { nullptr }; ///<! Topo-cluster: branch pointer to angular distance between principal cluster axis and direction of cluster from nominal vertex  
  TBranch* b_cluster_CENTER_X		 = { nullptr }; ///<! Topo-cluster: branch pointer to @f x_{\rm clus} @f coordinate of cluster center-of-gravity with nominal vertex at @f (x,y,z) = (0,0,0) @f
  TBranch* b_cluster_CENTER_Y            = { nullptr }; ///<! Topo-cluster: branch pointer to @f y_{\rm clus} @f coordinate of cluster center-of-gravity with nominal vertex at @f (x,y,z) = (0,0,0) @f
  TBranch* b_cluster_CENTER_Z	         = { nullptr }; ///<! Topo-cluster: branch pointer to @f z_{\rm clus} @f coordinate of cluster center-of-gravity with nominal vertex at @f (x,y,z) = (0,0,0) @f
  TBranch* b_cluster_CENTER_LAMBDA       = { nullptr }; ///<! Topo-cluster: branch pointer to distance of cluster center-of-gravity from calorimeter frontface
  TBranch* b_cluster_LATERAL	         = { nullptr }; ///<! Topo-cluster: branch pointer to normalized lateral dispersion measure of cluster
  TBranch* b_cluster_LONGITUDINAL        = { nullptr }; ///<! Topo-cluster: branch pointer to nomralized longitudinal dispersion measure of cluster
  TBranch* b_cluster_ENG_FRAC_EM	 = { nullptr }; ///<! Topo-cluster: branch pointer to cluster energy fraction in EMC
  TBranch* b_cluster_ENG_FRAC_MAX	 = { nullptr }; ///<! Topo-cluster: branch pointer to cluster energy fraction of cell with maximum signal
  TBranch* b_cluster_ENG_FRAC_CORE	 = { nullptr }; ///<! Topo-cluster: branch pointer to cluster energy fraction of core of four cells with largest signals
  TBranch* b_cluster_SECOND_ENG_DENS     = { nullptr }; ///<! Topo-cluster: branch pointer to energy-weighted second moment of cell signal density distribution in cluster
  TBranch* b_cluster_ISOLATION		 = { nullptr }; ///<! Topo-cluster: branch pointer to isolation moment of cluster
  TBranch* b_cluster_ENG_BAD_CELLS	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy in bad cells in cluster
  TBranch* b_cluster_N_BAD_CELLS	 = { nullptr }; ///<! Topo-cluster: branch pointer to number of bad cells in cluster
  TBranch* b_cluster_N_BAD_CELLS_CORR    = { nullptr }; ///<! Topo-cluster: branch pointer to number of bad cells with corrected signals in cluster
  TBranch* b_cluster_BAD_CELLS_CORR_E	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy in bad cells after correction
  TBranch* b_cluster_BADLARQ_FRAC	 = { nullptr }; ///<! Topo-cluster: branch pointer to fraction of cells with bad quality in LAr calorimeter
  TBranch* b_cluster_ENG_POS		 = { nullptr }; ///<! Topo-cluster: branch pointer to sum of cell energies @f E_{\em cell} > 0 @f in cluster
  TBranch* b_cluster_SIGNIFICANCE        = { nullptr }; ///<! Topo-cluster: branch pointer to total cluster signal significance
  TBranch* b_cluster_CELL_SIGNIFICANCE	 = { nullptr }; ///<! Topo-cluster: branch pointer to maximum significance found for cell signal in cluster
  TBranch* b_cluster_CELL_SIG_SAMPLING	 = { nullptr }; ///<! Topo-cluster: branch pointer to sampling layer number of cell with highest signal significance in cluster
  TBranch* b_cluster_AVG_LAR_Q		 = { nullptr }; ///<! Topo-cluster: branch pointer to average signal quality in LAr calorimeter cells in cluster
  TBranch* b_cluster_AVG_TILE_Q          = { nullptr }; ///<! Topo-cluster: branch pointer to average signal quality in Tile calorimeter cells in cluster
  TBranch* b_cluster_ENG_BAD_HV_CELLS	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy in cells with bad HV 
  TBranch* b_cluster_N_BAD_HV_CELLS	 = { nullptr }; ///<! Topo-cluster: branch pointer to number of cells with bad HV
  TBranch* b_cluster_PTD		 = { nullptr }; ///<! Topo-cluster: branch pointer to cluster signal compactness measure @f p_{\rm T}D @f
  TBranch* b_cluster_MASS                = { nullptr }; ///<! Topo-cluster: branch pointer to cluster mass
  TBranch* b_cluster_SECOND_TIME	 = { nullptr }; ///<! Topo-cluster: branch pointer to energy-square-weighted second moment of cell timing distribution
  TBranch* b_clusterResponse             = { nullptr }; ///<! Topo-cluster: branch pointer to cluster response at EM scale (may not be in input file)
  TBranch* b_clusterResponseLCW          = { nullptr }; ///<! Topo-cluster: branch pointer to cluster response at LCW scale (may not be in input file)
  TBranch* b_clusterResponseHAD          = { nullptr }; ///<! Topo-cluster: branch pointer to cluster response at hadronic scale (may not be in input file)
  TBranch* b_clusterResponsePred         = { nullptr }; ///<! Topo-cluster: branch pointer to cluster response at ML-predicted scale (may not be in input file)
  ///@}
  /// @brief Constructor of plotting module
  /// @param tree    pointer to a @c ROOT @c TTree (default is @c nullptr )
  /// @param varList list of variables to be read (determines list of booked and filled branches, default is all branches indicated by an empty list)
  ClusterTreePlotter(TTree *tree=nullptr,const std::vector<std::string>& varList={});
  /// @brief Baseclass destructor
  virtual ~ClusterTreePlotter(); 
  /// @brief Interface for implementinc cuts
  /// @param entry entry number in tree
  /// @return @c 0 if entry passes selection, else any number smaller than @c 0 .
  virtual int_t Cut(Long64_t entry);
  /// @brief Get an entry
  /// @param entry entry number in tree
  /// @return Number of bytes loaded for the given entry if found, else @c 0 or a negative number
  virtual int_t GetEntry(Long64_t entry);
  /// @brief Load the tree for a given entry
  /// @param entry entry number in tree
  /// @return If end of file is reached, or the tree could not be loaded for any other reason, a negative number is returned.
  virtual Long64_t LoadTree(Long64_t entry);
  /// @brief Initialize the tree by allocating the requested branches
  /// @param tree valid pointer to a booked tree
  virtual void Init(TTree *tree);
  /// @brief Main analysis loop 
  /// @param pdgId   use only particles with this PDG code in analysis (default @c 0 for jets or all particles)
  /// @param absVal  use the absolute value of @c pdgId when selecting particles (default is @c false )
  virtual void Loop(double pdgId=0,bool absVal=false);
  /// @brief Interface for notifications
  /// This function is called whenever a new file is opened.
  /// @return @c true by default
  virtual bool_t Notify();
  /// @brief Show entry interface
  /// This is not used in this analysis. This interface supports debugging as needed.
  /// @param entry entry number in tree
  virtual void Show(Long64_t entry = -1);
  /// @brief Bin normalizations
  /// @param h pointer to valid histogram
  /// This function works for all @c ROOT histograms which are derived from @c TH1 . 
  virtual void binNormalize(TH1* h); 
  /// @name Bitpatterns for scales and object types, and value types
  ///@{
  using ValueType  = Types::Value::Type;   ///< Synonym for value type
  using ValueName  = Types::Value::Name;   ///< Synonym for value name
  using ValueLabel = Types::Value::Label;  ///< Synonym for value label
  using ScaleType  = Types::Scale::Type;   ///< Synonym for scale type
  using ScaleName  = Types::Scale::Name;   ///< Synonym for scale name
  using ScaleLabel = Types::Scale::Label;  ///< Synonym for scale label
  enum class ClusterScale { RAW=ScaleType::RAW|Mask::cluster, LCW=ScaleType::LCW|Mask::cluster, TRUTH=ScaleType::TRUTH|Mask::cluster , ML=ScaleType::ML|Mask::cluster, UNKNOWN=ScaleType::Unknown|Mask::cluster  }; ///< Cluster scales
  enum class JetScale     { RAW=ScaleType::RAW|Mask::jet    , JES=ScaleType::JES|Mask::jet    , TRUTH=ScaleType::TRUTH|Mask::jet     ,                                 UNKNOWN=ScaleType::Unknown|Mask::jet      }; ///< Jet scales
  enum class ParticleScale{                                                                     TRUTH=ScaleType::TRUTH|Mask::particle,                                 UNKNOWN=ScaleType::Unknown|Mask::particle }; ///< Particle scales
  ///@}

protected:
  /// @brief Dictionary for object and scale labels
  const std::map<uint_t,std::string> m_scaleDict = {
    { static_cast<uint_t>(ClusterScale::RAW     ), "ClusterScale::RAW"     },
    { static_cast<uint_t>(ClusterScale::LCW     ), "ClusterScale::LCW"     },
    { static_cast<uint_t>(ClusterScale::TRUTH   ), "ClusterScale::TRUTH"   },
    { static_cast<uint_t>(ClusterScale::ML      ), "ClusterScale::ML"      },
    { static_cast<uint_t>(ClusterScale::UNKNOWN ), "ClusterScale::UNKNOWN" },
    { static_cast<uint_t>(JetScale::RAW         ), "JetScale::RAW"         },
    { static_cast<uint_t>(JetScale::JES         ), "JetScale::JES"         },
    { static_cast<uint_t>(JetScale::TRUTH       ), "JetScale::TRUTH"       },
    { static_cast<uint_t>(JetScale::UNKNOWN     ), "JetScale::UNKNOWN"     },
    { static_cast<uint_t>(ParticleScale::TRUTH  ), "ParticleScale::TRUTH"  },
    { static_cast<uint_t>(ParticleScale::UNKNOWN), "ParticleScale::UNKNOWN"}
  };
  /// @brief Reference for unknown object and scale type
  const std::string m_unknownScale = { "UnknownScale::UNKNOWN" };
  /// @brief Dictionary for value types
  const std::map<ValueType,ValueLabel> m_valueDict = { 
    { ValueType::E      , Types::Value::typeToLabel(ValueType::E      ) },
    { ValueType::pT     , Types::Value::typeToLabel(ValueType::pT     ) },
    { ValueType::y      , Types::Value::typeToLabel(ValueType::y      ) },
    { ValueType::PDGId  , Types::Value::typeToLabel(ValueType::PDGId  ) },
    { ValueType::Unknown, Types::Value::typeToLabel(ValueType::Unknown) }
  };
public:
  /// @brief Find label for object scale
  /// @param  rs  type of object and scale (bitpattern)
  /// @return Reference to non-modifiable label of obect and scale type if @c rs is a known bit pattern, else @c m_unknownScale . 
  const std::string& objectScale(uint_t rs) const;
  /// @brief Find value label from type
  /// @param vt   value type in dictionary
  /// @return Label of value type.
  const std::string& valueType(ValueType vt) const;
  /// @name Particle filter configurations
  /// @param ps       particle and scale type
  /// @param e        energy threshold
  /// @param pt       transverse momentum threshold
  /// @param rap      rapidity threshold
  /// @param rapmin   rapidity lower range delimiter
  /// @param rapmax   rapidity upper range delimiter
  ///@{
  void setParticleEmin     (ParticleScale ps,double e                   ); ///< Set minimum energy 
  void setParticlePtmin    (ParticleScale ps,double pt                  ); ///< Set minimum transverse momentum
  void setParticleAbsRapMin(ParticleScale ps,double rap                 ); ///< Set minimum absolute rapdity 
  void setParticleAbsRapMax(ParticleScale ps,double rap                 ); ///< Set maximum absolute rapidity
  void setParticleAbsRap   (ParticleScale ps,double rapmin,double rapmax); ///< Set range for absolute rapidity
  void setParticleRapMin   (ParticleScale ps,double rap                 ); ///< Set minimum for rapidity
  void setParticleRapMax   (ParticleScale ps,double rap                 ); ///< Set maximum for rapidity
  void setParticleRap      (ParticleScale ps,double rapmin,double rapmax); ///< Set range for rapidity
  ///@}
  ///@name Cluster filter configurations
  /// @param cs       cluster and scale type
  /// @param e        energy threshold
  /// @param pt       transverse momentum threshold
  /// @param rap      rapidity threshold
  /// @param rapmin   rapidity lower range delimiter
  /// @param rapmax   rapidity upper range delimiter
  ///@{
  void setClusterEmin     (ClusterScale cs,double e                   ); ///< Set minimum energy 
  void setClusterPtmin    (ClusterScale cs,double pt                  ); ///< Set minimum transverse momentum
  void setClusterAbsRapMin(ClusterScale cs,double rap                 ); ///< Set minimum absolute rapdity 
  void setClusterAbsRapMax(ClusterScale cs,double rap                 ); ///< Set maximum absolute rapidity
  void setClusterAbsRap   (ClusterScale cs,double rapmin,double rapmax); ///< Set range for absolute rapidity
  void setClusterRapMin   (ClusterScale cs,double rap                 ); ///< Set minimum for rapidity 
  void setClusterRapMax   (ClusterScale cs,double rap                 ); ///< Set maximum for rapidity 
  void setClusterRap      (ClusterScale cs,double rapmin,double rapmax); ///< Set range for rapidity
  ///@}
  ///@name Jet filter configurations
  /// @param js       jet and scale type
  /// @param e        energy threshold
  /// @param pt       transverse momentum threshold
  /// @param rap      rapidity threshold
  /// @param rapmin   rapidity lower range delimiter
  /// @param rapmax   rapidity upper range delimiter
  ///@{
  void setJetEMin     (JetScale js,double e                   ); ///< Set minimum energy  
  void setJetPtMin    (JetScale js,double pt                  ); ///< Set minimum transverse momentum 
  void setJetAbsRapMin(JetScale js,double rap                 ); ///< Set minimum absolute rapdity 
  void setJetAbsRapMax(JetScale js,double rap                 ); ///< Set maximum absolute rapidity
  void setJetAbsRap   (JetScale js,double rapmin,double rapmax); ///< Set range for absolute rapidity
  void setJetRapMin   (JetScale js,double rap                 ); ///< Set minimum for rapidity 
  void setJetRapMax   (JetScale js,double rap                 ); ///< Set maximum for rapidity 
  void setJetRap      (JetScale js,double rapmin,double rapmax); ///< Set range for rapidity
  ///@}
  /// @brief Print selectors
  /// @param os out stream reference
  /// @return Reference to output stream
  std::ostream& printContent(std::ostream& os) const;
  /// @brief Check if tree has a named branch
  /// @param bname name of branch to be checked.
  /// @return @c true if branch is found/booked, else @c false
  bool hasBranch(const std::string& bname) const;

private:

  /// @name Production flags
  ///@{
  bool m_isParticle  = { false }; ///< Indicates that analysis runs on particles if @c true
  bool m_isJet       = { false }; ///< Indicates that analysis runs on jets if @c true 
  ///@}

  /// @brief List of known leaves with booking status 
  std::map<std::string,bool> m_listOfLeaves = {
    { "seqNumber"                  , false },
    { "runNumber"                  , false },
    { "eventNumber"                , false },
    { "nPrimVtx"                   , false },
    { "avgMu"                      , false },
    { "truthE"                     , false },
    { "truthPt"                    , false },
    { "truthEta"                   , false },
    { "truthPhi"                   , false },
    { "truthPDG"                   , false },
    { "truthJetMatchingRadius"     , false },
    { "truthJetE"                  , false },
    { "truthJetPt" 	           , false },
    { "truthJetEta" 	           , false },
    { "truthJetPhi" 	           , false },
    { "jetCnt"                     , false },
    { "jetCalE"                    , false },
    { "jetCalPt"                   , false },
    { "jetCalEta"                  , false },
    { "jetCalPhi"                  , false },
    { "jetRawE"	                   , false },   
    { "jetRawPt"                   , false },
    { "jetRawEta"                  , false },
    { "jetRawPhi"                  , false },
    { "jetNConst"                  , false },
    { "nCluster"                   , false },
    { "clusterIndex" 	           , false },
    { "cluster_nCells" 	           , false },
    { "cluster_nCells_tot"         , false },
    { "clusterECalib"	           , false }, 
    { "clusterPtCalib"             , false },
    { "clusterEtaCalib"	           , false }, 
    { "clusterPhiCalib"	           , false }, 
    { "cluster_sumCellECalib"      , false },
    { "cluster_fracECalib"         , false }, 
    { "cluster_fracECalib_ref"     , false },
    { "clusterE"	           , false },    
    { "clusterPt"	           , false },
    { "clusterEta"	           , false },
    { "clusterPhi"	           , false },
    { "cluster_sumCellE"           , false },
    { "cluster_time" 	           , false },
    { "cluster_fracE"	           , false },
    { "cluster_fracE_ref"          , false },
    { "cluster_HAD_WEIGHT"         , false },
    { "cluster_OOC_WEIGHT"	   , false },
    { "cluster_DM_WEIGHT"	   , false },
    { "cluster_ENG_CALIB_TOT"	   , false },
    { "cluster_ENG_CALIB_OUT_T"	   , false },
    { "cluster_ENG_CALIB_DEAD_TOT" , false },
    { "cluster_ENG_CALIB_FRAC_EM"  , false },
    { "cluster_ENG_CALIB_FRAC_HAD" , false },
    { "cluster_ENG_CALIB_FRAC_REST", false },
    { "cluster_EM_PROBABILITY"	   , false },
    { "cluster_CENTER_MAG"	   , false },
    { "cluster_FIRST_ENG_DENS"	   , false },
    { "cluster_FIRST_PHI"	   , false },
    { "cluster_FIRST_ETA"	   , false },
    { "cluster_SECOND_R"           , false },
    { "cluster_SECOND_LAMBDA"	   , false },
    { "cluster_DELTA_PHI"	   , false },
    { "cluster_DELTA_THETA"	   , false },
    { "cluster_DELTA_ALPHA"	   , false },
    { "cluster_CENTER_X"           , false },
    { "cluster_CENTER_Y"	   , false },
    { "cluster_CENTER_Z"	   , false },
    { "cluster_CENTER_LAMBDA"	   , false },
    { "cluster_LATERAL"		   , false },
    { "cluster_LONGITUDINAL"       , false },
    { "cluster_ENG_FRAC_EM"	   , false },
    { "cluster_ENG_FRAC_MAX"  	   , false },
    { "cluster_ENG_FRAC_CORE"	   , false },
    { "cluster_SECOND_ENG_DENS"	   , false },
    { "cluster_ISOLATION"          , false },
    { "cluster_ENG_BAD_CELLS"	   , false },
    { "cluster_N_BAD_CELLS"   	   , false },
    { "cluster_N_BAD_CELLS_CORR"   , false },
    { "cluster_BAD_CELLS_CORR_E"   , false },
    { "cluster_BADLARQ_FRAC"       , false },
    { "cluster_ENG_POS"		   , false },
    { "cluster_SIGNIFICANCE"  	   , false },
    { "cluster_CELL_SIGNIFICANCE"  , false },
    { "cluster_CELL_SIG_SAMPLING"  , false },
    { "cluster_AVG_LAR_Q"          , false },
    { "cluster_AVG_TILE_Q"	   , false },
    { "cluster_ENG_BAD_HV_CELLS"   , false },
    { "cluster_N_BAD_HV_CELLS"	   , false },
    { "cluster_PTD"		   , false },
    { "cluster_MASS"               , false },
    { "cluster_SECOND_TIME"	   , false },
    { "clusterResponse"            , false },
    { "clusterResponseHAD"         , false },
    { "clusterResponseLCW"         , false },
    { "clusterResponsePred"        , false } 
  };
  /// @brief List of requested variables
  std::vector<Types::Variable::Id> m_varIdList = { };
  /// @brief Event counter 
  int    m_event = { -1 }; 
  /// @brief Jet transverse momentum cache
  double m_jetPt = { 0. };
  /// @name Filter maps
  ///@{
  typedef uint_t key_t;
  //                     +-----------------------------------------------< [0] value type used in filter 
  //                     |        +--------------------------------------< [1] min(value)      
  //                     |        |       +------------------------------< [2] max(value)
  //                     |        |       |      +-----------------------< [3] true if abs(value) should be used
  //                     |        |       |      |       +---------------< [4] operator in filter: ">", "<", "==", "!=" "<>"
  //                     |        |       |      |       |
  typedef std::tuple<ValueType,Float_t,Float_t,bool,std::string> frange_t;
  std::multimap<key_t,frange_t>                                  m_filters;
  ///@}
  /// @name Filter bindings
  ///@{
  typedef std::tuple<ValueType,mask_t> filterkey_t; ///< Type for value type paired with object and scale 
  typedef float_t*                     filterptr_t; ///< Pointer type for filtered quantity 
  typedef std::map<vkey_t,vptr_t>      filtermap_t; ///< Map type for filter maps
  /// Binding types with storage locations
  const filtermap_t m_accessValues = { 
    { { ValueType::E,     (mask_t)ClusterScale::RAW    }, &clusterE              },
    { { ValueType::E,     (mask_t)ClusterScale::LCW    }, &clusterECalib         },
    { { ValueType::E,     (mask_t)ClusterScale::TRUTH  }, &cluster_ENG_CALIB_TOT },
    { { ValueType::pT,    (mask_t)ClusterScale::RAW    }, &clusterPt             },
    { { ValueType::pT,    (mask_t)ClusterScale::LCW    }, &clusterPtCalib        },
    { { ValueType::y,     (mask_t)ClusterScale::RAW    }, &clusterEta            },
    { { ValueType::y,     (mask_t)ClusterScale::LCW    }, &clusterEtaCalib       },
    { { ValueType::E,     (mask_t)JetScale::RAW        }, &jetRawE               },
    { { ValueType::E,     (mask_t)JetScale::LCJES      }, &jetCalE               },
    { { ValueType::E,     (mask_t)JetScale::TRUTH      }, &truthJetE             },
    { { ValueType::pT,    (mask_t)JetScale::RAW        }, &jetRawPt              },
    { { ValueType::pT,    (mask_t)JetScale::LCJES      }, &jetCalPt              },
    { { ValueType::pT,    (mask_t)JetScale::TRUTH      }, &truthJetPt            },
    { { ValueType::y,     (mask_t)JetScale::RAW        }, &jetRawEta             },
    { { ValueType::y,     (mask_t)JetScale::LCJES      }, &jetCalEta             },
    { { ValueType::y,     (mask_t)JetScale::TRUTH      }, &truthJetEta           },
    { { ValueType::E,     (mask_t)ParticleScale::TRUTH }, &truthE                },
    { { ValueType::pT,    (mask_t)ParticleScale::TRUTH }, &truthPt               },
    { { ValueType::y,     (mask_t)ParticleScale::TRUTH }, &truthEta              },
    { { ValueType::PDGId, (mask_t)ParticleScale::TRUTH }, &truthPDG              }
  };
  /// Empty filter map for reference
  const filtermap_t m_accessEmpty = { };
  ///@}
  /// @name Helpers
  ///@{
  template<class T> bool isClusterKey (T key) { return ( key & Mask::cluster  ) == Mask::cluster ; } ///< Checks if a given key is associated with a topo-cluster
  template<class T> bool isJetKey     (T key) { return ( key & Mask::jet      ) == Mask::jet     ; } ///< Checks if a given key is associated witj a jet
  template<class T> bool isParticleKey(T key) { return ( key & Mask::particle ) == Mask::particle; } ///< Checks if a given key is associated with a particle
  /// @brief Retrieve a value using a given key
  /// @tparam T      value data type
  /// @param vtype   value type
  /// @param vscale  object and scale type
  /// @param val     reference to modifiable data word of type @c T 
  /// @return If the value type with the given object and scale type has a valid memory address bound to it, the data stored at the address is store in @c val and @c true is returned, else @c false . 
  template<class T> bool fillValue(ValueType vtype,mask_t vscale,T& val) {
    auto fval(m_accessValues.find({vtype,vscale})); if ( fval != m_accessValues.end() ) { val = static_cast< T >(*(fval->second)); return true; } else { return false; }
  }
  bool isParticle(); ///< Returns @c true if analysis runs on particles
  bool isJet()     ; ///< Returns @c true if analysis runs on jets
  ///@}
  /// @name State control
  /// Any changes in these numbers indicate a change if state (new run, new event, new particle or new jet).
  ///@{
  float_t m_runNumber   = { 0. }; ///< Cache for previous run number
  float_t m_eventNumber = { 0. }; ///< Cache for previous event number
  float_t m_truthPDG    = { 0. }; ///< Cache for previous truth PDG code
  float_t m_truthE      = { 0. }; ///< Cache for previous truth energy
  float_t m_truthJetPt  = { 0. }; ///< Cache for previous truth jet transverse momentum
  float_t m_truthJetEta = { 0. }; ///< Cache for previous truth jet rapidity
  ///@}
protected:
  /// @brief Check if a leaf is booked with the given name
  /// @param lname name of the leaf
  /// @return @c true if leaf is booked, else @c false
  virtual bool hasBookedLeaf(const std::string& lname);
  ///@name Filter objects
  ///@param cs    cluster object and scale mask
  ///@param js    jet object and scale mask
  ///@param ps    particle object and scale mask
  //?@param key   general mask
  ///@{ 
  virtual bool filter(ClusterScale  cs); ///< Filter clusters (returns @c true if accepted) 
  virtual bool filter(JetScale      js); ///< Filter jets (returns @c true if accepted) 
  virtual bool filter(ParticleScale ps); ///< Filter particles (returns @c true if accepted) 
  virtual bool select(mask_t key);       ///< 
  ///@}
  ///@name Processing flags
  ///@{
  virtual bool newEvent();   ///< New simulation event
  virtual bool newObject();  ///< New particle or jet
  ///@}
  ///@name Fill plots from other reconstruction objects
  ///@{
  virtual bool fillParticle(bool final=false); ///< Fills particle distributions
  virtual bool fillJet     (bool final=false); ///< Fills jet distributions
  ///@}

  virtual bool acceptBranch(const std::string& bname);

  template<class T> bool aboveThreshold(T value,T vthr)         { return value > vthr;                 }
  template<class T> bool belowThreshold(T value,T vthr)         { return value < vthr;                 }
  template<class T> bool withinRange   (T value,T vmin, T vmax) { return value > vmin && value < vmax; }
  template<class T> bool equalTo       (T value,T veqv)         { return value == veqv;                } 

private:
  ///@brief Plot module type 
  using Plotter = Hist::Plotter::Base;
  ///@name Particle plots
  ///@{
  
  struct ParticlePlotter {
    Plotter* energy   = { nullptr };
    Plotter* rapidity = { nullptr };
    Plotter* pT       = { nullptr };
    ParticlePlotter() { }
    ParticlePlotter(const ParticlePlotter& pplots);
    ParticlePlotter& operator=(const ParticlePlotter& pplots);
  };
  std::map<int_t,ParticlePlotter> particlePlots;
  ///@}
  ///@name Jet plots
  ///@{
  struct JetPlotter {
    Plotter* energy   = { nullptr };
    Plotter* rapidity = { nullptr };
    Plotter* pT       = { nullptr };
    Plotter* nconst   = { nullptr };
    Plotter* deltaR   = { nullptr };
    JetPlotter();
    JetPlotter(const JetPlotter& jplots); 
    JetPlotter& operator=(const JetPlotter& jplots);  
  };
  std::map<JetScale,JetPlotter> jetPlots;
  ///@}


   // -- cluster quantities
   TH1D* h_clus_rap_all = { (TH1D*)0 };
   TH1D* h_clus_rap_acc = { (TH1D*)0 };
   TH1D* h_clus_dep_all = { (TH1D*)0 };
   TH1D* h_clus_dep_acc = { (TH1D*)0 };
   TH1D* h_clus_eem_all = { (TH1D*)0 };
   TH1D* h_clus_eem_acc = { (TH1D*)0 };
   TH1D* h_clus_lcw_all = { (TH1D*)0 };
   TH1D* h_clus_lcw_acc = { (TH1D*)0 };
   TH1D* h_clus_eml_all = { (TH1D*)0 };
   TH1D* h_clus_eml_acc = { (TH1D*)0 };

   // -- evaluation scale: deposited energy
   TH2D* d_em_resp_edep_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_edep_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_edep_incl = { (TH2D*)0 };
   TH2D* d_em_resp_edep_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_edep_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_edep_e300 = { (TH2D*)0 };

   // -- evaluation scale: signal fraction
   TH2D* d_em_resp_efrc_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_efrc_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_efrc_incl = { (TH2D*)0 };
   TH2D* d_em_resp_efrc_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_efrc_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_efrc_e300 = { (TH2D*)0 };

   // -- evaluation scale: signal density
   TH2D* d_em_resp_rho__incl = { (TH2D*)0 };
   TH2D* d_lc_resp_rho__incl = { (TH2D*)0 };
   TH2D* d_ml_resp_rho__incl = { (TH2D*)0 };
   TH2D* d_em_resp_rho__e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_rho__e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_rho__e300 = { (TH2D*)0 };

   // -- evaluation scale: depth
   TH2D* d_em_resp_lamb_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_lamb_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_lamb_incl = { (TH2D*)0 };
   TH2D* d_em_resp_lamb_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_lamb_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_lamb_e300 = { (TH2D*)0 };

   // -- evaluation scale: longitudinal energy dispersion
   TH2D* d_em_resp_long_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_long_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_long_incl = { (TH2D*)0 };
   TH2D* d_em_resp_long_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_long_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_long_e300 = { (TH2D*)0 };

   // -- evaluation scale: lateral energy dispersion
   TH2D* d_em_resp_lat__incl = { (TH2D*)0 };
   TH2D* d_lc_resp_lat__incl = { (TH2D*)0 };
   TH2D* d_ml_resp_lat__incl = { (TH2D*)0 };
   TH2D* d_em_resp_lat__e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_lat__e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_lat__e300 = { (TH2D*)0 };

   // -- evaluation scale: signal significance
   TH2D* d_em_resp_sign_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_sign_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_sign_incl = { (TH2D*)0 };
   TH2D* d_em_resp_sign_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_sign_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_sign_e300 = { (TH2D*)0 };

   // -- evaluation scale: PtD
   TH2D* d_em_resp_ptd__incl = { (TH2D*)0 };
   TH2D* d_lc_resp_ptd__incl = { (TH2D*)0 };
   TH2D* d_ml_resp_ptd__incl = { (TH2D*)0 };
   TH2D* d_em_resp_ptd__e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_ptd__e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_ptd__e300 = { (TH2D*)0 };

   // -- evaluation scale: second moment of time
   TH2D* d_em_resp_time_incl = { (TH2D*)0 };
   TH2D* d_lc_resp_time_incl = { (TH2D*)0 };
   TH2D* d_ml_resp_time_incl = { (TH2D*)0 };
   TH2D* d_em_resp_time_e300 = { (TH2D*)0 };
   TH2D* d_lc_resp_time_e300 = { (TH2D*)0 };
   TH2D* d_ml_resp_time_e300 = { (TH2D*)0 };

};


inline bool               ClusterTreePlotter::hasBranch(const std::string& bname) const { auto fmap = m_listOfLeaves.find(bname); return fmap != m_listOfLeaves.end() && fmap->second; }
inline const std::string& ClusterTreePlotter::objectScale(uint_t rs   )           const { auto fscale(m_scaleDict.find(rs)); return fscale != m_scaleDict.end() ? fscale->second : m_unknownScale; } 
inline const std::string& ClusterTreePlotter::valueType  (ValueType vt)           const { return m_valueDict.at(vt); } 

#endif

#ifdef ClusterTreePlotter_cxx
ClusterTreePlotter::ClusterTreePlotter(TTree *tree,const std::vector<std::string>& varList) {
  if (tree == 0) {
    printf("[ClusterTreePlotter::ClusterTreePlotter()] ABRT no valid pointer to ROOT tree, module inactive\n"); 
  } else { 
    for ( const auto& entry : varList ) { auto vid = Types::Variable::Convert::toId(entry); if ( vid != Types::Variable::Id::UNKNOWN ) { m_varIdList.push_back(vid); } }
    if ( m_varIdList.empty() ) { 
      printf("[ClusterTreePlotter::ClusterTreePlotter()] INFO initialize tree \042%s\042 with all branches\n",tree->GetName());
    } else { 
      printf("[ClusterTreePlotter::ClusterTreePlotter()] INFO initialize tree \042%s\042 with %zu branches\n",tree->GetName(),m_varIdList.size());
    }
    Init(tree);
  }
}

ClusterTreePlotter::~ClusterTreePlotter()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ClusterTreePlotter::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   auto nb(fChain->GetEntry(entry));
   if ( nb > 0 ) {
     if ( b_clusterResponse    == nullptr ) { clusterResponse    = Analysis::Utils::localResponseEM (*this); }
     if ( b_clusterResponseHAD == nullptr ) { clusterResponseHAD = Analysis::Utils::localResponseHAD(*this); }   
     if ( b_clusterResponseLCW == nullptr ) { clusterResponseLCW = Analysis::Utils::localResponseLCW(*this); }   
   }
   return nb;
}
Long64_t ClusterTreePlotter::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if ( fChain == nullptr ) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ClusterTreePlotter::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  std::string bname = "";
  std::sort(m_varIdList.begin(),m_varIdList.end());
  
  CT_SET_BRANCH( seqNumber   )
  CT_SET_BRANCH( seqNumber   )
  CT_SET_BRANCH( runNumber   )
  CT_SET_BRANCH( eventNumber )
  CT_SET_BRANCH( nPrimVtx    )
  CT_SET_BRANCH( avgMu       )

  CT_SET_BRANCH( truthE   )
  CT_SET_BRANCH( truthPt  )
  CT_SET_BRANCH( truthEta )
  CT_SET_BRANCH( truthPhi )
  CT_SET_BRANCH( truthPDG )

  CT_SET_BRANCH( truthJetMatchingRadius )
  CT_SET_BRANCH( truthJetE              )
  CT_SET_BRANCH( truthJetPt 	        )
  CT_SET_BRANCH( truthJetEta 	        )
  CT_SET_BRANCH( truthJetPhi 	        )
  CT_SET_BRANCH( jetCnt                 ) 
  CT_SET_BRANCH( jetCalE                ) 
  CT_SET_BRANCH( jetCalPt               )
  CT_SET_BRANCH( jetCalEta              )
  CT_SET_BRANCH( jetCalPhi              )
  CT_SET_BRANCH( jetRawE                )
  CT_SET_BRANCH( jetRawPt               )
  CT_SET_BRANCH( jetRawEta              )
  CT_SET_BRANCH( jetRawPhi              )
  CT_SET_BRANCH( jetNConst	        )

  CT_SET_BRANCH( nCluster                   )
  CT_SET_BRANCH( clusterIndex               )
  CT_SET_BRANCH( cluster_nCells             )
  CT_SET_BRANCH( cluster_nCells_tot         )
  CT_SET_BRANCH( clusterECalib              )
  CT_SET_BRANCH( clusterPtCalib             )
  CT_SET_BRANCH( clusterEtaCalib            )
  CT_SET_BRANCH( clusterPhiCalib            )
  CT_SET_BRANCH( cluster_sumCellECalib      )
  CT_SET_BRANCH( cluster_fracECalib         )
  CT_SET_BRANCH( cluster_fracECalib_ref     )
  CT_SET_BRANCH( clusterE                   )
  CT_SET_BRANCH( clusterPt                  )
  CT_SET_BRANCH( clusterEta                 )
  CT_SET_BRANCH( clusterPhi                 )
  CT_SET_BRANCH( cluster_sumCellE           )
  CT_SET_BRANCH( cluster_time               )
  CT_SET_BRANCH( cluster_fracE              )
  CT_SET_BRANCH( cluster_fracE_ref          )
  CT_SET_BRANCH( cluster_EM_PROBABILITY     )
  CT_SET_BRANCH( cluster_HAD_WEIGHT         )
  CT_SET_BRANCH( cluster_OOC_WEIGHT         )
  CT_SET_BRANCH( cluster_DM_WEIGHT          )
  CT_SET_BRANCH( cluster_ENG_CALIB_TOT      )
  CT_SET_BRANCH( cluster_ENG_CALIB_OUT_T    )
  CT_SET_BRANCH( cluster_ENG_CALIB_DEAD_TOT )
  CT_SET_BRANCH( cluster_ENG_CALIB_TOT      )
  CT_SET_BRANCH( cluster_ENG_CALIB_OUT_T    )
  CT_SET_BRANCH( cluster_ENG_CALIB_DEAD_TOT )
  CT_SET_BRANCH( cluster_CENTER_MAG         )
  CT_SET_BRANCH( cluster_FIRST_ENG_DENS     )
  CT_SET_BRANCH( cluster_FIRST_PHI          )
  CT_SET_BRANCH( cluster_FIRST_ETA          )
  CT_SET_BRANCH( cluster_SECOND_R           )
  CT_SET_BRANCH( cluster_SECOND_LAMBDA      )
  CT_SET_BRANCH( cluster_DELTA_PHI          )
  CT_SET_BRANCH( cluster_DELTA_THETA        )
  CT_SET_BRANCH( cluster_DELTA_ALPHA        )
  CT_SET_BRANCH( cluster_CENTER_X           )
  CT_SET_BRANCH( cluster_CENTER_Y           )
  CT_SET_BRANCH( cluster_CENTER_Z           )
  CT_SET_BRANCH( cluster_CENTER_LAMBDA      )
  CT_SET_BRANCH( cluster_LATERAL            )
  CT_SET_BRANCH( cluster_LONGITUDINAL       )
  CT_SET_BRANCH( cluster_ENG_FRAC_EM        )
  CT_SET_BRANCH( cluster_ENG_FRAC_MAX       )
  CT_SET_BRANCH( cluster_ENG_FRAC_CORE      )
  CT_SET_BRANCH( cluster_SECOND_ENG_DENS    )
  CT_SET_BRANCH( cluster_ISOLATION          )
  CT_SET_BRANCH( cluster_ENG_BAD_CELLS      )
  CT_SET_BRANCH( cluster_N_BAD_CELLS        )
  CT_SET_BRANCH( cluster_N_BAD_CELLS_CORR   )
  CT_SET_BRANCH( cluster_BAD_CELLS_CORR_E   )
  CT_SET_BRANCH( cluster_BADLARQ_FRAC       )
  CT_SET_BRANCH( cluster_ENG_POS            )
  CT_SET_BRANCH( cluster_SIGNIFICANCE       )
  CT_SET_BRANCH( cluster_CELL_SIGNIFICANCE  )
  CT_SET_BRANCH( cluster_CELL_SIG_SAMPLING  )
  CT_SET_BRANCH( cluster_AVG_LAR_Q          )
  CT_SET_BRANCH( cluster_AVG_TILE_Q         )
  CT_SET_BRANCH( cluster_ENG_BAD_HV_CELLS   )
  CT_SET_BRANCH( cluster_N_BAD_HV_CELLS     )
  CT_SET_BRANCH( cluster_PTD                )
  CT_SET_BRANCH( cluster_MASS               )
  CT_SET_BRANCH( cluster_SECOND_TIME        )
  CT_SET_BRANCH( clusterResponse            )
  CT_SET_BRANCH( clusterResponseHAD         )
  CT_SET_BRANCH( clusterResponseLCW         )
  CT_SET_BRANCH( clusterResponsePred        )

  if ( b_truthE  != nullptr )    { m_isParticle = true; printf("[ClusterPlotterTree::Init()] INFO clusters in particles analysis\n"); }
  if ( b_jetCalE != nullptr )    { m_isJet      = true; printf("[ClusterPlotterTree::Init()] INFO clusters in jets analysis\n"     ); }
  if ( m_isParticle && m_isJet ) {                      printf("[ClusterTreePlotter::Init()] WARN mixed mode!\n"                   ); }

  // print branches
  std::string::size_t lw(0);  
  for ( const auto& entry : m_listOfBranches ) { lw = std::max(lw,entry.first.length()); }
  int ncols(8); int icols(0); size_t ictr(0);
  size_t nreq = m_varIdList.size() > 0 ? m_varIdList.size() : m_listOfLeaves.size(); 
  printf("[ClusterPlotterTree::Init()] INFO list of branches found or accepted, out of %zu requested and %zu total:\n",nreq,m_listOfLeaves.size());
  for ( const auto& entry : m_listOfBranches ) {
    if ( entry.second ) { 
      if ( icols == 0 ) {   
	printf("\n[ClusterTreePlotter::Init()] INFO ");
      }
      printf("%-*.*s ",int(lw),int(lw),entry.first.c_str());
      ++icols; ++ictr;
      if ( icols == ncols ) { icols == 0; }
    }
  }
  printf("\n");
  if ( ictr < m_listOfLeaves.size() ) { 
    icols = 0;
    printf("[ClusterPlotterTree::Init()] INFO list of branches not found or not accepted, out of %zu not requested and %zu total:\n",m_listOfLeaves.size()-ictr,m_listOfLeaves.size());
    for ( const auto& entry : m_listOfBranches ) {
      if ( entry.second ) { 
	if ( icols == 0 ) {   
	  printf("\n[ClusterTreePlotter::Init()] INFO ");
	}
	printf("%-*.*s ",int(lw),int(lw),entry.first.c_str());
	++icols;
	if ( icols == ncols ) { icols == 0; }
      }
    }
  }
 
  Notify();
}

Bool_t ClusterTreePlotter::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.
  std::string kana = m_isJet ? "jet" : "single particle";
  printf("[ClusterTreePlotter::Init()] INFO booked %zu plots for %s analysis\n",m_listOfLeaves.size(),kana.c_str());
  return kTRUE;
}

void ClusterTreePlotter::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ClusterTreePlotter::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

bool ClusterTreePlotter::acceptBranch(const std::string& bname) { 
  if ( m_varIdList.empty() ) { return true; }
  Types::Variable::Id vid = Types::Variable::Convert::toId(bname); 
  return vid != Types::Variable::Id::UNKNOWN && std::binary_search(m_varIdList.begin(),m_varIdList.end(),vid);
}

std::ostream& operator<<(std::ostream& ostr,const ClusterTreePlotter& plotter) { return plotter.printContent(ostr); } 

  // bname = std::string("seqNumber"             ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&seqNumber  ,&b_seqNumber  ); }
  // bname = std::string("runNumber"             ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&runNumber  ,&b_runNumber  ); }
  // bname = std::string("eventNumber"           ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&eventNumber,&b_eventNumber); }
  // bname = std::string("nPrimVtx"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&nPrimVtx   ,&b_nPrimVtx   ); }
  // bname = std::string("avgMu"                 ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&avgMu      ,&b_avgMu      ); }

  // bname = std::string("truthE"                ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthE  ,&b_truthE  ); }
  // bname = std::string("truthPt"               ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthPt ,&b_truthPt ); }
  // bname = std::string("truthEta"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthEta,&b_truthEta); }
  // bname = std::string("truthPhi"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthPhi,&b_truthPhi); }
  // bname = std::string("truthPDG"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthPDG,&b_truthPDG); }

  // bname = std::string("truthJetMatchingRadius"); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthJetMatchingRadius,&b_truthJetMatchingRadius); }
  // bname = std::string("truthJetE"             ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthJetE             ,&b_truthJetE             ); }
  // bname = std::string("truthJetPt" 	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthJetPt 	        ,&b_truthJetPt            ); }
  // bname = std::string("truthJetEta" 	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthJetEta 	        ,&b_truthJetEta           ); }
  // bname = std::string("truthJetPhi" 	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&truthJetPhi 	        ,&b_truthJetPhi           ); }
  // bname = std::string("jetCnt"                ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),$jetCnt                ,&b_jetCnt                ); } 
  // bname = std::string("jetCalE"               ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetCalE               ,&b_jetCalE               ); } 
  // bname = std::string("jetCalPt"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetCalPt              ,&b_jetCalPt              ); }
  // bname = std::string("jetCalEta"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetCalEta             ,&b_jetCalEta             ); }
  // bname = std::string("jetCalPhi"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetCalPhi             ,&b_jetCalPhi             ); }
  // bname = std::string("jetRawE"	              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetRawE               ,&b_jetRawE               ); }
  // bname = std::string("jetRawPt"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetRawPt              ,&b_jetRawPt              ); }
  // bname = std::string("jetRawEta"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetRawEta             ,&b_jetRawEta             ); }
  // bname = std::string("jetRawPhi"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetRawPhi             ,&b_jetRawPhi             ); }
  // bname = std::string("jetNConst"	      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&jetNConst		,&b_jetNConst             ); }

  // bname = std::string("nCluster"                  ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&nCluster                  ,&b_nCluster                  ); }
  // bname = std::string("clusterIndex"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterIndex              ,&b_clusterIndex              ); }
  // bname = std::string("cluster_nCells"            ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_nCells            ,&b_cluster_nCells            ); }
  // bname = std::string("cluster_nCells_tot"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_nCells_tot        ,&b_cluster_nCells_tot        ); }
  // bname = std::string("clusterECalib"             ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterECalib             ,&b_clusterECalib             ); }
  // bname = std::string("clusterPtCalib"            ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterPtCalib            ,&b_clusterPtCalib            ); }
  // bname = std::string("clusterEtaCalib"           ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterEtaCalib           ,&b_clusterEtaCalib           ); }
  // bname = std::string("clusterPhiCalib"           ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterPhiCalib           ,&b_clusterPhiCalib           ); }
  // bname = std::string("cluster_sumCellECalib"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_sumCellECalib     ,&b_cluster_sumCellECalib     ); }
  // bname = std::string("cluster_fracECalib"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_fracECalib        ,&b_cluster_fracECalib        ); }
  // bname = std::string("cluster_fracECalib_ref"    ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_fracECalib_ref    ,&b_cluster_fracECalib_ref    ); }
  // bname = std::string("clusterE"                  ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterE                  ,&b_clusterE                  ); }
  // bname = std::string("clusterPt"                 ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterPt                 ,&b_clusterPt                 ); }
  // bname = std::string("clusterEta"                ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterEta                ,&b_clusterEta                ); }
  // bname = std::string("clusterPhi"                ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&clusterPhi                ,&b_clusterPhi                ); }
  // bname = std::string("cluster_sumCellE"          ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_sumCellE          ,&b_cluster_sumCellE          ); }
  // bname = std::string("cluster_time"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_time              ,&b_cluster_time              ); }
  // bname = std::string("cluster_fracE"             ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_fracE             ,&b_cluster_fracE             ); }
  // bname = std::string("cluster_fracE_ref"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_fracE_ref         ,&b_cluster_fracE_ref         ); }
  // bname = std::string("cluster_EM_PROBABILITY"    ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_EM_PROBABILITY    ,&b_cluster_EM_PROBABILITY    ); }
  // bname = std::string("cluster_HAD_WEIGHT"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_HAD_WEIGHT        ,&b_cluster_HAD_WEIGHT        ); }
  // bname = std::string("cluster_OOC_WEIGHT"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_OOC_WEIGHT        ,&b_cluster_OOC_WEIGHT        ); }
  // bname = std::string("cluster_DM_WEIGHT"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_DM_WEIGHT         ,&b_cluster_DM_WEIGHT         ); }
  // bname = std::string("cluster_ENG_CALIB_TOT"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_TOT     ,&b_cluster_ENG_CALIB_TOT     ); }
  // bname = std::string("cluster_ENG_CALIB_OUT_T"   ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_OUT_T   ,&b_cluster_ENG_CALIB_OUT_T   ); }
  // bname = std::string("cluster_ENG_CALIB_DEAD_TOT"); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_DEAD_TOT,&b_cluster_ENG_CALIB_DEAD_TOT); }
  // bname = std::string("cluster_FRAC_CALIB_EM"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_TOT     ,&b_cluster_ENG_CALIB_TOT     ); }
  // bname = std::string("cluster_FRAC_CALIB_HAD"    ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_OUT_T   ,&b_cluster_ENG_CALIB_OUT_T   ); }
  // bname = std::string("cluster_FRAC_CALIB_REST"   ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_CALIB_DEAD_TOT,&b_cluster_ENG_CALIB_DEAD_TOT); }
  // bname = std::string("cluster_CENTER_MAG"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CENTER_MAG        ,&b_cluster_CENTER_MAG        ); }
  // bname = std::string("cluster_FIRST_ENG_DENS"    ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_FIRST_ENG_DENS    ,&b_cluster_FIRST_ENG_DENS    ); }
  // bname = std::string("cluster_FIRST_PHI"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_FIRST_PHI         ,&b_cluster_FIRST_PHI         ); }
  // bname = std::string("cluster_FIRST_ETA"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_FIRST_ETA         ,&b_cluster_FIRST_ETA         ); }
  // bname = std::string("cluster_SECOND_R"          ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_SECOND_R          ,&b_cluster_SECOND_R          ); }
  // bname = std::string("cluster_SECOND_LAMBDA"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_SECOND_LAMBDA     ,&b_cluster_SECOND_LAMBDA     ); }
  // bname = std::string("cluster_DELTA_PHI"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_DELTA_PHI         ,&b_cluster_DELTA_PHI         ); }
  // bname = std::string("cluster_DELTA_THETA"       ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_DELTA_THETA       ,&b_cluster_DELTA_THETA       ); }
  // bname = std::string("cluster_DELTA_ALPHA"       ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_DELTA_ALPHA       ,&b_cluster_DELTA_ALPHA       ); }
  // bname = std::string("cluster_CENTER_X"          ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CENTER_X          ,&b_cluster_CENTER_X          ); }
  // bname = std::string("cluster_CENTER_Y"          ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CENTER_Y          ,&b_cluster_CENTER_Y          ); }
  // bname = std::string("cluster_CENTER_Z"          ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CENTER_Z          ,&b_cluster_CENTER_Z          ); }
  // bname = std::string("cluster_CENTER_LAMBDA"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CENTER_LAMBDA     ,&b_cluster_CENTER_LAMBDA     ); }
  // bname = std::string("cluster_LATERAL"           ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_LATERAL           ,&b_cluster_LATERAL           ); }
  // bname = std::string("cluster_LONGITUDINAL"      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_LONGITUDINAL      ,&b_cluster_LONGITUDINAL      ); }
  // bname = std::string("cluster_ENG_FRAC_EM"       ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_FRAC_EM       ,&b_cluster_ENG_FRAC_EM       ); }
  // bname = std::string("cluster_ENG_FRAC_MAX"      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_FRAC_MAX      ,&b_cluster_ENG_FRAC_MAX      ); }
  // bname = std::string("cluster_ENG_FRAC_CORE"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_FRAC_CORE     ,&b_cluster_ENG_FRAC_CORE     ); }
  // bname = std::string("cluster_SECOND_ENG_DENS"   ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_SECOND_ENG_DENS   ,&b_cluster_SECOND_ENG_DENS   ); }
  // bname = std::string("cluster_ISOLATION"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ISOLATION         ,&b_cluster_ISOLATION         ); }
  // bname = std::string("cluster_ENG_BAD_CELLS"     ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_BAD_CELLS     ,&b_cluster_ENG_BAD_CELLS     ); }
  // bname = std::string("cluster_N_BAD_CELLS"       ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_N_BAD_CELLS       ,&b_cluster_N_BAD_CELLS       ); }
  // bname = std::string("cluster_N_BAD_CELLS_CORR"  ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_N_BAD_CELLS_CORR  ,&b_cluster_N_BAD_CELLS_CORR  ); }
  // bname = std::string("cluster_BAD_CELLS_CORR_E"  ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_BAD_CELLS_CORR_E  ,&b_cluster_BAD_CELLS_CORR_E  ); }
  // bname = std::string("cluster_BADLARQ_FRAC"      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_BADLARQ_FRAC      ,&b_cluster_BADLARQ_FRAC      ); }
  // bname = std::string("cluster_ENG_POS"           ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_POS           ,&b_cluster_ENG_POS           ); }
  // bname = std::string("cluster_SIGNIFICANCE"      ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_SIGNIFICANCE      ,&b_cluster_SIGNIFICANCE      ); }
  // bname = std::string("cluster_CELL_SIGNIFICANCE" ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CELL_SIGNIFICANCE ,&b_cluster_CELL_SIGNIFICANCE ); }
  // bname = std::string("cluster_CELL_SIG_SAMPLING" ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_CELL_SIG_SAMPLING ,&b_cluster_CELL_SIG_SAMPLING ); }
  // bname = std::string("cluster_AVG_LAR_Q"         ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_AVG_LAR_Q         ,&b_cluster_AVG_LAR_Q         ); }
  // bname = std::string("cluster_AVG_TILE_Q"        ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_AVG_TILE_Q        ,&b_cluster_AVG_TILE_Q        ); }
  // bname = std::string("cluster_ENG_BAD_HV_CELLS"  ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_ENG_BAD_HV_CELLS  ,&b_cluster_ENG_BAD_HV_CELLS  ); }
  // bname = std::string("cluster_N_BAD_HV_CELLS"    ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_N_BAD_HV_CELLS    ,&b_cluster_N_BAD_HV_CELLS    ); }
  // bname = std::string("cluster_PTD"               ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_PTD               ,&b_cluster_PTD               ); }
  // bname = std::string("cluster_MASS"              ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_MASS              ,&b_cluster_MASS              ); }
  // bname = std::string("cluster_SECOND_TIME"       ); if ( acceptBranch(bname) && fChain->FindLeaf(bname.c_str()) != nullptr ) { m_listOfLeaves[bname] = true; fChain->SetBranchAddress(bname.c_str(),&cluster_SECOND_TIME       ,&b_cluster_SECOND_TIME       ); }
  // if ( fChain->FindLeaf("CalibratedE"               ) != nullptr ) { m_listOfLeaves["CalibratedE"               ] = true; fChain->SetBranchAddress("CalibratedE"                ,&CalibratedE               ,&b_CalibratedE               ); }
  // if ( fChain->FindLeaf("Delta_Calib_E "            ) != nullptr ) { m_listOfLeaves["Delta_Calib_E "            ] = true; fChain->SetBranchAddress("Delta_Calib_E "             ,&Delta_Calib_E             ,&b_Delta_Calib_E             ); }
  // if ( fChain->FindLeaf("Delta_E"                   ) != nullptr ) { m_listOfLeaves["Delta_E"                   ] = true; fChain->SetBranchAddress("Delta_E"                    ,&Delta_E                   ,&b_Delta_E                   ); }

#endif // #ifdef ClusterTreePlotter_cxx
