
#include "HistPlotter.h"

const Hist::Plotter::Link Hist::Plotter::Base::m_defaultLinkConst = { nullptr, nullptr };
Hist::Plotter::Link Hist::Plotter::Base::m_defaultLink = { nullptr, nullptr };

Hist::Plotter::Base::Base(const std::string& pname) : Base::Config(pname,false) { }
Hist::Plotter::Base::~Base() { }

int Hist::Plotter::Base::configure(const Key& pname,Filler* pfill,Selector* pselect) {
  auto fplot = m_plotMap.find(pname);
  if ( fplot == m_plotMap.end() ) { 
    m_plotMap.insert({pname,{pselect,pfill}}); 
    info("configure","added plot %s\n",pname.c_str());
  } else { 
    warning("configure","attempt ignored, plots %s already\n",pname.c_str());
  }
  checkMap(); 
  return static_cast<int>(m_plotMap.size());
}

bool Hist::Plotter::Base::checkMap() { 
  auto fmap(m_plotMap.begin()); 
  while ( fmap != m_plotMap.end() ) { 
    if ( Plotter::ptrFill(*fmap) == nullptr ) { fmap = m_plotMap.erase(fmap); } else { ++fmap; }  
  }
  return !m_plotMap.empty();
}

bool Hist::Plotter::Base::plot(double weight) {
  // loop registered plots
  bool hasFilled(false);
  for ( auto& entry : m_plotMap ) {
    if ( Plotter::ptrFill(entry) != nullptr ) { 
      if ( ( Plotter::ptrSelector(entry) == nullptr || Plotter::ptrSelector(entry)->accept() ) && Plotter::ptrFill(entry)->valid() ) { Plotter::ptrFill(entry)->fill(weight); hasFilled = true; }
    }
  }
  return hasFilled;
}
