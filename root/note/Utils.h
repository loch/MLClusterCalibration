// -*- c++ -*-
#ifndef UTILS_H
#define UTILS_H

#include <string>

///@brief Utilities for ML callibration analysis
namespace Utils {
  ///@brief String manipulations
  namespace String {
    ///@brief Range modes 
    enum class RangeMode { FromBegin = 0x01, FromEnd = 0x10, Unknown = 0x00 };
    ///@brief Extract range 
    static std::string range(const std::string& tstr,const char cstart,const char cstop,RangeMode mstart=RangeMode::FromEnd,RangeMode mstop=RangeMode::FromEnd) { 
      auto fstart = mstart == RangeMode::FromEnd ? tstr.find_last_of(cstart) : tstr.find_first_of(cstart); if ( fstart == std::string::npos ) { fstart = 0;             } else { ++fstart; }	 
      auto fstop  = mstop  == RangeMode::FromEnd ? tstr.find_last_of(cstop ) : tstr.find_first_of(cstop) ; if ( fstop  == std::string::npos ) { fstop  = tstr.length(); }
      return fstop > fstart ? tstr.substr(fstart,fstop-fstart) : tstr;
    }
    ///@brief Extract file name from full path
    static std::string fileName(const std::string& path,bool noExt=true,const std::string& pathSep="/",const std::string& extSep=".") {
      return range(path,pathSep.c_str()[0],extSep.c_str()[0],RangeMode::FromEnd,RangeMode::FromEnd);
    }
  } // Utils::String
} // Utils
#endif
