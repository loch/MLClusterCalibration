// -*- c++ -*-
#ifndef HISTAXIS_H
#define HISTAXIS_H

#include "Types.h"
#include "Units.h"
#include "Math.h"
#include "HistBins.h"

#include <string>

#include <cmath>

#define AXIS_LUP_ENTRY( VAR ) { VAR, Hist::Axis::Descriptor( Types::Variable::Convert::toLabel( VAR ), Hist::Bins::Access::binning( VAR ) ) } 

namespace Hist {
  ///@brief Axis description and fill
  namespace Axis {
    ///@brief Axis descriptor
    class Descriptor {
    public:
      ///@brief Default constructor with empty (invalid) description
      Descriptor();
      ///@brief Loaded constructor using @c Bins::Binning object
      ///@param title    axis title
      ///@param binning  bin description object 
      ///@param logValue axis values are logarithms
      ///@param m        axis display mode
      Descriptor(const std::string& title,const Bins::Binning& binning,const Units::Descriptor& udescr=Units::Descriptor(),bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
      ///@brief Loaded constructor using bin description parameters
      ///@param title    axis title
      ///@param nbins    number of bins
      ///@param xmin     lower boundary of value range
      ///@param xmax     upper boundary of value range
      ///@param logEqui  if @c true, the range between @c xmin and @c xmax is binned such that the bin width is constant in @f \log_{10}(x) @f and the delimiters are given in @f x @f . Default is @c true .
      ///@param logValue axis values are logarithms
      ///@param m        axis display mode
      Descriptor(const std::string& title,int nbins,double xmin,double xmax,const Units::Descriptor& udescr=Units::Descriptor(),bool logEqui=false,bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
      ///@brief Loaded constructor using vector of bin limits
      ///@param title    axis title
      ///@param blimits  vector of bin boundaries in acending order
      ///@param logValue axis values are logarithms
      ///@param m        axis display mode
      Descriptor(const std::string& title,const Bins::Delimiters& blimits,const Units::Descriptor& udescr=Units::Descriptor(),bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
      ///@brief Copy constructor
      ///@param adescr reference to non-modifiable axis description as a source of the copy  
      Descriptor(const Descriptor& adescr);
      ///@brief Assignment operator
      ///@param adescr reference to non-modifiable axis description with its content copied into this descriptor
      Descriptor& operator=(const Descriptor& adescr);
      ///@name Accessors to data
      ///@{
      const std::string&       title()    const;       ///< Axis title
      const Bins::Binning&     binning()  const;       ///< Binning
      const Units::Descriptor& unit()     const;       ///< Unit
      bool                     logValue() const;       ///< Log value filling
      Types::Display::Mode     mode()     const;       ///< Axis indexing mode
      ///@}
      ///@name Axis parameter retrieval
      ///@{
      double                        scale()             const; ///< Acces unit ccale factor 
      double                        scale(double value) const; ///< Apply unit scale factor
      int                           nbins()             const; ///< Number of bins
      double                        xmin()              const; ///< Lower boundary of value range
      double                        xmax()              const; ///< Upper boundary of value range
      bool                          isValid()           const; ///< Returns @c true if axis descriptor is valid 
      bool                          isRegular()         const; ///< Returns @c true if binning of axis is reguilar (equidistant in value space)
      const Hist::Bins::Delimiters& delimiters()        const; ///< Returns list of bin boundaries for irregular binning
      ///@}
    private:
      std::string          m_title    = { ""                             }; ///< Axis title 
      Bins::Binning        m_binning  = Bins::Binning()                   ; ///< Axis binning
      Units::Descriptor    m_unit     = Units::Descriptor()               ; ///< Axis unit  
      bool                 m_logValue = { false                          }; ///< Axis indicator for @f \log_{10}(x) @f labels instead of @f x @f labels.
      Types::Display::Mode m_mode     = { Types::Display::Mode::Standard }; ///< Axis indexing mode       
    };
    ///@name Axis description factories
    ///@{
    ///@brief Returns an empty (invalid) axis descriptor
    Descriptor description();
    ///@brief Returns a loaded axis desscriptor from provided binning object
    ///@param title    axis title
    ///@param binning  axis binning provided by @c Bins::Binning type descriptor
    ///@param udescr   axis unit provided by @c Units::Descriptor type descriptor (default is no unit)
    ///@param logValue axis values are @f \log_{10}(x) @f if @c true , $f x $f if @c false (default is @c false )
    ///@param m        axis indexing mode (default is @c Types::Display::Mode::Standard )
    Descriptor description(const std::string& title,const Bins::Binning& binning,const Units::Descriptor& udescr=Units::Descriptor(),bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
    ///@brief Returns a loaded axis descriptor from bin parameters
    ///@param title    axis title
    ///@param nbins    number of bin on axis
    ///@param xmin     lower boundary of axis value range
    ///@param xmax     upper boundary of axis value range
    ///@param udescr   axis unit provided by @c Units::Descriptor type descriptor (default is no unit)
    ///@param logEqui  axis has equidistant binning in @f \log_{10}(x) @f if @c true, else equidistant binning in @f x @f
    ///@param logValue axis values are @f \log_{10}(x) @f if @c true , $f x $f if @c false (default is @c false )
    ///@param m        axis indexing mode (default is @c Types::Display::Mode::Standard )
    Descriptor description(const std::string& title,int nbins,double xmin,double xmax,const Units::Descriptor& udescr=Units::Descriptor(),bool logEqui=false,bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
    ///@brief Returns a loaded axis descriptor from bin delimiters
    ///@param title    axis title
    ///@param blimits  axis binning provided by @c Bins::Delimiters object
    ///@param udescr   axis unit provided by @c Units::Descriptor type descriptor (default is no unit)
    ///@param logValue axis values are @f \log_{10}(x) @f if @c true , $f x $f if @c false (default is @c false )
    ///@param m        axis indexing mode (default is @c Types::Display::Mode::Standard )
    Descriptor description(const std::string& title,const Bins::Delimiters& blimits,const Units::Descriptor& udescr=Units::Descriptor(),bool logValue=false,Types::Display::Mode m=Types::Display::Mode::Standard);
    ///@}
    ///@brief Default reference to invalid axis descriptor
    static const Descriptor invalidDescriptor = Descriptor();
    //@name Axis description accessors
    ///@{
    const std::string&       title             (const Descriptor& adescr)             ; ///< Axis title
    int                      nbins             (const Descriptor& adescr)             ; ///< Axis number of bins
    double                   xmin              (const Descriptor& adescr)             ; ///< Axis lower boundary of value range
    double                   xmax              (const Descriptor& adescr)             ; ///< Axis upper boundary of value range
    Types::Display::Mode     mode              (const Descriptor& adescr)             ; ///< Axis display mode 
    bool                     logValue          (const Descriptor& adescr)             ; ///< Axis has logarithmic value bins
    bool                     isRegular         (const Descriptor& adescr)             ; ///< Axis has equidistant bins
    bool                     logEqui           (const Descriptor& adescr)             ; ///< Axis has bins equidistant in @f \log_{10}(x) @f in linear value space
    const Bins::Binning&     binning           (const Descriptor& adescr)             ; ///< Axis bin descriptor
    const Units::Descriptor& unit              (const Descriptor& adescr)             ; ///< Axis unit descriptor
    const std::string&       unitName          (const Descriptor& adescr)             ; ///< Axis unit name
    double                   unitScale         (const Descriptor& adescr)             ; ///< Axis unit scale factor
    const std::string&       unitTitle         (const Descriptor& adescr)             ; ///< Axis unit title (for plot axis)
    double                   scale             (const Descriptor& adescr,double value); ///< Axis unit scale factor application
    bool                     isValid           (const Descriptor& adescr)             ; ///< Axis descriptor is valid if @c true   
    const Bins::Delimiters&  delimiters        (const Descriptor& adescr)             ; ///< Axis bin limits in ascending order
    ///@}
    ///@name Multi-dimensional axis
    /// Supporting multi-dimensional axis descriptors
    ///@{
    /// Cordinates
    enum class Coord { X = 0, Y = 1, Z = 2, T = 3, Unknown = -1 };
    /// Pure abstract interface to group implementations 
    class IGroup { 
    public: 
      virtual ~IGroup() { }                                ///< Abstracts base class destructor
      virtual const Descriptor& axis(Coord c) const = 0;   ///< Axis descriptor retrieval
    };
    /// Templated implementation of multi-dimensional axis descriptor
    /// @tparam N number of dimensions, acceptable values are [1,...,4]
    template<int N> class Group : virtual public IGroup {
    private:
      std::array<Descriptor,N> m_axes;   ///< Internal store 
      size_t index(Coord c) const;       ///< Index translator from enum for index into store 
    public:
      ///@brief Default contrustor instantiates empty axes group
      Group();
      ///@brief Loaded constructor constructs group of axis descriptiors from a vector 
      ///@param adescr a vector of descriptors which is internally expected to be order @c [0,..,N-1] for @c N coordinates.
      Group(const std::vector<Descriptor>& adescr);
      ///@brief Base class destructor
      virtual ~Group() { }
      ///@brief Access axis for a given coordinate
      ///@param c one of the coordinate indicators defind in @c Hist::Axis::Coord
      ///@return A reference to a non-modifiable axis descriptor which maybe valid if a descriptor for the given coordinate exists but will be invalid if there is none available for @c c 
      virtual const Descriptor& axis(Coord c) const;
      ///@name Set axes for giev coordinates
      ///@param c      coordinate identifier (one of @c Hist::Axis::Coord )
      ///@param adescr axis descriptor for coordinate
      ///@{ 
      virtual bool add(Coord c,const Descriptor& adescr);  ///< Add axis descriptor for coordinate @c c but do not overwrite existing valid descriptor
      virtual bool set(Coord c,const Descriptor& adescr);  ///< Set axis descriptor for coordinate @c c and possibly overwrite existing descriptor 
      ///@} 
      ///@brief Dimension of axes group 
      virtual size_t dimension() const; 
    };
    typedef Group<1> Axis1D; ///< 1-dim axis description
    typedef Group<2> Axis2D; ///< 2-dim axis description 
    typedef Group<3> Axis3D; ///< 3-dim axis description
    typedef Group<4> Axis4D; ///< 3-dim axis description
    ///@name Presets
    ///@{ 
    typedef std::map<Types::Variable::Id,Descriptor> lookup_t;   ///< @c Axis::Descriptor mapped on variable type
    ///@}
    ///@brief Lookup
    static const lookup_t Lookup = { 
      AXIS_LUP_ENTRY( Types::Variable::seqNumber                   ), AXIS_LUP_ENTRY( Types::Variable::runNumber                   ), AXIS_LUP_ENTRY( Types::Variable::eventNumber                 ),
      AXIS_LUP_ENTRY( Types::Variable::nPrimVtx                    ), AXIS_LUP_ENTRY( Types::Variable::avgMu                       ), AXIS_LUP_ENTRY( Types::Variable::truthE                      ), 
      AXIS_LUP_ENTRY( Types::Variable::truthPt                     ), AXIS_LUP_ENTRY( Types::Variable::truthEta                    ), AXIS_LUP_ENTRY( Types::Variable::truthPhi                    ),
      AXIS_LUP_ENTRY( Types::Variable::truthPDG                    ), AXIS_LUP_ENTRY( Types::Variable::truthJetMatchingRadius      ), AXIS_LUP_ENTRY( Types::Variable::truthJetE                   ),
      AXIS_LUP_ENTRY( Types::Variable::truthJetPt                  ), AXIS_LUP_ENTRY( Types::Variable::truthJetEta                 ), AXIS_LUP_ENTRY( Types::Variable::truthJetPhi                 ),
      AXIS_LUP_ENTRY( Types::Variable::jetCnt                      ), AXIS_LUP_ENTRY( Types::Variable::jetCalE                     ), AXIS_LUP_ENTRY( Types::Variable::jetCalPt                    ),
      AXIS_LUP_ENTRY( Types::Variable::jetCalEta                   ), AXIS_LUP_ENTRY( Types::Variable::jetCalPhi                   ), AXIS_LUP_ENTRY( Types::Variable::jetRawE	                   ),
      AXIS_LUP_ENTRY( Types::Variable::jetRawPt                    ), AXIS_LUP_ENTRY( Types::Variable::jetRawEta                   ), AXIS_LUP_ENTRY( Types::Variable::jetRawPhi                   ),
      AXIS_LUP_ENTRY( Types::Variable::jetNConst                   ), AXIS_LUP_ENTRY( Types::Variable::nCluster                    ), AXIS_LUP_ENTRY( Types::Variable::clusterIndex                ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_nCells              ), AXIS_LUP_ENTRY( Types::Variable::cluster_nCells_tot          ), AXIS_LUP_ENTRY( Types::Variable::clusterECalib	           ),
      AXIS_LUP_ENTRY( Types::Variable::clusterPtCalib              ), AXIS_LUP_ENTRY( Types::Variable::clusterEtaCalib	           ), AXIS_LUP_ENTRY( Types::Variable::clusterPhiCalib	           ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_sumCellECalib       ), AXIS_LUP_ENTRY( Types::Variable::cluster_fracECalib          ), AXIS_LUP_ENTRY( Types::Variable::cluster_fracECalib_ref      ),
      AXIS_LUP_ENTRY( Types::Variable::clusterE	                   ), AXIS_LUP_ENTRY( Types::Variable::clusterPt	           ), AXIS_LUP_ENTRY( Types::Variable::clusterEta	           ),
      AXIS_LUP_ENTRY( Types::Variable::clusterPhi	           ), AXIS_LUP_ENTRY( Types::Variable::cluster_sumCellE            ), AXIS_LUP_ENTRY( Types::Variable::cluster_time 	           ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_fracE	           ), AXIS_LUP_ENTRY( Types::Variable::cluster_fracE_ref           ), AXIS_LUP_ENTRY( Types::Variable::cluster_HAD_WEIGHT          ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_OOC_WEIGHT          ), AXIS_LUP_ENTRY( Types::Variable::cluster_DM_WEIGHT           ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_TOT       ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_OUT_T     ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_DEAD_TOT  ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_FRAC_EM   ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_FRAC_HAD  ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_CALIB_FRAC_REST ), AXIS_LUP_ENTRY( Types::Variable::cluster_EM_PROBABILITY      ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_CENTER_MAG          ), AXIS_LUP_ENTRY( Types::Variable::cluster_FIRST_PHI           ), AXIS_LUP_ENTRY( Types::Variable::cluster_FIRST_ETA           ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_SECOND_R            ), AXIS_LUP_ENTRY( Types::Variable::cluster_SECOND_LAMBDA       ), AXIS_LUP_ENTRY( Types::Variable::cluster_DELTA_PHI           ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_DELTA_THETA         ), AXIS_LUP_ENTRY( Types::Variable::cluster_DELTA_ALPHA         ), AXIS_LUP_ENTRY( Types::Variable::cluster_CENTER_X            ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_CENTER_Y            ), AXIS_LUP_ENTRY( Types::Variable::cluster_CENTER_Z            ), AXIS_LUP_ENTRY( Types::Variable::cluster_CENTER_LAMBDA       ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_LATERAL             ), AXIS_LUP_ENTRY( Types::Variable::cluster_LONGITUDINAL        ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_FRAC_EM         ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_FRAC_MAX        ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_FRAC_CORE       ), AXIS_LUP_ENTRY( Types::Variable::cluster_SECOND_ENG_DENS     ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ISOLATION           ), AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_BAD_CELLS       ), AXIS_LUP_ENTRY( Types::Variable::cluster_N_BAD_CELLS         ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_N_BAD_CELLS_CORR    ), AXIS_LUP_ENTRY( Types::Variable::cluster_BAD_CELLS_CORR_E    ), AXIS_LUP_ENTRY( Types::Variable::cluster_BADLARQ_FRAC        ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_POS             ), AXIS_LUP_ENTRY( Types::Variable::cluster_SIGNIFICANCE        ), AXIS_LUP_ENTRY( Types::Variable::cluster_CELL_SIGNIFICANCE   ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_CELL_SIG_SAMPLING   ), AXIS_LUP_ENTRY( Types::Variable::cluster_AVG_LAR_Q           ), AXIS_LUP_ENTRY( Types::Variable::cluster_AVG_TILE_Q          ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_ENG_BAD_HV_CELLS    ), AXIS_LUP_ENTRY( Types::Variable::cluster_N_BAD_HV_CELLS      ), AXIS_LUP_ENTRY( Types::Variable::cluster_PTD	           ),
      AXIS_LUP_ENTRY( Types::Variable::cluster_MASS                ), AXIS_LUP_ENTRY( Types::Variable::cluster_SECOND_TIME         ), AXIS_LUP_ENTRY( Types::Variable::Entries                     )                                          
    };
    ///@brief Access lookup table
    struct Find { static const Descriptor& descriptor(Types::Variable::Id vid) { return Lookup.at(vid); } };
  } // Hist::Axis
} // Hist

// ------------------ Descriptor data retrieval
inline const std::string&            Hist::Axis::Descriptor::title()             const { return m_title; }
inline bool                          Hist::Axis::Descriptor::logValue()          const { return m_logValue; }
inline Types::Display::Mode          Hist::Axis::Descriptor::mode()              const { return m_mode; }
inline const Units::Descriptor&      Hist::Axis::Descriptor::unit()              const { return m_unit; }
inline double                        Hist::Axis::Descriptor::scale()             const { return unit().scale; }
inline double                        Hist::Axis::Descriptor::scale(double value) const { return isValid() ? unit().scale * value : value; }
inline const Hist::Bins::Binning&    Hist::Axis::Descriptor::binning()           const { return m_binning; }
inline int                           Hist::Axis::Descriptor::nbins()             const { return binning().nbins(); }
inline double                        Hist::Axis::Descriptor::xmin()              const { return binning().xmin(); }
inline double                        Hist::Axis::Descriptor::xmax()              const { return binning().xmax(); }
inline bool                          Hist::Axis::Descriptor::isValid()           const { return binning().isValid(); }
inline bool                          Hist::Axis::Descriptor::isRegular()         const { return binning().isRegular(); }   
inline const Hist::Bins::Delimiters& Hist::Axis::Descriptor::delimiters()        const { return binning().limits(); }

// ------------------ Description factories
inline Hist::Axis::Descriptor Hist::Axis::description() { return Descriptor(); }
inline Hist::Axis::Descriptor Hist::Axis::description(const std::string& title,const Bins::Binning& binning,const Units::Descriptor& udescr,bool logValue,Types::Display::Mode m) { 
  return Descriptor(title,binning,udescr,logValue,m); 
}
inline Hist::Axis::Descriptor Hist::Axis::description(const std::string& title,int nbins,double xmin,double xmax,const Units::Descriptor& udescr,bool logEqui,bool logValue,Types::Display::Mode m) { 
  return Descriptor(title,nbins,xmin,xmax,udescr,logEqui,logValue,m); 
}
inline Hist::Axis::Descriptor Hist::Axis::description(const std::string& title,const Bins::Delimiters& blimits,const Units::Descriptor& udescr,bool logValue,Types::Display::Mode m) { 
  return Descriptor(title,blimits,udescr,logValue,m); 
}

// ------------------ Access data in given descriptor object
inline const std::string&         Hist::Axis::title    (const Descriptor& adescr)              { return adescr.title(); };
inline int                        Hist::Axis::nbins    (const Descriptor& adescr)              { return adescr.binning().nbins(); };
inline double                     Hist::Axis::xmin     (const Descriptor& adescr)              { return adescr.binning().xmin (); };
inline double                     Hist::Axis::xmax     (const Descriptor& adescr)              { return adescr.binning().xmax (); };
inline Types::Display::Mode       Hist::Axis::mode     (const Descriptor& adescr)              { return adescr.mode(); };
inline bool                       Hist::Axis::logValue (const Descriptor& adescr)              { return adescr.logValue() ; };
inline bool                       Hist::Axis::isRegular(const Descriptor& adescr)              { return adescr.isRegular(); };
inline bool                       Hist::Axis::logEqui  (const Descriptor& adescr)              { return !adescr.isRegular(); };
inline const std::string&         Hist::Axis::unitName (const Descriptor& adescr)              { return adescr.unit().name; }
inline double                     Hist::Axis::unitScale(const Descriptor& adescr)              { return adescr.unit().scale; }
inline const Hist::Bins::Binning& Hist::Axis::binning  (const Descriptor& adescr)              { return adescr.binning(); }
inline const Units::Descriptor&   Hist::Axis::unit     (const Descriptor& adescr)              { return adescr.unit(); }
inline double                     Hist::Axis::scale    (const Descriptor& adescr,double value) { return adescr.scale(value); }
inline bool                       Hist::Axis::isValid  (const Descriptor& adescr)              { return adescr.isValid(); }

#include "HistAxis.icc"
#endif
