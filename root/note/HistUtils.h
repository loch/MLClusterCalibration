// -*- c++ -*-
#ifndef HISTUTILS_H
#define HISTUTILS_H

#include <TDirectory.h>

#include <string>

namespace Hist {
  namespace Utils {
    bool hasDir(const std::string& dname); 
    TDirectory* changeDir(const std::string& dname,bool createIfNot=true);
  }
}
#endif
