
#include "HistUtils.h"

bool Hist::Utils::hasDir(const std::string& dname) { return TDirectory::CurrentDirectory().load()->GetDirectory(dname.c_str(),true,"Hist::Utils::hasDir()") != nullptr; }

TDirectory* Hist::Utils::changeDir(const std::string& dname,bool createIfNot) { 
  return createIfNot
    ? TDirectory::CurrentDirectory().load()->mkdir(dname.c_str(),dname.c_str(),true)
    : TDirectory::CurrentDirectory().load()->GetDirectory(dname.c_str(),true,"Hist::Utils::changeDir()");
}
