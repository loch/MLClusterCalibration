
#include "HistBins.h"

#include <cmath>

bool Hist::Bins::isOrdered(const Delimiters& blimits) {
  if ( blimits.size() < 2 ) { return !blimits.empty(); }
  size_t idx(1);
  while ( idx < blimits.size() && blimits.at(idx-1) < blimits.at(idx) ) { ++idx; }
  return idx == blimits.size();
}

Hist::Bins::Delimiters Hist::Bins::toLogValue(const Delimiters& blimits) { 
  Delimiters llimits; if ( llimits.capacity() < blimits.size() ) { llimits.reserve(blimits.size()); }
  for ( auto value : blimits ) { llimits.push_back(toLogValue(value)); }
  return llimits;
} 

Hist::Bins::Delimiters Hist::Bins::delimiters(int nbins,double xmin,double xmax,bool logEqui,bool logValue) {
  if ( !valid(nbins,xmin,xmax,(logEqui || logValue))  ) { return Delimiters(); }
  Delimiters blimits; if ( blimits.capacity() < static_cast<size_t>(nbins+1) ) { blimits.reserve(nbins+1); } 
  double vmin = logEqui || logValue ? std::log10(xmin) : xmin;
  double vmax = logEqui || logValue ? std::log10(xmax) : xmax; 
  double vwid((xmax-xmin)/(static_cast<double>(nbins)));
  if ( !logEqui ) { 
    while ( vmin < vmax ) { blimits.push_back(vmin); vmin += vwid; } 
    if ( blimits.back() < vmax ) { blimits.push_back(vmax); }
  } else { 
    double xlim(xmin); 
    while ( xlim < xmax ) { blimits.push_back(xlim); vmin += vwid; xlim = std::pow(10,vmin); }
    if ( blimits.back() < xmax ) { blimits.push_back(xmax); }
  }
  return blimits;
}

Hist::Bins::Descriptor::Descriptor() { }
Hist::Bins::Descriptor::Descriptor(int nb,double vmin,double vmax,bool logEqui,bool logValue) 
  : isValid(valid(nb,vmin,vmax,logValue))
  , nbins(nb)
  , xmin(!logValue ? vmin : toLogValue(vmin))
  , xmax(!logValue ? vmax : toLogValue(vmax))
  , isRegular(!logEqui) 
{ }
Hist::Bins::Descriptor::Descriptor(const Delimiters& blimits,bool logValue) 
  : isValid(!valid(blimits,false))
  , binning(!logValue ? blimits         : toLogValue(blimits)        )
  , nbins(static_cast<int>(blimits.size())-1)
  , xmin   (!logValue ? blimits.front() : toLogValue(blimits.front()))
  , xmax   (!logValue ? blimits.back()  : toLogValue(blimits.back() ))
  , isRegular(false) 
{ }
Hist::Bins::Descriptor::Descriptor(const Descriptor& bdescr) 
  : isValid(bdescr.isValid)
  , binning(bdescr.binning)
  , nbins(bdescr.nbins)
  , xmin(bdescr.xmin)
  , xmax(bdescr.xmax)
  , isRegular(bdescr.isRegular) 
{ } 
Hist::Bins::Descriptor& Hist::Bins::Descriptor::operator=(const Descriptor& bdescr) { 
  if ( this == &bdescr ) { return *this; }
  nbins     = bdescr.nbins    ;
  xmin      = bdescr.xmin     ;
  xmax      = bdescr.xmax     ;
  binning   = bdescr.binning  ;
  isValid   = bdescr.isValid  ;
  isRegular = bdescr.isRegular;  
  return *this;
}

Hist::Bins::Binning::Binning() { }
Hist::Bins::Binning::Binning(const Descriptor& bdescr) : m_bdescr(bdescr) { }
Hist::Bins::Binning::Binning(int nbins,double xmax,double xmin,bool logEqui,bool logValue) : m_bdescr(description(nbins,xmin,xmax,logEqui,logValue)) { }
Hist::Bins::Binning::Binning(const Delimiters& blimits,bool logValue)                      : m_bdescr(description(blimits,logValue)) { }

int Hist::Bins::Binning::index(double value,Types::Display::Mode m) const {
  if ( !this->isValid() || !this->inRange(value) ) { return -1; }
  int ibin = m == Types::Display::Mode::ROOT ? 1 : 0; 
  if ( this->isRegular() ) { 
    ibin += static_cast<int>((value-xmin())/(xmax()-xmin())); 
  } else {
    size_t kb(0); 
    while ( kb < limits().size() && value > limits().at(kb) ) { ++ibin; ++kb; }
  }
  return ibin;
} 
