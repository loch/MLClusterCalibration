// -*- c++ -*-
#ifndef HISTBINS_H
#define HISTBINS_H

#include "Types.h"
#include "Math.h"

#include <string>
#include <vector>

#include <limits>

#include <cmath>

#define BIN_DEF( NAME, N, XMIN, XMAX, LOGV, LOGEQ )	\
  std::string name = { #NAME } ;			\
  int    nbin = N ;					\
  double xmin = XMIN ;					\
  double xmax = XMAX ;					\
  bool   logValue = LOGV ;				\
  bool   logEqui  = LOGEQ

#define BIN_NBIN ( NSPACE ) NSPACE ::nbin
#define BIN_XMIN ( NSPACE ) NSPACE ::xmin
#define BIN_XMAX ( NSPACE ) NSPACE ::xmax
#define BIN_LOGV ( NSPACE ) NSPACE ::logValue
#define BIN_LOGEQ( NSPACE ) NSPACE ::logEqui

namespace Hist { 
  ///@brief Bin descriptor
  ///
  /// This namespace collects all functionality related to binning. This includes equidistant as well as non-equidistant
  /// binning. 
  namespace Bins {
    ///@brief List of bin delimiters in ascending order (no enforcement implementd in type) 
    typedef std::vector<double> Delimiters;
    ///@name Check bin description validity
    ///@param blimits array of strictly ascending bin delimiters
    ///@param nbins   number of bins
    ///@param xmin    lower boundary of binned value range
    ///@param xmax    upper boundary of binned value range
    ///@param forLog  bins are for a log-like binning if @c true (all bein delimiters and ranges must be greater 0) or not if @c false
    ///@{
    bool isOrdered(const Delimiters& blimits);                       ///< Checks if given array of delimiters is in ascending order
    bool valid(int nbins,double xmin,double xmax,bool forLog=false); ///< Checks if bin limits are valid.
    bool valid(const Delimiters& blimits,bool forLog=false);         ///< Checks if bin array is valid 
    ///@}
    ///@name Bin descriptor manipulations
    ///@param value   value @f x @f to be converted to @f \log_{10}(x) @f .
    ///@param blimits array of values @f x_{i} @f to be converted to @f \log_{10}(x_{i}) @f .
    ///@{
    double     toLogValue(double value);                     ///< Convert value @f x @f to @f \log_{10}(x) @f 
    Delimiters toLogValue(const Delimiters& blimits);        ///< Convert vector of values @f {x_{i} @f to @f \log_{10}(x_{i}) @f
    ///@}
    ///@brief Generate delimiters
    ///
    /// This function sets up binning in a given range with a given number of bins in equidistant bins.
    /// The optional distance measure is @f \log(x) @f . Another option converts the given range @f [x_{\rm min},x_{\rm max}] @f 
    /// to @f [\log(x_{\rm min}),\log(x_{\rm max})] @f .
    ///@param nbins    number of bins
    ///@param xmin     lower boundary of value range
    ///@param xmax     upper boundary of value range
    ///@param logEqui  if @c true, the range between @c xmin and @c xmax is binned such that the bin width is constant in @f \log_{10}(x) @f and the delimiters are given in @f x @f . Default is @c true .
    ///@param logValue if @c true, both @c xmin and @c xmax are first converted such the returned delimiters are spaced at a constant distance in @f \log_{10}(x) @f and the delimiter values are given in @f \log_{10}(x) @f as well.
    ///                Default is @c false .
    ///@return The function returns an empty vector if any of @c nbins , @c xmin or @c xmax do not describe a valid binning in linear (both @c logEqui=false and @c logValue=false ) or in logarithmic space (@c logEqui=true or @c logValue=true ).     
    Delimiters delimiters(int nbins,double xmin,double xmax,bool logEqui=true,bool logValue=false);
    ///@brief Bin Descriptor
    ///
    /// This data structure collects all parameters for various binning options.
    struct Descriptor {
      bool       isValid    = false;       ///< Valid descriptor if @c true
      Delimiters binning    = { };         ///< Bin delimiters
      int        nbins      = { -1 };      ///< Number of bins
      double     xmin       = { 0. };      ///< Lower delimiter of binning range
      double     xmax       = { 0. };      ///< Upper delimiter of binning range
      bool       isRegular  = { true };    ///< Regular (equidistant) binning if @c true
      ///@brief Default constructor generates empty (invalid) descriptor
      Descriptor();
      ///@brief Descriptor generated from number of bins and value range
      ///@param nb       number of bins
      ///@param vmin     lower limit of value range
      ///@param vmax     upper limit of value range
      ///@param logEqui  binning is equidistant in log(value) with delimiters in value
      ///@param logValue range is converted to log(vmin) and log(vmax) 
      Descriptor(int nb,double vmin,double vmax,bool logEqui=false,bool logValue=false);
      ///@brief Descriptor generated from list of boundaries
      ///@param blimits  vector of bin boundaries in ascending order 
      ///@param logValue boundaries are converted to log
      Descriptor(const Delimiters& blimits,bool logValue=false);
      ///@brief Copy constructor
      Descriptor(const Descriptor& bdescr);
      ///@brief Assignment operator
      Descriptor& operator=(const Descriptor& bdescr);
    };
    ///@name Bin descriptor factories
    ///@param nbins    number of bins
    ///@param xmin     lower edge of value range
    ///@param xmax     upper edge of value range
    ///@param logEqui  generate binning between @c xmin and @c xmax with bins equidistant in @f \log_{10}(x) @f if @c true
    ///@param logValue convert range  into @f \log_{10}(x_{\rm min}) @f) and @f \log_{10}(x_{\rm max}) @f or bin edges @f x_{i} @f into @f \log_{10}(x_{i}) @f if @c true
    ///@param blimits  vector of bin boundaries in ascending order 
    ///@{
    Descriptor description();                                                                         ///< Generate empty (invalid) descriptor
    Descriptor description(int nbins,double xmin,double xmax,bool logEqui=false,bool logValue=false); ///< Generate loaded descriptor (validity depends on provided arguments)
    Descriptor description(const Delimiters& blimits,bool logValue=false);                            ///< ///< Generate loaded descriptor from list of boundaries
    ///@}
    ///@brief Binning module
    class Binning {
    private:
      ///@brief Description of binning
      Descriptor m_bdescr = { Descriptor() };
    public:
      ///@brief Default constructor generates empty/invalid binning
      Binning();
      ///@brief Construct with bin description
      ///@param bdescr a binning descriptor of type @c Descriptor
      Binning(const Descriptor& bdescr);
      ///@brief Binning generated from number of bins and value range
      ///@param nb       number of bins
      ///@param vmin     lower limit of value range
      ///@param vmax     upper limit of value range
      ///@param logEqui  binning is equidistant in log(value) with delimiters in value
      ///@param logValue range is converted to log(vmin) and log(vmax) 
      Binning(int nbins,double xmax,double xmin,bool logEqui=false,bool logValue=false);
      ///@brief Generate binning from list of boundaries
      ///@param blimits  vector of bin boundaries in ascending order 
      ///@param logValue boundaries are converted to log
      Binning(const Delimiters& blimits,bool logValue=false);
      ///@name Access to binning
      ///@{
      bool              isValid()    const;  ///< Returns @c true if binning is valid
      int               nbins()      const;  ///< Returns number of bins
      double            xmin()       const;  ///< Returns lower boundary of value range
      double            xmax()       const;  ///< Returns upper boundary of value range
      const Delimiters& limits()     const;  ///< Returns list of bin boundaries
      bool              isRegular()  const;  ///< Returns @c true if binning is regular (equidistant in value space)
      ///@}
      ///@brief Check if value is in range
      ///
      /// The boundary checks follow the @c ROOT convention.
      ///@param value value to be tested
      ///@return @c true if value is in range of binning else @c false
      bool inRange(double value) const;
      ///@brief Find bin index for a given value
      ///@param value value to be binned
      ///@param m     indexing mode, @c Mode::Standard for [0,...,Nbins-1] and @c Mode::ROOT for [1,...,Nbins]. 
      int index(double value,Types::Display::Mode m=Types::Display::Mode::Standard) const;
    };
    ///@name Bin configurations
    ///
    /// Commmon binning definitions
    namespace Default {
      // struct EnergyBins {
      // 	std::string         name;
      // 	int                 nbin;
      // 	double              xmin;
      // 	double              xmax;
      // 	std::vector<double> bins;
      // 	bool                logValue;
      // 	bool                logEqui;
      // 	EnergyBins(const std::string& nm,int nb,double xmn,double xmx,const std::vector<double>& bns,bool lVal,bool lEqui) 
      // 	  : name(nm)
      // 	  , nbin(nb)
      // 	  , xmin(xmn)
      // 	  , xmax(xmx)
      // 	  , bins(bns)
      // 	  , logValue(lVal)
      // 	  , logEqui(lEqui)
      // 	{ }
      // };
      struct E              { BIN_DEF( energy        ,        90,         0.1,std::pow(10,3.5),         false,         true ); }; ///< Energy bins
      struct pT             { BIN_DEF( pT            ,        90,         0.1,std::pow(10,3.5),         false,         true ); }; ///< Transverse momentum bins
      struct Rap            { BIN_DEF( rapidity      ,        99,       -4.95,            4.95,         false,        false ); }; ///< Rapidity bins
      struct Eta            { BIN_DEF( pseudorapidity,        99,       -4.95,            4.95,         false,        false ); }; ///< Pseudorapidity bins
      struct Phi            { BIN_DEF( azimuth       ,        64, -Math::pi(),      Math::pi(),         false,        false ); }; ///< Azimuthal bins
      struct Norm           { BIN_DEF( norm          ,       101,      -0.005,           1.005,         false,        false ); }; ///< Binning for normalized quantities [0,...,1]
      struct Fraction       { BIN_DEF( fraction      ,       101,      -0.005,           1.005,         false,        false ); }; ///< Binning for normalized quantities [0,...,1]
      struct Percent        { BIN_DEF( percent       ,       101,       -0.05,          100.05,         false,        false ); }; ///< Binning for nomralized quantities in percent [0,...,100%]
      struct Response       { BIN_DEF( response      ,       501,      -0.005,           5.005,         false,        false ); }; ///< Binning for responses
      struct Collisions     { BIN_DEF( interaction   ,        70,        -0.5,            69.5,         false,        false ); }; ///< Binning for @f N_{\rm PV} @f and @f \mu @f 
      struct Time           { BIN_DEF( time          ,       301,      -150.5,           150.5,         false,        false ); }; ///< Binning for @f N_{\rm PV} @f and @f \mu @f 
      struct TimeDispersion { BIN_DEF( timedispersion,        40,       0.001,           1000.,         false,         true ); }; ///< Binning for @f N_{\rm PV} @f and @f \mu @f 
      struct Significance   { BIN_DEF( significance  ,       600,          0.,            150.,         false,        false ); }; ///< Binning for signal significance
    } // Hist::Bins::Default
    static Default::E              E              = Default::E             ();
    static Default::pT             pT             = Default::pT            ();
    static Default::Rap            Rap            = Default::Rap           ();
    static Default::Eta            Eta            = Default::Eta           ();
    static Default::Phi            Phi            = Default::Phi           ();
    static Default::Norm           Norm           = Default::Norm          ();
    static Default::Fraction       Fraction       = Default::Fraction      ();
    static Default::Percent        Percent        = Default::Percent       ();
    static Default::Response       Response       = Default::Response      ();
    static Default::Collisions     Collisions     = Default::Collisions    ();
    static Default::Time           Time           = Default::Time          ();
    static Default::TimeDispersion TimeDispersion = Default::TimeDispersion();
    static Default::Significance   Significance   = Default::Significance  (); 
    // Lookup
    typedef std::map<Types::Variable::Id,Binning> lookup_t;
    static const lookup_t Lookup = { 
      { Types::Variable::seqNumber                  , Binning() },
      { Types::Variable::runNumber                  , Binning() },
      { Types::Variable::eventNumber                , Binning() },
      { Types::Variable::nPrimVtx                   , Binning(Collisions.nbin    ,Collisions.xmin    ,Collisions.xmax    ,Collisions.logEqui    ,Collisions.logValue     ) },
      { Types::Variable::avgMu                      , Binning(Collisions.nbin    ,Collisions.xmin    ,Collisions.xmax    ,Collisions.logEqui    ,Collisions.logValue     ) },
      { Types::Variable::truthE                     , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::truthPt                    , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::truthEta                   , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::truthPhi                   , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::truthPDG                   , Binning() },
      { Types::Variable::truthJetMatchingRadius     , Binning(100                ,0.                 ,1.                 ,false                 ,false                   ) },
      { Types::Variable::truthJetE                  , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::truthJetPt                 , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::truthJetEta                , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::truthJetPhi                , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::jetCnt                     , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::jetCalE                    , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::jetCalPt                   , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::jetCalEta                  , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::jetCalPhi                  , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::jetRawE	            , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::jetRawPt                   , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::jetRawEta                  , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::jetRawPhi                  , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::jetNConst                  , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::nCluster                   , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::clusterIndex               , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::cluster_nCells             , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::cluster_nCells_tot         , Binning(100                ,0.                 ,100.               ,false                 ,false                   ) },
      { Types::Variable::clusterECalib	            , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::clusterPtCalib             , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::clusterEtaCalib	    , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::clusterPhiCalib	    , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::cluster_sumCellECalib      , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_fracECalib         , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_fracECalib_ref     , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::clusterE	            , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::clusterPt	            , Binning(pT.nbin            ,pT.xmin            ,pT.xmax            ,pT.logEqui            ,pT.logValue             ) },
      { Types::Variable::clusterEta	            , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::clusterPhi	            , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::cluster_sumCellE           , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_time 	            , Binning(Time.nbin          ,Time.xmin          ,Time.xmax          ,Time.logEqui          ,Time.logValue           ) },
      { Types::Variable::cluster_fracE	            , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_fracE_ref          , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_HAD_WEIGHT         , Binning(80                 ,0.                 ,4.                 ,false                 ,false                   ) },
      { Types::Variable::cluster_OOC_WEIGHT         , Binning(80                 ,0.                 ,4.                 ,false                 ,false                   ) },
      { Types::Variable::cluster_DM_WEIGHT	    , Binning(80                 ,0.                 ,4.                 ,false                 ,false                   ) },
      { Types::Variable::cluster_ENG_CALIB_TOT      , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_ENG_CALIB_OUT_T    , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_ENG_CALIB_DEAD_TOT , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_ENG_CALIB_FRAC_EM  , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_ENG_CALIB_FRAC_HAD , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_ENG_CALIB_FRAC_REST, Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_EM_PROBABILITY     , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_CENTER_MAG	    , Binning(100                ,0.                 ,5000.              ,false                 ,false                   ) },             
      { Types::Variable::cluster_FIRST_ENG_DENS     , Binning(80                 ,-8.                ,-4.                ,false                 ,true                    ) },             
      { Types::Variable::cluster_FIRST_PHI	    , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::cluster_FIRST_ETA	    , Binning(Eta.nbin           ,Eta.xmin           ,Eta.xmax           ,Eta.logEqui           ,Eta.logValue            ) },
      { Types::Variable::cluster_SECOND_R           , Binning() },
      { Types::Variable::cluster_SECOND_LAMBDA      , Binning() },
      { Types::Variable::cluster_DELTA_PHI	    , Binning(Phi.nbin           ,Phi.xmin           ,Phi.xmax           ,Phi.logEqui           ,Phi.logValue            ) },
      { Types::Variable::cluster_DELTA_THETA	    , Binning() },
      { Types::Variable::cluster_DELTA_ALPHA	    , Binning() },
      { Types::Variable::cluster_CENTER_X           , Binning() },
      { Types::Variable::cluster_CENTER_Y	    , Binning() },
      { Types::Variable::cluster_CENTER_Z	    , Binning() },
      { Types::Variable::cluster_CENTER_LAMBDA      , Binning(70                 ,0.001              ,10000.             ,true                  ,false                   ) }, 
      { Types::Variable::cluster_LATERAL	    , Binning(Norm.nbin          ,Norm.xmin          ,Norm.xmax          ,Norm.logEqui          ,Norm.logValue           ) },
      { Types::Variable::cluster_LONGITUDINAL       , Binning(Norm.nbin          ,Norm.xmin          ,Norm.xmax          ,Norm.logEqui          ,Norm.logValue           ) },
      { Types::Variable::cluster_ENG_FRAC_EM        , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_ENG_FRAC_MAX       , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_ENG_FRAC_CORE      , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_SECOND_ENG_DENS    , Binning() },
      { Types::Variable::cluster_ISOLATION          , Binning(Fraction.nbin      ,Fraction.xmin      ,Fraction.xmax      ,Fraction.logEqui      ,Fraction.logValue       ) },
      { Types::Variable::cluster_ENG_BAD_CELLS      , Binning() },
      { Types::Variable::cluster_N_BAD_CELLS        , Binning() },
      { Types::Variable::cluster_N_BAD_CELLS_CORR   , Binning() },
      { Types::Variable::cluster_BAD_CELLS_CORR_E   , Binning() },
      { Types::Variable::cluster_BADLARQ_FRAC       , Binning() },
      { Types::Variable::cluster_ENG_POS	    , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_SIGNIFICANCE       , Binning(Significance.nbin  ,Significance.xmin  ,Significance.xmax  ,Significance.logEqui  ,Significance.logValue   ) },
      { Types::Variable::cluster_CELL_SIGNIFICANCE  , Binning(Significance.nbin  ,Significance.xmin  ,Significance.xmax  ,Significance.logEqui  ,Significance.logValue   ) },
      { Types::Variable::cluster_CELL_SIG_SAMPLING  , Binning() },
      { Types::Variable::cluster_AVG_LAR_Q          , Binning() },
      { Types::Variable::cluster_AVG_TILE_Q         , Binning() },
      { Types::Variable::cluster_ENG_BAD_HV_CELLS   , Binning() },
      { Types::Variable::cluster_N_BAD_HV_CELLS     , Binning() },
      { Types::Variable::cluster_PTD	            , Binning(Norm.nbin          ,Norm.xmin          ,Norm.xmax          ,Norm.logEqui          ,Norm.logValue           ) },
      { Types::Variable::cluster_MASS               , Binning(E.nbin             ,E.xmin             ,E.xmax             ,E.logEqui             ,E.logValue              ) },
      { Types::Variable::cluster_SECOND_TIME        , Binning(TimeDispersion.nbin,TimeDispersion.xmin,TimeDispersion.xmax,TimeDispersion.logEqui,TimeDispersion.logValue ) },
    };
    ///@brief Lookup binning
    struct Access { static const Binning& binning(Types::Variable::Id vid) { return Lookup.at(vid); } };
  } // Hist::Bins
} // Hist
inline bool   Hist::Bins::valid(int nbins,double xmin,double xmax,bool forLog) { return forLog ? nbins > 0 && xmin < xmax && xmin > 0      : nbins > 0 && xmin < xmax; }
inline bool   Hist::Bins::valid(const Delimiters& blimits,bool forLog)         { return forLog ? isOrdered(blimits) && blimits.front() > 0 : isOrdered(blimits)      ; }
inline double Hist::Bins::toLogValue(double value)                             { return value > 0 ? std::log10(value) : std::numeric_limits<double>::lowest()         ; }

inline Hist::Bins::Descriptor Hist::Bins::description()                                                             { return Descriptor(); }
inline Hist::Bins::Descriptor Hist::Bins::description(int nbins,double xmin,double xmax,bool logEqui,bool logValue) { return logEqui ? Descriptor(delimiters(nbins,xmin,xmax,logEqui,logValue),logValue) : Descriptor(nbins,xmin,xmax,logEqui,logValue); }
inline Hist::Bins::Descriptor Hist::Bins::description(const Delimiters& blimits,bool logValue)                      { return Descriptor(blimits,logValue); }

inline bool                          Hist::Bins::Binning::isValid()             const { return m_bdescr.isValid;                  }
inline int                           Hist::Bins::Binning::nbins()               const { return m_bdescr.nbins;                    }
inline double                        Hist::Bins::Binning::xmin()                const { return m_bdescr.xmin;                     }
inline double                        Hist::Bins::Binning::xmax()                const { return m_bdescr.xmax;                     }
inline const Hist::Bins::Delimiters& Hist::Bins::Binning::limits()              const { return m_bdescr.binning;                  }
inline bool                          Hist::Bins::Binning::isRegular()           const { return m_bdescr.isRegular;                }
inline bool                          Hist::Bins::Binning::inRange(double value) const { return value >= xmin() && value < xmax(); }
#endif
