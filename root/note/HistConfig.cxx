
#include <TH1.h>
#include <TDirectory.h>

#include "HistConfig.h"

#include <limits>

#include <cstdio>
#include <cmath>

//////////////
// Registry //
//////////////

// -- static data and functions
std::string              Hist::Manager::Registry::m_defdir   = Hist::defaultDirectoryName;
Hist::Manager::Registry* Hist::Manager::Registry::m_instance = nullptr; 

bool Hist::Manager::Registry::add(const std::string& dname,base_t* hptr) {
  if ( hptr == nullptr ) { return false; }
  auto key(makeKey(dname,std::string(hptr->GetName())));
  auto fkey(this->find(key));
  if ( fkey == this->end() ) { 
    (*this)[key] = hptr;
    return true;
  } else {
    printf("[Hist::Manager::Registry::add(...)] WARN histogram \042%s\042 already registered, cannot be overwritten",hptr->GetName());
    return false;
  }
}

int Hist::Manager::Registry::write(bool noEmpties) {
  // write only filled distributions
  int nrec(0);
  if ( noEmpties ) {
    // write only non-empty histograms
    for ( auto& entry  : *this ) {
      base_t* hptr = Hist::Manager::histBasePtr(entry);
      if ( hptr != nullptr && hptr->GetEntries() > 0.) {
	const std::string& dname = this->directoryName(Hist::Manager::key(entry));
        if ( this->makeDir(dname) ) {
          TDirectory* thisDir  = TDirectory::CurrentDirectory(); thisDir->mkdir(dname.c_str(),dname.c_str(),true)->cd(); hptr->Write(); ++nrec; thisDir->cd(); 
        } else {
	  hptr->Write(); ++nrec;
	}
      } // valid non-empty histogram
    } // loop on registry
  } else { 
    // write everything
    for ( auto& entry  : *this ) {
      base_t* hptr = Hist::Manager::histBasePtr(entry);
      if ( hptr != nullptr ) {
	const std::string& dname = this->directoryName(Hist::Manager::key(entry));
        if ( this->makeDir(dname) ) {
          TDirectory* thisDir  = TDirectory::CurrentDirectory(); thisDir->mkdir(dname.c_str(),dname.c_str(),true)->cd(); hptr->Write(); ++nrec; thisDir->cd(); 
        } else {
	  hptr->Write(); ++nrec;
	}
      } // valid histogram, possibly empty
    } // loop on registry
  } // histogram selection
  return nrec;
}

////////////////
// Descriptor //
////////////////

Hist::Descriptor::Descriptor() 
{ }

Hist::Descriptor::Descriptor(const std::string& name,const std::string& title,const AxisVariableLinks& avlinks,bool isProfile)
  : m_name(name)
  , m_title(title)
  , m_axis(avlinks)
  , m_isProfile(isProfile)
{ 
  this->f_setDimension(); 
  this->f_messageInit("from store");
}
    
Hist::Descriptor::Descriptor(const std::string& name,const std::string& title,const std::vector<AxisVariable>& avars,const std::vector<AxisCoordinate>& acoords,const std::vector<Axis::Descriptor>& adescrs,bool isProfile)
  : m_name(name)
  , m_title(title)
  , m_isProfile(isProfile)
{
  if ( avars.size() == acoords.size() && avars.size() == adescrs.size() ) { 
    for ( size_t idx(0); idx < avars.size(); ++idx ) { m_axis[avars.at(idx)] = { acoords.at(idx), adescrs.at(idx) }; }
  }
  this->f_setDimension();
  this->f_messageInit("from arrays");
}

void Hist::Descriptor::f_setDimension() { m_dimension = 0; for ( const auto& entry : m_axis ) { if ( nbins(entry.first) > 0 ) { ++m_dimension; } } }

void Hist::Descriptor::f_messageInit(const std::string& msg) const {
  if ( msg != "" ) {
    printf("[Hist::Descriptor::Descriptor()] INFO descriptor [%s|%s] is set up for %i dimensions with %zu configured axes %s\n",this->name().c_str(),this->title().c_str(),this->dimension(),m_axis.size(),msg.c_str());
  } else {
    printf("[Hist::Descriptor::Descriptor()] INFO descriptor [%s|%s] is set up for %i dimensions with %zu configured axes\n"   ,this->name().c_str(),this->title().c_str(),this->dimension(),m_axis.size());
  }
}

Hist::AxisVariable Hist::Descriptor::f_var(AxisCoordinate c) {
  AxisCoordinateLinks::const_iterator fcor(m_coord.find(c));
  AxisVariable avar = AxisVariable::UNKNOWN;
  if ( fcor == m_coord.end() ) { 
    for ( auto entry : m_axis ) { 
      if ( std::get<0>(entry.second) == c ) { m_coord[c] = entry.first; avar = entry.first; }
    }
  } else {
    avar = fcor->second;
  }
  return avar;
}

Hist::AxisVariable Hist::Descriptor::f_var(AxisCoordinate c) const { AxisCoordinateLinks::const_iterator fcor(m_coord.find(c)); return fcor != m_coord.end() ? fcor->second : AxisVariable::UNKNOWN; }

