
#include "ObjectPlotter.h"

Object::Plotter::Base::Base(const std::string& name) : Object::Plotter::Base::base_t(name,true) { }

Object::Plotter::Base::~Base() { }

bool Object::Plotter::Base::configure(Object::Plotter::VariableId vid,const Object::Plotter::HistPlotter& hplotter) { 
  // check if already known
  auto fvid(m_plotMap.find(vid));
  if ( fvid != m_plotMap.end() ) { 
    warning("configure()","histogram plotter for variable %s already booked, ignored\n",Types::Variable::Convert::toChar(vid));
    return false; 
  }
  // add to plot map
  m_plotMap.insert(PlotMap::value_type(vid,hplotter));
  return true; 
}

bool Object::Plotter::Base::plot(double weight) {
  size_t nplots(0); 
  for ( auto& entry : m_plotMap ) { if ( entry.second.plot(weight) ) { ++nplots; } }
  return nplots == m_plotMap.size();
}
