// -*- c++ -*-
#ifndef CONFIG_H
#define CONFIG_H

///@brief 
/// This file is for the local configuration of the analysis. It provides macros controlling the 
/// compilation of the code.
#define PRAGMA_STR1( x ) #x
#define PRAGMA_STR2( x ) PRAGMA_STR1( x )
#define PRAGMA_MSG ( x ) message (__FILE__"(" PRAGMA_STR2( __LINE__ ) ") : " #x 
#define PRAGMA_MSG_PAR ( x, p ) message (__FILE__"(" PRAGMA_STR2( __LINE__ ) ") : " #x PRAGMA_STR2( p )
#define PRAGMA_MSG_FORM ( x, p ) (__FILE__"(" PRAGMA_STR2( __LINE__ ) ") : " #x PRAGMA_STR2( p )

#define XSTR(x) STR(x)
#define STR(x) #x

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#if __cplusplus > 201402L
#  define HAS_OPTIONAL
#  pragma message ("std::optional is available for c++ version " XSTR(__cplusplus))
#else
#  pragma message ("std::optional not available for c++ version " XSTR(__cplusplus))
#endif
#pragma GCC diagnostic pop

#ifndef RVALUE_DOUBLE_T
#  ifdef HAS_OPTIONAL
#    include <optional>
#    define RVALUE_DOUBLE_T std::optional<double>
#    define RVALUE_DOUBLE( X ) X.value
#  else
#    define RVALUE_DOUBLE_T double
#    define RVALUE_DOUBLE( X ) X
#  endif 
#endif

#endif
