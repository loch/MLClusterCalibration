// -*- c++ -*-
#ifndef BASE_H
#define BASE_H

#include <string>
#include <map>

#include <algorithm>

#include <cstdio>
#include <cstdarg>

///@brief Basic module features 
namespace Base {
  struct Msg { 
    ///@name Message level names
    ///@{
    static const char* msgLevelError  ; ///< Message level @c ERROR indicator
    static const char* msgLevelInfo   ; ///< Message level @c INFO indicator
    static const char* msgLevelWarning; ///< Message level @c WARNING indicator
    static const char* msgLevelDebug  ; ///< Message level @c DEBUG indicator
    static const char* msgLevelUnknown; ///< Unknown message level
    ///@}
    ///@brief Indicators for message level
    enum class Level { ERROR = 0x100, WARNING = 0x001, DEBUG = 0x011, INFO = 0x021, UNKNOWN = 0x000 };
    ///@name Dictionaries
    ///@{
    typedef std::map<Level,const char*> LevelDescriptionLookup;   ///< Dictionary type to look up description by identifier   
    typedef std::map<Level,const char*> LevelNameLookup;          ///< Dictionary type to look up name by identifier   
    typedef std::map<const char*,Level> LevelIdLookup;            ///< Dictionary type to look up identifier by name   
    static const LevelDescriptionLookup LevelIdToDescriptionDict; ///< Description of message levels
    static const LevelNameLookup        LevelIdToNameDict;        ///< Dictionary translating indicator to level name
    static const LevelIdLookup          LevelNameToIdDict;        ///< Dictionary translating name to indicator
    static const char* levelFromId  (Level mlvl);                 ///< Get name (array of @c char ) from indicator
    static Level       levelFromName(const char* name);           ///< Get indicator from name (array of @c char )
    ///@}
  }; // Base::Msg
  namespace Default {
    static const char* objectName ="DefaultObject"; ///< Default name of object
  } // Base::Default
  ///@brief Module base class provide basic configurations and functionality
  class Config {
  public:
    ///@brief Default constructor with presets
    Config();
    ///@brief Useful constructor
    ///@param name     module name
    ///@param isActive client provided flag indicate that the module is active (if @c true ) 
    Config(const std::string& name,bool isActive);
    ///@brief Base class destructor
    virtual ~Config();
    ///@brief Name of module
    virtual const std::string& name()     const;
    ///@brief Module status
    virtual bool               isActive() const;
  protected:
    ///@name Client support
    ///@{
    virtual void setActive(bool flag=true);                                                                         ///< Set module to active state if @c flag=true (default) else turn module off
    virtual void print    (const std::string& module,const std::string& level,const std::string& fmtStr,...) const; ///< General message
    virtual void info     (const std::string& module,const std::string& fmtStr,...)                          const; ///< INFO level message
    virtual void error    (const std::string& module,const std::string& fmtStr,...)                          const; ///< ERROR level message
    virtual void warning  (const std::string& module,const std::string& fmtStr,...)                          const; ///< WARNING level message
    virtual void debug    (const std::string& module,const std::string& fmtStr,...)                          const; ///< DEBUG level message
    ///@}
  private:
    ///@name State and identification stores
    ///@{
    std::string m_name   = { Base::Default::objectName }; ///< Module name
    bool        m_active = { false }                    ; ///< Module state
    ///@};
    ///@name Helpers
    ///@{
    void f_print(const char* module,const char* level,const char* fmtStr,va_list arglist) const; ///< Print functionality
    bool f_msgLevelCheck(const std::string& msglevel) const;                                     ///< Check if given message level is known
    ///@} 
  }; // Base::Config
} // Base

inline bool Base::Config::f_msgLevelCheck(const std::string& msglevel) const { return Base::Msg::levelFromName(msglevel.c_str()) != Base::Msg::Level::UNKNOWN; }
#endif
