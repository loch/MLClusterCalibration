
#include "Base.h"

///////////////
// Messaging //
///////////////

const char* Base::Msg::msgLevelError   = "ABRT";
const char* Base::Msg::msgLevelInfo    = "INFO";
const char* Base::Msg::msgLevelWarning = "WARN";
const char* Base::Msg::msgLevelDebug   = "DEBG";
const char* Base::Msg::msgLevelUnknown = "UNKN";

const Base::Msg::LevelDescriptionLookup Base::Msg::LevelIdToDescriptionDict = { 
  { Level::ERROR  , "ERROR (irrecoverable runtime error suggests termination of program)"              },
  { Level::WARNING, "INFO (message related to normal performance)"                                     },
  { Level::DEBUG  , "Warning (non-fatal performance issue may yield unexpected results or behaviours)" },
  { Level::INFO   , "Debug (temporary message checking on run time states, data content and features)" },
  { Level::UNKNOWN, "Unknown (given level is not known)"                                               }
};
const Base::Msg::LevelNameLookup Base::Msg::LevelIdToNameDict = { 
  { Level::ERROR  , msgLevelError   },
  { Level::INFO   , msgLevelInfo    },
  { Level::WARNING, msgLevelWarning },
  { Level::DEBUG  , msgLevelDebug   },
  { Level::UNKNOWN, msgLevelUnknown }
};
const Base::Msg::LevelIdLookup Base::Msg::LevelNameToIdDict = { 
  { msgLevelError   ,Level::ERROR   },
  { msgLevelInfo    ,Level::INFO    },
  { msgLevelWarning ,Level::WARNING },
  { msgLevelDebug   ,Level::DEBUG   },
  { msgLevelUnknown ,Level::UNKNOWN }
};
const char*      Base::Msg::levelFromId  (Level mlvl)       { return LevelIdToNameDict.at(mlvl); }
Base::Msg::Level Base::Msg::levelFromName(const char* name) { auto flvl(LevelNameToIdDict.find(name)); return flvl != LevelNameToIdDict.end() ? flvl->second : Level::UNKNOWN; }

/////////////////////////////////////
// Module configuration base class //
/////////////////////////////////////

Base::Config::Config() { }
Base::Config::Config(const std::string& name,bool isActive) : m_name(name), m_active(isActive) { }

Base::Config::~Config() { }

const std::string& Base::Config::name() const { return m_name; }

void Base::Config::f_print(const char* module,const char* level,const char* fmtStr,va_list arglist) const {
  // --
  static const  char*  _lbrkt  = "["  ;
  static const  char*  _rbrkt  = "]"  ;
  static const  char*  _fill   = "...";
  constexpr int        _lsize  = sizeof(_lbrkt)/sizeof(char);
  constexpr int        _rsize  = sizeof(_rbrkt)/sizeof(char);
  constexpr int        _fsize  = sizeof(_fill) /sizeof(char);
  constexpr int        _mwidth = 32;
  constexpr int        _lwidth = 4;
  constexpr int        _nwidth = _mwidth - ( _fsize + _rsize );
  static char          _buffer[256];
  // --
  auto nwrds = sprintf(_buffer,"%*.*s%s:%s%*.*s",_lsize,_lsize,_lbrkt,this->name().c_str(),module,_rsize,_rsize,_rbrkt);
  std::string lstr(_buffer);
  if ( nwrds  > _mwidth ) { lstr = lstr.substr(0,_nwidth) + std::string(_fill) + std::string(_rbrkt); }
  sprintf(_buffer,"%-*.*s %-*.*s %s",_mwidth,_mwidth,lstr.c_str(),_lwidth,_lwidth,level,fmtStr);
  vprintf(_buffer,arglist);
}

void Base::Config::print(const std::string& module,const std::string& level,const std::string& fmtStr,...) const {
  va_list arglist;
  va_start(arglist,fmtStr); 
  this->f_print(module.c_str(),level.c_str(),fmtStr.c_str(),arglist);
  va_end(arglist);
}

void Base::Config::info(const std::string& module,const std::string& fmtStr,...) const {
  va_list arglist;
  va_start(arglist,fmtStr); 
  this->f_print(module.c_str(),Base::Msg::msgLevelInfo,fmtStr.c_str(),arglist);
  va_end(arglist);
}

void Base::Config::error(const std::string& module,const std::string& fmtStr,...) const {
  va_list arglist;
  va_start(arglist,fmtStr); 
  this->f_print(module.c_str(),Base::Msg::msgLevelError,fmtStr.c_str(),arglist);
  va_end(arglist);
}

void Base::Config::warning(const std::string& module,const std::string& fmtStr,...) const {
  va_list arglist;
  va_start(arglist,fmtStr); 
  this->f_print(module.c_str(),Base::Msg::msgLevelWarning,fmtStr.c_str(),arglist);
  va_end(arglist);
}

void Base::Config::debug(const std::string& module,const std::string& fmtStr,...) const {
  va_list arglist;
  va_start(arglist,fmtStr); 
  this->f_print(module.c_str(),Base::Msg::msgLevelDebug,fmtStr.c_str(),arglist);
  va_end(arglist);
}

void Base::Config::setActive(bool flag) { m_active = flag; }
bool Base::Config::isActive() const { return m_active; }
