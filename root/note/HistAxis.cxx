
#include "HistAxis.h"

Hist::Axis::Descriptor::Descriptor() { };
Hist::Axis::Descriptor::Descriptor(const std::string& title,const Bins::Binning& binning,const Units::Descriptor& udescr,bool logValue,Types::Display::Mode m)
  : m_title   (title) 
  , m_binning (binning) 
  , m_unit    (udescr)
  , m_logValue(logValue)
  , m_mode    (m)
{ }
Hist::Axis::Descriptor::Descriptor(const std::string& title,int nbins,double xmin,double xmax,const Units::Descriptor& udescr,bool logEqui,bool logValue,Types::Display::Mode m)
  : m_title   (title) 
  , m_binning (Bins::description(nbins,xmin,xmax,logEqui,logValue)) 
  , m_unit    (udescr)
  , m_logValue(logValue)
  , m_mode    (m)
{ } 
Hist::Axis::Descriptor::Descriptor(const std::string& title,const Bins::Delimiters& blimits,const Units::Descriptor& udescr,bool logValue,Types::Display::Mode m)
  : m_title   (title) 
  , m_binning (Bins::description(blimits,logValue)) 
  , m_unit    (udescr)
  , m_logValue(logValue)
  , m_mode    (m)
{ } 
Hist::Axis::Descriptor::Descriptor(const Descriptor& adescr)
  : m_title   (adescr.m_title) 
  , m_binning (adescr.m_binning) 
  , m_unit    (adescr.m_unit) 
  , m_logValue(adescr.m_logValue)
  , m_mode    (adescr.m_mode)
{ }
Hist::Axis::Descriptor& Hist::Axis::Descriptor::operator=(const Descriptor& adescr) {
  if ( this == &adescr ) { return *this; }
  m_title    = adescr.m_title;
  m_binning  = adescr.m_binning;
  m_unit     = adescr.m_unit;
  m_logValue = adescr.m_logValue;
  m_mode     = adescr.m_mode;
  return *this;
}

//       const std::string&       title()    const;       ///< Axis title
//       const Bins::Binning&     binning()  const;       ///< Binning
//       const Units::Descriptor& unit()     const;       ///< Unit
//       bool                     logValue() const;       ///< Log value filling
//       Types::Display::Mode     mode()     const;       ///< Axis indexing mode
//       ///@}
//       ///@name Axis parameter retrieval
//       ///@{
//       double scale()             const;
//       double scale(double value) const;
//       int    nbins()             const;
//       double xmin()              const;
//       double xmax()              const;
//       bool   isValid()           const;





// const std::string& Hist::Axis::unitTitle(const Descriptor& adescr) {
//   std::string utitle;
//   switch ( mode(adescr) ) {
//   case Types::Display::Mode::Standard: utitle = unit(adescr).axis;       break;
//   case Types::Display::Mode::Latex   : utitle = unit(adescr).latex_axis; break; 
//   case Types::Display::Mode::ROOT    : utitle = unit(adescr).root_axis;  break;
//   default: break;
//   }
//   return utitle;
// }
