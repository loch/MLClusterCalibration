// -*- c++ -*-
#ifndef MATH_H
#define MATH_H

#include <cmath>

#include <limits>

#ifndef M_PI
#  define M_PI 3.14159265358979323846
#endif

#ifndef M_PIl
# define M_PIl 3.141592653589793238462643383279502884L
#endif

/// @brief A collection of math constants and functions
namespace Math {
    /// @brief Numerical constants
    namespace Constants {
        constexpr double pi     = M_PI;   ///< @f \pi @f 
        constexpr double twopi  = 2.*pi;  ///< @f 2\pi @f
        constexpr double halfpi = pi/2.;  ///< @f \pi/2 @f
        constexpr double phimin = -pi;    ///< Lower boundary of azimuth range (ATLAS convention)
        constexpr double phimax = pi;     ///< Upper boundary of azimuth range (ATLAS convention)
    }
    /// @name Functional accessors to constants
    /// @{ 
    double pi();               ///< Returns @c Constants::pi
    double twopi();            ///< Returns @c Constants::twopi
    double halfpi();           ///< Returns @c Constants::halfpi
    double phimin();           ///< Returns @c Constants::phimin
    double phimax();           ///< Returns @c Constants::phimax
    double phistd(double phi); ///< Returns azimuth @f \phi @f such that @f -\pi < \phi \ \pi @f
    ///@}
}

inline double Math::pi()     { return Constants::pi;    }
inline double Math::twopi()  { return Constants::twopi; }  
inline double Math::halfpi() { return Constants::halfpi;} 
inline double Math::phimin() { return Constants::phimin;}
inline double Math::phimax() { return Constants::phimax;}
inline double Math::phistd(double phi) { 
  while ( phi < Constants::phimin ) { phi += twopi(); } 
  while ( phi > Constants::phimax ) { phi -= twopi(); } 
  return phi;
}
#endif
