#ifndef HISTGROUP_H
#define HISTGROUP_H

// -- package includes
#include "Type.h"
#include "Variable.h"
#include "Axis.h"

// -- ROOT --
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TProfile2D.h>

// -- stdlib containers --
#include <string>
#include <vector>
#include <map>
#include <tuple>

// -- stdlib numericals --
#include <limits>
#include <cmath>

// -- stdlib I/O 
#include <cstdio>

// minimum value
#ifndef CTC_VMIN
#define CTC_VMIN( ENTRY ) std::get<0>( ENTRY )
#endif
// maximum value
#ifndef CTCVMAX      
#define CTC_VMAX( ENTRY ) std::get<1>( ENTRY )
#endif
// minimum log10(value)
#ifndef CTC_LMIN
#define CTC_LMIN( ENTRY ) std::get<2>( ENTRY )
#endif
// maximum log10(value)
#ifndef CTC_LMAX
#define CTC_LMAX( ENTRY ) std::get<3>( ENTRY )
#endif
// log value flag
#ifndef CTC_LOGF
#define CTC_LOGF( ENTRY ) std::get<4>( ENTRY )
#endif
// variable type
#ifndef CTC_VART
#define CTC_VART( ENTRY ) std::get<5>( ENTRY )
#endif
// minimum range (from quantile)
#ifndef CTC_RMIN
#define CTC_RMIN( ENTRY ) std::get<6>( ENTRY )
#endif
// maximum range (from quantile)
#ifndef CTC_RMAX
#define CTC_RMAX( ENTRY ) std::get<7>( ENTRY )
#endif
// minimum log10(range)
#ifndef CTC_LRMN
#define CTC_LRMN( ENTRY ) std::get<8>( ENTRY )
#endif
// maximum log10(range)
#ifndef CTC_LRMX
#define CTC_LRMX( ENTRY ) std::get<9>( ENTRY )
#endif

#define HDM_TITLE ( ENTRY ) std::get<0>( ENTRY )
#define HDM_NBIN  ( ENTRY ) std::get<1>( ENTRY )
#define HDM_XMIN  ( ENTRY ) std::get<2>( ENTRY )
#define HDM_XMAX  ( ENTRY ) std::get<3>( ENTRY )
#define HDM_XTITLE( ENTRY ) std::get<4>( ENTRY )
#define HDM_YTITLE( ENTRY ) std::get<5>( ENTRY )
#define HDM_LVALUE( ENTRY ) std::get<6>( ENTRY )
#define HDM_TYPE  ( ENTRY ) std::get<7>( ENTRY )
#define HDM_HPTR  ( ENTRY ) std::get<8>( ENTRY )

///@brief Collection of constants, data containers, look-up tables and descriptors
namespace HistGroup {
///@brief Histogram management
    namespace Histogram {
        
        
        
        
        
        
        
        
        
    ///@brief Description
    namespace Description {
        ///@brief Tags
        enum class Coordinate { X = 0x01, Y = 0x02, Z = 0x04, UNKNOWN = 0x00 };
        ///@brief Interface 
        class IDescriptor {
        public:
            virtual ~IDescriptor() { }
            ///@name Information access
            ///@{
            virtual const Histogram::Name&           name     ()                              = 0; ///< Histogram name
            virtual const Histogram::Title&          title    ()                              = 0; ///< Histogram title
            virtual       Histogram::BinNumber       nBins    (Coordinate axis=Coordinate::X) = 0; ///< Number of bins on given axis (default x-axis)
            virtual       Histogram::BinValue        upper    (Coordinate axis=Coordinate::X) = 0; ///< Lower value range limit for given axis (default x-axis)
            virtual       Histogram::BinValue        lower    (Coordinate axis=Coordinate::X) = 0; ///< Upper value range limit for given axis (default y-axis)
            virtual const Histogram::Binning&        binning  (Coordinate axis=Coordinate::X) = 0; ///< List of non-equidistant bin delimiters for given axis (default x-axis)
            virtual const Histogram::AxisDescriptor& axisDescr(Coordinate axis=Coordinate::X) = 0; ///< Full description of given axis (default x-axis)
            virtual       Histogram::LogFlag         logFlag  (Coordinate axis=Coordinate::X) = 0; ///< Flag indicating log values for given axis (default x-axis)
            ///@}
        };
        typedef std::tuple<Name,Title,AxisDescriptor,AxisDescriptor> Descriptor;
        typedef std::map<Key,Descriptor>                             DescriptorLookup;
        typedef std::map<Key,std::tuple<Name,Title> >                Collection;
        static const Collection = { 
	  { Content::clusterE              , {"clusterE"             ,"E_{clus}^{EM}"         } }, 
	  { Content::clusterEta            , {"clusterEta"           ,"y_{clus}^{EM}"         } }, 
	  { Content::clusterEtaCalib       , {"clusterEtaCalib"      ,"y_{clus}^{LCW}"        } }, 
	  { Content::cluster_CENTER_LAMBDA , {"cluster_CENTER_LAMBDA","#lambda_{center}"      } }, 
	  { Content::cluster_LONGITUDINAL  , {"cluster_LONGITUDINAL" ,"#LTm_{long}^{2}#GT"    } }, 
	  { Content::cluster_LATERAL       , {"cluster_LATERAL"      ,"#LTm_{lat}^{2}#GT"     } }, 
	  { Content::cluster_ENG_FRAC_EM   , {"cluster_ENG_FRAC_EM"  ,"f_{emc}"               } }, 
	  { Content::cluster_FIRST_ENG_DENS, {"cluster_FIRST_ENG_DEN","#LT#rho_{cell}^{EM}#GT"} }, 
	  { Content::cluster_SIGNIFICANCE  , {"cluster_SIGNIFICANCE" ,"#zeta_{clus}^{EM}"     } }, 
	  { Content::cluster_PTD           , {"cluster_PTD"          ,"p_{T}D"                } }, 
	  { Content::cluster_SECOND_TIME   , {"cluster_SECOND_TIME"  ,"#LT#sigma_{t}^{2}#GT"  } }, 
	  { Content::avgMu                 , {"avgMu"                ,"#LT#mu#GT"             } }, 
	  { Content::nPrimVtx              , {"nPrimVtx"             ,"N_{PV}"                } }, 
	  { Content::ResponseMeasure       , {"ResponseMeasure"      ,"R_{clus}^{EM}"         } }, 
	  { Content::ResponsePrediction    , {"ResponsePrediction"   ,"R_{clus}^{EM,predict}" } }, 
	  { Content::CalibratedE           , {"CalibratedE"          ,"E_{clus}^{cal}"        } }, 
	  { Content::cluster_ENG_CALIB_TOT , {"cluster_ENG_CALIB_TOT","E_{clus}^{dep}"        } }, 
	  { Content::UNKNOWN               , {"UNKNOWN"              ,"UNKNOWN"               } } 
	};
    } // HistGroup::Histogram::Description


    typedef std::map<name_t,hist_t*> collection_t;  
    typedef unsigned long            bitmask_t;
    static collection_t collection = collection_t();
    static int write(bool noEmpties=true) {
        int nhw(0);  
        if ( noEmpties ) {
            for ( auto& entry : collection ) {  if ( entry.second != nullptr && entry.second->GetEntries() != 0. ) { entry.second->Write(); ++nhw; } }
            return nhw;
        } else {
            for ( auto& entry : collection ) { if ( entry.second != nullptr ) { entry.second->Write(); ++nhw; } }
        }
        return nhw;
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,int nb,double xmin,double xmax,const std::string& xtitle="",const std::string& ytitle="") {
        if ( collection.find(hname) == collection.end() ) {
            H* h = new H(hname.cstr(),htitle.c_str(),nb,xmin,xmax); h->Sumw2(); 
            if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
            if ( ytitle != "" ) { h->GetYaxis()->SetTitle(ytitle.c_str()); } 
            collection.insert( { hname, h } );
            printf("[HistGroup::book(...)] INFO 1-d histogram with name \042%s\042 booked\n",h->GetName()); 
            return h;
        } else {
            printf("[HistGroup::book(...)] WARN 1-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str()); 
            return nullptr; 
        } 
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& bins,const std::string& xtitle="",const std::string& ytitle="") {
        if ( bins.size() < 2 ) { 
            printf("[HistGroup::book(...)] WARN 1-d histogram with name \042%s\042 cannot be booked, bin description has only %zu entries\n",hname.c_str(),bins.size());
            return nullptr; 
        }
        if ( collection.find(hname) != collection.end() ) {
           printf("[HistGroup::book(...)] WARN 1-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str());
           return nullptr; 
        }
        H* h = new H(hname.c_str(),htitle.c_str(),static_cast<int>(bins.size()-1),&bins.at(0)); h->Sumw2(); 
        if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
        if ( ytitle != "" ) { h->GetXaxis()->SetTitle(ytitle.c_str()); }
        collection.insert( { hname, h } );
        printf("[HistGroup::book(...)] INFO 1-d histogram with name \042%s\042 booked\n",h->GetName());
        return h;  
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,int nbx,double xmin,double xmax,int nby,double ymin,double ymax,const std::string& xtitle="",const std::string& ytitle="",const std::string& ztitle="") {
        if ( collection.find(hname) == collection.end() ) {
            H* h = new H(hname.c_str(),hname.c_str(),nbx,xmin,xmax,nby,ymin,ymax); h->Sumw2(); 
            if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
            if ( ytitle != "" ) { h->GetYaxis()->SetTitle(ytitle.c_str()); }
            if ( ztitle != "" ) { h->GetZaxis()->SetTitle(ztitle.c_str()); } 
            collection.insert( { hname, h } );
            printf("[HistGroup::book(...)] INFO 2-d histogram with name \042%s\042 booked\n",h->GetName()); 
            return h;
        } else {
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str()); 
            return nullptr;  
        }
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& bins,int nby,double ymin,double ymax,const std::string& xtitle="",const std::string& ytitle="",const std::string& ztitle="") {
        if ( bins.size() < 2 ) { 
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 cannot be booked, x-axis bin description has only %zu entries\n",hname.c_str(),bins.size());
            return nullptr; 
        }
        if ( collection.find(hname) != collection.end() ) {
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str()); 
            return nullptr;
        }
        H* h = new H(hname.c_str(),hname.c_str(),static_cast<int>(bins.size()-1),&bins.at(0),nby,ymin,ymax); h->Sumw2(); 
        if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
        if ( ytitle != "" ) { h->GetYaxis()->SetTitle(ytitle.c_str()); }
        if ( ztitle != "" ) { h->GetZaxis()->SetTitle(ztitle.c_str()); }
        collection.insert( { hname, h } );
        printf("[HistGroup::book(...)] INFO 2-d histogram with name \042%s\042 booked\n",h->GetName()); 
        return h;
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,int nbx,double xmin,double xmax,const std::vector<double>& bins,const std::string& xtitle="",const std::string& ytitle="",const std::string& ztitle="") {
        if ( bins.size() < 2 ) { 
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 cannot be booked, y-axis bin description has only %zu entries\n",hname.c_str(),bins.size());
            return nullptr; 
        }
        if ( collection.find(hname) != collection.end() ) {
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str()); 
            return nullptr;
        }
        H* h = new H(hname.c_str(),hname.c_str(),int nbx,double xmin,double xmax,static_cast<int>(bins.size()-1),&bins.at(0)); h->Sumw2(); 
        if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
        if ( ytitle != "" ) { h->GetYaxis()->SetTitle(ytitle.c_str()); }
        if ( ztitle != "" ) { h->GetZaxis()->SetTitle(ztitle.c_str()); }
        collection.insert( { hname, h } );
        printf("[HistGroup::book(...)] INFO 2-d histogram with name \042%s\042 booked\n",h->GetName()); 
        return h;
    }
    template<class H> static H* book(const std::string& hname,const std::string& htitle,const std::vector<double>& xbins,const std::vector<double>& ybins,const std::string& xtitle="",const std::string& ytitle="",const std::string& ztitle="") {
        if ( xbins.size() < 2 || ybins.size() < 2 ) { 
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 cannot be booked, x/y-axis bin description has only %zu/%zu entries\n",hname.c_str(),xbins.size(),ybins.size());
            return nullptr; 
        }
        if ( collection.find(hname) != collection.end() ) {
            printf("[HistGroup::book(...)] WARN 2-d histogram with name \042%s\042 already booked, ignored\n",hname.c_str()); 
            return nullptr;
        }
        H* h = new H(hname.c_str(),hname.c_str(),static_cast<int>(xbins.size()-1),&xbins.at(0),static_cast<int>(ybins.size()-1),&ybins.at(0)); h->Sumw2(); 
        if ( xtitle != "" ) { h->GetXaxis()->SetTitle(xtitle.c_str()); }
        if ( ytitle != "" ) { h->GetYaxis()->SetTitle(ytitle.c_str()); }
        if ( ztitle != "" ) { h->GetZaxis()->SetTitle(ztitle.c_str()); }
        collection.insert( { hname, h } );
        printf("[HistGroup::book(...)] INFO 2-d histogram with name \042%s\042 booked\n",h->GetName()); 
        return h;
    }
    template<class H> static std::vector<std::string> messages(typename std::map<std::string,H*>& map) {
        static char _buffer[1024];
        std::vector<std::string> msgCache; size_t entries(map.size()); if ( entries > msgCache.capacity() ) { msgCache.reserve(entries); }
        int kwidth(0); 
        int hwidth(0);
        for ( const auto& entry : map ) { 
            kwidth = std::max(static_cast<int>(entry.first.length()),kwidth);
            if ( entry.second != nullptr ) { hwidth = std::max(static_cast<int>(std::string(entry.second->GetName()).length()),hwidth); } 
        }
        for ( const auto& entry : map ) {
            std::string msg;
            if ( kwidth > 0 ) {
                sprintf(_buffer,"[%-*.*s] histogram ",kwidth,kwidth,entry.first.c_str());
                msg += std::string(_buffer);
            }
            if ( hwidth > 0 ) {
                std::string hname = entry.second != nullptr ? std::string(entry.second->GetName()) : "N/A";
                sprintf(_buffer,"at %p name: %-*.*s",(void*)entry.second,hwidth,hwidth,hname.c_str()); 
            }
            msgCache.push_back(msg);
        }
        return msgCache;
    }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    namespace Tag {
        static const std::string initial    = { "_init"  }; // distribution of inital data
        static const std::string inclusive  = { "_incl"  }; // distribution of inclusive data 
        static const std::string selected   = { "_filt"  }; // distribution of selected data 
        static const std::string exclusive  = { "_excl"  }; // distribution of exclusive data
        static const std::string difference = { "_diff"  }; // difference between distributions (d_i-h_i)
        static const std::string ratio      = { "_ratio" }; // ratio of distributions (d_i/h_i)
        static const std::string fraction   = { "_frac"  }; // fraction of data distributions  (d_i/h_i)
        static const std::string loss       = { "_loss"  }; // relative difference between data distributions (d_i-h_i)/h_i
        static const std::string unknown    = { "UNKNOWN"}; //
        static const std::vector<std::string> knownTags = {
            initial, inclusive, selected, exclusive, difference, ratio, fraction, loss, unknown
        };
        static bool known(const std::string& ctag) { auto ftag(std::find(knownTags.begin(),knownTags.end(),ctag)); return ftag != knownTags.end(); }
    } // HistGroup::Tag
    namespace Labels {
        static const std::map<std::string,std::tuple<std::string,std::string> > valueAxis = {
            {"clusterE"  ,               {"E_{clus}^{EM} [GeV]"                         ,"log_{10}(E_{clus}^{EM}/GeV)"       } },
            {"clusterEta",               {"y_{clus}^{EM}"                               ,""                                  } },
            {"clusterEtaCalib",          {"y_{clus}^{LCW}"                              ,""                                  } },
            {"cluster_CENTER_LAMBDA",    {"#lambda_{clus} [mm]"                         ,""                                  } },
            {"cluster_LONGITUDINAL",     {"m_{long}^{2}"                                ,""                                  } },
            {"cluster_LATERAL",          {"m_{lat}^{2}"                                 ,""                                  } },
            {"cluster_ENG_FRAC_EM",      {"f_{emc}"                                     ,""                                  } },
            {"cluster_FIRST_ENG_DENSITY",{"#rho_{clus} [GeV/mm^{3}"                     ,"log_{10}(#rho_{clus}/(GeV/mm^{3}))"} },
            {"cluster_SIGNIFICANCE",     {"#zeta_{clus}^{EM}"                           ,""                                  } },
            {"cluster_PTD",              {"p_{T}D"                                      ,""                                  } },
            {"cluster_SECOND_TIME",      {"#sigma_{t}^{2} [ns^{2}]"                     ,""                                  } },
            {"avgMu",                    {"#LT#mu#GT"                                   ,""                                  } },
            {"nPrimVtx",                 {"N_{PV}"                                      ,""                                  } },
            {"cluster_ENG_CALIB_TOT",    {"E_{clus}^{dep} [GeV]"                        ,"log_{10}(E_{clus}^{dep}/GeV"       } },
            {"ResponseMeasure",          {"R_{clus}^{EM} = E_{clus}^{EM}/E_{clus}^{dep}",""                                  } },
            {"ResponsePrediction",       {"R_{clus}^{EM,predict}"                       ,""                                  } },
            {"CalibratedE",              {"E_{clus}^{ML}"                               ,"log_{10}(E_{clus}^{dep}/GeV"       } }
        };
        static const std::string& variable(const std::string& varName,bool isLog=false) {
            static const std::string _unknown = "";
            auto fentry(variableAxis.find(varName));
            if ( fentry == variableAxis.end() ) { return _unknown; }
            return isLog ? std::get<1>(fentry.second) : std::get<0>(fentry.second);
        }
    } // HistGroup::Labels
    namespace Descriptor {
       } 
            static bool hasLogValue          (bitmask_t m) { return (m & Mode::VALUE_LOG) == Mode::VALUE_LOG; }
            static bool hasLinValue          (bitmask_t m) { return (m & Mode::VALUE_LIN) == Mode::VALUE_LIN; }
            static bool hasLogAxis           (bitmask_t m) { return (m & Mode::AXIS_LOG ) == Mode::AXIS_LOG;  }
            static bool hasLinAxis           (bitmask_t m) { return (m & Mode::AXIS_LIN ) == Mode::AXIS_LIN;  }
            static bool hasFixedBinning      (bitmask_t m) { return (m & Mode::FIXED    ) == Mode::FIXED;     }
            static bool hasVariableBinning   (bitmask_t m) { return (m & Mode::VARIABLE ) == Mode::VARIABLE;  }
            static bool hasFixedLinBinning   (bitmask_t m) { return hasFixedBinning(m)    && hasLinAxis(m);   }
            static bool hasFixedLogBinning   (bitmask_t m) { return hasFixedBinning(m)    && hasLogAxis(m);   }
            static bool hasVariableLinBinning(bitmask_t m) { return hasVariableBinning(m) && hasLinAxis(m);   }
            static bool hasVariableLogBinning(bitmask_t m) { return hasVAriableBinning(m) && hasLogAxis(m);   }
            
            
            
            
            
            
            
            
            
            static std::string printBinRanges(const std::vector<double>& bins) {
          namespace Axis {
            // possible modes are: { bin width, binning scale, value scale }
            // [1] {fixed   ,linear,linear}  FIXED_LIN_LIN    - equidistant bins in linear scale, linear scale value 
            // [2] {fixed   ,linear,log10 }  FIXED_LIN_LOG    - equidistant bins in linear scale, log10 scale value
            // [3] {fixed   ,log10 ,log10 }  FIXED_LOG_LOG    - equidistant bins in log10 scale, log10 scale value 
            // [4] {fixed   ,log10 ,linear}  FIXED_LOG_LIN    - equidistant bins in log10 scale, linear scale value
            // [5] {variable,linear,linear}  VARIABLE_LIN_LIN - variable width bins in linear scale, linear scale value
            // [6] {variable,linear,log10 }  VARIABLE_LIN_LOG - variable width bins in linear scale, log10 scale value
            // [7] {variable,log10 ,log10 }  VARIABLE_LOG_LOG - variable width bins in log10 scale, log10 scale variable
            // [8] {variable,log10 ,linear}  VARIABLE_LOG_LIN - variable width bins in log10 scale, linear scale values 
            enum class Mode {
                FIXED           =0x1000,
                VARIABLE        =0x2000,
                VALUE_LIN       =0x0001,
                VALUE_LOG       =0x0002,
                AXIS_LIN        =0x0010,
                AXIS_LOG        =0x0020,
                FIXED_LIN_LIN   =FIXED    | AXIS_LIN | VALUE_LIN,  // 0x1011,
                FIXED_LIN_LOG   =FIXED    | AXIS_LIN | VALUE_LOG,  // 0x1012,
                FIXED_LOG_LOG   =FIXED    | AXIS_LOG | VALUE_LOG,  // 0x1022,
                FIXED_LOG_LIN   =FIXED    | AXIS_LOG | VALUE_LIN,  // 0x1021,
                VARIABLE_LIN_LIN=VARIABLE | AXIS_LIN | VALUE_LIN,  // 0x2011,
                VARIABLE_LIN_LOG=VARIABLE | AXIS_LIN | VALUE_LOG,  // 0x2012,
                VARIABLE_LOG_LOG=VARIABLE | AXIS_LOG | VALUE_LOG,  // 0x2022,
                VARIABLE_LOG_LIN=VARIABLE | AXIS_LOG | VALUE_LIN,  // 0x2012,
                UNKNOWN         =0x0000
                   static char _buffer[1024];
                sprintf(_buffer,"number of bins %3zu in [%+10.3e,%+-10.3e]\n",bins.size(),bins.front(),bins.back());
                return std::string(_buffer);
            }
        } // HistGroup::Descriptor::Axis       
        namespace Hist1d {
            typedef std::tuple<std::string,int,double,double,std::string,std::string,bool,bitmask_t> Descriptor;
            typedef std::string                                                                      Key;
            typedef std::map<Key,Descriptor>                                                         DescriptorMap; 
            static const std::string& title   (const Descriptor& descr) { return HDM_TITLE ( descr ); }
            static       int          nbinx   (const Descriptor& descr) { return HDM_NBIN  ( descr ); }
            static       double       xbmin   (const Descriptor& descr) { return HDM_XMIN  ( descr ); }
            static       double       xbmax   (const Descriptor& descr) { return HDM_XMAX  ( descr ); }
            static const std::string& xtitle  (const Descriptor& descr) { return HDM_XTITLE( descr ); }
            static const std::string& ytitle  (const Descriptor& descr) { return HDM_YTILE ( descr ); }
            static       bool         logvalue(const Descriptor& descr) { return HDM_LVALUE( descr ); }
            static       bitmask_t    type    (const Descriptor& descr) { return HDM_TYPE  ( descr ); }
            static const DescriptorMap description = {
                {"clusterE"  ,               {"E_{clus}^{EM}"                               ,70 ,0.  ,2000. ,HistUtils::Labels::valueAxis.at("clusterE"                 ,false),"Entries",false},Mode::FIXED_LOG_LIN},
                {"clusterEta",               {"y_{clus}^{EM}"                               ,97 ,-4.85,4.85 ,HistUtils::Labels::valueAxis.at("clusterEta"               ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"clusterEtaCalib",          {"y_{clus}^{LCW}"                              ,97 ,-4.85,4.85 ,HistUtils::Labels::valueAxis.at("clusterEtaCalib"          ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_CENTER_LAMBDA",    {"#lambda_{clus}"                              ,200,0.   ,2000.,HistUtils::Labels::valueAxis.at("cluster_CENTER_LAMBDA"    ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_LONGITUDINAL",     {"m_{long}^{2}"                                ,101,-0.05,1.05 ,HistUtils::Labels::valueAxis.at("cluster_LONGITUDINAL"     ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_LATERAL",          {"m_{lat}^{2}"                                 ,101,-0.05,1.05 ,HistUtils::Labels::valueAxis.at("cluster_LATERAL"          ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_ENG_FRAC_EM",      {"f_{emc}"                                     ,101,-0.05,1.05 ,HistUtils::Labels::valueAxis.at("cluster_ENG_FRAC_EM"      ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_FIRST_ENG_DENSITY",{"#LT#rho_{cell}#GT_{clus}"                    ,80 ,1e-08,1e-04,HistUtils::Labels::valueAxis.at("cluster_FIRST_ENG_DENSITY",false),"Entries",false},Mode::FIXED_LOG_LIN},
                {"cluster_SIGNIFICANCE",     {"#zeta_{clus}^{EM}"                           ,100,0.   ,100. ,HistUtils::Labels::valueAxis.at("cluster_SIGNIFICANCE"     ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_PTD",              {"p_{T}D"                                      ,101,-0.05,1.05 ,HistUtils::Labels::valueAxis.at("cluster_PTD"              ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_SECOND_TIME",      {"#sigma_{t}^{2}"                              ,180,0.   ,180. ,HistUtils::Labels::valueAxis.at("cluster_SECOND_TIME"      ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"avgMu",                    {"#LT#mu#GT"                                   ,80 ,0.   ,80.  ,HistUtils::Labels::valueAxis.at("avgMu"                    ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"nPrimVtx",                 {"N_{PV}"                                      ,60 ,0.05 ,60.05,HistUtils::Labels::valueAxis.at("nPrimVtx"                 ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"cluster_ENG_CALIB_TOT",    {"E_{clus}^{dep}"                              ,70 ,0.   ,2000.,HistUtils::Labels::valueAxis.at("cluster_ENG_CALIB_TOT"    ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"ResponseMeasure",          {"R_{clus}^{EM} = E_{clus}^{EM}/E_{clus}^{dep}",100,0.   ,5.   ,HistUtils::Labels::valueAxis.at("ResponseMeasure"          ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"ResponsePrediction",       {"R_{clus}^{EM,predict}"                       ,100,0.   ,5.   ,HistUtils::Labels::valueAxis.at("ResponsePrediction"       ,false),"Entries",false},Mode::FIXED_LIN_LIN},
                {"CalibratedE",              {"log_{10}(E_{clus}^{dep}/GeV"                 ,70 ,0.   ,2000.,HistUtils::Labels::valueAxis.at("CalibratedE"              ,false),"Entries",false},Mode::FIXED_LIN_LIN}
            };
        } // HistGroup::Hist1d
    } // HistGroup::Descriptor
    namespace 
    namespace Hist1d {
        static template 

    } // Hist1d
    namespace LogE {
        static const int    nbin =  90;
        static const double xmin = -5.;
        static const double xmax =  4.;
    }
    namespace Rap {
        static const int    nbin = { 97 }; 
        static const double xmin = { -4.85 }; 
        static const double xmax = {  4.85 };
        static std::tuple<TH2D*,TH2D*> bookLCWvsEM() {  
            return { book<TH2D>("ClusterRapLCWvsClusterRapEM_init","y_{clus}^{LCW}(y_{clus}^{EM}",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","y_{clus}^{LCW}"),
                     book<TH2D>("ClusterRapLCWvsClusterRapEM_filt","y_{clus}^{LCW}(y_{clus}^{EM}",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","y_{clus}^{LCW}")
            };  
        }
        static std::tuple<TH2D*,TH2D*> bookDiffvsEM() {
            return { book<TH2D>("ClusterRapDiffvsClusterRapEM_init","(y_{clus}^{EM}-y_{clus}^{LCW})(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","y_{clus}^{EM}#minusy_{clus}^{LCW}"),
                     book<TH2D>("ClusterRapDiffvsClusterRapEM_filt","(y_{clus{^{EM}-y_{clus}^{LCW})(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","y_{clus}^{EM}#minusy_{clus}^{LCW}")
            };
        }
        static std::tuple<TH2D*,TH2D*> bookDiffvsLCW() {
            return { book<TH2D>("ClusterRapDiffvsClusterRapLCW_init","(y_{clus}^{EM}-y_{clus}^{LCW})(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{LCW}","y_{clus}^{EM}#minusy_{clus}^{LCW}"),
                     book<TH2D>("ClusterRapDiffvsClusterRapLCW_filt","(y_{clus{^{EM}-y_{clus}^{LCW})(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{LCW}","y_{clus}^{EM}#minusy_{clus}^{LCW}")
            };
        }
        static std::tuple<TH2D*,TH2D*> bookMomvsEM() {
            return { book<TH2D>("ClusterFirstEtavsClusterRapEM_init","#LTy_{cell}^{EM}#GT_{clus}(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","#LTy_{cell}^{EM}#GT_{clus}"),
                     book<TH2D>("ClusterFirstEtavsClusterRapEM_filt","#LTy_{cell}^{EM}#GT_{clus}(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{EM}","#LTy_{cell}^{EM}#GT_{clus}")
            };
        }
        static std::tuple<TH2D*,TH2D*> bookMomvsLCW() {
            return { book<TH2D>("ClusterFirstEtavsClusterRapLCW_init","#LTy_{cell}^{EM}#GT_{clus}(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{LCW}","#LTy_{cell}^{EM}#GT_{clus}"),
                     book<TH2D>("ClusterFirstEtavsClusterRapLCW_filt","#LTy_{cell}^{EM}#GT_{clus}(y_{clus}^{EM})",nbin,xmin,xmax,nbin,xmin,xmax,"y_{clus}^{LCW}","#LTy_{cell}^{EM}#GT_{clus}")
            };
        }
        static std::tuple<TH2D*,TH2D*> bookDiffvsMom() {
            return { book<TH2D>("ClusterRapDiffvsClusterFirstEta_init","(y_{clus}^{EM}-y_{clus}^{LCW})(#LTy_{cell}^{EM}#GT_{clus})",nbin,xmin,xmax,nbin,xmin,xmax,"#LTy_{cell}^{EM}#GT_{clus}","y_{clus}^{EM}#minusy_{clus}^{LCW}"),
                     book<TH2D>("ClusterRapDiffvsClusterFirstEta_filt","(y_{clus{^{EM}-y_{clus}^{LCW})(#LTy_{cell}^{EM}#GT_{clus})",nbin,xmin,xmax,nbin,xmin,xmax,"#LTy_{cell}^{EM}#GT_{clus}","y_{clus}^{EM}#minusy_{clus}^{LCW}")
            };
        }
        static std::tuple<TH2D*,TH2D*> bookDiffvsLogE() {
            return { book<TH2D>("ClusterRapDiffvsClusterLogE_init","(y_{clus}^{EM}-y_{clus}^{LCW})(log_{10}(E_{clus}^{EM}/GeV))",LogE::nbin,LogE::xmin,LogE::xmax,nbin,xmin,xmax,"log_{10}(E_{clus}^{EM}/GeV)","y_{clus}^{EM}#minusy_{clus}^{LCW}"),
                     book<TH2D>("ClusterRapDiffvsClusterLogE_filt","(y_{clus}^{EM}-y_{clus}^{LCW})(log_{10}(E_{clus}^{EM}/GeV))",LogE::nbin,LogE::xmin,LogE::xmax,nbin,xmin,xmax,"log_{10}(E_{clus}^{EM}/GeV)","y_{clus}^{EM}#minusy_{clus}^{LCW}")
            };
        }
        template<class H> static H* allocateHist(const std::string& hname) {
            auto fhist = HistGroup::collection.find(hname);
            return fhist == HistGroup::collection.end() || fhist->second == nullptr  ? nullptr : (H*)fhist->second; 
        }
        template<class DATA> static bool fillLCWvsEM(DATA* dptr,bool initFlag=true) { 
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterRapLCWvsClusterRapEM_init") : allocateHist<TH2D>("ClusterRapLCWvsClusterRapEM_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->clusterEta,dptr->clusterEtaCalib); return true; }  
        }
        template<class DATA> static bool fillDiffvsEM(DATA* dptr,bool initFlag=true) {
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterRapDiffvsClusterRapEM_init") : allocateHist<TH2D>("ClusterRapDiffvsClusterRapEM_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->clusterEta,dptr->clusterEta-dptr->clusterEtaCalib); return true; }
        }
         template<class DATA> static bool fillDiffvsLCW(DATA* dptr,bool initFlag=true) {
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterRapDiffvsClusterRapLCW_init") : allocateHist<TH2D>("ClusterRapDiffvsClusterRapLCW_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->clusterEta,dptr->clusterEta-dptr->clusterEtaCalib); return true; }
        }
        template<class DATA> static bool fillMomvsEM(DATA* dptr,bool initFlag=true) {
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterFirstEtavsClusterRapEM_init") : allocateHist<TH2D>("ClusterFirstEtavsClusterRapEM_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->clusterEta,dptr->cluster_FIRST_ETA); return true; }  
        }
        template<class DATA> static bool fillMomvsLCW(DATA* dptr,bool initFlag=true) {
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterFirstEtavsClusterRapLCW_init") : allocateHist<TH2D>("ClusterFirstEtavsClusterRapLCW_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->clusterEtaCalib,dptr->cluster_FIRST_ETA); return true; }
        }
        template<class DATA> static bool fillDiffvsMom(DATA* dptr,bool initFlag=true) {
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterRapDiffvsClusterFirstEta_init") : allocateHist<TH2D>("ClusterRapDiffvsClusterFirstEta_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(dptr->cluster_FIRST_ETA,dptr->clusterEta-dptr->clusterEtaCalib); return true; }   
        }
        template<class DATA> static bool fillDiffvsLogE(DATA* dptr,bool initFlag=true) {
            static Float_t _minLogVal = std::abs(std::numeric_limits<Float_t>::min()); 
            TH2D* hptr = initFlag ? allocateHist<TH2D>("ClusterRapDiffvsClusterLogE_init") : allocateHist<TH2D>("ClusterRapDiffvsClusterLogE_filt");
            if ( hptr == nullptr ) { return false; } else { hptr->Fill(std::log10(std::max(dptr->clusterE,_minLogVal)),dptr->clusterEta-dptr->clusterEtaCalib); return true; }
        }
    } // HistGroup::Rap
    namespace Spectra {
        template<class MAP> static int book(const MAP& histMap,const std::string& tag=HistUtils::Tag::unknown) {
            int nh(0);
            for ( const auto& entry : histMap ) {
                auto fhist(Descriptor::Hist1D::description.find(entry.first));
                std::string hname = HistUtils::Tag::knownTags(tag) ? entry.first + tag : entry.first;
                if ( fhist != Descriptor::Hist1D::description.end() ) {
                    // collect data
                    auto& descr = *fhist;
                    int         nbin(Descriptor::Hist1d::nbinx(descr));
                    double      xmin(Descriptor::Hist1d::xbmin(descr));
                    double      xmax(Descriptor::Hist1d::xbmax(descr)); 
                    bitmask_t   mask(Descriptor::Hist1d::type (descr));
                    const std::string& htitle = Descriptor::Hist1d::title (descr);
                    const std::string& xtitle = Descriptor::Hist1d::xtitle(descr);
                    const std::string& ytitle = Descriptor::Hist1d::ytitle(descr);
                    // histogram has equidistant bins (only available implementation 07.06.2022)
                    if ( Descriptor::Axis::hasFixedBinning(mask) ) {
                        if ( Descriptor::Axis::hasLinValue(mask) && Descriptor::Axis::hasLinAxis(mask) ) {           // equidistant bins
                            if ( book<TH1D>(hname,htitle,nbin,xmin,xmax,xtitle,ytitle) != nullptr ) { ++nh; }
                        } else if ( Descriptor::Axis::hasLinValue(mask) && Descriptor::Axis::hasLogAxis(mask) ) {    // variable value bins with equidistant log(value), filled with value
                            std::vector<double> bins(Descriptor::Axis::fixedLogRanges(nbin,xmin,xmax));
                            printf("[]HistGroup::Spectra::bookAll] INFO %s for histogram \042%s\042\n",Descriptor::Axis::printBinRange(bins),entry.first.c_str()); 
                            if ( book<TH1D>(hname,htitle,bins,xtitle,ytitle) != nullptr ) { ++nh; } 
                        } else if ( Descriptor::Axis::hasLogValue(mask) && Descriptor::Axis::hasLogAxis(mask) ) {    // equidistant bins in log space
                            HDM_LVALUE( descr ) = Descriptor::Hist1d::logvalue(descr);
                            if ( book<TH1D>(book<TH1>(hname,htitle,nbin,xmin,xmax,xtitle,ytitle) != nullptr ) { ++nh; } 
                        }            
                    }
                }
            }
            return nh;
        }
        template<class MAP,classs DATA> int fill(const MAP& histMap,DATA* dptr) {
            int nh(0); 
            for ( auto& entry : map ) { if ( Hist1d::fill(dptr,entry.first) ) { ++nh; }; }
            return nh;
        }
    } // HistGroup::Spectra
}         
#endif
