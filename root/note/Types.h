// -*- c++ -*-
#ifndef TYPES_H
#define TYPES_H

#include "config.h"

#include <string>
#include <map>

#include <cstddef>

#define TYPES_ID_NAME( NAME ) { NAME, #NAME }
#define TYPES_NAME_ID( NAME ) { #NAME, NAME }

#define TYPES_ENUM_NAME( ARG, ETYPE, MAP      ) static const std::string& toName( ETYPE ARG             ) { return MAP.at( ARG ); }
#define TYPES_ENUM_CHAR( ARG, ETYPE, MAP      ) static const char*        toChar( ETYPE ARG             ) { return MAP.at( ARG ).c_str(); } 
#define TYPES_NAME_ENUM( ARG, ETYPE, MAP, UNK ) static ETYPE              toId  (const std::string& ARG ) { auto fid( MAP.find( ARG )); return fid != MAP.end() ? fid->second : UNK ; }
#define TYPES_ENUM_LABEL( ARG, ETYPE, MAP     ) static const std::string& toLabel( ETYPE ARG            ) { return MAP.at( ARG ); } 

#pragma message ( XSTR( TYPES_ENUM_NAME( variableId, Types::Variable::Id, map  ) ) )

namespace Types {
  namespace Display {
    enum class Mode { Standard = 0x01, Latex = 0x02, ROOT = 0x04, Unknown = 0x00 };
    static const std::map<Mode,std::string> ModeToName = { 
      { Mode::Standard, "Standard" },
      { Mode::Latex   , "Latex"    },
      { Mode::ROOT    , "ROOT"     },
      { Mode::Unknown , "Unknown"  }
    };
    static const std::map<std::string,Mode> NameToMode = { 
      { "Standard", Mode::Standard },
      { "Latex"   , Mode::Latex    },
      { "ROOT"    , Mode::ROOT     },
      { "Unknown" , Mode::Unknown  }
    };
    struct Convert {
      TYPES_ENUM_NAME( m, Mode, ModeToName )
      TYPES_ENUM_CHAR( m, Mode, ModeToName )
      TYPES_NAME_ENUM( m, Mode, NameToMode, Mode::Unknown )
    };
  } // Types::Display
  ///@brief Moment types and identifiers
  namespace Moment { 
    ///@brief Identifiers
    ///@warning Do not change! These identifiers are copied from @c xAOD::CaloCluster and are therefore consistent with the official ones used in reconstruction.
    enum Id { HAD_WEIGHT         =   901,OOC_WEIGHT        =   902,DM_WEIGHT          =   903,ENG_CALIB_TOT       =  10001,ENG_CALIB_OUT_T = 10012,
	      ENG_CALIB_DEAD_TOT = 10040,ENG_CALIB_FRAC_EM = 10051,ENG_CALIB_FRAC_HAD = 10052,ENG_CALIB_FRAC_REST =  10053,EM_PROBABILITY  =   900,
	      FIRST_PHI          =   101,FIRST_ETA         =   102,SECOND_R           =   201,SECOND_LAMBDA       =   202,DELTA_PHI        =   301,
	      DELTA_THETA        =   302,DELTA_ALPHA       =   303,CENTER_X           =   401,CENTER_Y            =   402,CENTER_Z         =   403,
	      CENTER_MAG         =   404,CENTER_LAMBDA     =   501,LATERAL            =   601,LONGITUDINAL        =   602,ENG_FRAC_EM	   =   701,
	      ENG_FRAC_MAX       =   702,ENG_FRAC_CORE     =   703,FIRST_ENG_DENS     =   804,SECOND_ENG_DENS     =   805,ISOLATION        =   806,
              ENG_BAD_CELLS      =   807,N_BAD_CELLS       =   808,N_BAD_CELLS_CORR   =   809,BAD_CELLS_CORR_E    =   813,BADLARQ_FRAC     =   821,
              ENG_POS 		 =   822,SIGNIFICANCE      =   823,CELL_SIGNIFICANCE  =   824,CELL_SIG_SAMPLING   =   825,AVG_LAR_Q        =   826,
	      AVG_TILE_Q         =   827,ENG_BAD_HV_CELLS  =   828,N_BAD_HV_CELLS     =   829,PTD                 =   830,MASS             =   831,
	      SECOND_TIME        =   910, 
	      UNKNOWN            = 0
        };
    ///@brief Identifier dictionary
    static const std::map<Id,std::string> IdToName = {
      TYPES_ID_NAME( HAD_WEIGHT         ), TYPES_ID_NAME( OOC_WEIGHT        ), TYPES_ID_NAME( DM_WEIGHT          ), TYPES_ID_NAME( ENG_CALIB_TOT       ), TYPES_ID_NAME( ENG_CALIB_OUT_T ),
      TYPES_ID_NAME( ENG_CALIB_DEAD_TOT ), TYPES_ID_NAME( ENG_CALIB_FRAC_EM ), TYPES_ID_NAME( ENG_CALIB_FRAC_HAD ), TYPES_ID_NAME( ENG_CALIB_FRAC_REST ), TYPES_ID_NAME( EM_PROBABILITY  ),
      TYPES_ID_NAME( FIRST_PHI          ), TYPES_ID_NAME( FIRST_ETA         ), TYPES_ID_NAME( SECOND_R           ), TYPES_ID_NAME( SECOND_LAMBDA       ), TYPES_ID_NAME( DELTA_PHI       ),
      TYPES_ID_NAME( DELTA_THETA        ), TYPES_ID_NAME( DELTA_ALPHA       ), TYPES_ID_NAME( CENTER_X           ), TYPES_ID_NAME( CENTER_Y            ), TYPES_ID_NAME( CENTER_Z        ),
      TYPES_ID_NAME( CENTER_MAG         ), TYPES_ID_NAME( CENTER_LAMBDA     ), TYPES_ID_NAME( LATERAL            ), TYPES_ID_NAME( LONGITUDINAL        ), TYPES_ID_NAME( ENG_FRAC_EM     ),
      TYPES_ID_NAME( ENG_FRAC_MAX       ), TYPES_ID_NAME( ENG_FRAC_CORE     ), TYPES_ID_NAME( FIRST_ENG_DENS     ), TYPES_ID_NAME( SECOND_ENG_DENS     ), TYPES_ID_NAME( ISOLATION       ),
      TYPES_ID_NAME( ENG_BAD_CELLS      ), TYPES_ID_NAME( N_BAD_CELLS       ), TYPES_ID_NAME( N_BAD_CELLS_CORR   ), TYPES_ID_NAME( BAD_CELLS_CORR_E    ), TYPES_ID_NAME( BADLARQ_FRAC    ),
      TYPES_ID_NAME( ENG_POS 		  ), TYPES_ID_NAME( SIGNIFICANCE      ), TYPES_ID_NAME( CELL_SIGNIFICANCE  ), TYPES_ID_NAME( CELL_SIG_SAMPLING   ), TYPES_ID_NAME( AVG_LAR_Q       ),
      TYPES_ID_NAME( AVG_TILE_Q         ), TYPES_ID_NAME( ENG_BAD_HV_CELLS  ), TYPES_ID_NAME( N_BAD_HV_CELLS     ), TYPES_ID_NAME( PTD                 ), TYPES_ID_NAME( MASS            ),
      TYPES_ID_NAME( SECOND_TIME        ), 
      TYPES_ID_NAME( UNKNOWN            )
    };
    ///@brief Inverse identifier dictionary
    static const std::map<std::string,Id> NameToId = {
      TYPES_NAME_ID( HAD_WEIGHT         ), TYPES_NAME_ID( OOC_WEIGHT        ), TYPES_NAME_ID( DM_WEIGHT          ), TYPES_NAME_ID( ENG_CALIB_TOT       ), TYPES_NAME_ID( ENG_CALIB_OUT_T ),
      TYPES_NAME_ID( ENG_CALIB_DEAD_TOT ), TYPES_NAME_ID( ENG_CALIB_FRAC_EM ), TYPES_NAME_ID( ENG_CALIB_FRAC_HAD ), TYPES_NAME_ID( ENG_CALIB_FRAC_REST ), TYPES_NAME_ID( EM_PROBABILITY  ),
      TYPES_NAME_ID( FIRST_PHI          ), TYPES_NAME_ID( FIRST_ETA         ), TYPES_NAME_ID( SECOND_R           ), TYPES_NAME_ID( SECOND_LAMBDA       ), TYPES_NAME_ID( DELTA_PHI       ),
      TYPES_NAME_ID( DELTA_THETA        ), TYPES_NAME_ID( DELTA_ALPHA       ), TYPES_NAME_ID( CENTER_X           ), TYPES_NAME_ID( CENTER_Y            ), TYPES_NAME_ID( CENTER_Z        ),
      TYPES_NAME_ID( CENTER_MAG         ), TYPES_NAME_ID( CENTER_LAMBDA     ), TYPES_NAME_ID( LATERAL            ), TYPES_NAME_ID( LONGITUDINAL        ), TYPES_NAME_ID( ENG_FRAC_EM     ),
      TYPES_NAME_ID( ENG_FRAC_MAX       ), TYPES_NAME_ID( ENG_FRAC_CORE     ), TYPES_NAME_ID( FIRST_ENG_DENS     ), TYPES_NAME_ID( SECOND_ENG_DENS     ), TYPES_NAME_ID( ISOLATION       ),
      TYPES_NAME_ID( ENG_BAD_CELLS      ), TYPES_NAME_ID( N_BAD_CELLS       ), TYPES_NAME_ID( N_BAD_CELLS_CORR   ), TYPES_NAME_ID( BAD_CELLS_CORR_E    ), TYPES_NAME_ID( BADLARQ_FRAC    ),
      TYPES_NAME_ID( ENG_POS 		  ), TYPES_NAME_ID( SIGNIFICANCE      ), TYPES_NAME_ID( CELL_SIGNIFICANCE  ), TYPES_NAME_ID( CELL_SIG_SAMPLING   ), TYPES_NAME_ID( AVG_LAR_Q       ),
      TYPES_NAME_ID( AVG_TILE_Q         ), TYPES_NAME_ID( ENG_BAD_HV_CELLS  ), TYPES_NAME_ID( N_BAD_HV_CELLS     ), TYPES_NAME_ID( PTD                 ), TYPES_NAME_ID( MASS            ),
      TYPES_NAME_ID( SECOND_TIME        ), 
      TYPES_NAME_ID( UNKNOWN            )
    };
    ///@brief Dictionary lookup
    struct Convert { 
      ///@param  vid identifier
      ///@return Reference to unmodifiable @c std::string storing the identifier name. 
      TYPES_ENUM_NAME( vid, Id, IdToName )
      ///@brief Identifier-to-characters dictionary lookup (e.g., for printing)
      ///@param  vid identifier
      ///@return Pointer to unmodifiable character array storing the identifier name. 
      TYPES_ENUM_CHAR( vid, Id, IdToName )
      ///@brief Name-to-Id lookup 
      ///@param name reference to unmodifiable @c std::string containing the identifier name
      ///@return A valid Id if name is known, else the @c Moment::UNKNOWN tag 
      TYPES_NAME_ENUM( name, Id, NameToId, Id::UNKNOWN )
    };
  } // Types::Moment
  ///@brief Variable types and identifiers 
  namespace Variable {
    ///@brief Identifiers 
    enum Id { seqNumber                 =                    100001,runNumber                  =                     100002,eventNumber                 =                      100003,
	      nPrimVtx                  =                    100004,avgMu                      =                     100005,truthE                      =                      100101,
	      truthPt                   =                    100102,truthEta                   =                     100103,truthPhi                    =                      100104,
	      truthPDG                  =                    100110,truthJetE                  =                     100301,truthJetPt                  =                      100302,
	      truthJetEta               =                    100303,truthJetPhi                =                     100304,truthJetMatchingRadius      =                      100310,  
              jetCalE                   =                    100401,jetCalPt                   =                     100402,jetCalEta                   =                      100403,
	      jetCalPhi                 =                    100404,jetCnt                     =                     100411,jetRawE                     =                      100501,
	      jetRawPt                  =                    100502,jetRawEta                  =                     100503,jetRawPhi                   =                      100504,
	      jetNConst                 =                    100510,clusterIndex               =                     100611,cluster_nCells              =                      100612,
	      cluster_nCells_tot        =                    100613,clusterE                   =                     100601,clusterPt                   =                      100602,
	      clusterEta                =                    100603,clusterPhi                 =                     100604,nCluster                    =                      100610,
              clusterECalib             =                    100701,clusterPtCalib             =                     100702,clusterEtaCalib             =                      100703,
	      clusterPhiCalib           =                    100704,cluster_sumCellECalib      =                     100721,cluster_fracECalib          =                      100731,
	      cluster_fracECalib_ref    =                    100741,cluster_sumCellE           =                     100621,cluster_fracE               =                      100631,
	      cluster_fracE_ref         =                    100641,cluster_time               =                     100650,clusterResponse             =                      100801,
	      clusterResponseLCW        =                    100802,clusterResponsePred        =                     100803,
              cluster_HAD_WEIGHT        = Moment::HAD_WEIGHT       ,cluster_OOC_WEIGHT         = Moment::OOC_WEIGHT        ,cluster_DM_WEIGHT           = Moment::DM_WEIGHT          ,          
              cluster_ENG_CALIB_TOT     = Moment::ENG_CALIB_TOT    ,cluster_ENG_CALIB_OUT_T    = Moment::ENG_CALIB_OUT_T   ,cluster_ENG_CALIB_DEAD_TOT  = Moment::ENG_CALIB_DEAD_TOT ,           
              cluster_ENG_CALIB_FRAC_EM = Moment::ENG_CALIB_FRAC_EM,cluster_ENG_CALIB_FRAC_HAD = Moment::ENG_CALIB_FRAC_HAD,cluster_ENG_CALIB_FRAC_REST = Moment::ENG_CALIB_FRAC_REST,        
              cluster_EM_PROBABILITY    = Moment::EM_PROBABILITY   ,cluster_FIRST_PHI          = Moment::FIRST_PHI         ,cluster_FIRST_ETA           = Moment::FIRST_ETA          ,      
              cluster_SECOND_R          = Moment::SECOND_R         ,cluster_SECOND_LAMBDA      = Moment::SECOND_LAMBDA     ,cluster_DELTA_PHI           = Moment::DELTA_PHI          ,
	      cluster_DELTA_THETA       = Moment::DELTA_THETA      ,cluster_DELTA_ALPHA        = Moment::DELTA_ALPHA       ,cluster_CENTER_X            = Moment::CENTER_X           ,
	      cluster_CENTER_Y          = Moment::CENTER_Y         ,cluster_CENTER_Z           = Moment::CENTER_Z          ,cluster_CENTER_MAG          = Moment::CENTER_MAG         ,
	      cluster_CENTER_LAMBDA     = Moment::CENTER_LAMBDA    ,cluster_LATERAL            = Moment::LATERAL           ,cluster_LONGITUDINAL        = Moment::LONGITUDINAL       , 
              cluster_ENG_FRAC_EM      	= Moment::ENG_FRAC_EM      ,cluster_ENG_FRAC_MAX       = Moment::ENG_FRAC_MAX      ,cluster_ENG_FRAC_CORE       = Moment::ENG_FRAC_CORE      , 
              cluster_FIRST_ENG_DENS    = Moment::FIRST_ENG_DENS   ,cluster_SECOND_ENG_DENS    = Moment::SECOND_ENG_DENS   ,cluster_ISOLATION           = Moment::ISOLATION          , 
              cluster_ENG_BAD_CELLS     = Moment::ENG_BAD_CELLS    ,cluster_N_BAD_CELLS        = Moment::N_BAD_CELLS       ,cluster_N_BAD_CELLS_CORR    = Moment::N_BAD_CELLS_CORR   ,
	      cluster_BAD_CELLS_CORR_E  = Moment::BAD_CELLS_CORR_E ,cluster_BADLARQ_FRAC       = Moment::BADLARQ_FRAC      ,cluster_ENG_POS 	       	= Moment::ENG_POS 	     ,
	      cluster_SIGNIFICANCE      = Moment::SIGNIFICANCE     ,cluster_CELL_SIGNIFICANCE  = Moment::CELL_SIGNIFICANCE ,cluster_CELL_SIG_SAMPLING   = Moment::CELL_SIG_SAMPLING  ,
              cluster_AVG_LAR_Q         = Moment::AVG_LAR_Q        ,cluster_AVG_TILE_Q         = Moment::AVG_TILE_Q        ,cluster_ENG_BAD_HV_CELLS    = Moment::ENG_BAD_HV_CELLS   ,
	      cluster_N_BAD_HV_CELLS    = Moment::N_BAD_HV_CELLS   ,cluster_PTD                = Moment::PTD               ,cluster_MASS                = Moment::MASS               ,
	      cluster_SECOND_TIME       = Moment::SECOND_TIME      ,Entries                    = 20000                     ,UNKNOWN                     = Moment::UNKNOWN
    };
    static const std::map<Id,std::string> IdToName = {
      TYPES_ID_NAME( seqNumber                 ), TYPES_ID_NAME( runNumber                  ), TYPES_ID_NAME( eventNumber                 ), TYPES_ID_NAME( nPrimVtx                  ), TYPES_ID_NAME( avgMu                      ),     
      TYPES_ID_NAME( truthE                    ), TYPES_ID_NAME( truthPt                    ), TYPES_ID_NAME( truthEta                    ), TYPES_ID_NAME( truthPhi                  ), TYPES_ID_NAME( truthPDG                   ),     
      TYPES_ID_NAME( truthJetE                 ), TYPES_ID_NAME( truthJetPt                 ), TYPES_ID_NAME( truthJetEta                 ), TYPES_ID_NAME( truthJetPhi               ), TYPES_ID_NAME( truthJetMatchingRadius     ),  
      TYPES_ID_NAME( jetCalE                   ), TYPES_ID_NAME( jetCalPt                   ), TYPES_ID_NAME( jetCalEta                   ), TYPES_ID_NAME( jetCalPhi                 ), TYPES_ID_NAME( jetCnt                     ),
      TYPES_ID_NAME( jetRawE                   ), TYPES_ID_NAME( jetRawPt                   ), TYPES_ID_NAME( jetRawEta                   ), TYPES_ID_NAME( jetRawPhi                 ), TYPES_ID_NAME( jetNConst                  ),
      TYPES_ID_NAME( clusterIndex              ), TYPES_ID_NAME( cluster_nCells             ), TYPES_ID_NAME( cluster_nCells_tot          ), TYPES_ID_NAME( clusterE                  ), TYPES_ID_NAME( clusterPt                  ), 
      TYPES_ID_NAME( clusterEta                ), TYPES_ID_NAME( clusterPhi                 ), TYPES_ID_NAME( nCluster                    ), TYPES_ID_NAME( clusterECalib             ), TYPES_ID_NAME( clusterPtCalib             ), 
      TYPES_ID_NAME( clusterEtaCalib           ), TYPES_ID_NAME( clusterPhiCalib            ), TYPES_ID_NAME( cluster_sumCellECalib       ), TYPES_ID_NAME( cluster_fracECalib        ), TYPES_ID_NAME( cluster_fracECalib_ref     ),           
      TYPES_ID_NAME( cluster_sumCellE          ), TYPES_ID_NAME( cluster_fracE              ), TYPES_ID_NAME( cluster_fracE_ref           ), TYPES_ID_NAME( cluster_time              ), TYPES_ID_NAME( cluster_HAD_WEIGHT         ), 
      TYPES_ID_NAME( cluster_OOC_WEIGHT        ), TYPES_ID_NAME( cluster_DM_WEIGHT          ), TYPES_ID_NAME( cluster_ENG_CALIB_TOT       ), TYPES_ID_NAME( cluster_ENG_CALIB_OUT_T   ), TYPES_ID_NAME( cluster_ENG_CALIB_DEAD_TOT ),
      TYPES_ID_NAME( cluster_ENG_CALIB_FRAC_EM ), TYPES_ID_NAME( cluster_ENG_CALIB_FRAC_HAD ), TYPES_ID_NAME( cluster_ENG_CALIB_FRAC_REST ), TYPES_ID_NAME( cluster_EM_PROBABILITY    ), TYPES_ID_NAME( cluster_FIRST_PHI          ), 
      TYPES_ID_NAME( cluster_FIRST_ETA         ), TYPES_ID_NAME( cluster_SECOND_R           ), TYPES_ID_NAME( cluster_SECOND_LAMBDA       ), TYPES_ID_NAME( cluster_DELTA_PHI         ), TYPES_ID_NAME( cluster_DELTA_THETA        ), 
      TYPES_ID_NAME( cluster_DELTA_ALPHA       ), TYPES_ID_NAME( cluster_CENTER_X           ), TYPES_ID_NAME( cluster_CENTER_Y            ), TYPES_ID_NAME( cluster_CENTER_Z          ), TYPES_ID_NAME( cluster_CENTER_MAG         ), 
      TYPES_ID_NAME( cluster_CENTER_LAMBDA     ), TYPES_ID_NAME( cluster_LATERAL            ), TYPES_ID_NAME( cluster_LONGITUDINAL        ), TYPES_ID_NAME( cluster_ENG_FRAC_EM       ), TYPES_ID_NAME( cluster_ENG_FRAC_MAX       ), 
      TYPES_ID_NAME( cluster_ENG_FRAC_CORE     ), TYPES_ID_NAME( cluster_FIRST_ENG_DENS     ), TYPES_ID_NAME( cluster_SECOND_ENG_DENS     ), TYPES_ID_NAME( cluster_ISOLATION         ), TYPES_ID_NAME( cluster_ENG_BAD_CELLS      ), 
      TYPES_ID_NAME( cluster_N_BAD_CELLS       ), TYPES_ID_NAME( cluster_N_BAD_CELLS_CORR   ), TYPES_ID_NAME( cluster_BAD_CELLS_CORR_E    ), TYPES_ID_NAME( cluster_BADLARQ_FRAC      ), TYPES_ID_NAME( cluster_ENG_POS            ), 
      TYPES_ID_NAME( cluster_SIGNIFICANCE      ), TYPES_ID_NAME( cluster_CELL_SIGNIFICANCE  ), TYPES_ID_NAME( cluster_CELL_SIG_SAMPLING   ), TYPES_ID_NAME( cluster_AVG_LAR_Q         ), TYPES_ID_NAME( cluster_AVG_TILE_Q         ), 
      TYPES_ID_NAME( cluster_ENG_BAD_HV_CELLS  ), TYPES_ID_NAME( cluster_N_BAD_HV_CELLS     ), TYPES_ID_NAME( cluster_PTD                 ), TYPES_ID_NAME( cluster_MASS              ), TYPES_ID_NAME( cluster_SECOND_TIME        ),
      TYPES_ID_NAME( Entries                   ), TYPES_ID_NAME( UNKNOWN                    ), TYPES_ID_NAME( clusterResponse             ), TYPES_ID_NAME( clusterResponseLCW         ), TYPES_ID_NAME( clusterResponsePred        )
    };
    static const std::map<std::string,Id> NameToId = {
      TYPES_NAME_ID( seqNumber                 ), TYPES_NAME_ID( runNumber                  ), TYPES_NAME_ID( eventNumber                 ), TYPES_NAME_ID( nPrimVtx                  ), TYPES_NAME_ID( avgMu                      ),     
      TYPES_NAME_ID( truthE                    ), TYPES_NAME_ID( truthPt                    ), TYPES_NAME_ID( truthEta                    ), TYPES_NAME_ID( truthPhi                  ), TYPES_NAME_ID( truthPDG                   ),     
      TYPES_NAME_ID( truthJetE                 ), TYPES_NAME_ID( truthJetPt                 ), TYPES_NAME_ID( truthJetEta                 ), TYPES_NAME_ID( truthJetPhi               ), TYPES_NAME_ID( truthJetMatchingRadius     ),  
      TYPES_NAME_ID( jetCalE                   ), TYPES_NAME_ID( jetCalPt                   ), TYPES_NAME_ID( jetCalEta                   ), TYPES_NAME_ID( jetCalPhi                 ), TYPES_NAME_ID( jetCnt                     ),
      TYPES_NAME_ID( jetRawE                   ), TYPES_NAME_ID( jetRawPt                   ), TYPES_NAME_ID( jetRawEta                   ), TYPES_NAME_ID( jetRawPhi                 ), TYPES_NAME_ID( jetNConst                  ),
      TYPES_NAME_ID( clusterIndex              ), TYPES_NAME_ID( cluster_nCells             ), TYPES_NAME_ID( cluster_nCells_tot          ), TYPES_NAME_ID( clusterE                  ), TYPES_NAME_ID( clusterPt                  ), 
      TYPES_NAME_ID( clusterEta                ), TYPES_NAME_ID( clusterPhi                 ), TYPES_NAME_ID( nCluster                    ), TYPES_NAME_ID( clusterECalib             ), TYPES_NAME_ID( clusterPtCalib             ), 
      TYPES_NAME_ID( clusterEtaCalib           ), TYPES_NAME_ID( clusterPhiCalib            ), TYPES_NAME_ID( cluster_sumCellECalib       ), TYPES_NAME_ID( cluster_fracECalib        ), TYPES_NAME_ID( cluster_fracECalib_ref     ),           
      TYPES_NAME_ID( cluster_sumCellE          ), TYPES_NAME_ID( cluster_fracE              ), TYPES_NAME_ID( cluster_fracE_ref           ), TYPES_NAME_ID( cluster_time              ), TYPES_NAME_ID( cluster_HAD_WEIGHT         ), 
      TYPES_NAME_ID( cluster_OOC_WEIGHT        ), TYPES_NAME_ID( cluster_DM_WEIGHT          ), TYPES_NAME_ID( cluster_ENG_CALIB_TOT       ), TYPES_NAME_ID( cluster_ENG_CALIB_OUT_T   ), TYPES_NAME_ID( cluster_ENG_CALIB_DEAD_TOT ),
      TYPES_NAME_ID( cluster_ENG_CALIB_FRAC_EM ), TYPES_NAME_ID( cluster_ENG_CALIB_FRAC_HAD ), TYPES_NAME_ID( cluster_ENG_CALIB_FRAC_REST ), TYPES_NAME_ID( cluster_EM_PROBABILITY    ), TYPES_NAME_ID( cluster_FIRST_PHI          ), 
      TYPES_NAME_ID( cluster_FIRST_ETA         ), TYPES_NAME_ID( cluster_SECOND_R           ), TYPES_NAME_ID( cluster_SECOND_LAMBDA       ), TYPES_NAME_ID( cluster_DELTA_PHI         ), TYPES_NAME_ID( cluster_DELTA_THETA        ), 
      TYPES_NAME_ID( cluster_DELTA_ALPHA       ), TYPES_NAME_ID( cluster_CENTER_X           ), TYPES_NAME_ID( cluster_CENTER_Y            ), TYPES_NAME_ID( cluster_CENTER_Z          ), TYPES_NAME_ID( cluster_CENTER_MAG         ), 
      TYPES_NAME_ID( cluster_CENTER_LAMBDA     ), TYPES_NAME_ID( cluster_LATERAL            ), TYPES_NAME_ID( cluster_LONGITUDINAL        ), TYPES_NAME_ID( cluster_ENG_FRAC_EM       ), TYPES_NAME_ID( cluster_ENG_FRAC_MAX       ), 
      TYPES_NAME_ID( cluster_ENG_FRAC_CORE     ), TYPES_NAME_ID( cluster_FIRST_ENG_DENS     ), TYPES_NAME_ID( cluster_SECOND_ENG_DENS     ), TYPES_NAME_ID( cluster_ISOLATION         ), TYPES_NAME_ID( cluster_ENG_BAD_CELLS      ), 
      TYPES_NAME_ID( cluster_N_BAD_CELLS       ), TYPES_NAME_ID( cluster_N_BAD_CELLS_CORR   ), TYPES_NAME_ID( cluster_BAD_CELLS_CORR_E    ), TYPES_NAME_ID( cluster_BADLARQ_FRAC      ), TYPES_NAME_ID( cluster_ENG_POS            ), 
      TYPES_NAME_ID( cluster_SIGNIFICANCE      ), TYPES_NAME_ID( cluster_CELL_SIGNIFICANCE  ), TYPES_NAME_ID( cluster_CELL_SIG_SAMPLING   ), TYPES_NAME_ID( cluster_AVG_LAR_Q         ), TYPES_NAME_ID( cluster_AVG_TILE_Q         ), 
      TYPES_NAME_ID( cluster_ENG_BAD_HV_CELLS  ), TYPES_NAME_ID( cluster_N_BAD_HV_CELLS     ), TYPES_NAME_ID( cluster_PTD                 ), TYPES_NAME_ID( cluster_MASS              ), TYPES_NAME_ID( cluster_SECOND_TIME        ),
      TYPES_NAME_ID( Entries                   ), TYPES_NAME_ID( UNKNOWN                    ), TYPES_NAME_ID( clusterResponse             ), TYPES_NAME_ID( clusterResponseLCW        ), TYPES_NAME_ID( clusterResponsePred        )
    };
    ///@brief Formatted name lookup
    static const std::map<Id,std::string> IdToLabel = {
      { seqNumber                  , "N_{seq}"                            }, { runNumber                 , "N_{run}"                          }, 
      { eventNumber                , "N_{evt}"                            }, { nPrimVtx                  , "N_{PV}"                           },
      { avgMu                      , "#mu"                                }, { truthE                    , "E_{particle}"                     }, 
      { truthPt                    , "p_{T,particle}"                     }, { truthEta                  , "y_{particle}"                     }, 
      { truthPhi                   , "#phi_{particle}"                    }, { truthPDG                  , "I_{PDG}"                          },     
      { truthJetE                  , "E_{jet}^{truth}"                    }, { truthJetPt                , "p_{T,jet}^{truth}"                }, 
      { truthJetEta                , "y_{jet}^{truth}"                    }, { truthJetPhi               , "#phi_{jet}^{truth}"               }, 
      { truthJetMatchingRadius     , "R_{match}^{truth}"                  }, { jetCalE                   , "E_{jet}^{JES}"                    }, 
      { jetCalPt                   , "p_{T}^{JES}"                        }, { jetCalEta                 , "y_{jet}^{JES}"                    }, 
      { jetCalPhi                  , "#phi_{jet}^{truth}"                 }, { jetCnt                    , "N_{jet}"                          },
      { jetRawE                    , "E_{jet}^{const}"                    }, { jetRawPt                  , "p_{T}^{const}"                    }, 
      { jetRawEta                  , "y_{jet}^{const}"                    }, { jetRawPhi                 , "#phi_{jet}^{const}"               }, 
      { jetNConst                  , "N_{jet}^{const}"                    }, { clusterIndex              , "i_{clus}"                         }, 
      { cluster_nCells             , "N_{clus}^{cells}|E_{cell}>0"        }, { cluster_nCells_tot        , "N_{clus}^{cell}"                  }, 
      { clusterE                   , "E_{clus}^{EM}"                      }, { clusterPt                 , "p_{T,clus}^{EM}"                  }, 
      { clusterEta                 , "y_{clus}^{EM}"                      }, { clusterPhi                , "#phi_{clus}^{EM}"                 }, 
      { nCluster                   , "N_{clus}"                           }, { clusterECalib             , "E_{clus}^{LCW}"                   }, 
      { clusterPtCalib             , "p_{T,clus}^{LCW}"                   }, { clusterEtaCalib           , "y_{clus}^{LCW}"                   }, 
      { clusterPhiCalib            , "#phi_{clus}^{LCW}"                  }, { cluster_sumCellECalib     , "#sum E_{cell}^{LCW}"              }, 
      { cluster_fracECalib         , "E_{clus}^{LCW}/#sum E_{clus}^{LCW}" }, { cluster_fracECalib_ref    , "E_{clus}^{LCW}/E_{ref}"           },           
      { cluster_sumCellE           , "#sum E_{cell}"                      }, { cluster_fracE             , "E_{clus}^{EM}/#sum E_{clus}^{EM}" }, 
      { cluster_fracE_ref          , "E_{clus}^{EM}/E_{ref}"              }, { cluster_time              , "t_{clus}"                         }, 
      { cluster_HAD_WEIGHT         , "w_{cal}^{had}"                      }, { cluster_OOC_WEIGHT        , "w_{cal}^{OOC}"                    }, 
      { cluster_DM_WEIGHT          , "w_{cal}^{DM}"                       }, { cluster_ENG_CALIB_TOT     , "E_{clus}^{dep}"                   }, 
      { cluster_ENG_CALIB_OUT_T    , "E_{clus}^{OOC,dep}"                 }, { cluster_ENG_CALIB_DEAD_TOT, "E_{clus}^{DM,dep}"                },
      { cluster_ENG_CALIB_FRAC_EM  , "f_{calib}^{EM}"                     }, { cluster_ENG_CALIB_FRAC_HAD, "f_{calib}^{HAD}"                  }, 
      { cluster_ENG_CALIB_FRAC_REST, "f_{calib}^{other}"                  }, { cluster_EM_PROBABILITY    , "P_{clus}^{EM}"                    }, 
      { cluster_FIRST_PHI          , "#langle#phi_{cell}#rangle"          }, { cluster_FIRST_ETA         , "#langley_{cell}#rangle"           }, 
      { cluster_SECOND_R           , "#langler^{2}#rangle"                }, { cluster_SECOND_LAMBDA     , "#langle#lambda^{2}#rangle"        }, 
      { cluster_DELTA_PHI          , "#Delta#phi"                         }, { cluster_DELTA_THETA       , "#Delta#theta"                     }, 
      { cluster_DELTA_ALPHA        , "#Delta#alpha"                       }, { cluster_CENTER_X          , "X_{clus}"                         }, 
      { cluster_CENTER_Y           , "Y_{clus}"                           }, { cluster_CENTER_Z          , "Z_{clus}"                         }, 
      { cluster_CENTER_MAG         , "R_{clus}"                           }, { cluster_CENTER_LAMBDA     , "#lambda_{clus}"                   }, 
      { cluster_LATERAL            , "#langlem_{lat}^{2}#rangle"          }, { cluster_LONGITUDINAL      , "#langlem_{long}^{2}#rangle"       }, 
      { cluster_ENG_FRAC_EM        , "f_{clus}^{emc}"                     }, { cluster_ENG_FRAC_MAX      , "f_{clus}^{max}"                   }, 
      { cluster_ENG_FRAC_CORE      , "f_{clus}^{core}"                    }, { cluster_FIRST_ENG_DENS    , "#langle#rho_{cell}#rangle"        }, 
      { cluster_SECOND_ENG_DENS    , "#langle#rho_{cell}^{2}#rangle"      }, { cluster_ISOLATION         , "f_{clus}^{iso}"                   }, 
      { cluster_ENG_BAD_CELLS      , "E_{clus}^{bad}"                     }, { cluster_N_BAD_CELLS       , "N_{cell}^{bad}"                   }, 
      { cluster_N_BAD_CELLS_CORR   , "N_{cell}^{bad,corr}"                }, { cluster_BAD_CELLS_CORR_E  , "E_{clus}^{bad,corr}"              }, 
      { cluster_BADLARQ_FRAC       , "f_{clus}^{bad LAr}"                 }, { cluster_ENG_POS           , "E_{clus}^{EM#plus}"               }, 
      { cluster_SIGNIFICANCE       , "#zeta_{clus}^{EM}"                  }, { cluster_CELL_SIGNIFICANCE , "#zeta_{cell}^{max}"               }, 
      { cluster_CELL_SIG_SAMPLING  , "S_{cell}^{signif}"                  }, { cluster_AVG_LAR_Q         , "#langleQ_{cell}^{LAr}#rangle"     }, 
      { cluster_AVG_TILE_Q         , "#langleQ_{cell}^{Tile}#rangle"      }, { cluster_ENG_BAD_HV_CELLS  , "E_{clus}^{bad HV}"                }, 
      { cluster_N_BAD_HV_CELLS     , "N_{cell}^{bad HV}"                  }, { cluster_PTD               , "p_{T}D_{clus}"                    }, 
      { cluster_MASS               , "M_{clus}"                           }, { cluster_SECOND_TIME       , "#langlet_{cell}^{2}#rangle"       },
      { Entries                    , "Entries"                            }, { UNKNOWN                   , ""                                 },
      { clusterResponse            , "R_{clus}^{EM}"                      }, { clusterResponseLCW        , "R_{clus}^{LCW}"                   },
      { clusterResponsePred        , "R_{clus}^{pred}"                    }
    };
    ///@brief Dictionary lookup
    struct Convert { 
      ///@param  vid identifier
      ///@return Reference to unmodifiable @c std::string storing the identifier name. 
      TYPES_ENUM_NAME( vid, Id, IdToName )
      ///@brief Identifier-to-characters dictionary lookup (e.g., for printing)
      ///@param  vid identifier
      ///@return Pointer to unmodifiable character array storing the identifier name. 
      TYPES_ENUM_CHAR( vid, Id, IdToName )
      ///@brief Name-to-Id lookup 
      ///@param name reference to unmodifiable @c std::string containing the identifier name
      ///@return A valid Id if name is known, else the @c Moment::UNKNOWN tag 
      TYPES_NAME_ENUM( name, Id, NameToId, Id::UNKNOWN )
      ///@brief Id-to-label lookup
      ///@param vid  identifier
      ///@return Reference to unmodifiable @c std::string storing the label.
      TYPES_ENUM_LABEL( vid, Id, IdToLabel );
    };
  } // Types::Variable
  ///@brief Object identification
  namespace Object {
    ///@brief Object type identifiers
    enum class Type { Cluster  = 0x100, Jet = 0x200, Particle = 0x400, Unknown = 0x000 };
    ///@name Local useful types
    ///@{
    typedef std::string Name;
    ///@} 
    ///@name Dictionaries
    ///@{
    /// Identifier-to-name translation
    static const std::map<Type,Name> IdToName = { 
      { Type::Cluster , "Cluster"  },
      { Type::Jet     , "Jet"      },
      { Type::Particle, "Particle" },
      { Type::Unknown , "unknown"  }
    }; 
    /// Name-to-identifier translation
    static const std::map<Name,Type> NameToId = {
      { IdToName.at(Type::Cluster ), Type::Cluster  },
      { IdToName.at(Type::Jet     ), Type::Jet      },
      { IdToName.at(Type::Particle), Type::Particle },
      { IdToName.at(Type::Unknown ), Type::Unknown  }
    };
    ///@}
    ///@name Converters
    ///@{
    const Name& idToName(Type type);
    const Type  nameToId(const Name& name);
    ///@}
  }
  ///@brief Value descriptors
  namespace Value {
    ///@brief Labels for known value types
    enum class Type  { E=0x1000, pT=0x1010, y=0x1020, eta=0x1040, phi=0x1080, m=0x1100, q=0x1000, PDGId=0x2010, Unknown=0x0000 };
    ///@name Useful local types
    ///@{
    typedef std::string            Label;      ///< Label (short name, abbreviation)
    typedef std::string            Name;       ///< Name (full name)
    typedef std::tuple<Label,Name> Descriptor; ///< Descriptor
    ///@}
    ///@brief Label extractor
    /// Extracts lable from descriptor
    ///@param tdescr reference to non-modifiable descriptor
    ///@return Reference to non-modifiable label.
    const Label& label(const Descriptor& tdescr);
    ///@brief Name extractor
    /// Extracts lable from descriptor
    ///@param tdescr reference to non-modifiable descriptor
    ///@return Reference to non-modifiable name.
    const Name&  name (const Descriptor& tdescr);
    ///@name Dictionaries
    ///@{
    /// Type-to-descriptor translation
    static const std::map<Type,Descriptor> TypeDescription = {
      { Type::E      , { "E"      , "energy"              } },
      { Type::pT     , { "pT"     , "transverse momentum" } },
      { Type::y      , { "y"      , "rapidity"            } },
      { Type::eta    , { "eta"    , "pseudorapidity"      } },
      { Type::phi    , { "phi"    , "azimuth"             } },
      { Type::m      , { "m"      , "mass"                } },
      { Type::q      , { "q"      , "charge"              } },
      { Type::PDGId  , { "PDGId"  , "PDGId"               } },
      { Type::Unknown, { "Unknown", "Unknown"             } }
    };
    /// Label-to-type translation
    static const std::map<Label,Type> LabelToType = { 
      { label(TypeDescription.at(Type::E      )), Type::E       },
      { label(TypeDescription.at(Type::pT     )), Type::pT      },
      { label(TypeDescription.at(Type::y      )), Type::y       },
      { label(TypeDescription.at(Type::eta    )), Type::eta     },
      { label(TypeDescription.at(Type::phi    )), Type::phi     },
      { label(TypeDescription.at(Type::m      )), Type::m       },
      { label(TypeDescription.at(Type::q      )), Type::q       },
      { label(TypeDescription.at(Type::PDGId  )), Type::PDGId   },
      { label(TypeDescription.at(Type::Unknown)), Type::Unknown }
    };
    /// Name-to-type translation
    static const std::map<Name,Type> NameToType = { 
      { name(TypeDescription.at(Type::E      )), Type::E       },
      { name(TypeDescription.at(Type::pT     )), Type::pT      },
      { name(TypeDescription.at(Type::y      )), Type::y       },
      { name(TypeDescription.at(Type::eta    )), Type::eta     },
      { name(TypeDescription.at(Type::phi    )), Type::phi     },
      { name(TypeDescription.at(Type::m      )), Type::m       },
      { name(TypeDescription.at(Type::q      )), Type::q       },
      { name(TypeDescription.at(Type::PDGId  )), Type::PDGId   },
      { name(TypeDescription.at(Type::Unknown)), Type::Unknown }
    };
    ///@}
    ///@name Converters
    ///@param t     type of value
    ///@param name  reference to non-modifiable name
    ///@param label reference to non-modifiable label
    ///@{
    const Descriptor & descriptor (Type t);                   ///< Get descriptor for given value type
    const Name&        typeToName (Type t);                   ///< Get name for given value type
    const Label&       typeToLabel(Type t);                   ///< Get label for given value type
    const Type         nameToType (const std::string& name) ; ///< Get value type for given name
    const Type         labelToType(const std::string& label); ///< Get value type for given label
    ///@}      
  } // Types::Value
  namespace Scale { 
    enum class Type { RAW=0x10001, LCW=0x10002, JES=0x20002, HAD=0x10004, ML=0x10008, TRUTH=30002, Unknown=0x00000 }; 
    ///@name Useful local types
    ///@{
    typedef std::string            Label;      ///< Label (short name, abbreviation)
    typedef std::string            Name;       ///< Name (full name)
    typedef std::tuple<Label,Name> Descriptor; ///< Descriptor
    ///@}
    ///@brief Label extractor
    /// Extracts lable from descriptor
    ///@param tdescr reference to non-modifiable descriptor
    ///@return Reference to non-modifiable label.
    const Label& label(const Descriptor& tdescr);
    ///@brief Name extractor
    /// Extracts lable from descriptor
    ///@param tdescr reference to non-modifiable descriptor
    ///@return Reference to non-modifiable name.
    const Name&  name (const Descriptor& tdescr);
    ///@name Dictionaries
    ///@{
    /// Type-to-descriptor translation
    static const std::map<Type,Descriptor> TypeDescription = {
      { Type::RAW    , { "RAW"    , "raw (em) scale"           } },
      { Type::LCW    , { "LCW"    , "full local scale"         } },
      { Type::HAD    , { "HAD"    , "local hadronic scale"     } },
      { Type::ML     , { "ML"     , "predicted hadronic scale" } },
      { Type::JES    , { "JES"    , "jet energy scale"         } },
      { Type::TRUTH  , { "TRUTH"  , "true energy scale"        } },
      { Type::Unknown, { "Unknown", "Unknown"                  } }
    };
    /// Label-to-type translation
    static const std::map<Label,Type> LabelToType = { 
      { label(TypeDescription.at(Type::RAW    )), Type::RAW     },
      { label(TypeDescription.at(Type::LCW    )), Type::LCW     },
      { label(TypeDescription.at(Type::HAD    )), Type::HAD     },
      { label(TypeDescription.at(Type::ML     )), Type::ML      },
      { label(TypeDescription.at(Type::JES    )), Type::JES     },
      { label(TypeDescription.at(Type::TRUTH  )), Type::TRUTH   },
      { label(TypeDescription.at(Type::Unknown)), Type::Unknown }
    };
    /// Name-to-type translation
    static const std::map<Name,Type> NameToType = { 
      { name(TypeDescription.at(Type::RAW    )), Type::RAW     },
      { name(TypeDescription.at(Type::LCW    )), Type::LCW     },
      { name(TypeDescription.at(Type::HAD    )), Type::HAD     },
      { name(TypeDescription.at(Type::ML     )), Type::ML      },
      { name(TypeDescription.at(Type::JES    )), Type::JES     },
      { name(TypeDescription.at(Type::TRUTH  )), Type::TRUTH   },
      { name(TypeDescription.at(Type::Unknown)), Type::Unknown }
    };
    ///@}
    ///@name Converters
    ///@param t     type of value
    ///@param name  reference to non-modifiable name
    ///@param label reference to non-modifiable label
    ///@{
    const Descriptor & descriptor (Type t);                   ///< Get descriptor for given value type
    const Name&        typeToName (Type t);                   ///< Get name for given value type
    const Label&       typeToLabel(Type t);                   ///< Get label for given value type
    const Type         nameToType (const std::string& name) ; ///< Get value type for given name
    const Type         labelToType(const std::string& label); ///< Get value type for given label
    ///@}      
  } //Types::Scale}
  /// @brief Numerical types
  namespace Numerical {
      typedef std::size_t    size_t;     ///< Size type from C standard library
      typedef unsigned int   uint_t;     ///< Unsigned integral number type
      typedef int            int_t;      ///< Signed integral number type
      typedef short          short_t;    ///< Signed short integral number type
      typedef unsigned short ushort_t;   ///< Unsigned short integral number type
      typedef long           long_t;     ///< Signed long integral number type 
      typedef unsigned long  ulong_t;    ///< Unsigned long integral number type
      typedef long long      llong_t;    ///< Signed very long integral number type
      typedef float          float_t;    ///< Single precision real number type
      typedef double         double_t;   ///< Double precision real number type
      typedef unsigned int   mask_t;     ///< Bit pattern type
  } // Types::Numerical
} // Types

inline const Types::Object::Name& Types::Object::idToName(Types::Object::Type type)        { return IdToName.at(type); }
inline const Types::Object::Type  Types::Object::nameToId(const Types::Object::Name& name) { auto ftype(NameToId.find(name)); return ftype != NameToId.end() ? ftype->second : Type::Unknown; } 

inline const Types::Value::Label& Types::Value::label(const Types::Value::Descriptor& tdescr) { return std::get<0>(tdescr); }
inline const Types::Value::Name&  Types::Value::name (const Types::Value::Descriptor& tdescr) { return std::get<1>(tdescr); }

inline const Types::Value::Descriptor& Types::Value::descriptor (Types::Value::Type t)             { return TypeDescription.at(t); }
inline const Types::Value::Name&       Types::Value::typeToName (Types::Value::Type t)             { return name(descriptor(t));   }
inline const Types::Value::Label&      Types::Value::typeToLabel(Types::Value::Type t)             { return label(descriptor(t));  }
inline const Types::Value::Type        Types::Value::nameToType (const Types::Value::Name& name)   { auto ftype(NameToType.find(name))  ; return ftype != NameToType.end()  ? ftype->second : Type::Unknown; }
inline const Types::Value::Type        Types::Value::labelToType(const Types::Value::Label& label) { auto ftype(LabelToType.find(label)); return ftype != LabelToType.end() ? ftype->second : Type::Unknown; }      

inline const Types::Scale::Label& Types::Scale::label(const Types::Scale::Descriptor& tdescr) { return std::get<0>(tdescr); }
inline const Types::Scale::Name&  Types::Scale::name (const Types::Scale::Descriptor& tdescr) { return std::get<1>(tdescr); }

inline const Types::Scale::Descriptor& Types::Scale::descriptor (Types::Scale::Type t)             { return TypeDescription.at(t); }
inline const Types::Scale::Name&       Types::Scale::typeToName (Types::Scale::Type t)             { return name(descriptor(t));   }
inline const Types::Scale::Label&      Types::Scale::typeToLabel(Types::Scale::Type t)             { return label(descriptor(t));  }
inline const Types::Scale::Type        Types::Scale::nameToType (const Types::Scale::Name& name)   { auto ftype(NameToType.find(name))  ; return ftype != NameToType.end()  ? ftype->second : Type::Unknown; }
inline const Types::Scale::Type        Types::Scale::labelToType(const Types::Scale::Label& label) { auto ftype(LabelToType.find(label)); return ftype != LabelToType.end() ? ftype->second : Type::Unknown; }      

#endif
