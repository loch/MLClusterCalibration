// -*- c++ -*-
#ifndef UNITS_H
#define UNITS_H

#include "config.h"

#include <string>
#include <map> 
#include <vector>

#include <cmath>
#include <cstdio>

// #include <algorithm>

// #include <ostream>
// #include <iostream>

#include "Types.h"

#include "Math.h"

///@brief Default unit setting
/// The data for the ML calibration analysis  uses of GeV as default unit - all other default units are 
/// the same as defined in @c GaudiKernel/SystemOfUnits.h . This is a compiler option which can be set 
/// using @c -DUNITS_USE_GEV . 
#ifdef UNITS_USE_GEV
#  define UNITS_E_BASE 1.0E-03
#  define UNITS_E_NAME "GeV"
#else
#  define UNITS_E_BASE 1.0
#  define UNITS_E_NAME "MeV"   
#endif
// #ifdef PRAGMA_MSG_PAR
// #  pragma PRAGMA_MSG_PAR( "selected base unit scale factor ...:" , UNITS_E_BASE )
// #  pragma PRAGMA_MSG_PAR( "selected base unit name ...........:" , UNITS_E_NAME )
// #endif

///@brief Units
///
/// This code is cased on @c GaudiKernel/SystemOfUnits. The basic units are:
///
/// millimeter              (millimeter)
/// nanosecond              (nanosecond)
/// Mega electron Volt      (MeV)
/// positron charge         (eplus)
/// degree Kelvin           (kelvin)
/// the amount of substance (mole)
/// luminous intensity      (candela)
/// radian                  (radian)
/// steradian               (steradian)
///
/// The compiler directive @c UNITS_USE_GEV changes the MeV to GeV as the basic energy unit. 
namespace Units {
  ///@name Length, area, volume 
  ///@{
  static const std::string gDimensions  = "LinearDimension";     ///< Group of dimensional units
  static const std::string gXsect       = "CrossSection";        ///< Group of cross section units
  static const std::string gXsectInv    = "CrossSectionInverse"; ///< Group of inverse cross section units 
  constexpr double      millimeter   = 1.;
  constexpr double      millimeter2  = millimeter * millimeter;
  constexpr double      millimeter3  = millimeter * millimeter * millimeter;
  constexpr double      centimeter   = 10. * millimeter;
  constexpr double      centimeter2  = centimeter * centimeter;
  constexpr double      centimeter3  = centimeter * centimeter * centimeter;
  constexpr double      meter        = 1000. * millimeter;
  constexpr double      meter2       = meter * meter;
  constexpr double      meter3       = meter * meter * meter;
  constexpr double      kilometer    = 1000. * meter;
  constexpr double      kilometer2   = kilometer * kilometer;
  constexpr double      kilometer3   = kilometer * kilometer * kilometer;
  constexpr double      parsec       = 3.0856775807e+16 * meter;
  constexpr double      micrometer   = 1.0e-06 * meter;
  constexpr double      nanometer    = 1.0e-09 * meter;
  constexpr double      picometer    = 1.0e-12 * meter;
  constexpr double      femtometer   = 1.0e-15 * meter; 
  constexpr double      angstrom     = 1.0e-10 * meter;
  constexpr double      fermi        = femtometer;
  constexpr double      barn         = 1.0e-28 * meter2;
  constexpr double      millibarn    = 1.0e-03 * barn;
  constexpr double      microbarn    = 1.0e-06 * barn;
  constexpr double      nanobarn     = 1.0e-09 * barn;
  constexpr double      picobarn     = 1.0e-12 * barn;
  constexpr double      femtobarn    = 1.0e-15 * barn;
  constexpr double      liter        = 1.0e+03 * centimeter3;
  constexpr double      deciliter    = 1.0e-01 * liter;
  constexpr double      centiliter   = 1.0e-02 * liter;
  constexpr double      milliliter   = 1.0e-03 * liter;
  constexpr double      fm           = femtometer;
  constexpr double      pm           = picometer;
  constexpr double      nm           = nanometer;
  constexpr double      um           = micrometer;
  constexpr double      mm           = millimeter;
  constexpr double      mm2          = millimeter2;
  constexpr double      mm3          = millimeter3;
  constexpr double      cm           = centimeter;
  constexpr double      cm2          = centimeter2;
  constexpr double      cm3          = centimeter3;
  constexpr double      m            = meter;
  constexpr double      m2           = meter2;
  constexpr double      m3           = meter3;
  constexpr double      km           = kilometer;
  constexpr double      km2          = kilometer2;
  constexpr double      km3          = kilometer3;
  constexpr double      pc           = parsec;
  constexpr double      L            = liter;
  constexpr double      dL           = deciliter;
  constexpr double      cL           = centiliter; 
  constexpr double      mL           = milliliter;
  constexpr double      b            = barn;
  constexpr double      mb           = millibarn;
  constexpr double      ub           = microbarn;
  constexpr double      nb           = nanobarn; 
  constexpr double      pb           = picobarn; 
  constexpr double      fb           = femtobarn;
  ///@}
  ///@name Angles
  ///@{
  static const std::string gAngular     = "AngularDimension";
  constexpr double      radian       = 1.0;
  constexpr double      milliradian  = 1.0e-03 * radian;
  constexpr double      degree       = ( M_PI / 180. ) * radian;
  constexpr double      steradian    = 1.;
  constexpr double      rad          = radian;
  constexpr double      mrad         = milliradian;
  constexpr double      sr           = steradian;
  constexpr double      deg          = degree;
  ///@}
  ///@name Time
  ///@{
  static const std::string gTime        = "Time";
  static const std::string gFrequency   = "Frequency";
  constexpr double      nanosecond   = 1.;
  constexpr double      second       = 1.0e+09 * nanosecond;
  constexpr double      millisecond  = 1.0e-03 * second;
  constexpr double      microsecond  = 1.0e-06 * second;
  constexpr double      picosecond   = 1.0e-12 * second;
  constexpr double      femtosecond  = 1.0e-15 * second;
  constexpr double      minute       = 60.0 * second;
  constexpr double      hour         = 60.0 * minute;
  constexpr double      day          = 24.0 * hour;
  constexpr double      week         = 7.0 * day;
  constexpr double      hertz        = 1. / second;
  constexpr double      kilohertz    = 1.0e+03 * hertz;
  constexpr double      megahertz    = 1.0e+06 * hertz;
  constexpr double      gigahertz    = 1.0e+09 * hertz;
  constexpr double      fs           = femtosecond;
  constexpr double      ps           = picosecond;
  constexpr double      us           = microsecond;
  constexpr double      ns           = nanosecond;
  constexpr double      s            = second;
  constexpr double      ms           = millisecond;
  constexpr double      min          = minute;
  constexpr double      hr           = hour;
  constexpr double      dy           = day;
  constexpr double      wk           = week;
  constexpr double      Hz           = hertz;
  constexpr double      kHz          = kilohertz;
  constexpr double      MHz          = megahertz;
  constexpr double      GHz          = gigahertz;
  ///@}
  ///@name Electric charge
  ///@{
  static const std::string gCharge      = "ElectricCharge"; 
  constexpr double      eplus        = 1.;              // positron charge
  constexpr double      e_SI         = 1.602176487e-19; // positron charge in coulomb
  constexpr double      coulomb      = eplus / e_SI;    // coulomb = 6.24150 e+18 * eplus
  constexpr double      millicoulomb = 1.0e-03 * coulomb;
  constexpr double      microcoulomb = 1.0e-06 * coulomb;
  constexpr double      nanocoulomb  = 1.0e-09 * coulomb;
  constexpr double      picocoulomb  = 1.0e-12 * coulomb;
  constexpr double      femtocoulomb = 1.0e-15 * coulomb;
  constexpr double      e            = eplus;
  constexpr double      C            = coulomb;
  constexpr double      mC           = millicoulomb;
  constexpr double      uC           = microcoulomb;
  constexpr double      nC           = nanocoulomb ;
  constexpr double      pC           = picocoulomb ;
  constexpr double      fC           = femtocoulomb;
  ///@}
  ///@name Energy 
  ///@{
  static const std::string gEnergy          = "Energy";
  constexpr double      megaelectronvolt = UNITS_E_BASE ;
  constexpr double      electronvolt     = 1.0e-06 * megaelectronvolt;
  constexpr double      kiloelectronvolt = 1.0e-03 * megaelectronvolt;
  constexpr double      gigaelectronvolt = 1.0e+03 * megaelectronvolt;
  constexpr double      teraelectronvolt = 1.0e+06 * megaelectronvolt;
  constexpr double      petaelectronvolt = 1.0e+09 * megaelectronvolt;
  constexpr double      joule            = electronvolt / e_SI; // joule = 6.24150 e+12 * MeV
  constexpr double      MeV              = megaelectronvolt;
  constexpr double      eV               = electronvolt;
  constexpr double      keV              = kiloelectronvolt;
  constexpr double      GeV              = gigaelectronvolt;
  constexpr double      TeV              = teraelectronvolt;
  constexpr double      PeV              = petaelectronvolt;
  constexpr double      J                = joule;
  ///@}
  ///@name Mass
  ///@{
  static const std::string gMass            = "Mass";
  constexpr double      kilogram         = joule * second * second / ( meter * meter );
  constexpr double      gram             = 1.0e-03 * kilogram;
  constexpr double      milligram        = 1.0e-03 * gram;
  constexpr double      microgram        = 1.0e-03 * milligram;
  constexpr double      kg               = kilogram;
  constexpr double      g                = gram;
  constexpr double      mg               = milligram;
  constexpr double      ug               = microgram;
  ///@}
  ///@name Power, force and pressure
  ///@{
  static const std::string gPower           = "Power";
  static const std::string gForce           = "Force";
  static const std::string gPressure        = "Pressure";
  constexpr double      watt             = joule / second; // watt = 6.24150 e+3 * MeV/ns
  constexpr double      kilowatt         = 1.0e+03 * watt;
  constexpr double      megawatt         = 1.0e+03 * kilowatt;
  constexpr double      gigawatt         = 1.0e+03 * megawatt;
  constexpr double      milliwatt        = 1.0e-03 * watt;
  constexpr double      newton           = joule / meter;   // newton = 6.24150 e+9 * MeV/mm
  constexpr double      kilonewton       = joule / meter;   // newton = 6.24150 e+9 * MeV/mm
  constexpr double      hep_pascal       = newton / m2;     // pascal = 6.24150 e+3 * MeV/mm3
#ifndef pascal
  constexpr double      pascal           = hep_pascal;      // to match CLHEP
#endif
  constexpr double      bar              = 1.0e+05 * pascal; // bar    = 6.24150 e+8 * MeV/mm3
  constexpr double      millibar         = 1.0e-03 * bar;
  constexpr double      atmosphere       = 101325  * pascal; // atm    = 6.32420 e+8 * MeV/mm3
  constexpr double      hectopascal      = 1.0e+02 * pascal;
  constexpr double      kilopascal       = 1.0e+03 * pascal;
  constexpr double      W                = watt;
  constexpr double      kW               = kilowatt;
  constexpr double      MW               = megawatt;
  constexpr double      GW               = gigawatt;
  constexpr double      mW               = milliwatt;
  constexpr double      N                = newton;
  constexpr double      kN               = kilonewton;
  constexpr double      Pa               = pascal;
  constexpr double      hPa              = hectopascal;
  constexpr double      kPa              = kilopascal;
  constexpr double      mbar             = millibar;
  constexpr double      atm              = atmosphere;  
  ///@}
  ///@{
  ///@name Electrical energy
  ///@{
  static const std::string gElecEnergy      = "ElectricEnergy"; 
  constexpr double      watthour         = watt     * hour; 
  constexpr double      kilowatthour     = kilowatt * hour; 
  constexpr double      megawatthour     = megawatt * hour;
  constexpr double      gigawatthour     = gigawatt * hour;
  constexpr double      Wh               = watthour    ; 
  constexpr double      kWh              = kilowatthour; 
  constexpr double      MWh              = megawatthour;
  constexpr double      GWh              = gigawatthour;
  ///@}
  ///@name Electric current
  ///@{
  static const std::string gCurrent         = "ElectricCurrent";
  constexpr double      ampere           = coulomb / second; // ampere = 6.24150 e+9 * eplus/ns
  constexpr double      milliampere      = 1.0e-03 * ampere;
  constexpr double      microampere      = 1.0e-06 * ampere;
  constexpr double      nanoampere       = 1.0e-09 * ampere;
  constexpr double      A                = ampere; 
  constexpr double      mA               = milliampere; 
  constexpr double      uA               = microampere; 
  constexpr double      nA               = nanoampere; 
  ///@}
  ///@name ELectric potential
  ///@{
  static const std::string gVoltage         = "ElectricPotential";  
  constexpr double      megavolt         = megaelectronvolt / eplus;
  constexpr double      kilovolt         = 1.0e-03 * megavolt;
  constexpr double      volt             = 1.0e-03 * kilovolt;
  constexpr double      millivolt        = 1.0e-03 * volt;
  constexpr double      V                = volt;
  constexpr double      mV               = millivolt;
  constexpr double      MV               = megavolt;
  constexpr double      kV               = kilovolt;
  ///@}
  ///@name Electric resistance
  ///@{
  static const std::string gResistance      = "ElectricResistance"; 
  constexpr double      ohm              = volt / ampere; // ohm = 1.60217e-16*(MeV/eplus)/(eplus/ns)
  constexpr double      kiloohm          = 1.0e+03 * ohm;
  constexpr double      megaohm          = 1.0e+03 * kiloohm;
  constexpr double      milliohm         = 1.0e+03 * ohm;
  constexpr double      O                = ohm              ; // ohm = 1.60217e-16*(MeV/eplus)/(eplus/ns)
  constexpr double      kO               = kiloohm          ;
  constexpr double      MO               = megaohm          ;
  constexpr double      mO               = milliohm         ;
  ///@}
  ///@name Electric capacitance
  ///@{ 
  static const std::string gCapacitance     = "ElectricCapacitance";
  constexpr double      farad            = coulomb / volt; // farad = 6.24150e+24 * eplus/Megavolt
  constexpr double      millifarad       = 1.0e-03 * farad;
  constexpr double      microfarad       = 1.0e-06 * farad;
  constexpr double      nanofarad        = 1.0e-09 * farad;
  constexpr double      picofarad        = 1.0e-12 * farad;
  constexpr double      F                = farad;
  constexpr double      mF               = millifarad;
  constexpr double      uF               = microfarad;
  constexpr double      nF               = nanofarad; 
  constexpr double      pF               = picofarad;
  ///@}
  ///@name Magnetic flux, field and inductance
  ///@{
  static const std::string gMagFlux         = "MagneticFlux";
  static const std::string gMagField        = "MagneticField";
  static const std::string gMagInductance   = "MagneticInductance";
  constexpr double      weber            = volt * second; // weber = 1000*megavolt*ns
  constexpr double      tesla            = volt * second / meter2; // tesla =0.001*megavolt*ns/mm2
  constexpr double      gauss            = 1.0e-04 * tesla;
  constexpr double      kilogauss        = 1.0e+03 * gauss;
  constexpr double      henry            = weber / ampere; // henry = 1.60217e-7*MeV*(ns/eplus)**2
  constexpr double      Wb               = weber;
  constexpr double      T                = tesla;
  constexpr double      G                = gauss;
  constexpr double      kG               = kilogauss;
  constexpr double      Gs               = gauss;
  constexpr double      kGs              = kilogauss;
  constexpr double      H                = henry;
  ///@}  
  ///@name Temperature, amount of substance 
  ///@{
  static const std::string gTemperature    = "Temperature";
  static const std::string gSubstance      = "AmountOfSubstance";
  constexpr double      kelvin          = 1.0;
  constexpr double      mole            = 1.0;
  constexpr double      K               = kelvin;
  ///@}
  ///@name Radioactivity and absorbed dose
  ///@{
  static const std::string gRadioActivity  = "RadioActivity";
  static const std::string gDose           = "AbsorbedDose";
  constexpr double      becquerel       = 1. / second;
  constexpr double      curie           = 3.7e+10 * becquerel;
  constexpr double      kilobecquerel   = 1.0e+03 * becquerel;
  constexpr double      megabecquerel   = 1.0e+06 * becquerel;
  constexpr double      gigabecquerel   = 1.0e+09 * becquerel;
  constexpr double      millicurie      = 1.0e-03 * curie;
  constexpr double      microcurie      = 1.0e-06 * curie;
  constexpr double      gray            = joule / kilogram;
  constexpr double      kilogray        = 1.0e+03 * gray;
  constexpr double      milligray       = 1.0e-03 * gray;
  constexpr double      microgray       = 1.0e-06 * gray;
  constexpr double      Bq              = becquerel;
  constexpr double      kBq             = kilobecquerel;
  constexpr double      MBq             = megabecquerel;
  constexpr double      GBq             = gigabecquerel;
  constexpr double      Ci              = curie;
  constexpr double      mCi             = millicurie;
  constexpr double      uCi             = microcurie;
  constexpr double      Gy              = gray;      
  constexpr double      kGy             = kilogray; 
  constexpr double      mGy             = milligray;
  constexpr double      uGy             = microgray;
  ///@}
  ///@name Luminous intensity, flux and illuminance
  ///@{
  static const std::string gLight          = "Light";
  constexpr double      candela         = 1.;                  ///< Luminous intensity
  constexpr double      lumen           = candela * steradian; ///< Luminous flux
  constexpr double      lux             = lumen / meter2;      ///< Illuminance
  constexpr double      cd              = candela;
  constexpr double      lm              = lumen;
  constexpr double      lx              = lux;
  ///@}
  ///@name Miscellaneous
  static const std::string gNumerical     = "Numerical"; 
  constexpr double      perCent        = 0.01;
  constexpr double      perThousand    = 0.001;
  constexpr double      perMillion     = 0.000001;
  ///@}
  ///@brief Unit descriptor
  struct Descriptor {
    double      scale      = { 1. };    ///< Unit scale factor
    std::string name       = { "" };    ///< Unit name
    std::string abbr       = { "" };    ///< Unit abbreviation
    std::string symbol     = { "" };    ///< Unit screen symbol
    std::string axis       = { "" };    ///< Unit axis title (general)
    std::string latex_axis = { "" };    ///< Unit axis title (LaTeX)
    std::string root_axis  = { "" };    ///< Unit axis title (ROOT)
    std::string group      = { "" };    ///< Unit group
    bool        isBase     = { false }; ///< Unit is a base unit if @c true
    ///@brief Default (invalid) unit description
    Descriptor();
    ///@brief General (loaded) unit description
    Descriptor(double sfactor,const std::string& nm,const std::string& ab,const std::string& sy,const std::string& ax,const std::string& lx,const std::string& rx,const std::string& gp,bool fb=false);
    ///@brief Copy constructor
    Descriptor(const Descriptor& udescr);
    ///@brief Assignment operator
    Descriptor& operator=(const Descriptor& udescr);
  };
  ///@brief Helpers
  ///  static std::string addBackslash(const std::string& text) { return std::string("\\")+text; }
  ///@brief Unit description dictionary
  ///
  /// The key to this dictionary is the abbreviated unit name which is identical to the short name of the scale factor (if any, else it is the full name).
  class DescriptionLoader {
  public:
    typedef std::map<std::string,Descriptor> DataBase;
    static DescriptionLoader* instance();
    const DataBase& dataBase() const;
    const Descriptor& descriptor(const std::string& key) const; 
  private:
    DescriptionLoader() { }
    static DescriptionLoader* m_instance;
    static DataBase           m_database;
  };
  static const DescriptionLoader::DataBase& Descriptions = { DescriptionLoader::instance()->dataBase() };
  ///@name Access functions
  ///@{
  const Descriptor&  description(const std::string& key,const DescriptionLoader::DataBase& map=Descriptions);                                                       ///< Load specific @c Descriptor
  const std::string& name       (const std::string& key,const DescriptionLoader::DataBase& map=Descriptions);                                                       ///< Get unit name
  double             scale      (const std::string& key,const DescriptionLoader::DataBase& map=Descriptions);                                                       ///< Get unit scale factor
  const std::string& group      (const std::string& key,const DescriptionLoader::DataBase& map=Descriptions);                                                       ///< Get unit group 
  const std::string& axis       (const std::string& key,Types::Display::Mode m=Types::Display::Mode::Standard,const DescriptionLoader::DataBase& map=Descriptions); ///< Get a specific axis title
  ///@}
  ///@name Lookup by group
  ///@{
  typedef std::map<std::string,std::vector<std::string> > GroupDictType;
  static  GroupDictType  GroupMembers = { };                                                     ///< Lookup for units by group
  const   GroupDictType& groupMemberLookup(const DescriptionLoader::DataBase& map=Descriptions); ///< Retrieval/filling of unit lookup by group database 
  ///@}
  ///@name Display content
  ///@{
  std::vector<std::string> listing(const std::vector<std::string>& groups={},const DescriptionLoader::DataBase& map=Descriptions);                                    ///< Formated listing of unit description data base
  std::ostream& show(std::ostream& rstr,const std::string& preamble="",const std::vector<std::string> groups={},const DescriptionLoader::DataBase& map=Descriptions); ///< Streams listing 
  void print(const std::string& preamble="",const std::vector<std::string>& groups={},const DescriptionLoader::DataBase& map=Descriptions);                           ///< Streams listing to standard output
  ///@}
} // namespace Units
#endif
