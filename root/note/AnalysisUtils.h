// -*- c++ -*-
#ifndef ANALYSISUTILS_H
#define ANALYSISUTILS_H

#include "config.h"

#include "Types.h"

#include <cmath>

namespace Analysis {
  ///@brief Synonym for scale indicator
  using Scale = Types::Scale::Type;
  ///@brief Memory management support
  namespace Memory {
    ///@brief Interface to manage unique pointers
    namespace Interface {
      class UniquePtr {
      public:
	virtual ~UniquePtr() { }
	virtual operator bool() = 0;
      };
    } // Analysis::Memory::Interface
    template<class T> class UniquePtr : virtual public Interface::UniquePtr {
    public:
      UniquePtr(T& obj);
      virtual ~UniquePtr();
      virtual T* operator*(); 
      virtual T* operator->();
      virtual T* get();
      virtual const T* operator*()  const; 
      virtual const T* operator->() const;
      virtual const T* get()        const;
      virtual operator bool(); 
    private:
      T* m_ptr = { nullptr };
    };
  } // Analysis::Memory
  ///@brief Utilities
  namespace Utils {
    ///@brief Get local response
    /// The local response is calculated with reference to the energy deposited in the cells beloning to a cluster, which generally is not the
    /// total true energy associated with this topo-cluster. True energy losses due to out-of-cluster (OOC) or dead material (DM) deposits are 
    /// ignored.
    ///@tparam DSERVER   data server type
    ///@param  dserver   reference to non-modifiable data server module
    ///@param  rs        response scale (defaults to @c Scale::RAW )
    ///@return A valid response if known scale given in @c rs and the reference energy is greater than 0, else the returned value is 0.  
    template<class DSERVER> RVALUE_DOUBLE_T localResponse(const DSERVER& dserver,Scale rs=Scale::RAW) {
      Types::Numerical::double_t resp(std::numerical_limits<Types::Numerical::double_t>::lowest()); 
      if ( dserver.cluster_ENG_CALIB == 0. ) { return resp; }
      switch ( rs ) {
      case Scale::RAW: resp = dserver.clusterE/dserver.cluster_ENG_CALIB                            ; break;
      case Scale::HAD: resp = localResponse<DSERVER>(dserver,Scale::EM) * dserver.cluster_HAD_WEIGHT; break; 
      case Scale::LCW: resp = dserver.clusterECalib/dserver.cluster_ENG_CALIB                       ; break;
      default: break;
      }
#ifndef HAS_OPTIONAL
      return std::max(resp,0.);
#else
      return resp > 0. ? RVALUE_DOUBLE_TYPE( resp ) : RAVLUE_DOUBLE_T() ;
#endif
      ///@name Specialized responses
      ///@tparam DSERVER   data server type
      ///@param  dserver   reference to non-modifiable data server module
      ///@{
      template<class DSERVER> RVALUE_DOUBLE_T localResponseEM (const DSERVER& dserver) { return localResponse<DSERVER>(dserver,Scale::RAW); } ///< Local response at EM scale
      template<class DSERVER> RVALUE_DOUBLE_T localResponseLCW(const DSERVER& dserver) { return localResponse<DSERVER>(dserver,Scale::LCW); } ///< Local response at LCW scale
      template<class DSERVER> RVALUE_DOUBLE_T localResponseHAD(const DSERVER& dserver) { return localResponse<DSERVER>(dserver,Scale::HAD); } ///< Local response at HAD scale
      ///@}
    } 
  }
}

#endif
