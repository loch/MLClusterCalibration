// -*- c++ -*-
#ifndef HISTAPPLICATION_H
#define HISTAPPLICATION_H

#include "config.h"

// -- ROOT includes (needed for code in icc file)
#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TDirectory.h>

// -- Local includes
#include "Types.h"
#include "Base.h"
#include "HistConfig.h"

// -- c++std containers and data structures
#include <string>
#include <map>
#include <vector>
#include <tuple>

// -- c++std c functions
#include <cstdio>
#include <cstdarg>
#include <cmath>

// #ifdef HAS_OPTIONAL
// #  include <optional>
// #  define RVALUE_DOUBLE_T std::optional<double>
// #  define HIST_VALUE( V ) V.value() 
// #else
// #  define RVALUE_DOUBLE_T double
// #  define HIST_VALUE( V ) V
// #include <limits>
// #endif

#define HIST_VALUE( X ) RDOUBLE_VALUE( X )

#define HIST_FILL_NSPACE Hist::Appl::Fill

#define HIST_FILL_ENTRY( HNAME, VAR, TYPE, BOOKIT )				\
  { AxisVariable::VAR, HIST_FILL_NSPACE::Pointer< TYPE >( Hist::Descriptor( std::string(#HNAME), \
									     HIST_FILL_NSPACE::Label        (AxisVariable::VAR), \
									     HIST_FILL_NSPACE::Variables1d  (AxisVariable::VAR), \
									     HIST_FILL_NSPACE::Coordinates1d(                 ), \
									     HIST_FILL_NSPACE::Axes1d       (AxisVariable::VAR)), BOOKIT) }

namespace Hist {
  namespace Interface {
    ///@brief Fill interface
    class Fill {
    public:
      ///@brief Interface destructor
      virtual ~Fill() { }
      ///@brief Fill method interface
      virtual bool fill(double weight) = 0; 
      ///@brief Valid fill object
      virtual bool valid() const = 0;
    };
  } // Hist::Interface
  namespace Storage {
    ///@brief Storage element links memory address with scale factor and flag indicating @f \log_{10} @f instead of @f x @f fill values
    ///@tparam T   object type in memory (typically a simple numerical type)
    template<class T> using FillLink = std::tuple<T*,double,bool>; 
    ///@brief Storage class for @c Fill object
    ///@tparam T   object type in memory (typically a simple numerical type)
    template<class T> using FillBase = std::map<AxisCoordinate,FillLink<T> >;
    ///@brief Entry in @c Fill store
    template<class T> using FillEntry = typename FillBase<T>::value_type;
#ifdef HAS_OPTIONAL
    ///@brief Optional return type for value of object at address
    template<class T> using FillReturn = std::optional<T>;
#endif 
    ///@name Accessors for storage elements
    ///@{
#ifndef HAS_OPTIONAL
    template<class T> T                value   (const FillLink<T>&  data ); ///< Retrieve value at memory location from stored element
    template<class T> T                value   (const FillEntry<T>& entry); ///< Retrieve value at memory location from entry in storage
#else
    template<class T> FillReturn<T>    value   (const FillLink<T>&  data ); ///< Retrieve value at memory location from storage element, with status indicator
    template<class T> FillReturn<T>    value   (const FillEntry<T>& entry); ///< Retrieve value at memory location from entry in storage, with status indicator  
#endif 
    template<class T> double           scale   (const FillLink<T>&  data ); ///< Retrieve scale factor from storage element
    template<class T> double           scale   (const FillEntry<T>& entry); ///< Retrieve scale factor from storage elemen
    template<class T> bool             logValue(const FillLink<T>&  data ); ///< Retrieve flag indicating @f \log_{10}(x) @f fills from storage element
    template<class T> bool             logValue(const FillEntry<T>& entry); ///< Retrieve flag indicating @f \log_{10}(x) @f fills from storage element
    ///@}
    ///@brief Check validity of values as an argument for a logarithmic function
    template<class T> bool             checkValues(int nvalues,...);
  } // Hist::Storage
  ///@brief Fill a 1-dimensional histogram from a memory location
  ///@tparam T    type of variable stored at location 
  template<class T> class Filler : public Storage::FillBase<T>, public Base::Config, virtual public Interface::Fill {
  public:
    typedef T                                      value_type;       ///< Data type of value at linked memory
    typedef Storage::FillBase<value_type>          storage_type;     ///< Storage type (base type for fill object)
    typedef typename storage_type::mapped_type     payload_type;     ///< Stored value (payload) type
    typedef typename storage_type::value_type      entry_type;       ///< Storage entry (key and payload) type
    typedef typename storage_type::key_type        key_type;         ///< Key type
    typedef Interface::Fill                        interface_type;   ///< Interface type
    ///@brief Default constructor generates unusable fill object
    Filler();
    ///@brief Fully configured fill object
    ///@param hdescr reference to a non-modifiable a histogram descriptor 
    ///@param pdir   pointer to @c ROOT directory (default @c nullptr , no directory specified) 
    ///@param bookIt instantaneous histogram booking if @c true
    Filler(const Hist::Descriptor& hdescr,bool bookIt=true,TDirectory* pdir=nullptr);
    ///@brief Set address of memory location used to fill histogram
    ///@param c     axis coordinate identifier
    ///@param vptr  pointer to memory location 
    bool setAddress(AxisCoordinate c,T* vptr);
    ///@brief Retrieve histogram descriptor
    const Hist::Descriptor& histDescr() const;
    ///@brief Fill histogram 
    ///@param weight weight of entry (default is 1)
    virtual bool fill(double weight=1.0);
    ///@brief Check validity of fill object
    virtual bool valid() const;
    ///@brief Book histogram
    virtual bool book();
  private:
    typedef std::vector<AxisCoordinate> clist_t;
    Hist::Descriptor m_hdescr; 
    TH1*             m_hptr = { nullptr };
    TDirectory*      m_dptr = { nullptr };
    clist_t          m_listOfCoords = clist_t();
    RVALUE_DOUBLE_T  f_value(AxisCoordinate c)               const;
    value_type*      f_address(AxisCoordinate c)             const;
    bool             f_logValue(AxisCoordinate c)            const;
    RVALUE_DOUBLE_T  f_scale(AxisCoordinate c)               const;
    entry_type       f_entry(AxisCoordinate c)               const;
    bool             f_isInvalidValue(RVALUE_DOUBLE_T value) const; 
#ifndef HAS_OPTIONAL
    double           f_invalidValue(double value)            const;
    static double    m_invalidValue;
#endif     
  };
  ///@brief Histogram configurations
  namespace Appl {
    ///@name Useful types
    ///@{
    typedef std::map<AxisVariable,Hist::Interface::Fill*> FillMap;
    ///@}
    ///@brief Fill data helpers
    namespace Fill {
      static const std::map<int,std::vector<AxisCoordinate> > Coordinates = { { 0, { } }, { 1, { AxisCoordinate::X, AxisCoordinate::Y } }, { 2, { AxisCoordinate::X, AxisCoordinate::Y, AxisCoordinate::Z } } };
      static std::vector<AxisVariable>          Variables1d(AxisVariable xvar,AxisVariable yvar=AxisVariable::Entries)                   { return { xvar, yvar       }; }
      static std::vector<AxisVariable>          Variables2d(AxisVariable xvar,AxisVariable yvar,AxisVariable zvar=AxisVariable::Entries) { return { xvar, yvar, zvar }; }
      static const std::vector<AxisCoordinate>& Coordinates1d() { return Coordinates.at(1); } 
      static const std::vector<AxisCoordinate>& Coordinates2d() { return Coordinates.at(2); } 
      static std::vector<Axis::Descriptor>      Axes1d(AxisVariable xvar,AxisVariable yvar=AxisVariable::Entries) { return { Hist::Axis::Find::descriptor(xvar),Hist::Axis::Find::descriptor(yvar)}; }
      static std::vector<Axis::Descriptor>      Axes2d(AxisVariable xvar,AxisVariable yvar,AxisVariable zvar=AxisVariable::Entries) { 
	return { Hist::Axis::Find::descriptor(xvar),Hist::Axis::Find::descriptor(yvar),Hist::Axis::Find::descriptor(zvar) }; 
      }
      static const std::string& Label(AxisVariable avar) { return Types::Variable::IdToLabel.at(avar); }
      template<class T> static Hist::Interface::Fill* Pointer(const Hist::Descriptor& hdescr,bool bookIt=false) { return (Hist::Interface::Fill*)new Hist::Filler<T>(hdescr,bookIt); }
      ///@brief Fill map
      static const FillMap Fill1dMap = {
        HIST_FILL_ENTRY( h_seqNumber                  , seqNumber                  , int   , false ),
        HIST_FILL_ENTRY( h_runNumber                  , runNumber                  , int   , false ),
        HIST_FILL_ENTRY( h_eventNumber                , eventNumber                , int   , false ),
        HIST_FILL_ENTRY( h_nPrimVtx                   , nPrimVtx                   , int   , false ),
        HIST_FILL_ENTRY( h_avgMu                      , avgMu                      , float , false ),
        HIST_FILL_ENTRY( h_truthE                     , truthE                     , float , false ),
        HIST_FILL_ENTRY( h_truthPt                    , truthPt                    , float , false ),
        HIST_FILL_ENTRY( h_truthEta                   , truthEta                   , float , false ),
        HIST_FILL_ENTRY( h_truthPhi                   , truthPhi                   , float , false ),
        HIST_FILL_ENTRY( h_truthPDG                   , truthPDG                   , int   , false ),
        HIST_FILL_ENTRY( h_truthJetMatchingRadius     , truthJetMatchingRadius     , float , false ),
        HIST_FILL_ENTRY( h_truthJetE                  , truthJetE                  , float , false ),
        HIST_FILL_ENTRY( h_truthJetPt 	              , truthJetPt                 , float , false ),
        HIST_FILL_ENTRY( h_truthJetEta 	              , truthJetEta                , float , false ),
        HIST_FILL_ENTRY( h_truthJetPhi 	              , truthJetPhi                , float , false ),
        HIST_FILL_ENTRY( h_jetCnt                     , jetCnt                     , int   , false ),
        HIST_FILL_ENTRY( h_jetCalE                    , jetCalE                    , float , false ),
        HIST_FILL_ENTRY( h_jetCalPt                   , jetCalPt                   , float , false ),
        HIST_FILL_ENTRY( h_jetCalEta                  , jetCalEta                  , float , false ),
        HIST_FILL_ENTRY( h_jetCalPhi                  , jetCalPhi                  , float , false ),
        HIST_FILL_ENTRY( h_jetRawE	              , jetRawE	                   , float , false ),
        HIST_FILL_ENTRY( h_jetRawEta                  , jetRawEta                  , float , false ),
        HIST_FILL_ENTRY( h_jetRawPhi                  , jetRawPhi                  , float , false ),
        HIST_FILL_ENTRY( h_jetNConst                  , jetNConst                  , int   , false ),
        HIST_FILL_ENTRY( h_nCluster                   , nCluster                   , int   , false ),
        HIST_FILL_ENTRY( h_clusterIndex 	      , clusterIndex               , int   , false ),
        HIST_FILL_ENTRY( h_cluster_nCells 	      , cluster_nCells 	           , int   , false ),
        HIST_FILL_ENTRY( h_cluster_nCells_tot         , cluster_nCells_tot         , int   , false ),
        HIST_FILL_ENTRY( h_clusterECalib	      , clusterECalib	           , float , false ),
        HIST_FILL_ENTRY( h_clusterPtCalib             , clusterPtCalib             , float , false ),
        HIST_FILL_ENTRY( h_clusterEtaCalib	      , clusterEtaCalib	           , float , false ), 
        HIST_FILL_ENTRY( h_clusterPhiCalib	      , clusterPhiCalib	           , float , false ), 
        HIST_FILL_ENTRY( h_cluster_sumCellECalib      , cluster_sumCellECalib      , float , false ),
        HIST_FILL_ENTRY( h_cluster_fracECalib         , cluster_fracECalib         , float , false ),
        HIST_FILL_ENTRY( h_cluster_fracECalib_ref     , cluster_fracECalib_ref     , float , false ),
        HIST_FILL_ENTRY( h_clusterE	              , clusterE	           , float , false ),
        HIST_FILL_ENTRY( h_clusterEta	              , clusterEta	           , float , false ),
        HIST_FILL_ENTRY( h_clusterPhi	              , clusterPhi	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_sumCellE           , cluster_sumCellE           , float , false ),
        HIST_FILL_ENTRY( h_cluster_time 	      , cluster_time 	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_fracE	      , cluster_fracE	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_fracE_ref          , cluster_fracE_ref          , float , false ),
        HIST_FILL_ENTRY( h_cluster_HAD_WEIGHT         , cluster_HAD_WEIGHT         , float , false ),
        HIST_FILL_ENTRY( h_cluster_OOC_WEIGHT	      , cluster_OOC_WEIGHT         , float , false ),
        HIST_FILL_ENTRY( h_cluster_DM_WEIGHT	      , cluster_DM_WEIGHT	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_TOT      , cluster_ENG_CALIB_TOT      , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_OUT_T    , cluster_ENG_CALIB_OUT_T    , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_DEAD_TOT , cluster_ENG_CALIB_DEAD_TOT , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_FRAC_EM  , cluster_ENG_CALIB_FRAC_EM  , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_FRAC_HAD , cluster_ENG_CALIB_FRAC_HAD , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_CALIB_FRAC_REST, cluster_ENG_CALIB_FRAC_REST, float , false ),
        HIST_FILL_ENTRY( h_cluster_EM_PROBABILITY     , cluster_EM_PROBABILITY     , float , false ),
        HIST_FILL_ENTRY( h_cluster_CENTER_MAG	      , cluster_CENTER_MAG	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_FIRST_ENG_DENS     , cluster_FIRST_ENG_DENS     , float , false ),
        HIST_FILL_ENTRY( h_cluster_FIRST_PHI	      , cluster_FIRST_PHI	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_FIRST_ETA	      , cluster_FIRST_ETA	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_SECOND_R           , cluster_SECOND_R           , float , false ),
        HIST_FILL_ENTRY( h_cluster_SECOND_LAMBDA      , cluster_SECOND_LAMBDA      , float , false ),
        HIST_FILL_ENTRY( h_cluster_DELTA_PHI	      , cluster_DELTA_PHI	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_DELTA_THETA	      , cluster_DELTA_THETA	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_DELTA_ALPHA	      , cluster_DELTA_ALPHA	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_CENTER_X           , cluster_CENTER_X           , float , false ),
        HIST_FILL_ENTRY( h_cluster_CENTER_Y	      , cluster_CENTER_Y	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_CENTER_Z	      , cluster_CENTER_Z	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_CENTER_LAMBDA      , cluster_CENTER_LAMBDA      , float , false ),
        HIST_FILL_ENTRY( h_cluster_LATERAL	      , cluster_LATERAL	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_LONGITUDINAL       , cluster_LONGITUDINAL       , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_FRAC_EM	      , cluster_ENG_FRAC_EM        , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_FRAC_MAX       , cluster_ENG_FRAC_MAX       , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_FRAC_CORE      , cluster_ENG_FRAC_CORE      , float , false ),
        HIST_FILL_ENTRY( h_cluster_SECOND_ENG_DENS    , cluster_SECOND_ENG_DENS    , float , false ),
        HIST_FILL_ENTRY( h_cluster_ISOLATION          , cluster_ISOLATION          , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_BAD_CELLS      , cluster_ENG_BAD_CELLS      , float , false ),
        HIST_FILL_ENTRY( h_cluster_N_BAD_CELLS        , cluster_N_BAD_CELLS        , float , false ),
        HIST_FILL_ENTRY( h_cluster_N_BAD_CELLS_CORR   , cluster_N_BAD_CELLS_CORR   , float , false ),
        HIST_FILL_ENTRY( h_cluster_BAD_CELLS_CORR_E   , cluster_BAD_CELLS_CORR_E   , float , false ),
        HIST_FILL_ENTRY( h_cluster_BADLARQ_FRAC       , cluster_BADLARQ_FRAC       , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_POS	      , cluster_ENG_POS	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_SIGNIFICANCE       , cluster_SIGNIFICANCE       , float , false ),
        HIST_FILL_ENTRY( h_cluster_CELL_SIGNIFICANCE  , cluster_CELL_SIGNIFICANCE  , float , false ),
        HIST_FILL_ENTRY( h_cluster_CELL_SIG_SAMPLING  , cluster_CELL_SIG_SAMPLING  , float , false ),
        HIST_FILL_ENTRY( h_cluster_AVG_LAR_Q          , cluster_AVG_LAR_Q          , float , false ),
        HIST_FILL_ENTRY( h_cluster_AVG_TILE_Q	      , cluster_AVG_TILE_Q	   , float , false ),
        HIST_FILL_ENTRY( h_cluster_ENG_BAD_HV_CELLS   , cluster_ENG_BAD_HV_CELLS   , float , false ),
        HIST_FILL_ENTRY( h_cluster_N_BAD_HV_CELLS     , cluster_N_BAD_HV_CELLS     , float , false ),
        HIST_FILL_ENTRY( h_cluster_PTD		      , cluster_PTD	           , float , false ),
        HIST_FILL_ENTRY( h_cluster_MASS               , cluster_MASS               , float , false ),
        HIST_FILL_ENTRY( h_cluster_SECOND_TIME	      , cluster_SECOND_TIME        , float , false )
      };
      ///@brief Set memory addresses in map
      template<class DSTORE> FillMap setMemoryAddresses(DSTORE& dataStore, const std::vector<std::string>& varNames={});
    } // Hist::Appl::Fill
  } // Hist::Appl
} // Hist

    // static const FillMap fillDescription = {
    //   {"seqNumber"                  ,fptr<int  >({"h_seqNumber"                  ,flabel(AxisVariable::seqNumber                  ),fvars1d(AxisVariable::seqNumber                  ),fcoor1d(),faxis1d(AxisVariable::seqNumber                  )})},
    //   {"runNumber"                  ,fptr<int  >({"h_runNumber"                  ,flabel(AxisVariable::runNumber                  ),fvars1d(AxisVariable::runNumber                  ),fcoor1d(),faxis1d(AxisVariable::runNumber                  )})},
    //   {"eventNumber"                ,fptr<int  >({"h_eventNumber"                ,flabel(AxisVariable::eventNumber                ),fvars1d(AxisVariable::eventNumber                ),fcoor1d(),faxis1d(AxisVariable::eventNumber                )})},
    //   {"nPrimVtx"                   ,fptr<int  >({"h_nPrimVtx"                   ,flabel(AxisVariable::nPrimVtx                   ),fvars1d(AxisVariable::nPrimVtx                   ),fcoor1d(),faxis1d(AxisVariable::nPrimVtx                   )})},
    //   {"avgMu"                      ,fptr<float>({"h_avgMu"                      ,flabel(AxisVariable::avgMu                      ),fvars1d(AxisVariable::avgMu                      ),fcoor1d(),faxis1d(AxisVariable::avgMu                      )})},
    //   {"truthE"                     ,fptr<float>({"h_truthE"                     ,flabel(AxisVariable::truthE                     ),fvars1d(AxisVariable::truthE                     ),fcoor1d(),faxis1d(AxisVariable::truthE                     )})},
    //   {"truthPt"                    ,fptr<float>({"h_truthPt"                    ,flabel(AxisVariable::truthPt                    ),fvars1d(AxisVariable::truthPt                    ),fcoor1d(),faxis1d(AxisVariable::truthPt                    )})},
    //   {"truthEta"                   ,fptr<float>({"h_truthEta"                   ,flabel(AxisVariable::truthEta                   ),fvars1d(AxisVariable::truthEta                   ),fcoor1d(),faxis1d(AxisVariable::truthEta                   )})},
    //   {"truthPhi"                   ,fptr<float>({"h_truthPhi"                   ,flabel(AxisVariable::truthPhi                   ),fvars1d(AxisVariable::truthPhi                   ),fcoor1d(),faxis1d(AxisVariable::truthPhi                   )})},
    //   {"truthPDG"                   ,fptr<int  >({"h_truthPDG"                   ,flabel(AxisVariable::truthPDG                   ),fvars1d(AxisVariable::truthPDG                   ),fcoor1d(),faxis1d(AxisVariable::truthPDG                   )})},
    //   {"truthJetMatchingRadius"     ,fptr<float>({"h_truthJetMatchingRadius"     ,flabel(AxisVariable::truthJetMatchingRadius     ),fvars1d(AxisVariable::truthJetMatchingRadius     ),fcoor1d(),faxis1d(AxisVariable::truthJetMatchingRadius     )})},
    //   {"truthJetE"                  ,fptr<float>({"h_truthJetE"                  ,flabel(AxisVariable::truthJetE                  ),fvars1d(AxisVariable::truthJetE                  ),fcoor1d(),faxis1d(AxisVariable::truthJetE                  )})},
    //   {"truthJetPt" 	            ,fptr<float>({"h_truthJetPt" 	         ,flabel(AxisVariable::truthJetPt                 ),fvars1d(AxisVariable::truthJetPt                 ),fcoor1d(),faxis1d(AxisVariable::truthJetPt                 )})},
    //   {"truthJetEta" 	            ,fptr<float>({"h_truthJetEta" 	         ,flabel(AxisVariable::truthJetEta                ),fvars1d(AxisVariable::truthJetEta                ),fcoor1d(),faxis1d(AxisVariable::truthJetEta                )})},
    //   {"truthJetPhi"                ,fptr<float>({"h_truthJetPhi" 	         ,flabel(AxisVariable::truthJetPhi                ),fvars1d(AxisVariable::truthJetPhi                ),fcoor1d(),faxis1d(AxisVariable::truthJetPhi                )})},
    //   {"jetCnt"                     ,fptr<int  >({"h_jetCnt"                     ,flabel(AxisVariable::jetCnt                     ),fvars1d(AxisVariable::jetCnt                     ),fcoor1d(),faxis1d(AxisVariable::jetCnt                     )})},
    //   {"jetCalE"                    ,fptr<float>({"h_jetCalE"                    ,flabel(AxisVariable::jetCalE                    ),fvars1d(AxisVariable::jetCalE                    ),fcoor1d(),faxis1d(AxisVariable::jetCalE                    )})},
    //   {"jetCalPt"                   ,fptr<float>({"h_jetCalPt"                   ,flabel(AxisVariable::jetCalPt                   ),fvars1d(AxisVariable::jetCalPt                   ),fcoor1d(),faxis1d(AxisVariable::jetCalPt                   )})},
    //   {"jetCalEta"                  ,fptr<float>({"h_jetCalEta"                  ,flabel(AxisVariable::jetCalEta                  ),fvars1d(AxisVariable::jetCalEta                  ),fcoor1d(),faxis1d(AxisVariable::jetCalEta                  )})},
    //   {"jetCalPhi"                  ,fptr<float>({"h_jetCalPhi"                  ,flabel(AxisVariable::jetCalPhi                  ),fvars1d(AxisVariable::jetCalPhi                  ),fcoor1d(),faxis1d(AxisVariable::jetCalPhi                  )})},
    //   {"jetRawE"	            ,fptr<float>({"h_jetRawE"	                 ,flabel(AxisVariable::jetRawE	              ),fvars1d(AxisVariable::jetRawE	                 ),fcoor1d(),faxis1d(AxisVariable::jetRawE	              )})},
    //   {"jetRawEta"                  ,fptr<float>({"h_jetRawEta"                  ,flabel(AxisVariable::jetRawEta                  ),fvars1d(AxisVariable::jetRawEta                  ),fcoor1d(),faxis1d(AxisVariable::jetRawEta                  )})},
    //   {"jetRawPhi"                  ,fptr<float>({"h_jetRawPhi"                  ,flabel(AxisVariable::jetRawPhi                  ),fvars1d(AxisVariable::jetRawPhi                  ),fcoor1d(),faxis1d(AxisVariable::jetRawPhi                  )})},
    //   {"jetNConst"                  ,fptr<int  >({"h_jetNConst"                  ,flabel(AxisVariable::jetNConst                  ),fvars1d(AxisVariable::jetNConst                  ),fcoor1d(),faxis1d(AxisVariable::jetNConst                  )})},
    //   {"nCluster"                   ,fptr<int  >({"h_nCluster"                   ,flabel(AxisVariable::nCluster                   ),fvars1d(AxisVariable::nCluster                   ),fcoor1d(),faxis1d(AxisVariable::nCluster                   )})},
    //   {"clusterIndex" 	            ,fptr<int  >({"h_clusterIndex" 	         ,flabel(AxisVariable::clusterIndex               ),fvars1d(AxisVariable::clusterIndex               ),fcoor1d(),faxis1d(AxisVariable::clusterIndex               )})},
    //   {"cluster_nCells" 	    ,fptr<int  >({"h_cluster_nCells" 	         ,flabel(AxisVariable::cluster_nCells 	      ),fvars1d(AxisVariable::cluster_nCells 	         ),fcoor1d(),faxis1d(AxisVariable::cluster_nCells 	      )})},
    //   {"cluster_nCells_tot"         ,fptr<int  >({"h_cluster_nCells_tot"         ,flabel(AxisVariable::cluster_nCells_tot         ),fvars1d(AxisVariable::cluster_nCells_tot         ),fcoor1d(),faxis1d(AxisVariable::cluster_nCells_tot         )})},
    //   {"clusterECalib"	            ,fptr<float>({"h_clusterECalib"	         ,flabel(AxisVariable::clusterECalib	      ),fvars1d(AxisVariable::clusterECalib	         ),fcoor1d(),faxis1d(AxisVariable::clusterECalib	      )})}, 
    //   {"clusterPtCalib"             ,fptr<float>({"h_clusterPtCalib"             ,flabel(AxisVariable::clusterPtCalib             ),fvars1d(AxisVariable::clusterPtCalib             ),fcoor1d(),faxis1d(AxisVariable::clusterPtCalib             )})},
    //   {"clusterEtaCalib"	    ,fptr<float>({"h_clusterEtaCalib"	         ,flabel(AxisVariable::clusterEtaCalib	      ),fvars1d(AxisVariable::clusterEtaCalib	         ),fcoor1d(),faxis1d(AxisVariable::clusterEtaCalib	      )})}, 
    //   {"clusterPhiCalib"	    ,fptr<float>({"h_clusterPhiCalib"	         ,flabel(AxisVariable::clusterPhiCalib	      ),fvars1d(AxisVariable::clusterPhiCalib	         ),fcoor1d(),faxis1d(AxisVariable::clusterPhiCalib	      )})}, 
    //   {"cluster_sumCellECalib"      ,fptr<float>({"h_cluster_sumCellECalib"      ,flabel(AxisVariable::cluster_sumCellECalib      ),fvars1d(AxisVariable::cluster_sumCellECalib      ),fcoor1d(),faxis1d(AxisVariable::cluster_sumCellECalib      )})},
    //   {"cluster_fracECalib"         ,fptr<float>({"h_cluster_fracECalib"         ,flabel(AxisVariable::cluster_fracECalib         ),fvars1d(AxisVariable::cluster_fracECalib         ),fcoor1d(),faxis1d(AxisVariable::cluster_fracECalib         )})}, 
    //   {"cluster_fracECalib_ref"     ,fptr<float>({"h_cluster_fracECalib_ref"     ,flabel(AxisVariable::cluster_fracECalib_ref     ),fvars1d(AxisVariable::cluster_fracECalib_ref     ),fcoor1d(),faxis1d(AxisVariable::cluster_fracECalib_ref     )})},
    //   {"clusterE"	            ,fptr<float>({"h_clusterE"	                 ,flabel(AxisVariable::clusterE	              ),fvars1d(AxisVariable::clusterE	                 ),fcoor1d(),faxis1d(AxisVariable::clusterE	              )})},
    //   {"clusterEta"	            ,fptr<float>({"h_clusterEta"	         ,flabel(AxisVariable::clusterEta	              ),fvars1d(AxisVariable::clusterEta	         ),fcoor1d(),faxis1d(AxisVariable::clusterEta	              )})},
    //   {"clusterPhi"	            ,fptr<float>({"h_clusterPhi"	         ,flabel(AxisVariable::clusterPhi	              ),fvars1d(AxisVariable::clusterPhi	         ),fcoor1d(),faxis1d(AxisVariable::clusterPhi	              )})},
    //   {"cluster_sumCellE"           ,fptr<float>({"h_cluster_sumCellE"           ,flabel(AxisVariable::cluster_sumCellE           ),fvars1d(AxisVariable::cluster_sumCellE           ),fcoor1d(),faxis1d(AxisVariable::cluster_sumCellE           )})},
    //   {"cluster_time" 	            ,fptr<float>({"h_cluster_time" 	         ,flabel(AxisVariable::cluster_time 	             ),fvars1d(AxisVariable::cluster_time 	         ),fcoor1d(),faxis1d(AxisVariable::cluster_time 	      )})},
    //   {"cluster_fracE"	            ,fptr<float>({"h_cluster_fracE"	         ,flabel(AxisVariable::cluster_fracE	             ),fvars1d(AxisVariable::cluster_fracE	         ),fcoor1d(),faxis1d(AxisVariable::cluster_fracE	      )})},
    //   {"cluster_fracE_ref"          ,fptr<float>({"h_cluster_fracE_ref"          ,flabel(AxisVariable::cluster_fracE_ref          ),fvars1d(AxisVariable::cluster_fracE_ref          ),fcoor1d(),faxis1d(AxisVariable::cluster_fracE_ref          )})},
    //   {"cluster_HAD_WEIGHT"         ,fptr<float>({"h_cluster_HAD_WEIGHT"         ,flabel(AxisVariable::cluster_HAD_WEIGHT         ),fvars1d(AxisVariable::cluster_HAD_WEIGHT         ),fcoor1d(),faxis1d(AxisVariable::cluster_HAD_WEIGHT         )})},
    //   {"cluster_OOC_WEIGHT"	    ,fptr<float>({"h_cluster_OOC_WEIGHT"	 ,flabel(AxisVariable::cluster_OOC_WEIGHT	      ),fvars1d(AxisVariable::cluster_OOC_WEIGHT	 ),fcoor1d(),faxis1d(AxisVariable::cluster_OOC_WEIGHT	      )})},
    //   {"cluster_DM_WEIGHT"	    ,fptr<float>({"h_cluster_DM_WEIGHT"	         ,flabel(AxisVariable::cluster_DM_WEIGHT	      ),fvars1d(AxisVariable::cluster_DM_WEIGHT	         ),fcoor1d(),faxis1d(AxisVariable::cluster_DM_WEIGHT	      )})},
    //   {"cluster_ENG_CALIB_TOT"	    ,fptr<float>({"h_cluster_ENG_CALIB_TOT"      ,flabel(AxisVariable::cluster_ENG_CALIB_TOT      ),fvars1d(AxisVariable::cluster_ENG_CALIB_TOT      ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_TOT      )})},
    //   {"cluster_ENG_CALIB_OUT_T"    ,fptr<float>({"h_cluster_ENG_CALIB_OUT_T"    ,flabel(AxisVariable::cluster_ENG_CALIB_OUT_T    ),fvars1d(AxisVariable::cluster_ENG_CALIB_OUT_T    ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_OUT_T    )})},
    //   {"cluster_ENG_CALIB_DEAD_TOT" ,fptr<float>({"h_cluster_ENG_CALIB_DEAD_TOT" ,flabel(AxisVariable::cluster_ENG_CALIB_DEAD_TOT ),fvars1d(AxisVariable::cluster_ENG_CALIB_DEAD_TOT ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_DEAD_TOT )})},
    //   {"cluster_ENG_CALIB_FRAC_EM"  ,fptr<float>({"h_cluster_ENG_CALIB_FRAC_EM"  ,flabel(AxisVariable::cluster_ENG_CALIB_FRAC_EM  ),fvars1d(AxisVariable::cluster_ENG_CALIB_FRAC_EM  ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_FRAC_EM  )})},
    //   {"cluster_ENG_CALIB_FRAC_HAD" ,fptr<float>({"h_cluster_ENG_CALIB_FRAC_HAD" ,flabel(AxisVariable::cluster_ENG_CALIB_FRAC_HAD ),fvars1d(AxisVariable::cluster_ENG_CALIB_FRAC_HAD ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_FRAC_HAD )})},
    //   {"cluster_ENG_CALIB_FRAC_REST",fptr<float>({"h_cluster_ENG_CALIB_FRAC_REST",flabel(AxisVariable::cluster_ENG_CALIB_FRAC_REST),fvars1d(AxisVariable::cluster_ENG_CALIB_FRAC_REST),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_CALIB_FRAC_REST)})},
    //   {"cluster_EM_PROBABILITY"     ,fptr<float>({"h_cluster_EM_PROBABILITY"     ,flabel(AxisVariable::cluster_EM_PROBABILITY     ),fvars1d(AxisVariable::cluster_EM_PROBABILITY     ),fcoor1d(),faxis1d(AxisVariable::cluster_EM_PROBABILITY     )})},
    //   {"cluster_CENTER_MAG"	    ,fptr<float>({"h_cluster_CENTER_MAG"	 ,flabel(AxisVariable::cluster_CENTER_MAG	      ),fvars1d(AxisVariable::cluster_CENTER_MAG	 ),fcoor1d(),faxis1d(AxisVariable::cluster_CENTER_MAG	      )})},
    //   {"cluster_FIRST_ENG_DENS"     ,fptr<float>({"h_cluster_FIRST_ENG_DENS"     ,flabel(AxisVariable::cluster_FIRST_ENG_DENS     ),fvars1d(AxisVariable::cluster_FIRST_ENG_DENS     ),fcoor1d(),faxis1d(AxisVariable::cluster_FIRST_ENG_DENS     )})},
    //   {"cluster_FIRST_PHI"	    ,fptr<float>({"h_cluster_FIRST_PHI"	         ,flabel(AxisVariable::cluster_FIRST_PHI	      ),fvars1d(AxisVariable::cluster_FIRST_PHI	         ),fcoor1d(),faxis1d(AxisVariable::cluster_FIRST_PHI	      )})},
    //   {"cluster_FIRST_ETA"	    ,fptr<float>({"h_cluster_FIRST_ETA"	         ,flabel(AxisVariable::cluster_FIRST_ETA	      ),fvars1d(AxisVariable::cluster_FIRST_ETA	         ),fcoor1d(),faxis1d(AxisVariable::cluster_FIRST_ETA	      )})},
    //   {"cluster_SECOND_R"           ,fptr<float>({"h_cluster_SECOND_R"           ,flabel(AxisVariable::cluster_SECOND_R           ),fvars1d(AxisVariable::cluster_SECOND_R           ),fcoor1d(),faxis1d(AxisVariable::cluster_SECOND_R           )})},
    //   {"cluster_SECOND_LAMBDA"	    ,fptr<float>({"h_cluster_SECOND_LAMBDA"      ,flabel(AxisVariable::cluster_SECOND_LAMBDA      ),fvars1d(AxisVariable::cluster_SECOND_LAMBDA      ),fcoor1d(),faxis1d(AxisVariable::cluster_SECOND_LAMBDA      )})},
    //   {"cluster_DELTA_PHI"	    ,fptr<float>({"h_cluster_DELTA_PHI"	         ,flabel(AxisVariable::cluster_DELTA_PHI	      ),fvars1d(AxisVariable::cluster_DELTA_PHI	         ),fcoor1d(),faxis1d(AxisVariable::cluster_DELTA_PHI	      )})},
    //   {"cluster_DELTA_THETA"	    ,fptr<float>({"h_cluster_DELTA_THETA"	 ,flabel(AxisVariable::cluster_DELTA_THETA	      ),fvars1d(AxisVariable::cluster_DELTA_THETA	 ),fcoor1d(),faxis1d(AxisVariable::cluster_DELTA_THETA	      )})},
    //   {"cluster_DELTA_ALPHA"	    ,fptr<float>({"h_cluster_DELTA_ALPHA"	 ,flabel(AxisVariable::cluster_DELTA_ALPHA	      ),fvars1d(AxisVariable::cluster_DELTA_ALPHA	 ),fcoor1d(),faxis1d(AxisVariable::cluster_DELTA_ALPHA	      )})},
    //   {"cluster_CENTER_X"           ,fptr<float>({"h_cluster_CENTER_X"           ,flabel(AxisVariable::cluster_CENTER_X           ),fvars1d(AxisVariable::cluster_CENTER_X           ),fcoor1d(),faxis1d(AxisVariable::cluster_CENTER_X           )})},
    //   {"cluster_CENTER_Y"	    ,fptr<float>({"h_cluster_CENTER_Y"	         ,flabel(AxisVariable::cluster_CENTER_Y	      ),fvars1d(AxisVariable::cluster_CENTER_Y	         ),fcoor1d(),faxis1d(AxisVariable::cluster_CENTER_Y	      )})},
    //   {"cluster_CENTER_Z"	    ,fptr<float>({"h_cluster_CENTER_Z"	         ,flabel(AxisVariable::cluster_CENTER_Z	      ),fvars1d(AxisVariable::cluster_CENTER_Z	         ),fcoor1d(),faxis1d(AxisVariable::cluster_CENTER_Z	      )})},
    //   {"cluster_CENTER_LAMBDA"	    ,fptr<float>({"h_cluster_CENTER_LAMBDA"      ,flabel(AxisVariable::cluster_CENTER_LAMBDA      ),fvars1d(AxisVariable::cluster_CENTER_LAMBDA      ),fcoor1d(),faxis1d(AxisVariable::cluster_CENTER_LAMBDA      )})},
    //   {"cluster_LATERAL"	    ,fptr<float>({"h_cluster_LATERAL"	         ,flabel(AxisVariable::cluster_LATERAL	      ),fvars1d(AxisVariable::cluster_LATERAL	         ),fcoor1d(),faxis1d(AxisVariable::cluster_LATERAL	      )})},
    //   {"cluster_LONGITUDINAL"       ,fptr<float>({"h_cluster_LONGITUDINAL"       ,flabel(AxisVariable::cluster_LONGITUDINAL       ),fvars1d(AxisVariable::cluster_LONGITUDINAL       ),fcoor1d(),faxis1d(AxisVariable::cluster_LONGITUDINAL       )})},
    //   {"cluster_ENG_FRAC_EM"	    ,fptr<float>({"h_cluster_ENG_FRAC_EM"	 ,flabel(AxisVariable::cluster_ENG_FRAC_EM        ),fvars1d(AxisVariable::cluster_ENG_FRAC_EM        ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_FRAC_EM        )})},
    //   {"cluster_ENG_FRAC_MAX"       ,fptr<float>({"h_cluster_ENG_FRAC_MAX"       ,flabel(AxisVariable::cluster_ENG_FRAC_MAX       ),fvars1d(AxisVariable::cluster_ENG_FRAC_MAX       ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_FRAC_MAX       )})},
    //   {"cluster_ENG_FRAC_CORE"	    ,fptr<float>({"h_cluster_ENG_FRAC_CORE"      ,flabel(AxisVariable::cluster_ENG_FRAC_CORE      ),fvars1d(AxisVariable::cluster_ENG_FRAC_CORE      ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_FRAC_CORE      )})},
    //   {"cluster_SECOND_ENG_DENS"    ,fptr<float>({"h_cluster_SECOND_ENG_DENS"    ,flabel(AxisVariable::cluster_SECOND_ENG_DENS    ),fvars1d(AxisVariable::cluster_SECOND_ENG_DENS    ),fcoor1d(),faxis1d(AxisVariable::cluster_SECOND_ENG_DENS    )})},
    //   {"cluster_ISOLATION"          ,fptr<float>({"h_cluster_ISOLATION"          ,flabel(AxisVariable::cluster_ISOLATION          ),fvars1d(AxisVariable::cluster_ISOLATION          ),fcoor1d(),faxis1d(AxisVariable::cluster_ISOLATION          )})},
    //   {"cluster_ENG_BAD_CELLS"	    ,fptr<float>({"h_cluster_ENG_BAD_CELLS"      ,flabel(AxisVariable::cluster_ENG_BAD_CELLS      ),fvars1d(AxisVariable::cluster_ENG_BAD_CELLS      ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_BAD_CELLS      )})},
    //   {"cluster_N_BAD_CELLS"        ,fptr<float>({"h_cluster_N_BAD_CELLS"        ,flabel(AxisVariable::cluster_N_BAD_CELLS        ),fvars1d(AxisVariable::cluster_N_BAD_CELLS        ),fcoor1d(),faxis1d(AxisVariable::cluster_N_BAD_CELLS        )})},
    //   {"cluster_N_BAD_CELLS_CORR"   ,fptr<float>({"h_cluster_N_BAD_CELLS_CORR"   ,flabel(AxisVariable::cluster_N_BAD_CELLS_CORR   ),fvars1d(AxisVariable::cluster_N_BAD_CELLS_CORR   ),fcoor1d(),faxis1d(AxisVariable::cluster_N_BAD_CELLS_CORR   )})},
    //   {"cluster_BAD_CELLS_CORR_E"   ,fptr<float>({"h_cluster_BAD_CELLS_CORR_E"   ,flabel(AxisVariable::cluster_BAD_CELLS_CORR_E   ),fvars1d(AxisVariable::cluster_BAD_CELLS_CORR_E   ),fcoor1d(),faxis1d(AxisVariable::cluster_BAD_CELLS_CORR_E   )})},
    //   {"cluster_BADLARQ_FRAC"       ,fptr<float>({"h_cluster_BADLARQ_FRAC"       ,flabel(AxisVariable::cluster_BADLARQ_FRAC       ),fvars1d(AxisVariable::cluster_BADLARQ_FRAC       ),fcoor1d(),faxis1d(AxisVariable::cluster_BADLARQ_FRAC       )})},
    //   {"cluster_ENG_POS"	    ,fptr<float>({"h_cluster_ENG_POS"	         ,flabel(AxisVariable::cluster_ENG_POS	      ),fvars1d(AxisVariable::cluster_ENG_POS	         ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_POS	      )})},
    //   {"cluster_SIGNIFICANCE"       ,fptr<float>({"h_cluster_SIGNIFICANCE"       ,flabel(AxisVariable::cluster_SIGNIFICANCE       ),fvars1d(AxisVariable::cluster_SIGNIFICANCE       ),fcoor1d(),faxis1d(AxisVariable::cluster_SIGNIFICANCE       )})},
    //   {"cluster_CELL_SIGNIFICANCE"  ,fptr<float>({"h_cluster_CELL_SIGNIFICANCE"  ,flabel(AxisVariable::cluster_CELL_SIGNIFICANCE  ),fvars1d(AxisVariable::cluster_CELL_SIGNIFICANCE  ),fcoor1d(),faxis1d(AxisVariable::cluster_CELL_SIGNIFICANCE  )})},
    //   {"cluster_CELL_SIG_SAMPLING"  ,fptr<float>({"h_cluster_CELL_SIG_SAMPLING"  ,flabel(AxisVariable::cluster_CELL_SIG_SAMPLING  ),fvars1d(AxisVariable::cluster_CELL_SIG_SAMPLING  ),fcoor1d(),faxis1d(AxisVariable::cluster_CELL_SIG_SAMPLING  )})},
    //   {"cluster_AVG_LAR_Q"          ,fptr<float>({"h_cluster_AVG_LAR_Q"          ,flabel(AxisVariable::cluster_AVG_LAR_Q          ),fvars1d(AxisVariable::cluster_AVG_LAR_Q          ),fcoor1d(),faxis1d(AxisVariable::cluster_AVG_LAR_Q          )})},
    //   {"cluster_AVG_TILE_Q"	    ,fptr<float>({"h_cluster_AVG_TILE_Q"	 ,flabel(AxisVariable::cluster_AVG_TILE_Q	      ),fvars1d(AxisVariable::cluster_AVG_TILE_Q	 ),fcoor1d(),faxis1d(AxisVariable::cluster_AVG_TILE_Q	      )})},
    //   {"cluster_ENG_BAD_HV_CELLS"   ,fptr<float>({"h_cluster_ENG_BAD_HV_CELLS"   ,flabel(AxisVariable::cluster_ENG_BAD_HV_CELLS   ),fvars1d(AxisVariable::cluster_ENG_BAD_HV_CELLS   ),fcoor1d(),faxis1d(AxisVariable::cluster_ENG_BAD_HV_CELLS   )})},
    //   {"cluster_N_BAD_HV_CELLS"     ,fptr<float>({"h_cluster_N_BAD_HV_CELLS"     ,flabel(AxisVariable::cluster_N_BAD_HV_CELLS     ),fvars1d(AxisVariable::cluster_N_BAD_HV_CELLS     ),fcoor1d(),faxis1d(AxisVariable::cluster_N_BAD_HV_CELLS     )})},
    //   {"cluster_PTD"		    ,fptr<float>({"h_cluster_PTD"		 ,flabel(AxisVariable::cluster_PTD		      ),fvars1d(AxisVariable::cluster_PTD		 ),fcoor1d(),faxis1d(AxisVariable::cluster_PTD		      )})},
    //   {"cluster_MASS"               ,fptr<float>({"h_cluster_MASS"               ,flabel(AxisVariable::cluster_MASS               ),fvars1d(AxisVariable::cluster_MASS               ),fcoor1d(),faxis1d(AxisVariable::cluster_MASS               )})},
    //   {"cluster_SECOND_TIME"	    ,fptr<float>({"h_cluster_SECOND_TIME"	 ,flabel(AxisVariable::cluster_SECOND_TIME        ),fvars1d(AxisVariable::cluster_SECOND_TIME        ),fcoor1d(),faxis1d(AxisVariable::cluster_SECOND_TIME        )})}
    //   };

#endif
