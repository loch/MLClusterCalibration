// -*- c++ -*-
#ifndef HISTPLOTTER_H
#define HISTPLOTTER_H

#include "Base.h"

#include "HistSelector.h"
#include "HistApplication.h"

#include <string>
#include <tuple>
#include <map>

namespace Hist {
  ///@brief Collection of plotters and support
  namespace Plotter {
    ///@name Useful types
    ///@{
    typedef std::string                   Key;       ///< Key type for lookup of selector and fill 
    typedef Interface::Fill               Filler;    ///< Fill object type
    typedef Interface::Selector           Selector;  ///< Selector object type
    typedef std::tuple<Selector*,Filler*> Link;      ///< Type for keyed lookup of link between selector and fill object
    typedef std::map<Key,Link>            LinkMap;   ///< Lookup table type
    typedef typename LinkMap::value_type  LinkEntry; ///< Type of entry in lookup table
    ///@}   
    ///@name Accessors and settings
    ///@{
    const Key&         key        (const LinkEntry& entry);
    const Filler*      ptrFill    (const LinkEntry& entry);
    const Selector*    ptrSelector(const LinkEntry& entry);
    Filler*            ptrFill    (LinkEntry& entry);
    Selector*          ptrSelector(LinkEntry& entry);
    const Filler*      ptrFill    (const Link& link);
    const Selector*    ptrSelector(const Link& link);
    Filler*            ptrFill    (Link& link);
    Selector*          ptrSelector(Link& link);
    ///@}
    ///@brief Base class for plotters
    class Base : public ::Base::Config {
    public:
      ///@brief Constructor initiates a named plotter module 
      Base(const std::string& name="PlotterBase");
      ///@brief Base class destructor
      virtual ~Base();
      ///@brief Configure plotter 
      ///@param pname   name of plot group
      ///@param fptr    pointer to fill object
      ///@param sptr    pointer to selector object (default is @c nullptr , no selections)
      virtual int  configure(const Key& pname,Filler* fptr,Selector* sptr=nullptr);
      ///@brief Basic plotter implementation
      virtual bool plot(double weight=1.0);
      ///@brief 
    private:
      ///@name Plot lookup
      ///@{
      LinkMap    m_plotMap = { };           ///< Lookup table for links
      static const Link m_defaultLinkConst; ///< Default const reference for unknown link
      static Link       m_defaultLink;      ///< Default non-const reference for unknown link
      ///@}
      ///@name Helpers
      ///@{
      const Link& link(const Key& pname) const; ///< Link for a given key (reference to non-modifiable @c Link object)
      Link&       link(const Key& pname);       ///< Link for a given key (reference to modifiable @c Link object)
      bool        checkMap();                   ///< Check link map consistency
      ///@}
    };   
  }
}

inline const Hist::Plotter::Filler*   Hist::Plotter::ptrFill    (const Link& link) { return std::get<1>(link)   ; }
inline const Hist::Plotter::Selector* Hist::Plotter::ptrSelector(const Link& link) { return std::get<0>(link)   ; }
inline Hist::Plotter::Filler*         Hist::Plotter::ptrFill    (Link& link)       { return std::get<1>(link)   ; }
inline Hist::Plotter::Selector*       Hist::Plotter::ptrSelector(Link& link)       { return std::get<0>(link)   ; }

inline const Hist::Plotter::Key&      Hist::Plotter::key        (const LinkEntry& entry) { return entry.first;               }
inline const Hist::Plotter::Filler*   Hist::Plotter::ptrFill    (const LinkEntry& entry) { return ptrFill    (entry.second); }
inline const Hist::Plotter::Selector* Hist::Plotter::ptrSelector(const LinkEntry& entry) { return ptrSelector(entry.second); }
inline Hist::Plotter::Filler*         Hist::Plotter::ptrFill    (LinkEntry& entry)       { return ptrFill    (entry.second); }
inline Hist::Plotter::Selector*       Hist::Plotter::ptrSelector(LinkEntry& entry)       { return ptrSelector(entry.second); }

inline const Hist::Plotter::Link& Hist::Plotter::Base::link(const Key& pname) const { auto flnk = m_plotMap.find(pname); return flnk != m_plotMap.end() ? flnk->second : m_defaultLinkConst; }
inline Hist::Plotter::Link&       Hist::Plotter::Base::link(const Key& pname)       { auto flnk = m_plotMap.find(pname); return flnk != m_plotMap.end() ? flnk->second : m_defaultLink     ; }    
#endif
