
#include "Units.h"

#include <string>
#include <vector>
#include <map>

#include <algorithm>

#include <ostream>
#include <iostream>

////////////////
// Descriptor //
////////////////

Units::Descriptor::Descriptor() { }
Units::Descriptor::Descriptor(double sfactor,const std::string& nm,const std::string& ab,const std::string& sy,const std::string& ax,const std::string& lx,const std::string& rx,const std::string& gp,bool fb) 
  : scale(sfactor)
  , name(nm)
  , abbr(ab)
  , symbol(sy)
  , axis(ax)
  , latex_axis(lx)
  , root_axis(rx)
  , group(gp)
  , isBase(fb || ab == UNITS_E_NAME) 
{ }
Units::Descriptor::Descriptor(const Descriptor& udescr)
  : Descriptor(udescr.scale,udescr.name,udescr.abbr,udescr.symbol,udescr.axis,udescr.latex_axis,udescr.root_axis,udescr.group,udescr.isBase)
{ }
Units::Descriptor& Units::Descriptor::operator=(const Descriptor& udescr) {
  if ( this == &udescr ) { return *this; }
  scale      = udescr.scale;
  name       = udescr.name; 
  abbr       = udescr.abbr; 
  symbol     = udescr.symbol; 
  axis       = udescr.axis; 
  latex_axis = udescr.latex_axis; 
  root_axis  = udescr.root_axis; 
  group      = udescr.group;
  isBase     = udescr.isBase;
  return *this;
}

//////////////////////////
// Description database //
//////////////////////////

Units::DescriptionLoader*          Units::DescriptionLoader::m_instance = nullptr;
Units::DescriptionLoader*          Units::DescriptionLoader::instance() { if ( m_instance == nullptr ) { m_instance = new DescriptionLoader(); } return m_instance; }
Units::DescriptionLoader::DataBase Units::DescriptionLoader::m_database = {
  { "mm"    , Descriptor(mm   ,"millimeter"      ,"mm"  ,"mm"   ,"mm"         ,"\\ensuremath{\\text{mm}}"        ,"mm"           ,gDimensions   ,true )},
  { "mm2"   , Descriptor(mm2  ,"millimeter2"     ,"mm2" ,"mm2"  ,"mm^{2}"     ,"\\ensuremath{\\text{mm}^{2}}"    ,"mm^{2}"       ,gDimensions   ,false)},
  { "mm3"   , Descriptor(mm3  ,"millimeter3"     ,"mm3" ,"mm3"  ,"mm^{3}"     ,"\\ensuremath{\\text{mm}^{3}"     ,"mm^{3}"       ,gDimensions   ,false)},
  { "cm"    , Descriptor(cm   ,"centimeter"      ,"cm"  ,"cm"   ,"cm"         ,"\\ensuremath{\\text{cm}}"        ,"cm"           ,gDimensions   ,false)},
  { "cm2"   , Descriptor(cm2  ,"centimeter2"     ,"cm2" ,"cm2"  ,"cm^{2}"     ,"\\ensuremath{\\text{cm}^{2}}"    ,"cm^{2}"       ,gDimensions   ,false)},
  { "cm3"   , Descriptor(cm3  ,"centimeter3"     ,"cm3" ,"cm3"  ,"cm^{3}"     ,"\\ensuremath{\\text{cm}^{3}}"    ,"cm^{3}"       ,gDimensions   ,false)},
  { "m"     , Descriptor(m    ,"meter"           ,"m"   ,"mm"   ,"mm"         ,"\\ensuremath{\\text{mm}}"        ,"mm"           ,gDimensions   ,false)},
  { "m2"    , Descriptor(m2   ,"meter2"          ,"m2"  ,"mm2"  ,"m^{2}"      ,"\\ensuremath{\\text{m}^{2}}"     ,"m^{2}"        ,gDimensions   ,false)},
  { "m3"    , Descriptor(m3   ,"meter3"          ,"m3"  ,"mm3"  ,"m^{3}"      ,"\\ensuremath{\\text{m}^{3}}"     ,"m^{3}"        ,gDimensions   ,false)},
  { "km"    , Descriptor(km   ,"centimeter"      ,"km"  ,"km"   ,"km"         ,"\\ensuremath{\\text{km}}"        ,"km"           ,gDimensions   ,false)},
  { "km2"   , Descriptor(km2  ,"centimeter2"     ,"km2" ,"km2"  ,"km^{2}"     ,"\\ensuremath{\\text{km}^{2}}"    ,"km^{2}"       ,gDimensions   ,false)},
  { "km3"   , Descriptor(km3  ,"centimeter3"     ,"km3" ,"km3"  ,"km^{3}"     ,"\\ensuremath{\\text{km}^{3}}"    ,"km^{3}"       ,gDimensions   ,false)},
  { "pc"    , Descriptor(pc   ,"parsec"          ,"pc"  ,"pc"   ,"pc"         ,"\\ensuremath{\\text{pc}}"        ,"pc"           ,gDimensions   ,false)},
  { "um"    , Descriptor(um   ,"micrometer"      ,"um"  ,"μm"   ,"\\mu m"     ,"\\ensuremath{\\mu\\text{m}}"     ,"#mum"         ,gDimensions   ,false)},
  { "nm"    , Descriptor(nm   ,"nanometer"       ,"nm"  ,"nm"   ,"nm"         ,"\\ensuremath{\\text{nm}}"        ,"nm"           ,gDimensions   ,false)},
  { "pm"    , Descriptor(pm   ,"picometer"       ,"pm"  ,"pm"   ,"pm"         ,"\\ensuremath{\\text{pm}}"        ,"pm"           ,gDimensions   ,false)},
  { "fm"    , Descriptor(fm   ,"femtometer"      ,"fm"  ,"fm"   ,"fm"         ,"\\ensuremath{\\text{fm}}"        ,"fm"           ,gDimensions   ,false)},
  { "fermi" , Descriptor(fm   ,"femtometer"      ,"fm"  ,"fm"   ,"fm"         ,"\\ensuremath{\\text{fm}}"        ,"fm"           ,gDimensions   ,false)},
  { "A"     , Descriptor(A    ,"angstrom"        ,"A"   ,"A"    ,"\\AA"       ,"\\ensuremath{\\AA}"              ,"A"            ,gDimensions   ,false)},
  { "b"     , Descriptor(b    ,"barn"            ,"b"   ,"b"    ,"b"          ,"\\ensuremath{\\text{b}}"         ,"b"            ,gXsect        ,false)},
  { "mb"    , Descriptor(mb   ,"millibarn"       ,"mb"  ,"mb"   ,"mb"         ,"\\ensuremath{\\text{mb}}"        ,"mb"           ,gXsect        ,false)},
  { "ub"    , Descriptor(ub   ,"microbarn"       ,"ub"  ,"μb"   ,"\\mu b"     ,"\\ensuremath{\\mu\\text{b}}"     ,"#mub"         ,gXsect        ,false)},
  { "nb"    , Descriptor(nb   ,"nanobarn"        ,"nb"  ,"nb"   ,"nb"         ,"\\ensuremath{\\text{nb}}"        ,"nb"           ,gXsect        ,false)},
  { "pb"    , Descriptor(pb   ,"picobarn"        ,"pb"  ,"pb"   ,"pb"         ,"\\ensuremath{\\text{pb}}"        ,"pb"           ,gXsect        ,false)},
  { "fb"    , Descriptor(fb   ,"femtobarn"       ,"fb"  ,"fb"   ,"fb"         ,"\\ensuremath{\\text{fb}}"        ,"fb"           ,gXsect        ,false)},
  { "ib"    , Descriptor(1./b ,"ibarn"           ,"ib"  ,"b^-1" ,"b^{-1}"     ,"\\ensuremath{\\text{b}^{-1}}"    ,"b^{-1}"       ,gXsectInv     ,false)},
  { "imb"   , Descriptor(1./mb,"imillibarn"      ,"imb" ,"mb^-1","mb^{-1}"    ,"\\ensuremath{\\text{mb}^{-1}}"   ,"mb^{-1}"      ,gXsectInv     ,false)},
  { "iub"   , Descriptor(1./ub,"imicrobarn"      ,"iub" ,"μb^-1","\\mu b^{-1}","\\ensuremath{\\mu\\text{b}^{-1}}","#mub^{-1}"    ,gXsectInv     ,false)},
  { "inb"   , Descriptor(1./nb,"inanobarn"       ,"inb" ,"nb^-1","nb^{-1}"    ,"\\ensuremath{\\text{nb}^{-1}}"   ,"nb^{-1}"      ,gXsectInv     ,false)},
  { "ipb"   , Descriptor(1./pb,"ipicobarn"       ,"ipb" ,"pb^-1","pb^{-1}"    ,"\\ensuremath{\\text{pb}^{-1}}"   ,"pb^{-1}"      ,gXsectInv     ,false)},
  { "ifb"   , Descriptor(1./fb,"ifemtobarn"      ,"ifb" ,"fb^-1","fb^{-1}"    ,"\\ensuremath{\\text{fb}^{-1}}"   ,"fb^{-1}"      ,gXsectInv     ,false)},
  { "L"     , Descriptor(L    ,"liter"           ,"L"   ,"l"    ,"l"          ,"\\ensuremath{\\text{l}}"         ,"l"            ,gDimensions   ,false)},
  { "dL"    , Descriptor(dL   ,"deciliter"       ,"dL"  ,"dl"   ,"dl"         ,"\\ensuremath{10^{-1}\\text{l}}"  ,"10^{#minus1}l",gDimensions   ,false)},
  { "cL"    , Descriptor(cL   ,"centiliter"      ,"cL"  ,"cl"   ,"cl"         ,"\\ensuremath{10^{-2}\\text{l}}"  ,"10^{#minus2}l",gDimensions   ,false)},
  { "mL"    , Descriptor(mL   ,"milliliter"      ,"mL"  ,"ml"   ,"ml"         ,"\\ensuremath{10^{-3}\\text{l}}"  ,"10^{#minus3}l",gDimensions   ,false)},
  { "rad"   , Descriptor(rad  ,"radian"          ,"rad" ,"rad"  ,"rad"        ,"\\ensuremath{\\text{rad}}"       ,"rad"          ,gAngular      ,true )},
  { "mrad"  , Descriptor(mrad ,"mradian"         ,"mrad","mrad" ,"mrad"       ,"\\ensuremath{\\text{mrad}}"      ,"mrad"         ,gAngular      ,false)},
  { "deg"   , Descriptor(deg  ,"degree"          ,"deg" ,"°"    ,"^{\\circ}"  ,"\\ensuremath{^{\\circ}}"         ,"^{#circ}"     ,gAngular      ,false)},
  { "sr"    , Descriptor(sr   ,"steradian"       ,"sr"  ,"sr"   ,"sr"         ,"\\ensuremath{\\text{sr}}"        ,"sr"           ,gAngular      ,true )},
  { "ns"    , Descriptor(ns   ,"nanosecond"      ,"ns"  ,"ns"   ,"ns"         ,"\\ensuremath{\\text{ns}}"        ,"ns"           ,gTime         ,true )},
  { "s"     , Descriptor(s    ,"second"          ,"s"   ,"s"    ,"s"          ,"\\ensuremath{\\text{s}}"         ,"s"            ,gTime         ,false)},
  { "ms"    , Descriptor(ms   ,"millisecond"     ,"ms"  ,"ms"   ,"ms"         ,"\\ensuremath{\\text{ms}}"        ,"ms"           ,gTime         ,false)},
  { "us"    , Descriptor(us   ,"microsecond"     ,"us"  ,"μs"   ,"\\mu s"     ,"\\ensuremath{\\mu\\text{s}}"     ,"#mus"         ,gTime         ,false)},
  { "ps"    , Descriptor(ps   ,"picosecond"      ,"ps"  ,"ps"   ,"ps"         ,"\\ensuremath{\\text{ps}}"        ,"ps"           ,gTime         ,false)},
  { "fs"    , Descriptor(fs   ,"femtosecond"     ,"fs"  ,"fs"   ,"fs"         ,"\\ensuremath{\\text{fs}}"        ,"fs"           ,gTime         ,false)},
  { "min"   , Descriptor(min  ,"minute"          ,"min" ,"min"  ,"min"        ,"\\ensuremath{\\text{mn}}"        ,"mn"           ,gTime         ,false)},
  { "hr"    , Descriptor(hr   ,"hour"            ,"hr"  ,"hr"   ,"hrs"        ,"\\ensuremath{\\text{hrs}}"       ,"hrs"          ,gTime         ,false)}, 
  { "dy"    , Descriptor(dy   ,"day"             ,"dy"  ,"dy"   ,"days"       ,"\\ensuremath{\\text{days}}"      ,"days"         ,gTime         ,false)},
  { "wk"    , Descriptor(wk   ,"week"            ,"wk"  ,"wk"   ,"weeks"      ,"\\ensuremath{\\text{weeks}}"     ,"weeks"        ,gTime         ,false)},
  { "Hz"    , Descriptor(Hz   ,"hertz"           ,"Hz"  ,"Hz"   ,"Hz"         ,"\\ensuremath{\\text{Hz}}"        ,"Hz"           ,gFrequency    ,false)},      
  { "kHz"   , Descriptor(kHz  ,"kilohertz"       ,"kHz" ,"kHz"  ,"kHz"        ,"\\ensuremath{\\text{kHz}}"       ,"kHz"          ,gFrequency    ,false)},
  { "MHz"   , Descriptor(MHz  ,"megahertz"       ,"MHz" ,"MHz"  ,"MHz"        ,"\\ensuremath{\\text{MHz}}"       ,"MHz"          ,gFrequency    ,false)},
  { "GHz"   , Descriptor(GHz  ,"gigahertz"       ,"GHz" ,"GHz"  ,"GHz"        ,"\\ensuremath{\\text{GHz}}"       ,"GHz"          ,gFrequency    ,false)},
  { "eV"    , Descriptor(eV   ,"electronvolt"    ,"eV"  ,"eV"   ,"eV"         ,"\\ensuremath{\\text{eV}}"        ,"eV"           ,gEnergy       ,false)},      
  { "keV"   , Descriptor(keV  ,"kiloelectronvolt","keV" ,"keV"  ,"keV"        ,"\\ensuremath{\\text{keV}}"       ,"keV"          ,gEnergy       ,false)},  
  { "MeV"   , Descriptor(MeV  ,"megaelectronvolt","MeV" ,"MeV"  ,"MeV"        ,"\\ensuremath{\\text{MeV}}"       ,"MeV"          ,gEnergy       ,false)},  
  { "GeV"   , Descriptor(GeV  ,"gigaelectronvolt","GeV" ,"GeV"  ,"GeV"        ,"\\ensuremath{\\text{GeV}}"       ,"GeV"          ,gEnergy       ,false)},  
  { "TeV"   , Descriptor(TeV  ,"teraelectronvolt","TeV" ,"TeV"  ,"TeV"        ,"\\ensuremath{\\text{TeV}}"       ,"TeV"          ,gEnergy       ,false)},  
  { "PeV"   , Descriptor(PeV  ,"petaelectronvolt","PeV" ,"PeV"  ,"PeV"        ,"\\ensuremath{\\text{PeV}}"       ,"PeV"          ,gEnergy       ,false)},  
  { "mg"    , Descriptor(mg   ,"milligram"       ,"mg"  ,"mg"   ,"mg"         ,"\\ensuremath{\\text{mg}}"        ,"mg"           ,gMass         ,false)},
  { "ug"    , Descriptor(g    ,"microgram"       ,"ug"  ,"g"    ,"g"          ,"\\ensuremath{\\text{g}}"         ,"g"            ,gMass         ,false)},
  { "g"     , Descriptor(g    ,"gram"            ,"g"   ,"μg"   ,"\\mu g"     ,"\\ensuremath{\\mu\\text{g}}"     ,"#mug"         ,gMass         ,false)},
  { "kg"    , Descriptor(kg   ,"kilogram"        ,"kg"  ,"kg"   ,"kg"         ,"\\ensuremath{\\text{kg}}"        ,"kg"           ,gMass         ,false)}, 
  { "W"     , Descriptor(W    ,"watt"            ,"W"   ,"W"    ,"W"          ,"\\ensuremath{\\text{W}}"         ,"W"            ,gPower        ,false)},
  { "kW"    , Descriptor(kW   ,"kilowatt"        ,"kW"  ,"kW"   ,"kW"         ,"\\ensuremath{\\text{kW}}"        ,"kW"           ,gPower        ,false)},
  { "MW"    , Descriptor(MW   ,"megawatt"        ,"MW"  ,"MW"   ,"MW"         ,"\\ensuremath{\\text{MW}}"        ,"MW"           ,gPower        ,false)},
  { "GW"    , Descriptor(GW   ,"gigawatt"        ,"GW"  ,"GW"   ,"GW"         ,"\\ensuremath{\\text{GW}}"        ,"GW"           ,gPower        ,false)},
  { "A"     , Descriptor(A    ,"ampere"          ,"A"   ,"A"    ,"A"          ,"\\ensuremath{\\text{A}}"         ,"A"            ,gCurrent      ,false)},
  { "mA"    , Descriptor(mA   ,"milliampere"     ,"mA"  ,"mA"   ,"mA"         ,"\\ensuremath{\\text{mA}}"        ,"mA"           ,gCurrent      ,false)},
  { "uA"    , Descriptor(uA   ,"microampere"     ,"uA"  ,"μA"   ,"\\mu A"     ,"\\ensuremath{\\mu\\text{A}}"     ,"#muA"         ,gCurrent      ,false)},
  { "nA"    , Descriptor(nA   ,"nanoampere"      ,"nA"  ,"nA"   ,"nA"         ,"\\ensuremath{\\text{nA}}"        ,"nA"           ,gCurrent      ,false)},
  { "J"     , Descriptor(J    ,"joule"           ,"J"   ,"J"    ,"J"          ,"\\ensuremath{\\text{J}} "        ,"J"            ,gElecEnergy   ,false)},
  { "Wh"    , Descriptor(Wh   ,"watthour"        ,"Wh"  ,"Wh"   ,"Wh"         ,"\\ensuremath{\\text{Wh}}"        ,"Wh"           ,gElecEnergy   ,false)},
  { "kWh"   , Descriptor(kWh  ,"kilowatthour"    ,"kWh" ,"kWh"  ,"kWh"        ,"\\ensuremath{\\text{kWh}}"       ,"kWh"          ,gElecEnergy   ,false)},
  { "MWh"   , Descriptor(MWh  ,"megawatthour"    ,"MWh" ,"MWh"  ,"MWh"        ,"\\ensuremath{\\text{MWh}}"       ,"MWh"          ,gElecEnergy   ,false)},
  { "GWh"   , Descriptor(GWh  ,"gigawatthour"    ,"GWh" ,"GWh"  ,"GWh"        ,"\\ensuremath{\\text{GWh}}"       ,"GWh"          ,gElecEnergy   ,false)},
  { "e"     , Descriptor(e    ,"eplus"           ,"e"   ,"e"    ,"e"          ,"\\ensuremath{q_{\\text{e}}}"     ,"q_{e}"        ,gCharge       ,true )},     
  { "C"     , Descriptor(C    ,"coulomb"         ,"C"   ,"C"    ,"C"          ,"\\ensuremath{\\text{C}}"         ,"C"            ,gCharge       ,false)},
  { "mC"    , Descriptor(mC   ,"millicoulomb"    ,"mC"  ,"mC"   ,"mC"         ,"\\ensuremath{\\text{mC}}"        ,"mC"           ,gCharge       ,false)},
  { "uC"    , Descriptor(uC   ,"microcoulomb"    ,"uC"  ,"μC"   ,"\\mu C"     ,"\\ensuremath{\\mu\\text{C}}"     ,"#muC"         ,gCharge       ,false)},
  { "nC"    , Descriptor(nC   ,"nanocoulomb"     ,"nC"  ,"nC"   ,"nC"         ,"\\ensuremath{\\text{nC}}"        ,"nC"           ,gCharge       ,false)},
  { "pC"    , Descriptor(pC   ,"picocoulomb"     ,"pC"  ,"pC"   ,"pC"         ,"\\ensuremath{\\text{pC}}"        ,"pC"           ,gCharge       ,false)},
  { "fC"    , Descriptor(fC   ,"femtocoulomb"    ,"fC"  ,"fC"   ,"fC"         ,"\\ensuremath{\\text{fC}}"        ,"fC"           ,gCharge       ,false)},
  { "N"     , Descriptor(N    ,"newton"          ,"N"   ,"N"    ,"N"          ,"\\ensuremath{\\text{N}}"         ,"N"            ,gForce        ,false)},               
  { "kN"    , Descriptor(kN   ,"kilonewton"      ,"kN"  ,"kN"   ,"kN"         ,"\\ensuremath{\\text{kN}}"        ,"kN"           ,gForce        ,false)},   
  { "V"     , Descriptor(V    ,"volt"            ,"V"   ,"V"    ,"V"          ,"\\ensuremath{\\text{V}}"         ,"V"            ,gVoltage      ,false)},     
  { "kV"    , Descriptor(kV   ,"kilovolt"        ,"kV"  ,"kV"   ,"kV"         ,"\\ensuremath{\\text{kV}}"        ,"kV"           ,gVoltage      ,false)},    
  { "MV"    , Descriptor(MV   ,"megavolt"        ,"MV"  ,"MV"   ,"MV"         ,"\\ensuremath{\\text{MV}}"        ,"MV"           ,gVoltage      ,false)},
  { "O"     , Descriptor(O    ,"ohm"             ,"O"   ,"Ω"    ,"\\Omega"    ,"\\ensuremath{\\Omega}"           ,"#Omega"       ,gResistance   ,false)},     
  { "kO"    , Descriptor(kO   ,"kiloohm"         ,"kO"  ,"kΩ"   ,"k\\Omega"   ,"\\ensuremath{\\text{k}\\Omega}"  ,"k#Omega"      ,gResistance   ,false)}, 
  { "MO"    , Descriptor(MO   ,"megaohm"         ,"MO"  ,"MΩ"   ,"M\\Omega"   ,"\\ensuremath{\\text{M}\\Omega}"  ,"M#Omega"      ,gResistance   ,false)}, 
  { "F"     , Descriptor(F    ,"farad"           ,"F"   ,"F"    ,"F"          ,"\\ensuremath{\\text{F}}"         ,"F"            ,gCapacitance  ,false)}, 
  { "mF"    , Descriptor(mF   ,"millifarad"      ,"mF"  ,"mF"   ,"mF"         ,"\\ensuremath{\\text{mF}}"        ,"mF"           ,gCapacitance  ,false)},
  { "uF"    , Descriptor(uF   ,"microfarad"      ,"uF"  ,"μF"   ,"\\mu F"     ,"\\ensuremath{\\mu\\text{F}}"     ,"#muF"         ,gCapacitance  ,false)},
  { "nF"    , Descriptor(nF   ,"nanofarad"       ,"nF"  ,"nF"   ,"nF"         ,"\\ensuremath{\\text{nF}}"        ,"nF"           ,gCapacitance  ,false)},
  { "pF"    , Descriptor(pF   ,"picofarad"       ,"pF"  ,"pF"   ,"pF"         ,"\\ensuremath{\\text{pF}}"        ,"pF"           ,gCapacitance  ,false)},
  { "Wb"    , Descriptor(Wb   ,"weber"           ,"Wb"  ,"Wb"   ,"Wb"         ,"\\ensuremath{\\text{Wb}}"        ,"Wb"           ,gMagFlux      ,false)},    
  { "T"     , Descriptor(T    ,"tesla"           ,"T"   ,"T"    ,"T"          ,"\\ensuremath{\\text{T}}"         ,"T"            ,gMagField     ,false)},
  { "Gs"    , Descriptor(Gs   ,"gauss"           ,"Gs"  ,"Gs"   ,"Gs"         ,"\\ensuremath{\\text{Gs}}"        ,"Gs"           ,gMagField     ,false)},
  { "kGs"   , Descriptor(kGs  ,"kilogauss"       ,"kGs" ,"kGs"  ,"kGs"        ,"\\ensuremath{\\text{kGs}}"       ,"kGs"          ,gMagField     ,false)},
  { "H"     , Descriptor(H    ,"henry"           ,"H"   ,"H"    ,"H"          ,"\\ensuremath{\\text{H}}"         ,"H"            ,gMagInductance,false)},
  { "mole"  , Descriptor(mole ,"mole"            ,"mole","mole" ,"mole"       ,"\\ensuremath{\\text{mole}}"      ,"mole"         ,gSubstance    ,true )},
  { "degK"  , Descriptor(K    ,"kelvin"          ,"K"   ,"°K"   ,"^{\\circ}K" ,"\\ensuremath{^{\\circ}\\text{K}}","^{#circ}K"    ,gTemperature  ,true )},
  { "degC"  , Descriptor(-1.0 ,"celsius"         ,"C"   ,"°C"   ,"^{\\circ}C" ,"\\ensuremath{^{\\circ}\\text{C}}","^{#circ}C"    ,gTemperature  ,false)},
  { "degF"  , Descriptor(-1.0 ,"fahrenheit"      ,"F"   ,"°F"   ,"^{\\circ}F" ,"\\ensuremath{^{\\circ}\\text{F}}","^{#circ}F"    ,gTemperature  ,false)},
  { "Bq"    , Descriptor(Bq   ,"becquerel"       ,"Bq"  ,"Bq"   ,"Bq"         ,"\\ensuremath{\\text{Bq}}"        ,"Bq"           ,gRadioActivity,false)},    
  { "kBq"   , Descriptor(kBq  ,"kilobecquerel"   ,"kBq" ,"kBq"  ,"kBq"        ,"\\ensuremath{\\text{kBq}}"       ,"kBq"          ,gRadioActivity,false)},
  { "MBq"   , Descriptor(MBq  ,"megabecquerel"   ,"MBq" ,"MBq"  ,"MBq"        ,"\\ensuremath{\\text{MBq}}"       ,"MBq"          ,gRadioActivity,false)},
  { "GBq"   , Descriptor(GBq  ,"gigabecquerel"   ,"GBq" ,"GBq"  ,"GBq"        ,"\\ensuremath{\\text{GBq}}"       ,"GBq"          ,gRadioActivity,false)},
  { "Ci"    , Descriptor(Ci   ,"curie"           ,"Ci"  ,"Ci"   ,"Ci"         ,"\\ensuremath{\\text{Ci}}"        ,"Ci"           ,gRadioActivity,false)},
  { "mCi"   , Descriptor(mCi  ,"millicurie"      ,"mCi" ,"mCi"  ,"mCi"        ,"\\ensuremath{\\text{mCi}}"       ,"mCi"          ,gRadioActivity,false)},
  { "uCi"   , Descriptor(uCi  ,"microcurie"      ,"uCi" ,"μCi"  ,"\\mu Ci"    ,"\\ensuremath{\\mu\\text{Ci}}"    ,"#muCi"        ,gRadioActivity,false)},
  { "uGy"   , Descriptor(uGy  ,"microgray"       ,"uGy" ,"μGy"  ,"\\mu Gy"    ,"\\ensuremath{\\mu\\text{Gy}}"    ,"#muGy"        ,gDose         ,false)}, 
  { "mGy"   , Descriptor(mGy  ,"milligray"       ,"mGy" ,"mGy"  ,"mGy"        ,"\\ensuremath{\\text{mGy}}"       ,"mGy"          ,gDose         ,false)},
  { "Gy"    , Descriptor(Gy   ,"gray"            ,"Gy"  ,"Gy"   ,"Gy"         ,"\\ensuremath{\\text{Gy}}"        ,"Gy"           ,gDose         ,false)},
  { "kGy"   , Descriptor(kGy  ,"kilogray"        ,"kGy" ,"kGy"  ,"kGy"        ,"\\ensuremath{\\text{kGy}}"       ,"kGy"          ,gDose         ,false)},
  { "Pa"    , Descriptor(Pa   ,"pascal"          ,"Pa"  ,"Pa"   ,"Pa"         ,"\\ensuremath{\\text{Pa}}"        ,"Pa"           ,gPressure     ,false)},   
  { "hPa"   , Descriptor(hPa  ,"hectoPascal"     ,"hPa" ,"hPa"  ,"hPa"        ,"\\ensuremath{\\text{hPa}}"       ,"hPa"          ,gPressure     ,false)},
  { "kPa"   , Descriptor(kPa  ,"kilopascal"      ,"kPa" ,"kPa"  ,"kPa"        ,"\\ensuremath{\\text{kPa}}"       ,"kPa"          ,gPressure     ,false)},
  { "mbar"  , Descriptor(mbar ,"millibar"        ,"mbar","mbar" ,"mbar"       ,"\\ensuremath{\\text{mbar}}"      ,"mbar"         ,gPressure     ,false)},
  { "atm"   , Descriptor(atm  ,"atmosphere"      ,"atm" ,"atm"  ,"atm"        ,"\\ensuremath{\\text{atm}}"       ,"atm"          ,gPressure     ,false)},
  { "cd"    , Descriptor(cd   ,"candela"         ,"cd"  ,"cd"   ,"cd"         ,"\\ensuremath{\\text{cd}}"        ,"cd"           ,gLight        ,true )},
  { "lm"    , Descriptor(lm   ,"lumen"           ,"lm"  ,"lm"   ,"lm"         ,"\\ensuremath{\\text{lm}}"        ,"lm"           ,gLight        ,false)},
  { "lx"    , Descriptor(lx   ,"lux"             ,"lx"  ,"lx"   ,"lx"         ,"\\ensuremath{\\text{lx}}"        ,"lx"           ,gLight        ,false)},
  { "UNK"   , Descriptor(0.   ,"unit not known"  ,"UNK" ,""     ,""           ,""                                ,""             ,""            ,false)}
};
const Units::DescriptionLoader::DataBase& Units::DescriptionLoader::dataBase()                         const { return m_database; }
const Units::Descriptor&                  Units::DescriptionLoader::descriptor(const std::string& key) const { return Units::description(key,this->dataBase()); }


//std::map<std::string,std::vector<std::string> > Units::GroupMembers = { };
const std::map<std::string,std::vector<std::string> >& Units::groupMemberLookup(const DescriptionLoader::DataBase& map) {
  if ( GroupMembers.empty() ) {
    for ( const auto& entry : map ) { 
      if ( GroupMembers.find(entry.second.group) == GroupMembers.end() ) { GroupMembers[entry.second.group] = std::vector<std::string>(); }
      GroupMembers[entry.second.group].push_back(entry.first);
    }
  }
  return GroupMembers;
} 

////////////////////////////////////
// Description database functions //
////////////////////////////////////

const Units::Descriptor& Units::description(const std::string& key,const DescriptionLoader::DataBase& map) { auto fmap = map.find(key); return fmap != map.end() ? fmap->second : map.at("UNK"); } 
const std::string&       Units::name       (const std::string& key,const DescriptionLoader::DataBase& map) { return Units::description(key,map).name;  }
double                   Units::scale      (const std::string& key,const DescriptionLoader::DataBase& map) { return Units::description(key,map).scale; }
const std::string&       Units::group      (const std::string& key,const DescriptionLoader::DataBase& map) { return Units::description(key,map).group; }
const std::string&       Units::axis       (const std::string& key,Types::Display::Mode m,const DescriptionLoader::DataBase& map) {
  const auto& udescr = Units::description(key,map);
  switch ( m ) {
  case Types::Display::Mode::Latex:
    return udescr.latex_axis;
  case Types::Display::Mode::ROOT:
    return udescr.root_axis;
  case Types::Display::Mode::Standard:
    return udescr.axis;
  default:
    return udescr.axis;
  }
}

//////////////////////////////////
// Description database display //
//////////////////////////////////

std::vector<std::string> Units::listing(const std::vector<std::string>& groups,const DescriptionLoader::DataBase& map) {
  static char _buffer[256];
  static std::string _unit_short  = "Short name"       ;
  static std::string _unit_symbol = "Symbol"           ;
  static std::string _unit_long   = "Unit (long name)" ;
  static std::string _unit_group  = "Group"            ;
  static std::string _unit_scale  = "Scale"            ;
  std::string::size_type kwidth(_unit_short .length()); // symbol
  std::string::size_type swidth(_unit_symbol.length()); // name
  std::string::size_type nwidth(_unit_long  .length()); // key
  std::string::size_type gwidth(_unit_group .length()); // group
  std::string::size_type fwidth(_unit_scale .length()); // scale
  for ( const auto& entry : map ) {
    kwidth = std::max(kwidth,entry.first.length());
    nwidth = std::max(nwidth,entry.second.name.length());
    swidth = std::max(swidth,entry.second.symbol.length());
    gwidth = std::max(gwidth,entry.second.group.length());
  }
  int ikw(static_cast<int>(kwidth));
  int inw(static_cast<int>(nwidth));
  int isw(static_cast<int>(swidth));
  int igw(static_cast<int>(gwidth));
  int ifw(static_cast<int>(fwidth));
  int idw(3); 
  std::vector<std::string> lines; lines.reserve(map.size()+10);
  ifw = std::max(9,ifw-idw);
  sprintf(_buffer,"%-*.*s %-*.*s %-*.*s  %-*.*s %-*.*s",igw,igw,_unit_group.c_str(),inw,inw,_unit_long.c_str(),ifw,ifw,_unit_scale.c_str(),isw,isw,_unit_short.c_str(),ikw,ikw,_unit_symbol.c_str());
  lines.push_back(_buffer);
  if ( groups.empty() ) { 
    for ( const auto& entry : map ) {
      if ( entry.second.abbr != "UNK" ) { 
	if ( entry.second.scale >= 0. ) {
	  sprintf(_buffer,"%-*.*s %-*.*s %-*.*e  %-*.*s %-*.*s",
		  igw,igw,entry.second.group.c_str() ,
		  inw,inw,entry.second.name.c_str()  ,
		  ifw,idw,entry.second.scale         ,
		  isw,isw,entry.second.abbr.c_str()  ,
		  ikw,ikw,entry.second.symbol.c_str());
	} else {
	  sprintf(_buffer,"%-*.*s %-*.*s %-*.*s  %-*.*s %-*.*s",
		  igw,igw,entry.second.group.c_str() ,
		  inw,inw,entry.second.name.c_str()  ,
		  ifw,ifw,"<noscale>"                ,
		  isw,isw,entry.second.abbr.c_str()  ,
		  ikw,ikw,entry.second.symbol.c_str());
	}
	lines.push_back(_buffer);
      }
    }
    auto fiter(lines.begin()); ++fiter;
    std::sort(fiter,lines.end(),[](std::string& a,std::string& b) { return a.substr(0,a.find_first_of(' ')) < b.substr(0,b.find_first_of(' ')); } );
  } else {
    const auto& gdict = groupMemberLookup();
    sprintf(_buffer,"%-*.*s %-*.*s %-*.*s  %-*.*s %-*.*s",igw,igw,_unit_group.c_str(),inw,inw,_unit_long.c_str(),ifw,ifw,_unit_scale.c_str(),isw,isw,_unit_short.c_str(),ikw,ikw,_unit_symbol.c_str());
    for ( const auto& gname : groups ) { 
      auto fdict = gdict.find(gname); 
      if ( fdict != gdict.end() ) { 
	for ( const auto& key : fdict->second ) {
	  if ( map.at(key).scale >= 0. ) { 
	    sprintf(_buffer,"%-*.*s %-*.*s %-*.*e  %-*.*s %-*.*s",
		    igw,igw,map.at(key).group.c_str() ,
		    inw,inw,map.at(key).name.c_str()  ,
		    ifw,idw,map.at(key).scale         ,
		    isw,isw,map.at(key).abbr.c_str()  ,
		    ikw,ikw,map.at(key).symbol.c_str());
	  } else {
	    sprintf(_buffer,"%-*.*s %-*.*s %-*.*s  %-*.*s %-*.*s",
		    igw,igw,map.at(key).group.c_str() ,
		    inw,inw,map.at(key).name.c_str()  ,
		    ifw,idw,"<noscale>"               ,
		    isw,isw,map.at(key).abbr.c_str()  ,
		    ikw,ikw,map.at(key).symbol.c_str());
	  }
	  lines.push_back(_buffer);
	} // loop on units in group
      } // lookup of requested group
    } // group loop
  } // inclusive/groups
  return lines;
}

std::ostream& Units::show(std::ostream& rstr,const std::string& preamble,const std::vector<std::string> groups,const DescriptionLoader::DataBase& map) {
  std::vector<std::string> ll = Units::listing(groups,map);
  if ( preamble == "" ) { 
    for ( const auto& entry : ll ) { rstr << entry << std::endl; }
  } else {
    for ( const auto& entry : ll ) { rstr << preamble << " " << entry << std::endl; }
  }
  return rstr; 
}

void Units::print(const std::string& preamble,const std::vector<std::string>& groups,const DescriptionLoader::DataBase& map) {
  Units::show(std::cout,preamble,groups,map) << std::flush; 
}

