// -*- c++ -*-
#ifndef HISTSELECTOR_H
#define HISTSELECTOR_H

#include "Base.h"

#include <string>
#include <vector>

#include <limits>

#include <iostream>

namespace Hist {
  namespace Interface {
    ///@brief Selector abstract base class (semi-loaded interface)
    class Selector : public Base::Config {
    public:
      ///@brief Interface destructor
      virtual ~Selector() { }
      ///@brief Clone selector
      virtual Selector* clone() = 0;
      ///@name Chain selectors
      ///@{
      virtual int chain (const Selector& psel) = 0; 
      virtual operator+ (const Selector& psel) = 0;
      virtual operator+=(const Selector& psel) = 0;
      ///@}
      ///@name Interfaces to decision methods
      ///@{
      virtual bool operator()() = 0; ///< Returns @c true if variable passes selection
      virtual bool accept()     = 0; ///< Returns @c true if variable passes selection
      virtual bool reject()     = 0; ///< Returns @c true if variable does not pass selection
      virtual operator bool()   = 0; ///< Returns @c true if variable passes the selection
      ///@}
      ///@name Messaging
      ///@{
      virtual std::ostream& summary(std::ostream& ostr) const = 0; ///< Stream a summary 
      virtual void          print()                     const = 0; ///< Print summary
      ///@}
    };
  }
  ///@brief Selection statistics 
  struct SelectorStats {
    int accept = { 0 };  ///< Number of accepts
    int reject = { 0 };  ///< Number of rejects
    int total  = { 0 };  ///< Number of invocations
    SelectorStats() { }  ///< Default constructor
  };
  ///@brief Selector base 
  /// This class implements a do-nothing selector. It acts as a base class for more meaningful implementations.
  ///@tparam T  type of variable providing value for selection
  template<class T> class Selector : ::Base::Config, virtual public Interface::Selector {
  public:
    ///@brief Default constructor sets up unconfigured objects
    Selector();
    ///@brief Construcor with configuration
    ///@param name     name of selector
    ///@param vaddress memory address storing variable for selection (default is none, indicated by @c nullptr )  
    Selector(const std::string& name,const T* vaddress=nullptr);
    ///@brief Copy constructor
    ///@param psel  reference to non-modifiable @c Selector object (source)
    Selector(const Selector& psel); 
    ///@brief Base class destructor
    virtual ~Selector();
    ///@brief Clone selector
    virtual Interface::Selector* clone();
    ///@name Selector chain
    /// Implements an @C AND decision tree
    virtual int add   (const Interface::Selector& psel);
    virtual operator+ (const Interface::Selector& psel);
    virtual operator+=(const Interface::Selector& psel);
    ///@name Decision methods
    ///@{ 
    virtual bool                     operator()(); ///< Returns @c true  if accessed value is accepted (forward to @c accept() )           
    virtual bool                     accept();     ///< Returns @c true  if accessed value is accepted
    virtual bool                     reject();     ///< Returns @c false if accessed value is rejected
    virtual operator                 bool();       ///< Returns @c false if accessed value is rejected
    ///@}
    ///@name Messaging
    ///@{
    virtual std::ostream&            summary(std::ostream& ostr) const;  ///< Stream a summary of this selector
    virtual std::vector<std::string> message()                   const;  ///< Get a formatted message as a list of strings
    virtual void                     print()                     const;  ///< Print to screen
    ///@}
  protected:
    SelectorStats m_stats    = { }; 
    T             f_value() const;
  private:
    const T*      m_vaddress = { nullptr };
  };
  ///@brief Select below threshold
  template<class T> class SelectorBelow : public Selector<T> {
  public:
    SelectorBelow(); 
    SelectorBelow(const std::string& name,const T* vaddress,T threshold);
    virtual bool accept();  
  private:
    T m_threshold = { std::numeric_limits< T >::max() };
  }; 
  ///@brief Select above threshold
  template<class T> class SelectorAbove : public Selector<T> {
  public:
    SelectorAbove(); 
    SelectorAbove(const std::string& name,const T* vaddress,T threshold);
    virtual bool accept();  
  private:
    T m_threshold = { std::numeric_limits< T >::lowest() };
  }; 
  ///@brief Selector in range
  template<class T> class SelectorRange : public Selector<T> {
  public:
    SelectorRange(); 
    SelectorRange(const std::string& name,const T* vaddress,T vmin,T vmax);
    virtual bool accept();
  private:
    T m_vmin = { std::numeric_limits<T>::lowest() };
    T m_vmax = { std::numeric_limits<T>::max()    };
  };
  ///@brief Concrete applications
  struct Select {
    static bool greater(double value,double threshold);          ///< Select if above threshold 
    static bool greater(int    value,int    threshold);          
    static bool greater(float  value,float  threshold);
    static bool smaller(double value,double threshold);
    static bool smaller(int    value,int    threshold);
    static bool smaller(float  value,float  threshold);
    static bool inRange(double value,double rmin,double rmax);
    static bool inRange(int    value,int    rmin,int    rmax);
    static bool inRange(float  value,float  rmin,float  rmax);
  };
  ///@brief Filter
  // namespace Filter {
  //   template<class T> class Impl {
  //   public:
  //   }; 
  // }; 
  ///@{
  /// Above threshold
  ///@}
} // Hist

inline std::ostream& operator<<(std::ostream& ostr,Hist::Interface::Selector& pselect) { return pselect.summary(ostr); }

#include "HistSelector.icc"
#endif
